#### Architecture

This project contains two versions of the mostly same web application.

One version consists of an Express.js-based API (contained in the /api folder), and a
React-based single-page web UI (contained in the /react_app folder) which employs the
Redux Toolkit for global state management.

The other, newer version is a self-contained Next.js-based multi-page application
(contained in the /next-app folder) which employs the newer App Router functionality of
Next, such that the use of server actions obviates the need for an API.  Also, the
multi-page structure of the app renders global client state management unproductive, so
Redux is not employed.  There is no global state atom on the client.

Both versions of the application were coded entirely in TypeScript, and employ the same
MongoDB v3.4 database named SuperiorPrint.

#### The Common project

The project in the /common top-level folder of the repo contains folders of code which are
shared by the two versions of the application:

_/domain:_ Contains classes representing the entities which comprise the project's domain
model. These classes are coded to maintain strict encapsulation of their contained data.

_/data-transfer:_ Also contains classes representing the entities which comprise the
project's domain model.  However, these classes comprise bags of data fields which are not
encapsulated.  They are the representation of the domain model entities which are sent
over the network.  On the client, they are hydrated into the corresponding classes from
the /domain folder.  They are noy hydrated this way on the server, as the benefit would be
too small to warrant the inclusion of the code to first hydrate the entity and then later
dehydrate it for transfer to the client.

_/util:_ Contains utility classes which are useful to both app versions.

_/utility_scripts:_ Contains scripts which employ the JavaScript MongoDB database driver
to make modifications to the SuperiorPrint database, mostly to fix data inconsistencies
caused by past bugs in the apps' code.

It was considered whether to use TypeScript project references to link the application
projects to the common project, but they turned out to not be necessary as far as I could
tell, so they were not used.

#### Setup of the older version of the application

In the /api folder, run `npm i`.

In the /react_app folder, run `npm i`.

#### Setup of the newer version of the application

In the /next-app folder, run `npm i`.

#### Starting the Mongo database server

Start the database server with a command like this from the database's root folder:

"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" -dbpath .\db

(Running the mongod.bat file in that same folder executes the above command.)

Ensure that the dbpath provided above is for the version of the database (dev vs. prod)
you are wanting to employ.

#### Running the backend API of the older application version

Compile the backend code files by entering "npm run compile" (or, "npx tsc -w") from the
/api folder. Per the project's tsconfig.json file, the output from the compiler will be
placed in the /api/dist subfolder.  The backend can be run (and also, debugged) by
creating a Run/Debug configuration in Intellij IDEA where the file to run is dist/api.js.

#### Running the frontend app of the older application version
 
To build and serve the frontend React app, enter "npm run start" from the /react_app
folder.  This runs the webpack-dev-server to serve the app at port 8080.  Calls from the
frontend app to the backend's API are re-routed to port 80 using a webpack proxy defined
in the webpack config file (at react_app/webpack.config.js). React Fast Refresh is enabled
for the frontend app, so that code changes to the app's React components are reflected in
the running app without it needed to be reloaded, while maintaining the current state of
those components.
 
#### Running the newer application version

To start the Next.js development server, enter "npm run dev" from the /next-app folder.
The dev server will serve the frontend of the application on port 3000.
