const path = require("path");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const ReactRefreshTypeScript = require("react-refresh-typescript");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = {
  entry: "main.tsx",
  mode: "development",
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle.js",
    publicPath: "/",
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".css", ".less"],
    modules: ["src", "node_modules"],
    plugins: [new TsconfigPathsPlugin({})],
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf)$/,
        use: ["file-loader"],
      },
      {
        test: /\.tsx?$/,
        use: {
          loader: "ts-loader",
          options: {
            getCustomTransformers: () => ({
              before: [ReactRefreshTypeScript()],
            }),
          },
        },
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "postcss-loader" },
        ],
        exclude: [/sweetalert2\.min\.css/, /react-datetime\.css/],
      },
      {
        test: [/sweetalert2\.min\.css/, /react-datetime\.css/],
        use: [{ loader: "style-loader" }, { loader: "css-loader" }],
      },
      { test: /\.js$/, enforce: "pre", loader: "source-map-loader" },
    ],
  },
  devServer: {
    historyApiFallback: true,
    proxy: {
      "/api": {
        target: "http://127.0.0.1:80", // can't use 'localhost' here; see https://stackoverflow.com/a/76253231/1493347
        secure: false,
      },
    },
  },
  plugins: [new ReactRefreshWebpackPlugin()],
};
