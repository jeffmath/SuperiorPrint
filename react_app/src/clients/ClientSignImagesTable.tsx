import * as React from "react";
import { useEffect } from "react";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { ImageModal } from "../widgets/ImageModal";
import { ImageUploadModal } from "../widgets/ImageUploadModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import { clientSignImageService } from "./ClientSignImageService";
import {
  imageDeleted,
  viewImageSelected,
  imagesFetched,
  imageModalClosed,
  imageUploadModalOpened,
  imageUploadModalClosed,
} from "./clientSignImagesTableSlice";
import { yesNoDialog } from "../util/yesNoDialog";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { Id } from "@/domain/Id";
import { Button } from "@/components/ui/button";

export interface ClientSignImagesTableProps {
  clientId: Id;
}

export function ClientSignImagesTable({ clientId }: ClientSignImagesTableProps) {
  const dispatch = useAppDispatch();
  const { images, imageForView, isImageModalOpen, isImageUploadModalOpen } =
    useAppSelector((state) => state.clientSignImagesTableState);
  const urlForImageView =
    imageForView && `/api/signImage/${imageForView.renderKeyString()}`;
  const urlForImageUpload = `/api/signImages/${clientId}`;

  useEffect(() => {
    clientSignImageService
      .fetchImages(clientId)
      .then((images) => dispatch(imagesFetched(images)));
  }, []);

  function handleViewClick(image: ClientSignImageInfo) {
    dispatch(viewImageSelected(image));
  }

  function handleAddClick() {
    dispatch(imageUploadModalOpened());
  }

  function handleImageModalClose() {
    dispatch(imageModalClosed());
  }

  function handleImagesUpload() {
    clientSignImageService
      .fetchImages(clientId)
      .then((images) => dispatch(imagesFetched(images)));
  }

  function handleImageUploadModalClose() {
    dispatch(imageUploadModalClosed());
  }

  function handleDeleteClick(image: ClientSignImageInfo) {
    yesNoDialog(
      "Are you sure you want to delete this sign image?",
      (yes) =>
        yes &&
        clientSignImageService
          .deleteImage(clientId, image.renderKeyString())
          .then(() => dispatch(imageDeleted(image.renderKeyString()))),
    );
  }

  return (
    <div className="text-left mt-8">
      <h4 className="form-header">Sign images</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add sign image(s)
      </Button>
      <table className="table text-left mt-4">
        <thead>
          <tr>
            <th>Name</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          {images.map((image, i) => (
            <tr key={i}>
              <td className="min-w-80">{image.renderNameString()}</td>
              <td className="min-w-36 flex flex-row space-x-2 items-center justify-center">
                <Button size="xs" onClick={() => handleViewClick(image)}>
                  View
                </Button>
                <Button
                  variant="destructive"
                  size="xs"
                  onClick={() => handleDeleteClick(image)}
                >
                  <FontAwesomeIcon icon={faRemove} />
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <ImageModal
        isOpen={isImageModalOpen}
        imageName={imageForView?.renderNameString()}
        imageUrl={urlForImageView}
        onClose={handleImageModalClose}
      />
      <ImageUploadModal
        isOpen={isImageUploadModalOpen}
        isForMultiple={true}
        uploadUrl={urlForImageUpload}
        onUploaded={handleImagesUpload}
        onRequestClose={handleImageUploadModalClose}
      />
    </div>
  );
}
