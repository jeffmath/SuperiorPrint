import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { Id } from "@/domain/Id";

export interface ClientSignImagesTableState {
  images: ClientSignImageInfo[];
  imageForView?: ClientSignImageInfo;
  isImageModalOpen: boolean;
  isImageUploadModalOpen: boolean;
}

const initialState: ClientSignImagesTableState = {
  images: [],
  isImageModalOpen: false,
  isImageUploadModalOpen: false,
};

export const clientSignImagesTableSlice = createSlice({
  name: "clientSignImagesTable",
  initialState,
  reducers: {
    imagesFetched: (state, action: PayloadAction<ClientSignImageInfo[]>) => {
      state.images = action.payload;
    },
    viewImageSelected: (state, action: PayloadAction<ClientSignImageInfo>) => {
      state.imageForView = action.payload;
      state.isImageModalOpen = true;
    },
    imageModalClosed: (state) => {
      state.isImageModalOpen = false;
    },
    imageUploadModalOpened: (state) => {
      state.isImageUploadModalOpen = true;
    },
    imageUploadModalClosed: (state) => {
      state.isImageUploadModalOpen = false;
    },
    imageDeleted: (state, action: PayloadAction<Id>) => {
      const index = state.images.findIndex(
        (image) => image.renderKeyString() === action.payload,
      );
      if (index >= 0) state.images.splice(index, 1);
    },
  },
});

export const {
  imagesFetched,
  viewImageSelected,
  imageModalClosed,
  imageUploadModalOpened,
  imageUploadModalClosed,
  imageDeleted,
} = clientSignImagesTableSlice.actions;

export default clientSignImagesTableSlice.reducer;
