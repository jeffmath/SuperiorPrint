import * as superagent from "superagent";
import { auth } from "../logins/Auth";
import { onError } from "../util/errorDialog";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { Id } from "@/domain/Id";
import { ClientSignImageInfoDto } from "@/data-transfer/ClientSignImageDto";

class ClientSignImageService {
  async fetchImages(clientId: Id): Promise<ClientSignImageInfo[]> {
    try {
      const res = await superagent.get(`/api/signImages/${clientId}`).set(auth.header);
      return (res.body as ClientSignImageInfoDto[]).map((dto) =>
        ClientSignImageInfo.fromDto(dto),
      );
    } catch (err) {
      onError(err, "Could not fetch client sign images");
      return [];
    }
  }

  async deleteImage(clientId: Id, imageId: Id): Promise<void> {
    try {
      await superagent.delete(`/api/signImage/${clientId}/${imageId}`).set(auth.header);
    } catch (err) {
      onError(err, "Could not delete sign image");
      throw err;
    }
  }
}

export const clientSignImageService = new ClientSignImageService();
