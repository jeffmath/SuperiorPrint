import * as superagent from "superagent";
import { auth } from "../logins/Auth";
import { onError } from "../util/errorDialog";
import { LocationDto } from "@/data-transfer/LocationDto";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";

class PropertyService {
  async fetchPropertiesForClient(clientId: Id): Promise<NamedEntity[]> {
    try {
      const res = await superagent
        .get(`/api/properties/client/${clientId}`)
        .set(auth.header);
      return (res.body as NamedEntityDto[]).map((dto) => NamedEntity.fromDto(dto));
    } catch (err) {
      onError(err, `Could not fetch properties for client`);
      throw err;
    }
  }

  addLocation(propertyId: Id): Promise<LocationDto> {
    return superagent
      .post(`/api/location/${propertyId}`)
      .set(auth.header)
      .then((res) => res.body as LocationDto)
      .catch((err) => {
        onError(err, `Could not add location`);
        throw err;
      });
  }

  updateLocationName(propertyId: Id, locationId: Id, name: string): Promise<void> {
    return superagent
      .put(`/api/location/${propertyId}/${locationId}/${name}`)
      .set(auth.header)
      .then(() => {})
      .catch((err) => {
        onError(err, `Could not update location name`);
        throw err;
      });
  }

  deleteLocation(propertyId: Id, locationId: Id): Promise<void> {
    return superagent
      .delete(`/api/location/${propertyId}/${locationId}`)
      .set(auth.header)
      .then(() => {})
      .catch((err) => {
        onError(err, `Could not delete location`);
        throw err;
      });
  }

  deleteLocationImage(propertyId: Id, locationId: Id): Promise<void> {
    return superagent
      .delete(`/api/locationImage/${propertyId}/${locationId}`)
      .set(auth.header)
      .then(() => {})
      .catch((err) => {
        onError(err, `Could not delete location image`);
        throw err;
      });
  }
}

export const propertyService = new PropertyService();
