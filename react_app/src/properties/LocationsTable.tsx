import * as React from "react";
import { Location } from "@/domain/Location";
import { ImageModal } from "../widgets/ImageModal";
import { ImageUploadModal } from "../widgets/ImageUploadModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencil, faRemove, faFloppyDisk } from "@fortawesome/free-solid-svg-icons";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { State } from "../store/State";
import { propertyService } from "./PropertyService";
import {
  locationAdded,
  locationDeleted,
  locationImageDeleted,
  locationImageUploaded,
  locationNameEdited,
} from "./propertySlice";
import {
  imageModalClosed,
  imageUploadModalClosed,
  locationImageUploadRequested,
  locationImageViewRequested,
  locationSaved,
  locationSelectedForEdit,
} from "./locationsTableSlice";
import { yesNoDialog } from "../util/yesNoDialog";
import { Id } from "@/domain/Id";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

export function LocationsTable() {
  const dispatch = useAppDispatch();
  const {
    isEditingMap,
    locationForImageView,
    isImageModalOpen,
    locationForImageUpload,
    isImageUploadModalOpen,
  } = useAppSelector((state) => state.locationsTableState);
  const property = useAppSelector((state) => state.property!);
  const urlForImageView = useAppSelector((state) => getUrlForImageView(state));
  const urlForImageUpload = useAppSelector((state) => getUrlForImageUpload(state));

  function handleNameChange(location: Location, name: string) {
    dispatch(
      locationNameEdited({
        property,
        location,
        name,
      }),
    );
  }

  function handleSaveClick(location: Location) {
    const locationId = location.renderKeyString();
    propertyService
      .updateLocationName(
        property.renderKeyString(),
        locationId,
        location.renderNameString(),
      )
      .then(() => {
        dispatch(locationSaved(locationId));
      });
  }

  function handleAddClick() {
    propertyService.addLocation(property.renderKeyString()).then((locationDto) => {
      const location = Location.fromDto(locationDto);
      dispatch(locationAdded({ property, location }));
    });
  }

  function handleEditClick(location: Location) {
    dispatch(locationSelectedForEdit(location.renderKeyString()));
  }

  function handleViewImageClick(location: Location) {
    dispatch(locationImageViewRequested(location));
  }

  function handleDeleteImageClick(location: Location) {
    yesNoDialog("Are you sure you want to delete this location's image?", (yes) => {
      const locationId = location.renderKeyString();
      yes &&
        propertyService
          .deleteLocationImage(property.renderKeyString(), locationId)
          .then(() => dispatch(locationImageDeleted({ property, location })));
    });
  }

  function handleUploadImageClick(location: Location) {
    dispatch(locationImageUploadRequested(location));
  }

  function handleDeleteClick(location: Location) {
    const locationId = location.renderKeyString();
    yesNoDialog(
      "Are you sure you want to delete this location?",
      (yes) =>
        yes &&
        propertyService
          .deleteLocation(property.renderKeyString(), locationId)
          .then(() => dispatch(locationDeleted({ property, location }))),
    );
  }

  return (
    <div className="text-left mt-8">
      <h4 className="form-header inline-block">Locations</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add location
      </Button>
      <table className="table text-left mt-4">
        <thead>
          <tr>
            <th>Name</th>
            <th className="text-center">Image</th>
            <th className="text-center">Delete</th>
          </tr>
        </thead>
        <tbody>
          {property?.mapLocations((location, i) => {
            const id = location.renderKeyString();
            const name = location.renderNameString();
            const imageId = location.renderImageKeyString();
            return (
              <tr key={i}>
                <td className="min-w-80">
                  {!isEditingMap[id] ? (
                    <>
                      <span>{name}</span>
                      <Button
                        variant="secondary"
                        size="xs"
                        className="ml-4"
                        onClick={() => handleEditClick(location)}
                      >
                        <FontAwesomeIcon icon={faPencil} />
                      </Button>
                    </>
                  ) : (
                    <div className="flex items-center">
                      <Input
                        value={name}
                        required
                        className="form-control inline-block flex-1"
                        onChange={(e) =>
                          handleNameChange(location, e.currentTarget.value)
                        }
                        onKeyUp={(e) => {
                          if (e.key === "Enter") {
                            e.stopPropagation();
                            handleSaveClick(location);
                          }
                        }}
                      />
                      <Button
                        size="xs"
                        className="ml-1"
                        onClick={() => handleSaveClick(location)}
                      >
                        <FontAwesomeIcon icon={faFloppyDisk} size="sm" />
                      </Button>
                    </div>
                  )}
                </td>
                <td className="min-w-48 flex flex-row space-x-2 items-center justify-center">
                  {imageId && (
                    <Button size="xs" onClick={() => handleViewImageClick(location)}>
                      View
                    </Button>
                  )}
                  {imageId && (
                    <Button
                      variant="destructive"
                      size="xs"
                      onClick={() => handleDeleteImageClick(location)}
                    >
                      <FontAwesomeIcon icon={faRemove} />
                    </Button>
                  )}
                  <Button
                    variant="secondary"
                    size="xs"
                    onClick={() => handleUploadImageClick(location)}
                  >
                    Upload
                  </Button>
                </td>
                <td className="text-center">
                  <Button
                    variant="destructive"
                    size="xs"
                    onClick={() => handleDeleteClick(location)}
                  >
                    <FontAwesomeIcon icon={faRemove} />
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <ImageModal
        isOpen={isImageModalOpen}
        imageName={locationForImageView?.renderNameString()}
        imageUrl={urlForImageView}
        onClose={() => dispatch(imageModalClosed())}
      />
      <ImageUploadModal
        isOpen={isImageUploadModalOpen}
        isForMultiple={false}
        uploadUrl={urlForImageUpload}
        onUploaded={(imageId: Id) =>
          dispatch(
            locationImageUploaded({
              property,
              location: locationForImageUpload!,
              imageId,
            }),
          )
        }
        onRequestClose={() => dispatch(imageUploadModalClosed())}
      />
    </div>
  );
}

function getUrlForImageView(state: State): string | undefined {
  const location = state.locationsTableState.locationForImageView;
  return location && "/api/locationImage/" + location.renderImageKeyString();
}

function getUrlForImageUpload(state: State): string | undefined {
  const location = state.locationsTableState.locationForImageUpload;
  if (!location || !state.property) return undefined;
  return (
    "/api/locationImage/" +
    state.property.renderKeyString() +
    "/" +
    location.renderKeyString()
  );
}
