import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Location } from "@/domain/Location";
import { locationAdded } from "./propertySlice";
import { Id } from "@/domain/Id";

export interface LocationsTableState {
  isEditingMap: { [id: string]: boolean };
  locationForImageView?: Location;
  isImageModalOpen: boolean;
  locationForImageUpload?: Location;
  isImageUploadModalOpen: boolean;
}

const initialState: LocationsTableState = {
  isEditingMap: {},
  isImageModalOpen: false,
  isImageUploadModalOpen: false,
};

export const locationsTableSlice = createSlice({
  name: "locationsTable",
  initialState,
  reducers: {
    locationSelectedForEdit: (state, action: PayloadAction<Id>) => {
      state.isEditingMap[action.payload] = true;
    },
    locationSaved: (state, action: PayloadAction<Id>) => {
      state.isEditingMap[action.payload] = false;
    },
    locationImageViewRequested: (state, action: PayloadAction<Location>) => {
      state.locationForImageView = action.payload;
      state.isImageModalOpen = true;
    },
    imageModalClosed: (state) => {
      state.isImageModalOpen = false;
    },
    locationImageUploadRequested: (state, action: PayloadAction<Location>) => {
      state.locationForImageUpload = action.payload;
      state.isImageUploadModalOpen = true;
    },
    imageUploadModalClosed: (state) => {
      state.isImageUploadModalOpen = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(locationAdded, (state, action) => {
      state.isEditingMap[action.payload.location.renderKeyString()] = true;
    });
  },
});

export const {
  locationSelectedForEdit,
  locationSaved,
  locationImageViewRequested,
  imageModalClosed,
  locationImageUploadRequested,
  imageUploadModalClosed,
} = locationsTableSlice.actions;

export default locationsTableSlice.reducer;
