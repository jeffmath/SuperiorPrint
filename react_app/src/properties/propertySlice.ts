import { Property } from "@/domain/Property";
import { PropertyType } from "../entity/EntityType";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Location } from "@/domain/Location";
import reduceReducers from "reduce-reducers";
import { Reducer } from "redux";
import { getEntitySlice } from "../entity/createEntitySlice";
import { Id } from "@/domain/Id";

interface PropertyPayload {
  property: Property;
}

interface UpdateLocationNamePayload extends PropertyPayload {
  location: Location;
  name: string;
}

interface UpdateLocationImagePayload extends PropertyPayload {
  location: Location;
  imageId: Id;
}

interface AddLocationPayload extends PropertyPayload {
  location: Location;
}

interface DeleteLocationPayload extends PropertyPayload {
  location: Location;
}

interface DeleteLocationImagePayload extends PropertyPayload {
  location: Location;
}

export const propertySlice = createSlice({
  name: "property",
  initialState: <Property | null>null,
  reducers: {
    locationNameEdited: (state, action: PayloadAction<UpdateLocationNamePayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.property)) {
        state.onLocationNameEdited(payload.location, payload.name);
      }
    },
    locationImageUploaded: (state, action: PayloadAction<UpdateLocationImagePayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.property)) {
        state.onLocationImageSelected(payload.location, payload.imageId);
      }
    },
    locationDeleted: (state, action: PayloadAction<DeleteLocationPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.property)) {
        state.removeLocation(payload.location);
      }
    },
    locationAdded: (state, action: PayloadAction<AddLocationPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.property)) {
        state.addLocation(payload.location);
      }
    },
    locationImageDeleted: (state, action: PayloadAction<DeleteLocationImagePayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.property)) {
        state.removeLocationImage(payload.location);
      }
    },
  },
});

export const {
  locationNameEdited,
  locationImageUploaded,
  locationDeleted,
  locationAdded,
  locationImageDeleted,
} = propertySlice.actions;

// mix in the property-entity-reducer
export default reduceReducers(
  getEntitySlice(PropertyType).reducer as Reducer<Property | null>,
  propertySlice.reducer,
);
