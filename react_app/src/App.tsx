import * as React from "react";
import { useEffect } from "react";
import { Navigation } from "./Navigation";
import { showNewUserPasswordWarning } from "./logins/showNewUserPasswordWarning";
import {
  ClientType,
  ContactType,
  FinishingType,
  MaterialType,
  PrinterType,
  PropertyType,
  SurfaceType,
  UserType,
} from "./entity/EntityType";
import { loadEntities } from "./entities/loadEntities";
import { useDispatch, useSelector } from "react-redux";
import { State } from "./store/State";
import { auth } from "./logins/Auth";
import { Route, Routes } from "react-router-dom";
import { EntitiesList } from "./entities/EntitiesList";
import { EntityPage } from "./entity/EntityPage";
import { JobsPage } from "./jobs/JobsPage";
import { WorkOrder } from "./jobs/WorkOrder";
import { Deck } from "./jobs/Deck";
import { ChangePasswordForm } from "./logins/ChangePasswordForm";
import { LoginForm } from "./logins/LoginForm";
import { SmallSetEntitiesTable } from "./smallSetEntities/SmallSetEntitiesTable";

export function App() {
  // if the user is logged in from before, set up the authorization for calls
  // made from this instance of the app
  if (auth.isLoggedIn()) auth.createHeader();

  // when the user has logged in, load all the entities used in the app
  const isLoggedIn = useSelector((state: State) => state.loginState.isLoggedIn);
  const dispatch = useDispatch();
  useEffect(() => {
    if (isLoggedIn) {
      auth.createHeader();
      loadEntities(dispatch).then();
    }
  }, [isLoggedIn]);

  return (
    <div>
      {!isLoggedIn && (
        <div className="2xl:container">
          <LoginForm />
        </div>
      )}
      {isLoggedIn && (
        <div>
          <div className="bg-[#f8f8f8] p-1">
            <Navigation />
          </div>
          <div className="2xl:container p-4">
            <Routes>
              <Route path="/jobs" element={<JobsPage />} />
              <Route path="/workOrder" element={<WorkOrder />} />
              <Route path="/deck" element={<Deck />} />
              <Route
                path="/properties"
                element={<EntitiesList entityType={PropertyType} />}
              />
              <Route
                path="/property/:id"
                element={<EntityPage entityType={PropertyType} />}
              />
              <Route path="/clients" element={<EntitiesList entityType={ClientType} />} />
              <Route
                path="/client/:id"
                element={<EntityPage entityType={ClientType} />}
              />
              <Route
                path="/contacts"
                element={<EntitiesList entityType={ContactType} />}
              />
              <Route
                path="/contact/:id"
                element={<EntityPage entityType={ContactType} />}
              />
              <Route
                path="/surfaces"
                element={<SmallSetEntitiesTable entityType={SurfaceType} />}
              />
              <Route
                path="/materials"
                element={<SmallSetEntitiesTable entityType={MaterialType} />}
              />
              <Route
                path="/finishings"
                element={<SmallSetEntitiesTable entityType={FinishingType} />}
              />
              <Route
                path="/printers"
                element={<SmallSetEntitiesTable entityType={PrinterType} />}
              />
              <Route
                path="/users"
                element={
                  <SmallSetEntitiesTable
                    entityType={UserType}
                    onEntityAdd={showNewUserPasswordWarning}
                  />
                }
              />
              <Route path="/changePassword" element={<ChangePasswordForm />} />
            </Routes>
          </div>
        </div>
      )}
    </div>
  );
}
