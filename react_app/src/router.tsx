import * as React from "react";
import { Provider } from "react-redux";
import { store } from "./store/Store";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { App } from "./App";

export const router = (
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Routes>
          <Route path="*" element={<App />} />
        </Routes>
      </div>
    </BrowserRouter>
  </Provider>
);
