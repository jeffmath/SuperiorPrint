import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Button } from "@/components/ui/button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import * as React from "react";
import { Id } from "@/domain/Id";
import { NamedEntity } from "@/domain/NamedEntity";

export interface ClientSelectProps {
  clientId?: Id;
  clients: NamedEntity[];
  onChange: (id: Id) => void;
  onRemove: () => void;
}

export function ClientSelect({
  clientId,
  clients,
  onChange,
  onRemove,
}: ClientSelectProps) {
  return (
    <div className="inline-flex items-center space-x-1">
      <Label htmlFor="clientSelect">Client</Label>
      <Select value={clientId || ""} onValueChange={onChange}>
        <SelectTrigger id="clientSelect" className="w-72">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          {clients.map((client, i) => {
            const params = client.renderOptionParams();
            return (
              <SelectItem key={i} value={params.value}>
                {params.label}
              </SelectItem>
            );
          })}
        </SelectContent>
      </Select>
      <Button variant="secondary" size="xs" className="ml-1" onClick={onRemove}>
        <FontAwesomeIcon icon={faRemove} />
      </Button>
    </div>
  );
}
