import { Contact } from "@/domain/Contact";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { NamedEntity } from "@/domain/NamedEntity";
import { Reducer } from "redux";
import reduceReducers from "reduce-reducers";
import { getEntitySlice } from "../entity/createEntitySlice";
import { ContactType } from "../entity/EntityType";

interface ContactPayload {
  contact: Contact;
}

export interface UpdatePhoneNumberPayload extends ContactPayload {
  phoneNumber: string;
}

export interface UpdateIsInstallerPayload extends ContactPayload {
  isInstaller: boolean;
}

export interface UpdateIsForSuperiorPrintPayload extends ContactPayload {
  isForSuperiorPrint: boolean;
}

export interface UpdateClientPayload extends ContactPayload {
  client?: NamedEntity;
}

export interface UpdatePropertyPayload extends ContactPayload {
  property?: NamedEntity;
}

export const contactSlice = createSlice({
  name: "contact",
  initialState: <Contact | null>null,
  reducers: {
    phoneNumberEdited: (state, action: PayloadAction<UpdatePhoneNumberPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.contact)) {
        state.onPhoneNumberEdited(payload.phoneNumber);
      }
    },
    isInstallerEdited: (state, action: PayloadAction<UpdateIsInstallerPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.contact)) {
        state.isInstaller = payload.isInstaller;
      }
    },
    isForSuperiorPrintEdited: (
      state,
      action: PayloadAction<UpdateIsForSuperiorPrintPayload>,
    ) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.contact)) {
        state.isForSuperiorPrint = payload.isForSuperiorPrint;
      }
    },
    clientSelected: (state, action: PayloadAction<UpdateClientPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.contact)) {
        state.onClientSelected(payload.client);
      }
    },
    propertySelected: (state, action: PayloadAction<UpdatePropertyPayload>) => {
      const { payload } = action;
      if (state?.isSameEntity(payload.contact)) {
        state.onPropertySelected(payload.property);
      }
    },
  },
});

export const {
  phoneNumberEdited,
  isInstallerEdited,
  isForSuperiorPrintEdited,
  clientSelected,
  propertySelected,
} = contactSlice.actions;

// mix in the contact-entity-reducer
export default reduceReducers(
  getEntitySlice(ContactType).reducer as Reducer<Contact | null>,
  contactSlice.reducer,
);
