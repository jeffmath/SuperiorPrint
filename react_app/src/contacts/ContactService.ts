import { Contact } from "@/domain/Contact";
import * as superagent from "superagent";
import { auth } from "../logins/Auth";
import { onError } from "../util/errorDialog";
import { Client } from "@/domain/Client";
import { Property } from "@/domain/Property";
import { ContactDto } from "@/data-transfer/ContactDto";

class ContactService {
  async fetchSuperiorContacts(
    allClients: Client[],
    allProperties: Property[],
  ): Promise<Contact[]> {
    try {
      const res = await superagent.get(`/api/contacts/superior`).set(auth.header);
      return (res.body as ContactDto[]).map((dto) =>
        Contact.fromContactDto(dto, allClients, allProperties),
      );
    } catch (err) {
      onError(err, `Could not fetch Superior Print contacts`);
      throw err;
    }
  }

  async fetchInstallers(
    allClients: Client[],
    allProperties: Property[],
  ): Promise<Contact[]> {
    try {
      const res = await superagent.get(`/api/contacts/installers`).set(auth.header);
      return (res.body as ContactDto[]).map((dto) =>
        Contact.fromContactDto(dto, allClients, allProperties),
      );
    } catch (err) {
      onError(err, `Could not fetch installer contacts`);
      throw err;
    }
  }
}

export const contactService = new ContactService();
