import * as React from "react";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  clientSelected,
  isForSuperiorPrintEdited,
  isInstallerEdited,
  phoneNumberEdited,
  propertySelected,
} from "./contactSlice";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

export function ContactSubform() {
  const contact = useAppSelector((state) => state.contact!);
  const clients = useAppSelector((state) => state.allClientStubs);
  const properties = useAppSelector((state) => state.allPropertyStubs);
  const dispatch = useAppDispatch();
  let { isInstaller, isForSuperiorPrint } = contact;

  function handlePhoneNumberChange(newValue: string) {
    dispatch(
      phoneNumberEdited({
        contact,
        phoneNumber: newValue,
      }),
    );
  }

  function handleIsInstallerChange() {
    dispatch(isInstallerEdited({ contact, isInstaller: !isInstaller }));
  }

  function handleIsForSuperiorPrint() {
    dispatch(
      isForSuperiorPrintEdited({
        contact,
        isForSuperiorPrint: !isForSuperiorPrint,
      }),
    );
  }

  function handleClientChange(newValue: Id | undefined) {
    dispatch(
      clientSelected({
        contact,
        client: newValue ? clients.find((c) => c.hasSameKey(newValue)) : undefined,
      }),
    );
  }

  function handlePropertyChange(newValue: Id | undefined) {
    dispatch(
      propertySelected({
        contact,
        property: newValue ? properties.find((p) => p.hasSameKey(newValue)) : undefined,
      }),
    );
  }

  return (
    <div className="flex flex-col space-y-4 mt-4">
      <div>
        <Label htmlFor="phoneNumber">Phone number</Label>
        <Input
          id="phoneNumber"
          className="w-36"
          value={contact.renderPhoneNumberString()}
          onChange={(e) => handlePhoneNumberChange(e.currentTarget.value)}
        />
      </div>
      <div className="flex items-center space-x-1">
        <Checkbox
          id="installerCheckbox"
          checked={isInstaller}
          onCheckedChange={handleIsInstallerChange}
        />
        <Label htmlFor="installerCheckbox">Is installer</Label>
      </div>
      <div className="flex items-center space-x-1">
        <Checkbox
          id="superiorCheckbox"
          checked={isForSuperiorPrint}
          onChange={handleIsForSuperiorPrint}
        />
        <Label htmlFor="superiorCheckbox">Works for Superior Print</Label>
      </div>
      <div>
        <Label htmlFor="clientSelect">Is with client</Label>
        <div className="flex flex-row items-center space-x-2">
          <Select
            value={contact.renderClientKeyString()}
            onValueChange={handleClientChange}
          >
            <SelectTrigger id="clientSelect" className="w-72">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {clients.map((client, i) => {
                const params = client.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
          <Button
            variant="secondary"
            size="xs"
            className="ml-2"
            onClick={() => handleClientChange(undefined)}
          >
            <FontAwesomeIcon icon={faRemove} />
          </Button>
        </div>
      </div>
      <div>
        <Label htmlFor="propertySelect">Is with property</Label>
        <div className="flex flex-row items-center space-x-2">
          <Select
            value={contact.renderPropertyKeyString()}
            onValueChange={handlePropertyChange}
          >
            <SelectTrigger id="propertySelect" className="w-72">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {properties.map((property, i) => {
                const params = property.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
          <Button
            variant="secondary"
            size="xs"
            onClick={() => handlePropertyChange(undefined)}
          >
            <FontAwesomeIcon icon={faRemove} />
          </Button>
        </div>
      </div>
    </div>
  );
}
