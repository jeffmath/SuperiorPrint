import * as React from "react";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { Checkbox } from "@/components/ui/checkbox";
import { Label } from "@/components/ui/label";
import { useAppSelector } from "../store/reduxHooks";
import { cn } from "@/lib/utils";
import { ClientSelect } from "./ClientSelect";
import { PropertySelect } from "./PropertySelect";

export interface ContactsSearchBarProps {
  className?: string;
  clients: NamedEntity[];
  properties: NamedEntity[];
  onShouldSearchForInstallersChange: (value: boolean) => void;
  onClientIdToSearchForChange: (id: Id) => void;
  onRemoveClient: () => void;
  onPropertyIdToSearchForChange: (id: Id) => void;
  onRemoveProperty: () => void;
}

export function ContactsSearchBar({
  className,
  clients,
  properties,
  onShouldSearchForInstallersChange,
  onClientIdToSearchForChange,
  onRemoveClient,
  onPropertyIdToSearchForChange,
  onRemoveProperty,
}: ContactsSearchBarProps) {
  const { installersOnly, clientId, propertyId } = useAppSelector(
    (state) => state.contactsSearchBarState,
  );

  if (!clients && !properties) return null;
  return (
    <div
      className={cn(className, "flex flex-wrap items-center justify-center space-x-8")}
    >
      <div className="inline-flex items-center space-x-1">
        <Checkbox
          id="installers-checkbox"
          checked={installersOnly}
          onCheckedChange={() => onShouldSearchForInstallersChange(!installersOnly)}
        />
        <Label htmlFor="installers-checkbox">Installers</Label>
      </div>
      <ClientSelect
        clientId={clientId}
        clients={clients}
        onChange={onClientIdToSearchForChange}
        onRemove={onRemoveClient}
      />
      <PropertySelect
        propertyId={propertyId}
        properties={properties}
        onChange={onPropertyIdToSearchForChange}
        onRemove={onRemoveProperty}
      />
    </div>
  );
}
