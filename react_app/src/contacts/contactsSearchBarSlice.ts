import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";
import { clientsSlice, propertiesSlice } from "../entities/createEntitiesSlice";

export interface ContactsSearchBarState {
  installersOnly: boolean;
  clientId?: Id;
  propertyId?: Id;
}

const initialState: ContactsSearchBarState = {
  installersOnly: false,
};

export const contactsSearchBarSlice = createSlice({
  name: "contactsSearchBar",
  initialState,
  reducers: {
    installersOnlyEdited: (state, action: PayloadAction<boolean>) => {
      state.installersOnly = action.payload;
    },
    clientSelected: (state, action: PayloadAction<Id | undefined>) => {
      state.clientId = action.payload;
    },
    propertySelected: (state, action: PayloadAction<Id | undefined>) => {
      state.propertyId = action.payload;
    },
  },
  extraReducers: (builder) => {
    // reconcile when client selected in client-filter dropdown has been deleted
    builder.addCase(clientsSlice.actions.entityDeleted, (state, action) => {
      if (action.payload.key === state.clientId) {
        state.clientId = undefined;
      }
    });
    // reconcile when property selected in property-filter dropdown has been deleted
    builder.addCase(propertiesSlice.actions.entityDeleted, (state, action) => {
      if (action.payload.key === state.propertyId) {
        state.propertyId = undefined;
      }
    });
  },
});

export const { installersOnlyEdited, clientSelected, propertySelected } =
  contactsSearchBarSlice.actions;

export default contactsSearchBarSlice.reducer;
