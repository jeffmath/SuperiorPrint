import { Contact } from "@/domain/Contact";
import { contactsSlice } from "../entities/createEntitiesSlice";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const installersSlice = createSlice({
  name: "installers",
  initialState: <Contact[]>[],
  reducers: {
    installersFetched: (_state, action: PayloadAction<Contact[]>) => action.payload,
  },
  extraReducers: (builder) => {
    builder.addCase(contactsSlice.actions.entitySaved, (state, action) => {
      const contact = <Contact>action.payload.entity;
      return state.map((contact1) =>
        contact1.isSameEntity(contact) ? contact : contact1,
      );
    });
  },
});

export const { installersFetched } = installersSlice.actions;

export default installersSlice.reducer;
