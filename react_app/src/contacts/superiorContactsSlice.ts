import { Contact } from "@/domain/Contact";
import { contactsSlice } from "../entities/createEntitiesSlice";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const superiorContactsSlice = createSlice({
  name: "superiorContacts",
  initialState: <Contact[]>[],
  reducers: {
    contactsFetched: (_state, action: PayloadAction<Contact[]>) => action.payload,
  },
  extraReducers: (builder) => {
    builder.addCase(contactsSlice.actions.entitySaved, (state, action) => {
      const contact = <Contact>action.payload.entity;
      // if the contact is for Superior and is not in our list, add it
      if (contact.isForSuperiorPrint && !state.find((c) => c.isSameEntity(contact)))
        return [...state, contact];
      // if the contact is not for Superior and is in our list, remove it
      if (!contact.isForSuperiorPrint && state.find((c) => c.isSameEntity(contact)))
        return state.filter((c) => !c.isSameEntity(contact));
      // otherwise, if the contact is in our list, update it
      return state.map((contact1) =>
        contact1.isSameEntity(contact) ? contact : contact1,
      );
    });
  },
});

export const { contactsFetched } = superiorContactsSlice.actions;

export default superiorContactsSlice.reducer;
