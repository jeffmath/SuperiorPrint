import { auth } from "./Auth";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";

export interface LoginState {
  userName: string;
  password: string;
  isLoggedIn: boolean;
  isLoginInvalid: boolean;
  userId: Id;
}

const initialState: LoginState = {
  userName: "",
  password: "",
  isLoggedIn: auth.isLoggedIn(),
  isLoginInvalid: false,
  userId: auth.isLoggedIn() ? auth.getLogin().userId : null,
};

export const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    userNameEdited: (state, action: PayloadAction<string>) => {
      state.userName = action.payload;
    },
    passwordEdited: (state, action: PayloadAction<string>) => {
      state.password = action.payload;
    },
    loginInvalidated: (state) => {
      state.isLoginInvalid = true;
    },
    loginValidated: (state, action: PayloadAction<Id>) => {
      state.isLoggedIn = true;
      state.isLoginInvalid = false;
      state.userId = action.payload;
    },
    userLoggedOut: (state) => {
      state.isLoggedIn = false;
    },
  },
});

export const {
  userNameEdited,
  passwordEdited,
  userLoggedOut,
  loginInvalidated,
  loginValidated,
} = loginSlice.actions;

export default loginSlice.reducer;
