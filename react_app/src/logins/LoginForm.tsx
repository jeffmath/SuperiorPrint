import * as React from "react";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import {
  loginInvalidated,
  passwordEdited,
  loginValidated,
  userNameEdited,
} from "./loginSlice";
import { loginService } from "./LoginService";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

export function LoginForm() {
  const dispatch = useAppDispatch();
  const { userName, password, isLoginInvalid } = useAppSelector(
    (state) => state.loginState,
  );
  const isLoginInfoInvalid = !userName.length || !password.length;

  function handleLoginClick() {
    loginService.login(userName, password).then(async (login) => {
      if (login.isPasswordWrong) return dispatch(loginInvalidated());
      if (login.isTokenExpired || login.hasErrorOccurred) return;

      dispatch(loginValidated(login.userId));
    });
  }

  return (
    <div className="text-left mt-2.5">
      <h4 className="form-header">Log in</h4>
      <div className="mt-2">
        <Label htmlFor="userName">User name</Label>
        <Input
          id="userName"
          type="text"
          className="w-36"
          value={userName}
          onChange={(e) => dispatch(userNameEdited(e.currentTarget.value))}
        />
      </div>
      <div className="mt-2">
        <Label htmlFor="password">Password</Label>
        <Input
          id="password"
          type="password"
          className="w-36"
          value={password}
          onChange={(e) => dispatch(passwordEdited(e.currentTarget.value))}
        />
      </div>
      <Button className="mt-4" onClick={handleLoginClick} disabled={isLoginInfoInvalid}>
        Log in
      </Button>
      {isLoginInvalid && (
        <p className="mt-1.5 error">Invalid log in. Please try again.</p>
      )}
    </div>
  );
}
