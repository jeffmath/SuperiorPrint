const sweetalert = require("sweetalert2");
import "../../node_modules/sweetalert2/dist/sweetalert2.min.css";

export function showNewUserPasswordWarning() {
  const message =
    'The new user has a default password of "spae16".  This represents a security' +
    " vulnerability. Please have the new user change their password as soon as" +
    " is possible.";
  const config = {
    title: "Warning",
    html: message,
    type: "warning",
    showCancelButton: false,
    confirmButtonText: "Ok",
  };
  sweetalert(config);
}
