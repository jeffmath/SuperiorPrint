import * as React from "react";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import {
  passwordChanged,
  passwordEdited,
  reenteredPasswordEdited,
} from "./changePasswordSlice";
import { loginService } from "./LoginService";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";

export function ChangePasswordForm() {
  const dispatch = useAppDispatch();
  const userId = useAppSelector((state) => state.loginState.userId);
  const { password, reenteredPassword, isPasswordChanged } = useAppSelector(
    (state) => state.changePasswordState,
  );

  function handleChangePasswordClick() {
    loginService.changePassword(userId, password).then(() => dispatch(passwordChanged()));
  }

  return (
    <div className="text-left">
      <h4 className="form-header">Change password</h4>
      <p>Your password entries must match, and be at least five characters long.</p>
      <div className="mt-2">
        <Label htmlFor="newPassword">Password</Label>
        <Input
          id="newPassword"
          name="newPassword"
          type="password"
          className="w-56"
          value={password}
          onChange={(e) => dispatch(passwordEdited(e.currentTarget.value))}
        />
      </div>
      <div className="mt-2">
        <Label htmlFor="reenteredPassword">Re-enter password</Label>
        <Input
          id="reenteredPassword"
          type="password"
          className="w-56"
          value={reenteredPassword}
          onChange={(e) => dispatch(reenteredPasswordEdited(e.currentTarget.value))}
        />
      </div>
      {!isPasswordChanged && (
        <Button
          className="mt-4"
          onClick={handleChangePasswordClick}
          disabled={password.length < 5 || password !== reenteredPassword}
        >
          Change password
        </Button>
      )}
      {isPasswordChanged && <p className="mt-1.5">Password successfully changed.</p>}
    </div>
  );
}
