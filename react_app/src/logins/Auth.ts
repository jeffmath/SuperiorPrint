import { Login } from "./Login";

class Auth {
  key = "superior-print-token";

  /**
   * A headers object which is passed along with calls to the web API
   * in order to authenticate the user.  We try to only create this
   * object as necessary, and certainly not with every web API call.
   */
  header: Object = {};

  /**
   * Saves the given authentication token to local storage, so
   * it will survive any page refreshes.
   */
  saveLogin(login: Login) {
    localStorage[this.key] = JSON.stringify(login);
    this.createHeader();
  }

  /**
   * Returns the authentication token which was previously stored
   * in local storage.
   */
  getLogin() {
    const storedString = localStorage[this.key];
    return storedString ? JSON.parse(storedString) : null;
  }

  /**
   * Returns whether this object possesses a non-expired
   * authentication token.
   */
  isLoggedIn() {
    const login = this.getLogin();
    if (!login) return false;

    return this.isValidToken(login.token);
  }

  isValidToken(token: string): boolean {
    try {
      // return whether the token has yet to expire
      const payload = JSON.parse(atob(token.split(".")[1]));
      return payload.exp > Date.now() / 1000;
    } catch (e) {
      console.log("Could not parse authentication token");
      return false;
    }
  }

  /**
   * Removes the authentication token which is in local storage.
   */
  logOut() {
    localStorage.removeItem(this.key);
  }

  /**
   * (Internal) Creates the authorizationHeader object.
   */
  createHeader() {
    const login = this.getLogin();
    this.header = {
      Authorization: "Bearer " + login.token,
    };
  }
}

export const auth = new Auth();
