import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface ChangePasswordState {
  password: string;
  reenteredPassword: string;
  isPasswordChanged: boolean;
}

const initialState: ChangePasswordState = {
  password: "",
  reenteredPassword: "",
  isPasswordChanged: false,
};

export const changePasswordSlice = createSlice({
  name: "changePassword",
  initialState,
  reducers: {
    passwordEdited: (state, action: PayloadAction<string>) => {
      state.password = action.payload;
      state.isPasswordChanged = false;
    },
    reenteredPasswordEdited: (state, action: PayloadAction<string>) => {
      state.reenteredPassword = action.payload;
      state.isPasswordChanged = false;
    },
    passwordChanged: (state) => {
      state.isPasswordChanged = true;
    },
  },
});

export const { passwordEdited, reenteredPasswordEdited, passwordChanged } =
  changePasswordSlice.actions;

export default changePasswordSlice.reducer;
