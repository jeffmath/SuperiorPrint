import { auth } from "./Auth";
import { onError } from "../util/errorDialog";
import * as superagent from "superagent";
import { Login } from "./Login";
import { Id } from "@/domain/Id";

class LoginService {
  async changePassword(userId: Id, password: string): Promise<void> {
    try {
      await superagent.put(`/api/changePassword/${userId}/${password}`).set(auth.header);
    } catch (err) {
      onError(err, `Could not change password`);
      throw err;
    }
  }

  async login(userName: string, password: string): Promise<Login> {
    try {
      let res = await superagent.get(`/api/login/${userName}/${password}`);
      if (res.text === "Incorrect password") return <Login>{ isPasswordWrong: true };

      const login = res.body as Login;
      if (!auth.isValidToken(login.token)) {
        onError("Login unsuccessful", "Token is no longer valid");
        return <Login>{ isTokenExpired: true };
      }

      auth.saveLogin(login);
      return login;
    } catch (err) {
      onError(err, `Login attempt failed`);
      return <Login>{ hasErrorOccurred: true };
    }
  }
}

export const loginService = new LoginService();
