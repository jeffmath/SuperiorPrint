import { Id } from "@/domain/Id";

export interface Login {
  token: string;
  userId: Id;
  isPasswordWrong: boolean;
  isTokenExpired: boolean;
  hasErrorOccurred: boolean;
}
