import * as React from "react";
import { OptionParams } from "@/util/params";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { ReactNode } from "react";

export interface SelectFieldProps {
  label: string;
  className?: string;
  id: Id;
  options: OptionParams[];
  value: string;
  suppliedButton?: ReactNode;
  onChange: (newValue: string) => void;
}

export function SelectField({
  label,
  className,
  id,
  options,
  value,
  suppliedButton,
  onChange,
}: SelectFieldProps) {
  return (
    <div className="relative">
      <div className="inline-block">
        <Label htmlFor={id}>{label}</Label>
        <Select value={value || ""} onValueChange={onChange}>
          <SelectTrigger id={id} className={className || "min-w-24"}>
            <SelectValue />
          </SelectTrigger>
          <SelectContent>
            <SelectItem value="">&nbsp;</SelectItem>
            {(options || []).map((option, i) => (
              <SelectItem key={i} value={option.value}>
                {option.label}
              </SelectItem>
            ))}
          </SelectContent>
        </Select>
      </div>
      {suppliedButton || <></>}
    </div>
  );
}
