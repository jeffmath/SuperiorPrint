import React from "react";
import { useNavigate } from "react-router-dom";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@/components/ui/button";

export function BackButton() {
  const navigate = useNavigate();
  return (
    <Button variant="secondary" size="xs" className="ml-4" onClick={() => navigate(-1)}>
      <FontAwesomeIcon icon={faArrowLeft} />
    </Button>
  );
}
