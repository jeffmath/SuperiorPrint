import * as React from "react";
import {
  Pagination,
  PaginationContent,
  PaginationItem,
  PaginationLink,
} from "@/components/ui/pagination";

export interface FirstLetterFilterProps {
  className?: string;
  currentLetter: string;
  onLetterSelect: (letter: string) => void;
}

export function FirstLetterFilter({
  className,
  currentLetter,
  onLetterSelect,
}: FirstLetterFilterProps) {
  return (
    <Pagination className={className}>
      <PaginationContent className="flex-wrap justify-center">
        {letters.map((letter, i) => (
          <PaginationItem key={i} onClick={() => onLetterSelect(letter)}>
            <PaginationLink href="#" isActive={letter === currentLetter}>
              {letter === "*" ? "All" : letter === "0" ? "0-9" : letter}
            </PaginationLink>
          </PaginationItem>
        ))}
      </PaginationContent>
    </Pagination>
  );
}

// create the array of letters (and otherwise) by which filtering may occur
const letters = [] as string[];
letters[0] = "*";
letters[1] = "0";
const aCode = "A".charCodeAt(0);
for (let i = 0; i < 26; i++) letters[i + 2] = String.fromCharCode(aCode + i);
