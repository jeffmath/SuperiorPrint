import * as React from "react";
import "react-datetime/css/react-datetime.css";

import Datetime from "react-datetime";
import { Moment } from "moment";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { useState } from "react";
const moment = require("moment");

export interface DateTimeFieldProps {
  labelText: string;
  dateTime?: Date;
  shouldShowTime?: boolean;
  onDateTimeChanged(dateTime?: Date): void;
}

export function DateTimeField({
  shouldShowTime,
  labelText,
  dateTime,
  onDateTimeChanged,
}: DateTimeFieldProps) {
  const [isDatePickerOpen, setIsDatePickerOpen] = useState(false);

  return (
    <div>
      <Label htmlFor="dateInput">{labelText}</Label>
      <Datetime
        value={dateTime ? moment(dateTime) : null}
        timeFormat={shouldShowTime || false}
        renderInput={(props, openCalendar, closeCalendar) => {
          return (
            <Input
              className={shouldShowTime ? "w-40" : "w-28"}
              value={props.value}
              onClick={() => {
                if (!isDatePickerOpen) openCalendar();
                else closeCalendar();
                setIsDatePickerOpen(!isDatePickerOpen);
              }}
              onBlur={() => setIsDatePickerOpen(false)}
              onChange={props.onChange}
            ></Input>
          );
        }}
        onChange={(moment) =>
          onDateTimeChanged(moment ? (moment as Moment).toDate() : undefined)
        }
      />
    </div>
  );
}
