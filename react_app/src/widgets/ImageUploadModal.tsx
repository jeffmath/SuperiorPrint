import * as React from "react";
import { useState } from "react";
import { modalStyle } from "./ModalStyle";
import * as Modal from "react-modal";
import { fileUploadService } from "../util/FileUploadService";
import { Id } from "@/domain/Id";

// an old version of this package is used here because the newer ones no longer provide
// image-preview thumbnails
const Dropzone = require("react-dropzone");

export interface ImageUploadModalValueProps {
  isForMultiple: boolean;
  isOpen: boolean;
  uploadUrl?: string;
}

export interface ImageUploadModalEventProps {
  onRequestClose: () => void;
  onUploaded: (imageId: Id) => void;
}

export type ImageUploadModalProps = ImageUploadModalValueProps &
  ImageUploadModalEventProps;

export function ImageUploadModal(props: ImageUploadModalProps) {
  const [files, setFiles] = useState([] as File[]);
  const [isUploaded, setIsUploaded] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>(undefined);
  const [wasOpen, setWasOpen] = useState(false);

  // reset this modal's state each time it is opened
  if (props.isOpen !== wasOpen) {
    setWasOpen(props.isOpen);
    if (props.isOpen) {
      setFiles([]);
      setIsUploaded(false);
      setErrorMessage(undefined);
      return null;
    }
  }

  function uploadImages(files: File[]) {
    // if no files were accepted, don't do anything
    if (!files || !files.length) return;

    setFiles(files);
    fileUploadService
      .uploadFiles(files, props.uploadUrl!)
      .then((res) => {
        setIsUploaded(true);
        props.onUploaded(res.text);
      })
      .catch((err) => setErrorMessage(err.response?.body || err.message));
  }

  return (
    <Modal
      isOpen={props.isOpen}
      contentLabel="Modal"
      style={modalStyle}
      onRequestClose={props.onRequestClose}
      ariaHideApp={false}
    >
      <div className="modal-header">
        <h4 className="modal-title inline-block">
          Upload image{props.isForMultiple ? "s" : ""}
        </h4>
      </div>
      <form name="uploadForm" className="modal-body text-left">
        {!files.length && (
          <Dropzone onDrop={uploadImages} multiple={props.isForMultiple} accept="image/*">
            <div>
              Drag {filesNoun(props.isForMultiple)} here, or click to select{" "}
              {filesNoun(props.isForMultiple)} to upload.
            </div>
          </Dropzone>
        )}
        {files.length > 0 && (
          <div>
            File{props.isForMultiple ? "s" : ""}:
            <ul>
              {files.map((f, i) => (
                <li key={i} style={{ fontSize: "small" }}>
                  {f.name}
                  <br />
                  <img src={(f as any).preview} className="size-12" alt="image" />
                </li>
              ))}
            </ul>
            <span className="error">{errorMessage}</span>
            {isUploaded && "Success!"}
          </div>
        )}
      </form>
    </Modal>
  );
}

function filesNoun(isForMultiple: boolean) {
  return isForMultiple ? "files" : "a file";
}
