import * as React from "react";
import { Contact } from "@/domain/Contact";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

export interface ContactSelectProps {
  labelText: string;
  contacts: Contact[];
  selectedContactId?: Id;
  onContactSelect(id: Id): void;
}

export function ContactSelect({
  labelText,
  contacts,
  selectedContactId,
  onContactSelect,
}: ContactSelectProps) {
  return (
    <div>
      <Label htmlFor="contactSelect">{labelText}</Label>
      <Select value={selectedContactId || ""} onValueChange={onContactSelect}>
        <SelectTrigger id="contactSelect" className="min-w-33">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="">&nbsp;</SelectItem>
          {contacts.map((contact, i) => {
            const params = contact.renderOptionParams();
            return (
              <SelectItem key={i} value={params.value}>
                {params.label}
              </SelectItem>
            );
          })}
        </SelectContent>
      </Select>
      <div>
        <FontAwesomeIcon icon={faPhone} />
        &nbsp;
        {selectedContactId &&
          contacts
            .find((c) => c.hasSameKey(selectedContactId))
            ?.renderPhoneNumberString()}
      </div>
    </div>
  );
}
