import React from "react";
import {
  Pagination,
  PaginationContent,
  PaginationFirst,
  PaginationItem,
  PaginationLast,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";

export interface PageSelectorProps {
  className?: string;
  currentPage: number;
  totalItems: number;
  onChange: (pageNumber: number) => void;
}

export function PageSelector({
  className,
  currentPage,
  totalItems,
  onChange,
}: PageSelectorProps) {
  const itemsPerPage = 10;
  const lastPage = Math.ceil(totalItems / itemsPerPage);
  function getPageRange(): number[] {
    const result = [];
    const minPage = Math.max(1, currentPage - 2);
    const maxPage = Math.min(currentPage + 2, lastPage);
    for (let i = minPage; i <= maxPage; i++) {
      result.push(i);
    }
    return result;
  }

  return (
    <Pagination className={className} currentPage={currentPage} lastPage={lastPage}>
      <PaginationContent>
        <PaginationItem>
          <PaginationFirst href="#" onClick={() => onChange(1)} />
        </PaginationItem>
        <PaginationItem>
          <PaginationPrevious
            href="#"
            onClick={() => currentPage > 1 && onChange(currentPage - 1)}
          />
        </PaginationItem>
        {getPageRange().map((page, i) => (
          <PaginationItem key={i}>
            <PaginationLink
              href="#"
              isActive={page === currentPage}
              onClick={() => onChange(page)}
            >
              {page}
            </PaginationLink>
          </PaginationItem>
        ))}
        <PaginationItem>
          <PaginationNext href="#" onClick={() => onChange(currentPage + 1)} />
        </PaginationItem>
        <PaginationItem>
          <PaginationLast
            href="#"
            onClick={() => currentPage < lastPage - 1 && onChange(lastPage)}
          />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  );
}
