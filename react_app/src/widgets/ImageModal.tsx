import * as React from "react";

const Modal = require("react-modal");
import { modalStyle } from "./ModalStyle";

export interface ImageModalValueProps {
  isOpen: boolean;
  imageName?: string;
  imageUrl?: string;
}

export interface ImageModalEventProps {
  onClose: () => void;
}

export type ImageModalProps = ImageModalValueProps & ImageModalEventProps;

export function ImageModal(props: ImageModalProps) {
  return (
    <Modal
      isOpen={props.isOpen}
      contentLabel="Modal"
      style={modalStyle}
      onRequestClose={props.onClose}
      ariaHideApp={false}
    >
      <div className="modal-header">
        <h4 className="modal-title inline-block">{props.imageName}</h4>
      </div>
      <div className="modal-body text-center">
        <img
          src={props.imageUrl}
          style={{ maxWidth: "600px", maxHeight: "600px" }}
          alt="image"
        />
      </div>
    </Modal>
  );
}
