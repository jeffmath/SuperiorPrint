import * as React from "react";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove, faSearch } from "@fortawesome/free-solid-svg-icons";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";

export interface SearchFieldValueProps {
  domain: string;
  searchText: string;
}

export interface SearchFieldEventProps {
  onChange: (value: string) => void;
  onSubmit: () => void;
}

export type SearchFieldProps = SearchFieldValueProps & SearchFieldEventProps;

export function SearchField({
  searchText,
  domain,
  onChange,
  onSubmit,
}: SearchFieldProps) {
  const [entryRemoved, setEntryRemoved] = useState(false);

  //----------------------
  // this section is here because we have to wait for the removal's change
  // to take effect before we can submit that change

  useEffect(() => {
    if (entryRemoved) {
      onSubmit();
      setEntryRemoved(false);
    }
  }, [entryRemoved]);

  function onRemove() {
    onChange("");
    setEntryRemoved(true);
  }

  //--------------------

  return (
    <div className="inline-flex items-center space-x-1">
      <Label htmlFor="searchText">Search in {domain}</Label>
      <Input
        type="text"
        id="searchText"
        value={searchText || ""}
        className="inline-block w-36"
        onKeyUp={(e) => {
          if (e.key === "Enter") onSubmit();
        }}
        onChange={(e) => onChange(e.currentTarget.value)}
      />
      <Button variant="secondary" size="xs" onClick={() => onSubmit()}>
        <FontAwesomeIcon icon={faSearch} />
      </Button>
      <Button variant="secondary" size="xs" onClick={onRemove}>
        <FontAwesomeIcon icon={faRemove} />
      </Button>
    </div>
  );
}
