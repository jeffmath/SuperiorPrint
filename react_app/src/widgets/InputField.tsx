import * as React from "react";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";

export interface InputFieldProps {
  label: string;
  id: Id;
  className: string;
  value: string;
  onChange: (newValue: string) => void;
}

export function InputField({ label, id, className, value, onChange }: InputFieldProps) {
  return (
    <div>
      <Label htmlFor={id}>{label}</Label>
      <Input
        value={value || ""}
        id={id}
        className={className}
        onChange={(e) => onChange(e.currentTarget.value)}
      />
    </div>
  );
}
