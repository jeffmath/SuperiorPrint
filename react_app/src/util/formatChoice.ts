import * as moment from "moment";

export function formatDateChoice(date1?: Date, date2?: Date): string {
  return date1 || date2 ? moment(date1 || date2).format("M/D/YYYY") : "";
}

export function formatTimeChoice(date1?: Date, date2?: Date): string {
  return date1 || date2 ? moment(date1 || date2).format("h:mm a") : "";
}
