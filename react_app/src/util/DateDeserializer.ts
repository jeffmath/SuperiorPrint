/**
 * Converts date-resembling string properties of an object to dates.
 */
export class DateDeserializer {
  /**
   * A regex for capturing dates.
   */
  private static regexIso8601: RegExp =
    /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;

  /**
   * Recursively searches the given input object for string properties
   * which look like dates, converting them to dates.
   */
  static deserializeDates(input: any) {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;

    for (let key in input) {
      if (!input.hasOwnProperty(key)) continue;

      let value = input[key];
      let match;
      // Check for string properties which look like dates.
      if (typeof value === "string" && (match = value.match(this.regexIso8601))) {
        const milliseconds = Date.parse(match[0]);
        if (!isNaN(milliseconds)) {
          input[key] = new Date(milliseconds);
        }
      } else if (typeof value === "object") {
        // Recurse into object
        this.deserializeDates(value);
      }
    }
  }
}
