const sweetalert = require("sweetalert2");
import "../../node_modules/sweetalert2/dist/sweetalert2.min.css";

export function yesNoDialog(
  text: string,
  callback: (result: boolean) => void,
  title: string = "",
  type: string = "warning",
) {
  const config = {
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No",
  };
  sweetalert(config)
    .then(() => callback(true))
    .catch((ignore: any) => callback(false));
}
