import * as superagent from "superagent";
import { auth } from "../logins/Auth";

class FileUploadService {
  uploadFiles(files: File[], uploadUrl: string): Promise<superagent.Response> {
    const upload = superagent.post(uploadUrl).set(auth.header);
    files.forEach((f) => upload.attach(f.name, f));
    return upload;
  }
}

export const fileUploadService = new FileUploadService();
