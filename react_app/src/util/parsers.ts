export function parseIntOrNothing(value: string) {
  return !value.length ? 0 : parseInt(value);
}

export function parseFloatOrNothing(value: string) {
  return !value.length ? 0 : parseFloat(value);
}
