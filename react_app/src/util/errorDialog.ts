const sweetalert = require("sweetalert2");
import "../../node_modules/sweetalert2/dist/sweetalert2.min.css";

export function onError(error: any, text: string) {
  const message =
    error.response?.body ||
    error.response?.text?.substring(0, error.response.text.indexOf("<br>")) ||
    error.message;
  console.log(text + (message ? ": " + message : ""));
  errorDialog(text, message);
}

export function errorDialog(text: string, message: string) {
  const config = {
    title: "Error",
    html: text + (message ? ":<br/> " + message : ""),
    type: "error",
    showCancelButton: false,
    confirmButtonText: "Ok",
  };
  sweetalert(config);
}
