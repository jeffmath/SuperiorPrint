import * as React from "react";
import { useEffect, useState } from "react";
import { FirstLetterFilter } from "../widgets/FirstLetterFilter";
import { Contact } from "@/domain/Contact";
import { ContactsSearchBar } from "../contacts/ContactsSearchBar";
import { ClientType, ContactType, EntityType, PropertyType } from "../entity/EntityType";
import { SearchField } from "../widgets/SearchField";
import { EntitiesPage } from "./EntitiesPage";
import { NamedEntity } from "@/domain/NamedEntity";
import { State } from "../store/State";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { getEntitiesFilterSlice } from "./createEntitiesFilterSlice";
import { entitiesService, hydratorForPagedEntity } from "./EntitiesService";
import { getEntitySlice } from "../entity/createEntitySlice";
import { useNavigate } from "react-router-dom";
import {
  clientSelected,
  propertySelected,
  installersOnlyEdited,
} from "../contacts/contactsSearchBarSlice";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Id } from "@/domain/Id";
import { getEntitiesPageSlice } from "./createEntitiesPageSlice";
import { PageSelector } from "../widgets/PageSelector";
import { cn } from "@/lib/utils";
import { Button } from "@/components/ui/button";

export interface EntitiesListProps {
  entityType: EntityType;
}

export const entitiesPerPage = 10;

export function EntitiesList({ entityType }: EntitiesListProps) {
  const dispatch = useAppDispatch();
  const entitiesPage = useAppSelector((state) => getEntities(entityType, state));
  const searchBarState = useAppSelector((state) => state.contactsSearchBarState);
  const filter = useAppSelector((state) => getFilter(entityType, state));
  const clients = useAppSelector((state) => state.allClientStubs);
  const properties = useAppSelector((state) => state.allPropertyStubs);
  const filterActions = getEntitiesFilterSlice(entityType).actions;
  const pageActions = getEntitiesPageSlice(entityType).actions;
  const hydrate = hydratorForPagedEntity(entityType, clients, properties);
  const isForContacts = entityType === ContactType;
  const navigate = useNavigate();
  const [submittedSearchText, setSubmittedSearchText] = useState(filter.searchText);

  useEffect(() => {
    if (!isForContacts || (clients.length && properties.length)) {
      entitiesService
        .fetchEntitiesPage(
          entityType,
          filter.pageNumber,
          filter.firstLetter,
          submittedSearchText,
          entityType === ContactType ? searchBarState : undefined,
          entityType === ContactType
            ? (dto: NamedEntityDto) =>
                Contact.fromContactDto(dto as ContactDto, clients, properties)
            : undefined,
        )
        .then((page) => dispatch(pageActions.entitiesPageFetched({ entityType, page })));
    }
  }, [
    isForContacts,
    clients,
    properties,
    entityType,
    filter.pageNumber,
    filter.firstLetter,
    searchBarState,
    submittedSearchText,
  ]);

  function isPastLagePage(pageNumber: number, totalEntities: number) {
    return totalEntities > 0 && pageNumber > Math.ceil(totalEntities / entitiesPerPage);
  }

  // if the page number is beyond the current number of pages of entities available (likely
  // because the filter value was just changed to be more selective), reset it to one
  const { entities, total: totalEntities } = entitiesPage || { total: 0 };
  useEffect(() => {
    if (isPastLagePage(filter.pageNumber, totalEntities)) handlePageNumberSelect(1);
  }, [filter.pageNumber, totalEntities]);

  function handleSelect(entity: NamedEntity) {
    entitiesService
      .fetchEntity(entityType, entity.renderKeyString(), hydrate)
      .then((entity) => {
        dispatch(
          getEntitySlice(entityType).actions.entityFetched({ entity, entityType }),
        );
        navigate(`/${entityType.name}/${entity.renderKeyString()}`);
      });
  }

  function handlePageNumberSelect(pageNumber: number) {
    dispatch(filterActions.pageNumberSelected({ entityType, pageNumber }));
  }

  function handleSearchTextChange(searchText: string) {
    dispatch(filterActions.searchTextEdited({ entityType, searchText }));
  }

  function handleFirstLetterSelect(firstLetter: string) {
    dispatch(filterActions.firstLetterSelected({ entityType, firstLetter }));
  }

  function handleAddClick() {
    entitiesService.addEntity(entityType, hydrate).then((entity) => {
      dispatch(pageActions.pagedEntityAdded({ entityType, entity }));
      dispatch(filterActions.pageNumberSelected({ entityType, pageNumber: 1 }));
    });
  }

  function handleShouldSearchForInstallersChange(value: boolean) {
    dispatch(installersOnlyEdited(value));
  }

  function handleClientIdToSearchForChange(id: Id) {
    dispatch(clientSelected(id));
  }

  function handleRemoveClient() {
    dispatch(clientSelected(undefined));
  }

  function handlePropertyIdToSearchForChange(id: Id) {
    dispatch(propertySelected(id));
  }

  function handleRemoveProperty() {
    dispatch(propertySelected(undefined));
  }

  return (
    <div className="text-center">
      <SearchField
        domain="names"
        searchText={filter.searchText}
        onChange={handleSearchTextChange}
        onSubmit={() => setSubmittedSearchText(filter.searchText)}
      />
      {isForContacts && (
        <ContactsSearchBar
          className="mt-3.5"
          clients={clients}
          properties={properties}
          onShouldSearchForInstallersChange={handleShouldSearchForInstallersChange}
          onClientIdToSearchForChange={handleClientIdToSearchForChange}
          onRemoveClient={handleRemoveClient}
          onPropertyIdToSearchForChange={handlePropertyIdToSearchForChange}
          onRemoveProperty={handleRemoveProperty}
        />
      )}
      <FirstLetterFilter
        className="mt-3"
        currentLetter={filter.firstLetter}
        onLetterSelect={handleFirstLetterSelect}
      />
      <PageSelector
        className="mt-2.5"
        currentPage={filter.pageNumber}
        totalItems={totalEntities}
        onChange={handlePageNumberSelect}
      />
      <div className="flex flex-col items-center w-full mt-5">
        <table
          className={cn(
            "table table-hover text-left",
            isForContacts ? "w-[50rem]" : "w-[30rem]",
          )}
        >
          <thead>
            {!isForContacts && (
              <tr>
                <th>Name</th>
              </tr>
            )}
            {isForContacts && (
              <tr>
                <th>Name</th>
                <th className="text-center">Phone number</th>
                <th className="text-center">Type</th>
                <th className="text-center">Is contact for</th>
              </tr>
            )}
          </thead>
          <tbody>
            {entities?.map((entity, i) => (
              <tr key={i} onClick={() => handleSelect(entity)}>
                <td>{entity.renderNameString() || "(No name)"}</td>
                {isForContacts && (
                  <td className="text-center">
                    {(entity as Contact).renderPhoneNumberString()}
                  </td>
                )}
                {isForContacts && (
                  <td className="text-center">{(entity as Contact).renderType()}</td>
                )}
                {isForContacts && (
                  <td className="text-center">
                    {getWhoIsContactFor(entity as Contact, clients, properties)}
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="mt-5">
        <Button variant="secondary" onClick={handleAddClick}>
          Add {entityType.name}
        </Button>
      </div>
    </div>
  );
}

function getWhoIsContactFor(
  contact: Contact,
  clients: NamedEntity[],
  properties: NamedEntity[],
): string | undefined {
  const client = clients.find((c) => contact.isForClient(c));
  if (client) return client.renderNameString();

  const property = properties.find((p) => contact.isForProperty(p));
  if (property) return property.renderNameString();

  return undefined;
}

function getEntities(entityType: EntityType, state: State): EntitiesPage | null {
  switch (entityType) {
    case PropertyType:
      return state.propertiesPage;
    case ClientType:
      return state.clientsPage;
    case ContactType:
      return state.contactsPage;
  }
  throw "Invalid (or no) entity-type given";
}

function getFilter(entityType: EntityType, state: State) {
  switch (entityType) {
    case PropertyType:
      return state.propertiesFilter;
    case ClientType:
      return state.clientsFilter;
    case ContactType:
      return state.contactsFilter;
  }
  throw "Invalid (or no) entity-type given";
}
