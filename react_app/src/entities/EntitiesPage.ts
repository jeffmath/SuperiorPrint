import { NamedEntity } from "@/domain/NamedEntity";

export interface EntitiesPage {
  entities: NamedEntity[];
  total: number;
}
