import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  ClientType,
  ContactType,
  EntityType,
  FinishingType,
  MaterialType,
  PrinterType,
  PropertyType,
  SurfaceType,
  UserType,
} from "../entity/EntityType";

interface ActionPayload {
  entityType: EntityType;
}

interface ReceiveEntitiesPayload extends ActionPayload {
  entities: NamedEntity[];
}

interface UpdateEntityNamePayload extends ActionPayload {
  entity: NamedEntity;
  name: string;
}

interface AddEntityPayload extends ActionPayload {
  entity: NamedEntity;
}

interface SaveEntityPayload extends ActionPayload {
  entity: NamedEntity;
}

interface DeleteEntityPayload extends ActionPayload {
  key: Id;
}

function createEntitiesSlice(name: string, entityType: EntityType) {
  return createSlice({
    name,
    initialState: <NamedEntity[]>[],
    reducers: {
      entitiesFetched: (state, action: PayloadAction<ReceiveEntitiesPayload>) =>
        action.payload.entityType === entityType ? action.payload.entities : state,
      entityNameEdited: (state, action: PayloadAction<UpdateEntityNamePayload>) => {
        const { payload } = action;
        if (payload.entityType === entityType) {
          state
            .find((entity1) => entity1.isSameEntity(payload.entity))
            ?.onNameEdited(payload.name);
        }
      },
      entityAdded: (state, action: PayloadAction<AddEntityPayload>) =>
        action.payload.entityType === entityType
          ? [action.payload.entity, ...state]
          : state,
      entitySaved: (state, action: PayloadAction<SaveEntityPayload>) =>
        action.payload.entityType === entityType
          ? state.map((i) =>
              i.isSameEntity(action.payload.entity) ? action.payload.entity : i,
            )
          : state,
      entityDeleted: (state, action: PayloadAction<DeleteEntityPayload>) =>
        action.payload.entityType === entityType
          ? state.filter((i) => !i.hasSameKey(action.payload.key))
          : state,
    },
  });
}

export const clientsSlice = createEntitiesSlice("clients", ClientType);
export const propertiesSlice = createEntitiesSlice("properties", PropertyType);
export const contactsSlice = createEntitiesSlice("contacts", ContactType);
export const surfacesSlice = createEntitiesSlice("surfaces", SurfaceType);
export const materialsSlice = createEntitiesSlice("materials", MaterialType);
export const finishingsSlice = createEntitiesSlice("finishings", FinishingType);
export const printersSlice = createEntitiesSlice("printers", PrinterType);
export const usersSlice = createEntitiesSlice("users", UserType);

export function getEntitiesSlice(entityType: EntityType) {
  switch (entityType) {
    case ClientType:
      return clientsSlice;
    case PropertyType:
      return propertiesSlice;
    case ContactType:
      return contactsSlice;
    case SurfaceType:
      return surfacesSlice;
    case MaterialType:
      return materialsSlice;
    case FinishingType:
      return finishingsSlice;
    case PrinterType:
      return printersSlice;
    case UserType:
      return usersSlice;
    default:
      throw "Invalid entity-type given";
  }
}
