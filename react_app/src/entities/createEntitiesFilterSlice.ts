import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ClientType, ContactType, EntityType, PropertyType } from "../entity/EntityType";

export interface EntitiesFilterState {
  searchText: string;
  firstLetter: string;
  pageNumber: number;
}

const initialState: EntitiesFilterState = {
  searchText: "",
  firstLetter: "*",
  pageNumber: 1,
};

interface ActionPayload {
  entityType: EntityType;
}

interface FirstLetterSelectedPayload extends ActionPayload {
  firstLetter: string;
}

interface SearchTextEditedPayload extends ActionPayload {
  searchText: string;
}

interface PageNumberSelectedPayload extends ActionPayload {
  pageNumber: number;
}

function createEntitiesFilterSlice(name: string, entityType: EntityType) {
  return createSlice({
    name,
    initialState,
    reducers: {
      firstLetterSelected: (state, action: PayloadAction<FirstLetterSelectedPayload>) => {
        if (action.payload.entityType === entityType)
          state.firstLetter = action.payload.firstLetter;
      },
      searchTextEdited: (state, action: PayloadAction<SearchTextEditedPayload>) => {
        if (action.payload.entityType === entityType)
          state.searchText = action.payload.searchText;
      },
      pageNumberSelected: (state, action: PayloadAction<PageNumberSelectedPayload>) => {
        if (action.payload.entityType === entityType)
          state.pageNumber = action.payload.pageNumber;
      },
    },
  });
}

export const propertiesFilterSlice = createEntitiesFilterSlice(
  "propertiesFilter",
  PropertyType,
);
export const clientsFilterSlice = createEntitiesFilterSlice("clientsFilter", ClientType);
export const contactsFilterSlice = createEntitiesFilterSlice(
  "contactsFilter",
  ContactType,
);

export function getEntitiesFilterSlice(entityType: EntityType) {
  switch (entityType) {
    case ClientType:
      return clientsFilterSlice;
    case PropertyType:
      return propertiesFilterSlice;
    case ContactType:
      return contactsFilterSlice;
    default:
      throw "Invalid entity-type given";
  }
}
