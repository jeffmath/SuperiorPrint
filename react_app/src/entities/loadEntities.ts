import { entitiesService } from "./EntitiesService";
import { contactService } from "../contacts/ContactService";
import {
  ClientType,
  FinishingType,
  MaterialType,
  PrinterType,
  PropertyType,
  SurfaceType,
  UserType,
} from "../entity/EntityType";
import { Property } from "@/domain/Property";
import { AppDispatch } from "../store/Store";
import {
  clientsSlice,
  propertiesSlice,
  finishingsSlice,
  materialsSlice,
  printersSlice,
  surfacesSlice,
  usersSlice,
} from "./createEntitiesSlice";
import { installersFetched } from "../contacts/installersSlice";
import { contactsFetched } from "../contacts/superiorContactsSlice";

export async function loadEntities(dispatch: AppDispatch) {
  const service = entitiesService;
  service
    .fetchEntities(SurfaceType)
    .then((entities) =>
      dispatch(
        surfacesSlice.actions.entitiesFetched({ entityType: SurfaceType, entities }),
      ),
    );
  service.fetchEntities(MaterialType).then((entities) =>
    dispatch(
      materialsSlice.actions.entitiesFetched({
        entityType: MaterialType,
        entities,
      }),
    ),
  );
  service.fetchEntities(FinishingType).then((entities) =>
    dispatch(
      finishingsSlice.actions.entitiesFetched({
        entityType: FinishingType,
        entities,
      }),
    ),
  );
  service.fetchEntities(PrinterType).then((entities) =>
    dispatch(
      printersSlice.actions.entitiesFetched({
        entityType: PrinterType,
        entities,
      }),
    ),
  );
  service
    .fetchEntities(UserType)
    .then((entities) =>
      dispatch(usersSlice.actions.entitiesFetched({ entityType: UserType, entities })),
    );

  const allClients = await service.fetchEntities(ClientType);
  dispatch(
    clientsSlice.actions.entitiesFetched({
      entityType: ClientType,
      entities: allClients,
    }),
  );
  const allProperties = (await service.fetchEntities(PropertyType)) as Property[];
  dispatch(
    propertiesSlice.actions.entitiesFetched({
      entityType: PropertyType,
      entities: allProperties,
    }),
  );

  contactService
    .fetchSuperiorContacts(allClients, allProperties)
    .then((contacts) => dispatch(contactsFetched(contacts)));
  contactService
    .fetchInstallers(allClients, allProperties)
    .then((contacts) => dispatch(installersFetched(contacts)));
}
