import { EntitiesPage } from "./EntitiesPage";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { ClientType, ContactType, EntityType, PropertyType } from "../entity/EntityType";

interface ActionPayload {
  entityType: EntityType;
}

interface EntitiesPageFetchedPayload extends ActionPayload {
  page: EntitiesPage;
}

interface PagedEntityAddedPayload extends ActionPayload {
  entity: NamedEntity;
}

interface PagedEntitySavedPayload extends ActionPayload {
  entity: NamedEntity;
}

interface PagedEntityDeletedPayload extends ActionPayload {
  key: Id;
}

export function createEntitiesPageSlice(name: string, entityType: EntityType) {
  return createSlice({
    name,
    initialState: <EntitiesPage | null>null,
    reducers: {
      entitiesPageFetched: (state, action: PayloadAction<EntitiesPageFetchedPayload>) =>
        action.payload.entityType === entityType ? action.payload.page : state,
      pagedEntityAdded: (state, action: PayloadAction<PagedEntityAddedPayload>) => {
        if (action.payload.entityType === entityType && state) {
          state.entities.unshift(action.payload.entity);
          state.total += 1;
        }
      },
      pagedEntitySaved: (state, action: PayloadAction<PagedEntitySavedPayload>) => {
        const { payload } = action;
        if (payload.entityType === entityType && state) {
          const { entity } = payload;
          state.entities = state.entities.map((i) =>
            i.isSameEntity(entity) ? entity : i,
          );
        }
      },
      pagedEntityDeleted: (state, action: PayloadAction<PagedEntityDeletedPayload>) => {
        const { payload } = action;
        if (payload.entityType === entityType && state) {
          state.entities = state.entities.filter((i) => !i.hasSameKey(payload.key));
        }
      },
    },
  });
}

export const propertiesPageSlice = createEntitiesPageSlice(
  "propertiesPage",
  PropertyType,
);
export const clientsPageSlice = createEntitiesPageSlice("clientsPage", ClientType);
export const contactsPageSlice = createEntitiesPageSlice("contactsPage", ContactType);

export function getEntitiesPageSlice(entityType: EntityType) {
  switch (entityType) {
    case ClientType:
      return clientsPageSlice;
    case PropertyType:
      return propertiesPageSlice;
    case ContactType:
      return contactsPageSlice;
    default:
      throw "Invalid entity-type given";
  }
}
