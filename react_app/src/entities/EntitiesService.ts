import * as superagent from "superagent";
import { auth } from "../logins/Auth";
import { onError } from "../util/errorDialog";
import { ContactType, EntityType, PropertyType } from "../entity/EntityType";
import { EntitiesPage } from "./EntitiesPage";
import { NamedEntity } from "@/domain/NamedEntity";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { store } from "../store/Store";
import { getEntitiesSlice } from "./createEntitiesSlice";
import { Property } from "@/domain/Property";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { Contact } from "@/domain/Contact";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Id } from "@/domain/Id";
import { getEntitiesPageSlice } from "./createEntitiesPageSlice";

class EntitiesService {
  async fetchEntities(
    entityType: EntityType,
    hydrate: (dto: NamedEntityDto) => NamedEntity = (dto) => NamedEntity.fromDto(dto),
  ): Promise<NamedEntity[]> {
    const { pluralName } = entityType;
    try {
      const res = await superagent.get(`/api/${pluralName}`).set(auth.header);
      return res.body.map(hydrate);
    } catch (err) {
      onError(err, `Could not retrieve ${pluralName}`);
      return [];
    }
  }

  async fetchEntitiesPage(
    entityType: EntityType,
    page: number,
    firstLetter: string,
    nameSearchText: string,
    otherSearchOptions: object = {},
    hydrate: (dto: NamedEntityDto) => NamedEntity = (dto) => NamedEntity.fromDto(dto),
  ): Promise<EntitiesPage> {
    try {
      const res = await superagent
        .get(`/api/${entityType.pluralName}/${page}`)
        .query({ firstLetter, nameSearchText, ...otherSearchOptions })
        .set(auth.header);
      return {
        entities: res.body.entities.map(hydrate),
        total: res.body.total,
      };
    } catch (err) {
      onError(err, `Could not fetch next page of ${entityType.pluralName}`);
      throw err;
    }
  }

  async fetchEntity(
    entityType: EntityType,
    id: Id,
    hydrate: (dto: NamedEntityDto) => NamedEntity = (dto) => NamedEntity.fromDto(dto),
  ): Promise<NamedEntity> {
    try {
      const res = await superagent.get(`/api/${entityType.name}/${id}`).set(auth.header);
      return hydrate(res.body);
    } catch (err) {
      onError(err, `Could not retrieve ${entityType.name}`);
      throw err;
    }
  }

  async addEntity(
    entityType: EntityType,
    hydrate?: (dto: NamedEntityDto) => NamedEntity,
  ): Promise<NamedEntity> {
    try {
      const res = await superagent.post(`/api/${entityType.name}`).set(auth.header);
      const dto = res.body as NamedEntityDto;
      return hydrate?.(dto) || NamedEntity.fromDto(dto);
    } catch (err) {
      onError(err, `Could not add to ${entityType.pluralName}`);
      throw err;
    }
  }

  async updateEntity(
    entity: NamedEntity,
    entityType: EntityType,
    isForPagedEntity: boolean,
  ): Promise<void> {
    try {
      await superagent
        .put(`/api/${entityType.name}`)
        .set(auth.header)
        .send(entity.renderDto());
    } catch (err) {
      onError(err, `Could not save to ${entityType.pluralName}`);
      throw err;
    }
    store.dispatch(
      isForPagedEntity
        ? getEntitiesPageSlice(entityType).actions.pagedEntitySaved({
            entityType,
            entity,
          })
        : getEntitiesSlice(entityType).actions.entitySaved({ entityType, entity }),
    );
  }

  async deleteEntity(
    entity: NamedEntity,
    entityType: EntityType,
    isForPagedEntity: boolean,
  ): Promise<void> {
    await superagent
      .delete(`/api/${entityType.name}/${entity.renderKeyString()}`)
      .set(auth.header)
      .catch((err) => {
        onError(err, `Could not delete from ${entityType.pluralName}`);
        throw err;
      });
    const key = entity.renderKeyString();
    store.dispatch(
      isForPagedEntity
        ? getEntitiesPageSlice(entityType).actions.pagedEntityDeleted({ entityType, key })
        : getEntitiesSlice(entityType).actions.entityDeleted({ entityType, key }),
    );
  }
}

export const entitiesService = new EntitiesService();

export function hydratorForPagedEntity(
  entityType: EntityType,
  clients: NamedEntity[],
  properties: NamedEntity[],
) {
  return entityType === PropertyType
    ? (dto: NamedEntityDto) => Property.fromDto(dto as PropertyDto)
    : entityType === ContactType
      ? (dto: NamedEntityDto) =>
          Contact.fromContactDto(dto as ContactDto, clients, properties)
      : undefined;
}
