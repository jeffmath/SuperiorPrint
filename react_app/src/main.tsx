import { createRoot } from "react-dom/client";
import { router } from "./router";
import "./tailwind/output.css";

createRoot(document.getElementById("react-container")!).render(router);
