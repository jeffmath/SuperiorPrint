import * as React from "react";
import { useDispatch } from "react-redux";
import { auth } from "./logins/Auth";
import { Link, useLocation } from "react-router-dom";
import Logo from "../public/images/logo.jpg";
import { AppDispatch } from "./store/Store";
import { userLoggedOut } from "./logins/loginSlice";
import {
  NavigationMenu,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

export function Navigation() {
  const dispatch = useDispatch();
  const location = useLocation();

  function NavLink({ path, text }: { path: string; text: string }) {
    return (
      <NavigationMenuItem>
        <NavigationMenuLink
          asChild={true}
          active={location.pathname.endsWith(path)}
          className={navigationMenuTriggerStyle()}
        >
          <Link to={path}>{text}</Link>
        </NavigationMenuLink>
      </NavigationMenuItem>
    );
  }

  function OtherLink({ path, text }: { path: string; text: string }) {
    return (
      <Link to={path}>
        <DropdownMenuItem>{text}</DropdownMenuItem>
      </Link>
    );
  }

  return (
    <NavigationMenu className="2xl:container justify-start">
      <div className="hidden sm:flex sm:flex-row">
        <img src={Logo} className="max-h-8" alt="Superior Print and Exhibit" />
        <span className="ml-4 text-lg">Management</span>
      </div>
      <NavigationMenuList className="ml-4">
        <NavLink path="jobs" text="Jobs" />
        <NavLink path="properties" text="Properties" />
        <NavLink path="clients" text="Clients" />
        <NavLink path="contacts" text="Contacts" />
        <NavigationMenuItem>
          <DropdownMenu>
            <DropdownMenuTrigger className={navigationMenuTriggerStyle()}>
              Other
            </DropdownMenuTrigger>
            <DropdownMenuContent>
              <OtherLink path="surfaces" text="Surfaces" />
              <OtherLink path="materials" text="Materials" />
              <OtherLink path="finishings" text="Finishings" />
              <OtherLink path="printers" text="Printers" />
              <OtherLink path="users" text="Users" />
              <OtherLink path="changePassword" text="Change password" />
              <DropdownMenuItem onClick={() => logOut(dispatch)}>
                Log out
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </NavigationMenuItem>
      </NavigationMenuList>
    </NavigationMenu>
  );
}

function logOut(dispatch: AppDispatch) {
  auth.logOut();
  dispatch(userLoggedOut());
}
