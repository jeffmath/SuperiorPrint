import { LineItem } from "@/domain/LineItem";
import { jobDeleted } from "./jobSlice";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";

export interface UpdateClientSignImagePayload {
  id: Id;
  i: number;
}

export const lineItemSlice = createSlice({
  name: "lineItem",
  initialState: <LineItem | null>null,
  reducers: {
    lineItemSelected: (_state, action: PayloadAction<LineItem | null>) => action.payload,
    nameEdited: (state, action: PayloadAction<string>) => {
      state!.onNameEdited(action.payload);
    },
    codeEdited: (state, action: PayloadAction<string>) => {
      state!.onCodeEdited(action.payload);
    },
    locationSelected: (state, action: PayloadAction<Id>) => {
      state!.onLocationSelected(action.payload);
    },
    widthEdited: (state, action: PayloadAction<number>) => {
      state!.onWidthEdited(action.payload);
    },
    heightEdited: (state, action: PayloadAction<number>) => {
      state!.onHeightEdited(action.payload);
    },
    surfaceSelected: (state, action: PayloadAction<Id>) => {
      state!.onSurfaceSelected(action.payload);
    },
    materialSelected: (state, action: PayloadAction<Id>) => {
      state!.onMaterialSelected(action.payload);
    },
    finishingSelected: (state, action: PayloadAction<Id>) => {
      state!.onFinishingSelected(action.payload);
    },
    quantityEdited: (state, action: PayloadAction<number | undefined>) => {
      state!.onQuantityEdited(action.payload);
    },
    priceEachEdited: (state, action: PayloadAction<number | undefined>) => {
      state!.onPriceEachEdited(action.payload);
    },
    printerSelected: (state, action: PayloadAction<Id>) => {
      state!.onPrinterSelected(action.payload);
    },
    clientSignImageSelected: (
      state,
      action: PayloadAction<UpdateClientSignImagePayload>,
    ) => {
      const { id, i } = action.payload;
      state!.onClientSignImageSelected(id, i);
    },
    artworkReceivedDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onArtworkReceivedDateEdited(action.payload);
    },
    artworkApprovedDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onArtworkApprovedDateEdited(action.payload);
    },
    shipDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onShipDateEdited(action.payload);
    },
    inHandDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onInHandDateEdited(action.payload);
    },
    installDateTimeEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onInstallDateTimeEdited(action.payload);
    },
    strikeDateTimeEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onStrikeDateEdited(action.payload);
    },
    installNotesEdited: (state, action: PayloadAction<string>) => {
      state!.onInstallNotesEdited(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(jobDeleted, () => null);
  },
});

export const {
  lineItemSelected,
  nameEdited,
  codeEdited,
  locationSelected,
  widthEdited,
  heightEdited,
  surfaceSelected,
  materialSelected,
  finishingSelected,
  quantityEdited,
  priceEachEdited,
  printerSelected,
  clientSignImageSelected,
  artworkReceivedDateEdited,
  artworkApprovedDateEdited,
  shipDateEdited,
  inHandDateEdited,
  installDateTimeEdited,
  strikeDateTimeEdited,
  installNotesEdited,
} = lineItemSlice.actions;

export default lineItemSlice.reducer;
