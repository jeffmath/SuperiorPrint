import { Job } from "@/domain/Job";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";
import { LineItem } from "@/domain/LineItem";

export const jobSlice = createSlice({
  name: "job",
  initialState: <Job | null>null,
  reducers: {
    jobSupplied: (_state, action: PayloadAction<Job>) => action.payload,
    jobDeleted: (_state, _action: PayloadAction<Id>) => null,
    numberEdited: (state, action: PayloadAction<string>) => {
      state!.onNumberEdited(action.payload);
    },
    clientContactSelected: (state, action: PayloadAction<Id>) => {
      state!.onClientContactSelected(action.payload);
    },
    propertyContactSelected: (state, action: PayloadAction<Id>) => {
      state!.onPropertyContactSelected(action.payload);
    },
    installContactSelected: (state, action: PayloadAction<Id>) => {
      state!.onInstallContactSelected(action.payload);
    },
    superiorContact1Selected: (state, action: PayloadAction<Id>) => {
      state!.onSuperiorContact1Selected(action.payload);
    },
    superiorContact2Selected: (state, action: PayloadAction<Id>) => {
      state!.onSuperiorContact2Selected(action.payload);
    },
    artworkReceivedDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onArtworkReceivedDateEdited(action.payload);
    },
    artworkApprovedDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onArtworkApprovedDateEdited(action.payload);
    },
    shipDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onShipDateEdited(action.payload);
    },
    inHandDateEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onInHandDateEdited(action.payload);
    },
    installDateTimeEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onInstallDateTimeEdited(action.payload);
    },
    strikeDateTimeEdited: (state, action: PayloadAction<Date | undefined>) => {
      state!.onStrikeDateTimeEdited(action.payload);
    },
    lineItemAdded: (state, action: PayloadAction<LineItem>) => {
      state!.onLineItemAdded(action.payload);
    },
    lineItemUpdated: (state, action: PayloadAction<LineItem>) => {
      state!.onLineItemUpdated(action.payload);
    },
    lineItemDeleted: (state, action: PayloadAction<Id>) => {
      state!.onLineItemDeleted(action.payload);
    },
  },
});

export const {
  jobSupplied,
  jobDeleted,
  numberEdited,
  clientContactSelected,
  propertyContactSelected,
  installContactSelected,
  superiorContact1Selected,
  superiorContact2Selected,
  artworkReceivedDateEdited,
  artworkApprovedDateEdited,
  shipDateEdited,
  inHandDateEdited,
  installDateTimeEdited,
  strikeDateTimeEdited,
  lineItemAdded,
  lineItemUpdated,
  lineItemDeleted,
} = jobSlice.actions;

export default jobSlice.reducer;
