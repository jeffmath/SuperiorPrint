import * as React from "react";
import { ContactSelect } from "../widgets/ContactSelect";
import { JobDatesPanel } from "./JobDatesPanel";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { jobService } from "./JobService";
import { jobUpdated } from "./jobDetailsSlice";
import { yesNoDialog } from "../util/yesNoDialog";
import {
  artworkApprovedDateEdited,
  artworkReceivedDateEdited,
  clientContactSelected,
  inHandDateEdited,
  installContactSelected,
  installDateTimeEdited,
  jobDeleted,
  numberEdited,
  propertyContactSelected,
  shipDateEdited,
  strikeDateTimeEdited,
  superiorContact1Selected,
  superiorContact2Selected,
} from "./jobSlice";
import { Id } from "@/domain/Id";
import { selectJobClientContacts, selectJobPropertyContacts } from "./jobContactsSlice";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";

export function JobDetails() {
  const dispatch = useAppDispatch();
  const job = useAppSelector((state) => state.job!);
  const client = useAppSelector((state) => state.jobClient!);
  const property = useAppSelector((state) => state.jobProperty!);
  const clientContacts = useAppSelector(selectJobClientContacts);
  const propertyContacts = useAppSelector(selectJobPropertyContacts);
  const installContacts = useAppSelector((state) => state.installers);
  const superiorContacts = useAppSelector((state) => state.superiorContacts);
  const isDirty = useAppSelector((state) => state.jobDetailsState.isDirty);
  const clientLinkParams = client?.renderRouteLinkParams();

  function handleSaveClick() {
    jobService.updateJob(job!).then(() => dispatch(jobUpdated()));
  }

  function handleDeleteClick() {
    yesNoDialog(
      "Are you sure you want to delete this job, and all its line items?",
      (yes) =>
        yes &&
        jobService
          .deleteJob(job!)
          .then(() => dispatch(jobDeleted(job!.renderKeyString()))),
    );
  }

  function handleNumberChange(newValue: string) {
    dispatch(numberEdited(newValue));
  }

  function handleClientContactSelection(id: Id) {
    dispatch(clientContactSelected(id));
  }

  function handlePropertyContactSelection(id: Id) {
    dispatch(propertyContactSelected(id));
  }

  function handleInstallContactSelection(id: Id) {
    dispatch(installContactSelected(id));
  }

  function handleSuperiorContact1Selection(id: Id) {
    dispatch(superiorContact1Selected(id));
  }

  function handleSuperiorContact2Selection(id: Id) {
    dispatch(superiorContact2Selected(id));
  }

  function handleArtworkReceivedDateChange(newValue: Date | undefined) {
    dispatch(artworkReceivedDateEdited(newValue));
  }

  function handleArtworkApprovedDateChange(newValue: Date | undefined) {
    dispatch(artworkApprovedDateEdited(newValue));
  }

  function handleShipDateChange(newValue: Date | undefined) {
    dispatch(shipDateEdited(newValue));
  }

  function handleInHandDateChange(newValue: Date | undefined) {
    dispatch(inHandDateEdited(newValue));
  }

  function handleInstallDateTimeChange(newValue: Date | undefined) {
    dispatch(installDateTimeEdited(newValue));
  }

  function handleStrikeDateTimeChange(newValue: Date | undefined) {
    dispatch(strikeDateTimeEdited(newValue));
  }

  if (!job || !property || !client) return null;
  return (
    <div className="mt-8 flex flex-col space-y-4">
      <div className="flex flex-row space-x-4 items-center">
        <h4 className="form-header">Job details</h4>
        <Button disabled={!isDirty} onClick={handleSaveClick}>
          Save
        </Button>
        <Button variant="destructive" onClick={handleDeleteClick}>
          Delete
        </Button>
      </div>
      <div className="text-lg">
        <span className="font-bold">Client:</span>&nbsp;
        <Link to={`/client/${clientLinkParams.toId}`} className="hover:underline">
          {clientLinkParams.text}
        </Link>
        <span className="font-bold ml-7">Property:</span>&nbsp;
        <Link to={`/property/${property.renderKeyString()}`} className="hover:underline">
          {property.renderNameString()}
        </Link>
      </div>
      <div>
        <div className="flex flex-row flex-wrap space-x-6 items-baseline">
          <div>
            <Label htmlFor="jobNumberField">Number</Label>
            <Input
              value={job.renderNumberString()}
              id="jobNumberField"
              className="min-w-24"
              onChange={(e) => handleNumberChange(e.currentTarget.value)}
            />
          </div>
          <ContactSelect
            labelText="Client contact"
            contacts={clientContacts}
            selectedContactId={job.renderClientContactKeyString()}
            onContactSelect={handleClientContactSelection}
          />
          <ContactSelect
            labelText="Property contact"
            contacts={propertyContacts}
            selectedContactId={job.renderPropertyContactKeyString()}
            onContactSelect={handlePropertyContactSelection}
          />
          <ContactSelect
            labelText="Install contact"
            contacts={installContacts}
            selectedContactId={job.renderInstallContactKeyString()}
            onContactSelect={handleInstallContactSelection}
          />
          <ContactSelect
            labelText="Superior P&E contact 1"
            contacts={superiorContacts}
            selectedContactId={job.renderSuperiorPrintContact1KeyString()}
            onContactSelect={handleSuperiorContact1Selection}
          />
          <ContactSelect
            labelText="Superior P&E contact 2"
            contacts={superiorContacts}
            selectedContactId={job.renderSuperiorPrintContact2KeyString()}
            onContactSelect={handleSuperiorContact2Selection}
          />
        </div>
        <h5 className="font-bold">Default dates/times</h5>
        <JobDatesPanel
          jobDates={job.renderJobDates()}
          onArtworkReceivedDateChange={handleArtworkReceivedDateChange}
          onArtworkApprovedDateChange={handleArtworkApprovedDateChange}
          onShipDateChange={handleShipDateChange}
          onInHandDateChange={handleInHandDateChange}
          onInstallDateTimeChange={handleInstallDateTimeChange}
          onStrikeDateTimeChange={handleStrikeDateTimeChange}
        />
      </div>
    </div>
  );
}
