import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";

export interface JobCreatorState {
  selectedPropertyId: Id | null;
}

const initialState: JobCreatorState = {
  selectedPropertyId: null,
};

export const jobCreatorSlice = createSlice({
  name: "jobCreator",
  initialState,
  reducers: {
    propertySelected: (state, action: PayloadAction<Id | null>) => {
      state.selectedPropertyId = action.payload;
    },
  },
});

export const { propertySelected } = jobCreatorSlice.actions;

export default jobCreatorSlice.reducer;
