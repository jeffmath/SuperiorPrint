import { Contact } from "@/domain/Contact";
import { contactsSlice } from "../entities/createEntitiesSlice";
import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { State } from "../store/State";

export const jobContactsSlice = createSlice({
  name: "jobContacts",
  initialState: <Contact[]>[],
  reducers: {
    jobContactsSupplied: (_state, action: PayloadAction<Contact[]>) => action.payload,
  },
  extraReducers: (builder) => {
    builder.addCase(contactsSlice.actions.entitySaved, (state, action) => {
      const { entity } = action.payload;
      return state.map((contact) =>
        contact.isSameEntity(entity) ? <Contact>entity : contact,
      );
    });
  },
});

export const { jobContactsSupplied } = jobContactsSlice.actions;

export default jobContactsSlice.reducer;

const selectJobContacts = (state: State) => state.jobContacts;
const selectJobClient = (state: State) => state.jobClient;
const selectJobProperty = (state: State) => state.jobProperty;

export const selectJobClientContacts = createSelector(
  [selectJobContacts, selectJobClient],
  (contacts, jobClient) => contacts.filter((c) => c.isForClient(jobClient!)),
);

export const selectJobPropertyContacts = createSelector(
  [selectJobContacts, selectJobProperty],
  (contacts, jobProperty) => contacts.filter((c) => c.isForProperty(jobProperty!)),
);
