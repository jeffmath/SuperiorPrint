import * as React from "react";
import { ImageModal } from "../widgets/ImageModal";
import { JobDatesPanel } from "./JobDatesPanel";
import { parseFloatOrNothing, parseIntOrNothing } from "../util/parsers";
import { InputField } from "../widgets/InputField";
import { SelectField } from "../widgets/SelectField";
import { ImageUploadModal } from "../widgets/ImageUploadModal";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { yesNoDialog } from "../util/yesNoDialog";
import { jobService } from "./JobService";
import {
  clientSignImageSelectedForViewing,
  imageModalClosed,
  imageModalOpened,
  imageUploadModalClosed,
  imageUploadModalOpened,
  unsavedLineItemSaved,
} from "./lineItemDetailsSlice";
import { lineItemDeleted, lineItemUpdated } from "./jobSlice";
import {
  lineItemSelected,
  artworkApprovedDateEdited,
  artworkReceivedDateEdited,
  clientSignImageSelected,
  codeEdited,
  finishingSelected,
  heightEdited,
  inHandDateEdited,
  installDateTimeEdited,
  installNotesEdited,
  locationSelected,
  materialSelected,
  nameEdited,
  priceEachEdited,
  printerSelected,
  quantityEdited,
  shipDateEdited,
  strikeDateTimeEdited,
  surfaceSelected,
  widthEdited,
} from "./lineItemSlice";
import { clientSignImageService } from "../clients/ClientSignImageService";
import { imagesFetched } from "./jobClientSignImagesSlice";
import { useEffect } from "react";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";

export function LineItemDetails() {
  const dispatch = useAppDispatch();
  const job = useAppSelector((state) => state.job!);
  const property = useAppSelector((state) => state.jobProperty!);
  const clientSignImages = useAppSelector((state) => state.jobClientSignImages);
  const lineItem = useAppSelector((state) => state.lineItem!);
  const surfaces = useAppSelector((state) => state.surfaces);
  const materials = useAppSelector((state) => state.materials);
  const finishings = useAppSelector((state) => state.finishings);
  const printers = useAppSelector((state) => state.printers);
  const {
    isDirty,
    clientSignImageToView,
    isImageModalOpen,
    isImageUploadModalOpen,
    unsavedLineItem,
  } = useAppSelector((state) => state.lineItemDetailsState);
  const clientSignImageKeys = lineItem.renderClientSignImageKeyStrings();

  useEffect(() => {
    if (unsavedLineItem) {
      yesNoDialog("Save changes to job line item?", (answer) => {
        if (answer)
          jobService.updateLineItem(job, unsavedLineItem).then(() => {
            dispatch(lineItemUpdated(unsavedLineItem));
            dispatch(unsavedLineItemSaved());
          });
      });
    }
  }, [unsavedLineItem]);

  function handleSaveClick() {
    jobService.updateLineItem(job, lineItem).then(() => {
      dispatch(lineItemUpdated(lineItem));
    });
  }

  function handleDeleteClick() {
    yesNoDialog("Are you sure you want to delete this line item?", (yes) => {
      yes &&
        jobService
          .deleteLineItem(job.renderKeyString(), lineItem.renderKeyString())
          .then(() => {
            dispatch(lineItemDeleted(lineItem.renderKeyString()));
            dispatch(lineItemSelected(null));
          });
    });
  }

  function handleViewSignImageClick(key: string) {
    dispatch(
      clientSignImageSelectedForViewing(
        clientSignImages.find((c) => c.renderKeyString() === key)!,
      ),
    );
    dispatch(imageModalOpened());
  }

  async function handleClientSignImagesUpload() {
    const images = await clientSignImageService.fetchImages(job.renderClientKeyString());
    dispatch(imagesFetched(images));
  }

  const navigate = useNavigate();
  if (!lineItem) return null;
  const code = lineItem.renderCodeString();
  return (
    <div className="mt-8 flex flex-col space-y-4">
      <div className="flex flex-row space-x-4 items-center">
        <h4 className="form-header">
          {(lineItem.renderNameString() || ``) + (code ? ` (${code})` : "")}
        </h4>
        <Button onClick={handleSaveClick} disabled={!isDirty}>
          Save
        </Button>
        <Button variant="secondary" onClick={() => navigate("/workOrder")}>
          View work order
        </Button>
        <Button variant="secondary" onClick={() => navigate("/deck")}>
          View deck
        </Button>
        <Button variant="destructive" onClick={handleDeleteClick}>
          Delete
        </Button>
      </div>
      <div className="flex flex-row space-x-4 items-center">
        <InputField
          label="Name"
          id="nameField"
          className="w-72"
          value={lineItem.renderNameString()}
          onChange={(value) => dispatch(nameEdited(value))}
        />
        <InputField
          label="Code"
          id="codeField"
          className="w-24"
          value={lineItem.renderCodeString()}
          onChange={(value) => dispatch(codeEdited(value))}
        />
        <SelectField
          label="Location"
          className="min-w-24"
          id="locationSelect"
          options={property.renderLocationsOptionParams()}
          value={lineItem.renderLocationKeyString()}
          onChange={(value) => dispatch(locationSelected(value))}
        />
      </div>
      <div className="flex flex-row space-x-4 items-center">
        <InputField
          label="Width"
          id="widthField"
          className="w-20"
          value={lineItem.renderWidthString()}
          onChange={(value) => dispatch(widthEdited(parseIntOrNothing(value)))}
        />
        <InputField
          label="Height"
          id="heightField"
          className="w-20"
          value={lineItem.renderHeightString()}
          onChange={(value) => dispatch(heightEdited(parseIntOrNothing(value)))}
        />
        <SelectField
          label="Surface"
          id="surfaceSelect"
          options={surfaces.map((surface) => surface.renderOptionParams())}
          value={lineItem.renderSurfaceKeyString()}
          onChange={(value) => dispatch(surfaceSelected(value))}
        />
        <SelectField
          label="Material"
          id="materialSelect"
          options={materials.map((material) => material.renderOptionParams())}
          value={lineItem.renderMaterialKeyString()}
          onChange={(value) => dispatch(materialSelected(value))}
        />
        <SelectField
          label="Finishing"
          id="finishingSelect"
          options={finishings.map((finishing) => finishing.renderOptionParams())}
          value={lineItem.renderFinishingKeyString()}
          onChange={(value) => dispatch(finishingSelected(value))}
        />
      </div>
      <div className="flex flex-row space-x-4 items-center-inline">
        <InputField
          label="Quantity"
          id="quantityField"
          className="w-20"
          value={lineItem.renderQuantityString()}
          onChange={(value) => dispatch(quantityEdited(parseIntOrNothing(value)))}
        />
        <InputField
          label="Price each"
          id="priceEach"
          className="w-24 text-right"
          value={lineItem.renderPriceEachString()}
          onChange={(value) => dispatch(priceEachEdited(parseFloatOrNothing(value)))}
        />
        <div className="text-right grid">
          <label
            className="font-semibold text-sm relative top-[3px]"
            htmlFor="totalPrice"
          >
            Total price
          </label>
          <span className="relative top-0.5" id="totalPrice">
            {lineItem.renderTotalPriceString()}
          </span>
        </div>
        <SelectField
          label="Printer"
          id="printerSelect"
          options={printers.map((printer) => printer.renderOptionParams())}
          value={lineItem.renderPrinterKeyString()}
          onChange={(value) => dispatch(printerSelected(value))}
        />
      </div>
      <div className="flex flex-row flex-wrap space-x-4 items-center">
        {clientSignImageKeys.map((key, i) => (
          <SelectField
            key={i}
            label={`Sign ${i + 1}`}
            id={`sign${i + 1}`}
            className="min-w-24"
            options={clientSignImages.map((image) => image.renderOptionParams())}
            value={key || ""}
            suppliedButton={
              key && (
                <Button
                  variant="secondary"
                  size="xs"
                  className="ml-1 top-2"
                  onClick={() => handleViewSignImageClick(key)}
                >
                  View
                </Button>
              )
            }
            onChange={(value) => dispatch(clientSignImageSelected({ id: value, i }))}
          />
        ))}
        {clientSignImageToView && (
          <ImageModal
            isOpen={isImageModalOpen}
            imageName={clientSignImageToView.renderNameString()}
            imageUrl={"/api/signImage/" + clientSignImageToView.renderKeyString()}
            onClose={() => dispatch(imageModalClosed())}
          />
        )}
      </div>
      <div>
        <Button
          variant="secondary"
          size="xs"
          onClick={() => dispatch(imageUploadModalOpened())}
        >
          Upload sign images
        </Button>
        <ImageUploadModal
          isOpen={isImageUploadModalOpen}
          isForMultiple={true}
          uploadUrl={"/api/signImages/" + job.renderClientKeyString()}
          onUploaded={handleClientSignImagesUpload}
          onRequestClose={() => dispatch(imageUploadModalClosed())}
        />
      </div>
      <div>
        <h5 className="nb-1 font-bold">Override dates/times</h5>
        <JobDatesPanel
          jobDates={lineItem.renderJobDates()}
          onArtworkReceivedDateChange={(newValue) =>
            dispatch(artworkReceivedDateEdited(newValue))
          }
          onArtworkApprovedDateChange={(newValue) =>
            dispatch(artworkApprovedDateEdited(newValue))
          }
          onShipDateChange={(newValue) => dispatch(shipDateEdited(newValue))}
          onInHandDateChange={(newValue) => dispatch(inHandDateEdited(newValue))}
          onInstallDateTimeChange={(newValue) =>
            dispatch(installDateTimeEdited(newValue))
          }
          onStrikeDateTimeChange={(newValue) => dispatch(strikeDateTimeEdited(newValue))}
        />
      </div>
      <div>
        <Label htmlFor="installNotes">Installation notes</Label>
        <Textarea
          id="installNotes"
          value={lineItem.renderInstallNotesString()}
          className="form-control !w-full"
          rows={4}
          onChange={(e) => dispatch(installNotesEdited(e.currentTarget.value))}
        ></Textarea>
      </div>
    </div>
  );
}
