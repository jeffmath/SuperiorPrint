import * as React from "react";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import {
  jobsFetched,
  propertiesFetched,
  clientSelected,
  jobSelected,
  propertySelected,
} from "./jobSelectorSlice";
import { propertyService } from "../properties/PropertyService";
import { jobService } from "./JobService";
import { jobClientSupplied } from "./jobClientSlice";
import { jobSupplied } from "./jobSlice";
import { jobPropertySupplied } from "./jobPropertySlice";
import { jobContactsSupplied } from "./jobContactsSlice";
import { lineItemSelected } from "./lineItemSlice";
import { clientSignImageService } from "../clients/ClientSignImageService";
import { imagesFetched } from "./jobClientSignImagesSlice";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

export function JobSelector() {
  const dispatch = useAppDispatch();
  const clients = useAppSelector((state) => state.allClientStubs);
  const { selectedClientId, properties, selectedPropertyId, jobs, selectedJobId } =
    useAppSelector((state) => state.jobSelectorState);

  function handleClientSelection(id: Id) {
    dispatch(clientSelected(id));
    dispatch(propertySelected(null));
    if (id)
      propertyService
        .fetchPropertiesForClient(id)
        .then((properties) => dispatch(propertiesFetched(properties)));
  }

  function handlePropertySelection(propertyId: Id) {
    dispatch(propertySelected(propertyId));
    if (selectedClientId && propertyId)
      jobService
        .fetchJobs(selectedClientId, propertyId)
        .then((jobs) => dispatch(jobsFetched(jobs)));
  }

  function handleJobSelection(id: Id) {
    dispatch(jobSelected(id));
    if (id)
      jobService.fetchJob(id, clients, properties).then((result) => {
        dispatch(jobSupplied(result.job));
        dispatch(jobClientSupplied(result.client));
        dispatch(jobPropertySupplied(result.property));
        dispatch(jobContactsSupplied(result.contacts));
        dispatch(lineItemSelected(null));
        clientSignImageService
          .fetchImages(result.job.renderClientKeyString())
          .then((images) => dispatch(imagesFetched(images)));
      });
  }

  return (
    <div>
      <h4 className="form-header">Select a job</h4>
      <div className="mt-2 flex flex-row space-x-4 items-center">
        <div>
          <Label htmlFor="clientSelect">Client</Label>
          <Select value={selectedClientId || ""} onValueChange={handleClientSelection}>
            <SelectTrigger id="clientSelect" className="min-w-36">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {clients.map((client, i) => {
                const params = client.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
        </div>
        {properties.length > 0 && (
          <div>
            <Label htmlFor="propertySelect">Property</Label>
            <Select
              value={selectedPropertyId || ""}
              onValueChange={handlePropertySelection}
            >
              <SelectTrigger id="propertySelect" className="min-w-36">
                <SelectValue />
              </SelectTrigger>
              <SelectContent>
                {properties.map((property, i) => {
                  const params = property.renderOptionParams();
                  return (
                    <SelectItem key={i} value={params.value}>
                      {params.label}
                    </SelectItem>
                  );
                })}
              </SelectContent>
            </Select>
          </div>
        )}
        {jobs.length > 0 && (
          <div>
            <Label htmlFor="jobSelect">Job</Label>
            <Select value={selectedJobId || ""} onValueChange={handleJobSelection}>
              <SelectTrigger id="jobSelect" className="min-w-24">
                <SelectValue />
              </SelectTrigger>
              <SelectContent>
                {jobs.map((job, i) => {
                  return (
                    <SelectItem key={i} value={job.id}>
                      {job.number}
                    </SelectItem>
                  );
                })}
              </SelectContent>
            </Select>
          </div>
        )}
      </div>
    </div>
  );
}
