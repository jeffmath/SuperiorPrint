import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Location } from "@/domain/Location";

export interface LineItemsState {
  imageLocation?: Location;
  isImageModalOpen: boolean;
  isImageUploadModalOpen: boolean;
}

const initialState: LineItemsState = {
  isImageModalOpen: false,
  isImageUploadModalOpen: false,
};

export const lineItemsSlice = createSlice({
  name: "lineItems",
  initialState,
  reducers: {
    viewOrUploadImageLocationSelected: (state, action: PayloadAction<Location>) => {
      state.imageLocation = action.payload;
    },
    imageModalOpened: (state) => {
      state.isImageModalOpen = true;
    },
    imageModalClosed: (state) => {
      state.isImageModalOpen = false;
    },
    imageUploadModalOpened: (state) => {
      state.isImageUploadModalOpen = true;
    },
    imageUploadModalClosed: (state) => {
      state.isImageUploadModalOpen = false;
    },
  },
});

export const {
  viewOrUploadImageLocationSelected,
  imageModalOpened,
  imageModalClosed,
  imageUploadModalOpened,
  imageUploadModalClosed,
} = lineItemsSlice.actions;

export default lineItemsSlice.reducer;
