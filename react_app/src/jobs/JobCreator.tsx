import * as React from "react";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { Id } from "@/domain/Id";
import { jobService } from "./JobService";
import { jobSupplied } from "./jobSlice";
import { jobClientSupplied } from "./jobClientSlice";
import { jobPropertySupplied } from "./jobPropertySlice";
import { jobContactsSupplied } from "./jobContactsSlice";
import { lineItemSelected } from "./lineItemSlice";
import { clientSignImageService } from "../clients/ClientSignImageService";
import { imagesFetched } from "./jobClientSignImagesSlice";
import { propertySelected } from "./jobCreatorSlice";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Button } from "@/components/ui/button";

export function JobCreator() {
  const dispatch = useAppDispatch();
  const selectedClientId = useAppSelector(
    (state) => state.jobSelectorState.selectedClientId,
  );
  const properties = useAppSelector((state) => state.allPropertyStubs);
  const selectedPropertyId = useAppSelector(
    (state) => state.jobCreatorState.selectedPropertyId,
  );

  function handlePropertySelection(newValue: Id) {
    dispatch(propertySelected(newValue));
  }

  function handleCreateJobClick() {
    jobService.createJob(selectedClientId!, selectedPropertyId!).then((result) => {
      dispatch(jobSupplied(result.job));
      dispatch(jobClientSupplied(result.client));
      dispatch(jobPropertySupplied(result.property));
      dispatch(jobContactsSupplied(result.contacts));
      dispatch(lineItemSelected(null));
      clientSignImageService
        .fetchImages(selectedClientId!)
        .then((images) => dispatch(imagesFetched(images)));
    });
  }

  if (!selectedClientId) return null;
  return (
    <div>
      <h4 className="form-header">Or, create a job</h4>
      <div className="mt-2 flex flex-row space-x-2 items-end">
        <div>
          <Label htmlFor="propertySelect">For property</Label>
          <Select
            value={selectedPropertyId || ""}
            onValueChange={handlePropertySelection}
          >
            <SelectTrigger id="propertySelect" className="min-w-36">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {properties.map((job, i) => {
                const params = job.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
        </div>
        <Button
          variant="secondary"
          className="ml-4"
          disabled={!selectedPropertyId}
          onClick={handleCreateJobClick}
        >
          Create
        </Button>
      </div>
    </div>
  );
}
