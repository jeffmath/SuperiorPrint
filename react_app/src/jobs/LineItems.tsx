import * as React from "react";
import { ImageModal } from "../widgets/ImageModal";
import { Property } from "@/domain/Property";
import { ImageUploadModal } from "../widgets/ImageUploadModal";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { lineItemSelected } from "./lineItemSlice";
import {
  imageModalClosed,
  imageModalOpened,
  imageUploadModalClosed,
  imageUploadModalOpened,
  viewOrUploadImageLocationSelected,
} from "./lineItemsSlice";
import { jobService } from "./JobService";
import { lineItemAdded } from "./jobSlice";
import { LineItem } from "@/domain/LineItem";
import { Button } from "@/components/ui/button";

export function LineItems() {
  const dispatch = useAppDispatch();
  const job = useAppSelector((state) => state.job!);
  const jobProperty = useAppSelector((state) => state.jobProperty!);
  const selectedLineItem = useAppSelector((state) => state.lineItem);
  const imageLocation = useAppSelector((state) => state.lineItemsState.imageLocation);
  const isImageModalOpen = useAppSelector(
    (state) => state.lineItemsState.isImageModalOpen,
  );
  const isImageUploadModalOpen = useAppSelector(
    (state) => state.lineItemsState.isImageUploadModalOpen,
  );

  function handleLineItemSelect(lineItem: LineItem) {
    dispatch(lineItemSelected(lineItem));
  }

  function handleViewLocationImageClick(lineItem: LineItem, property: Property) {
    dispatch(
      viewOrUploadImageLocationSelected(lineItem.findLocationInProperty(property)!),
    );
    dispatch(imageModalOpened());
  }

  function handleImageModalClose() {
    dispatch(imageModalClosed());
  }

  function handleUploadLocationImageClick(lineItem: LineItem, property: Property) {
    dispatch(
      viewOrUploadImageLocationSelected(lineItem.findLocationInProperty(property)!),
    );
    dispatch(imageUploadModalOpened());
  }

  function handleImageUploadModalClose() {
    dispatch(imageUploadModalClosed());
  }

  function handleAddLineItemClick() {
    jobService.addLineItem(job!).then((lineItem) => dispatch(lineItemAdded(lineItem)));
  }

  if (!job || !jobProperty) return null;
  return (
    <div className="mt-8">
      <h4 className="form-header">Job line items</h4>
      <table className="table table-hover text-left w-full mt-2">
        <thead>
          <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Location</th>
            <th className="text-right">Price</th>
            <th className="text-center">Image</th>
          </tr>
        </thead>
        <tbody>
          {job.mapLineItems((lineItem, i) => {
            const location = lineItem.findLocationInProperty(jobProperty);
            return (
              <tr
                key={i}
                className={
                  selectedLineItem && lineItem.isSameEntity(selectedLineItem)
                    ? "active"
                    : ""
                }
                onClick={() => handleLineItemSelect(lineItem)}
              >
                <td>{lineItem.renderNameString()}</td>
                <td>{lineItem.renderCodeString()}</td>
                <td>{location && location.renderNameString()}</td>
                <td className="text-right">{lineItem.renderTotalPriceString()}</td>
                <td className="text-center">
                  {location && location.renderImageKeyString() && (
                    <Button
                      size="xs"
                      onClick={(e) => {
                        e.stopPropagation();
                        handleViewLocationImageClick(lineItem, jobProperty);
                      }}
                    >
                      View
                    </Button>
                  )}
                  {location && !location.renderImageKeyString() && (
                    <Button
                      variant="secondary"
                      size="xs"
                      onClick={(e) => {
                        e.stopPropagation();
                        handleUploadLocationImageClick(lineItem, jobProperty);
                      }}
                    >
                      Upload
                    </Button>
                  )}
                </td>
              </tr>
            );
          })}
        </tbody>
        <tfoot>
          <tr>
            <td>
              <Button variant="secondary" onClick={handleAddLineItemClick}>
                Add line item
              </Button>
            </td>
            <td />
            <td className="text-right">Total Price:</td>
            <td className="text-right">{job.renderTotalPriceString()}</td>
            <td />
          </tr>
        </tfoot>
      </table>
      {imageLocation && (
        <div>
          <ImageModal
            isOpen={isImageModalOpen}
            imageName={imageLocation.renderNameString()}
            imageUrl={"/api/locationImage/" + imageLocation.renderImageKeyString()}
            onClose={handleImageModalClose}
          />
          <ImageUploadModal
            isOpen={isImageUploadModalOpen}
            isForMultiple={false}
            uploadUrl={
              "/api/locationImage/" +
              job.renderPropertyKeyString() +
              "/" +
              imageLocation.renderKeyString()
            }
            onUploaded={(imageId) => imageLocation?.onImageIdAssigned(imageId)}
            onRequestClose={handleImageUploadModalClose}
          />
        </div>
      )}
    </div>
  );
}
