import * as React from "react";
import { DateTimeField } from "../widgets/DateTimeField";
import { JobDates } from "@/domain/JobDates";

export interface JobDatesPanelProps {
  jobDates: JobDates;
  onArtworkReceivedDateChange: (d: Date) => void;
  onArtworkApprovedDateChange: (d: Date) => void;
  onShipDateChange: (d: Date) => void;
  onInHandDateChange: (d: Date) => void;
  onInstallDateTimeChange: (d: Date) => void;
  onStrikeDateTimeChange: (d: Date) => void;
}

export function JobDatesPanel(props: JobDatesPanelProps) {
  const { jobDates } = props;
  return (
    <div className="border border-solid border-black p-0 pb-2.5 px-2.5 inline-flex flex-row flex-wrap space-x-4 items-center">
      <DateTimeField
        labelText="Artwork received"
        dateTime={jobDates.artworkReceivedDate}
        onDateTimeChanged={props.onArtworkReceivedDateChange}
      />
      <DateTimeField
        labelText="Artwork approved"
        dateTime={jobDates.artworkApprovedDate}
        onDateTimeChanged={props.onArtworkApprovedDateChange}
      />
      <DateTimeField
        labelText="Ship date"
        dateTime={jobDates.shipDate}
        onDateTimeChanged={props.onShipDateChange}
      />
      <DateTimeField
        labelText="In-hand date"
        dateTime={jobDates.inHandDate}
        onDateTimeChanged={props.onInHandDateChange}
      />
      <DateTimeField
        labelText="Install date & time"
        shouldShowTime={true}
        dateTime={jobDates.installDateTime}
        onDateTimeChanged={props.onInstallDateTimeChange}
      />
      <DateTimeField
        labelText="Strike date & time"
        shouldShowTime={true}
        dateTime={jobDates.strikeDateTime}
        onDateTimeChanged={props.onStrikeDateTimeChange}
      />
    </div>
  );
}
