import { Client } from "@/domain/Client";
import { clientsSlice } from "../entities/createEntitiesSlice";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const jobClientSlice = createSlice({
  name: "jobClient",
  initialState: <Client | null>null,
  reducers: {
    jobClientSupplied: (_state, action: PayloadAction<Client | null>) => action.payload,
  },
  extraReducers: (builder) => {
    builder.addCase(clientsSlice.actions.entitySaved, (state, action) => {
      const { entity } = action.payload;
      return state?.isSameEntity(entity) ? entity : state;
    });
  },
});

export const { jobClientSupplied } = jobClientSlice.actions;

export default jobClientSlice.reducer;
