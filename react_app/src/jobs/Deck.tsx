import * as React from "react";
import Logo from "../../public/images/logo.jpg";
import { useAppSelector } from "../store/reduxHooks";
import { selectDistinctLineItemClientSignImages } from "./jobClientSignImagesSlice";
import { selectAllPotentialJobRelatedContacts } from "../store/multiSliceSelectors";

export function Deck() {
  const job = useAppSelector((state) => state.job!);
  const client = useAppSelector((state) => state.jobClient!);
  const property = useAppSelector((state) => state.jobProperty!);
  const contacts = useAppSelector(selectAllPotentialJobRelatedContacts);
  const lineItem = useAppSelector((state) => state.lineItem!);
  const location = lineItem.findLocationInProperty(property);
  const clientSignImages = useAppSelector(selectDistinctLineItemClientSignImages);
  const surfaces = useAppSelector((state) => state.surfaces);
  const materials = useAppSelector((state) => state.materials);
  const finishings = useAppSelector((state) => state.finishings);
  const printers = useAppSelector((state) => state.printers);

  return (
    <div className="deck">
      <div className="flex flex-row justify-between">
        <div className="text-left">
          <img src={Logo} className="ml-3.5 text-left max-h-12" alt="logo" />
        </div>
        <div className="text-right font-bold">
          {client.renderNameString() + " - " + property.renderNameString()}
        </div>
      </div>
      <div className="deck-line-item-header text-left">
        {lineItem.renderCodeString()}
        <span className="ml-20">{lineItem.renderNameString()}</span>
      </div>
      <div className="w-full flex flex-row flex-nowrap justify-between pb-1 border-b border-solid border-black">
        <div className="w-full flex flex-row flex-nowrap">
          {clientSignImages?.map((image, i) => (
            <div key={i} style={{ display: image.renderCssDisplayValue() }}>
              <img
                src={"/api/signImage/" + image.renderKeyString()}
                alt="Sign image"
                className="contained-image max-h-96"
              />
            </div>
          ))}
        </div>
        {location?.hasImage() && (
          <div>
            <img
              src={"/api/locationImage/" + location.renderImageKeyString()}
              className="contained-image max-h-96"
              alt="Location image"
            />
          </div>
        )}
      </div>
      <div className="w-full flex flex-row">
        <div className="flex-1 basis-3/5">
          <table className="deck-table w-full">
            <tbody>
              <tr>
                <td>File link(s):</td>
                <td colSpan={3}>
                  {clientSignImages?.map((image, i) => (
                    <div key={i}>{image.renderNameString()}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Size:</td>
                <td>
                  {lineItem.renderWidthString()}
                  <span className="ml-4">x </span>
                  {lineItem.renderHeightString()}
                </td>
              </tr>
              <tr>
                <td>Quantity:</td>
                <td>{lineItem.renderQuantityString()}</td>
              </tr>
              <tr>
                <td>Artwork received:</td>
                <td>
                  {lineItem.renderArtworkReceivedDateString() ||
                    job.renderArtworkReceivedDateString()}
                </td>
              </tr>
              <tr>
                <td>Artwork approved:</td>
                <td>
                  {lineItem.renderArtworkApprovedDateString() ||
                    job.renderArtworkApprovedDateString()}
                </td>
              </tr>
              <tr>
                <td>In-hand date:</td>
                <td>
                  {lineItem.renderInHandDateString() || job.renderInHandDateString()}
                </td>
              </tr>
              <tr>
                <td>Install date:</td>
                <td>
                  {lineItem.renderInstallDateString() || job.renderInstallTimeString()}
                </td>
                <td className="non-bold">Strike date:</td>
                <td>
                  {lineItem.renderStrikeDateString() || job.renderStrikeDateString()}
                </td>
              </tr>
              <tr>
                <td>Install time:</td>
                <td>
                  {lineItem.renderInstallTimeString() || job.renderInstallDateString()}
                </td>
                <td className="non-bold">Strike time:</td>
                <td>
                  {lineItem.renderStrikeTimeString() || job.renderStrikeTimeString()}
                </td>
              </tr>
              <tr>
                <td>Client contact:</td>
                <td>{job.renderClientContactNameString(contacts)}</td>
                <td>{job.renderClientContactPhoneNumberString(contacts)}</td>
              </tr>
              <tr>
                <td>Property contact:</td>
                <td>{job.renderPropertyContactNameString(contacts)}</td>
                <td>{job.renderPropertyContactPhoneNumberString(contacts)}</td>
              </tr>
              <tr>
                <td>Install contact:</td>
                <td>{job.renderInstallContactNameString(contacts)}</td>
                <td>{job.renderInstallContactPhoneNumberString(contacts)}</td>
              </tr>
              <tr>
                <td>SPE contact 1:</td>
                <td>{job.renderSuperiorPrintContact1NameString(contacts)}</td>
                <td>{job.renderSuperiorPrintContact1PhoneNumberString(contacts)}</td>
              </tr>
              <tr>
                <td>SPE contact 2:</td>
                <td>{job.renderSuperiorPrintContact2NameString(contacts)}</td>
                <td>{job.renderSuperiorPrintContact2PhoneNumberString(contacts)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex-1 basis-2/5">
          <table className="deck-table">
            <tbody>
              <tr>
                <td>Location:</td>
                <td>{location?.renderNameString()}</td>
              </tr>
              <tr>
                <td>Material:</td>
                <td>{lineItem.renderMaterialString(materials)}</td>
              </tr>
              <tr>
                <td>Surface:</td>
                <td>{lineItem.renderSurfaceString(surfaces)}</td>
              </tr>
              <tr>
                <td>Finishing:</td>
                <td>{lineItem.renderFinishingString(finishings)}</td>
              </tr>
              <tr>
                <td>Printer:</td>
                <td>{lineItem.renderPrinterString(printers)}</td>
              </tr>
              <tr>
                <td className="install-notes" colSpan={2}>
                  <div style={{ minHeight: "110px" }}>
                    INSTALLATION NOTES:
                    <br />
                    {lineItem.renderInstallNotesLines().map((l, i) => (
                      <div key={i}>{l}</div>
                    ))}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
