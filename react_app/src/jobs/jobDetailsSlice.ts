import {
  artworkApprovedDateEdited,
  artworkReceivedDateEdited,
  clientContactSelected,
  inHandDateEdited,
  installContactSelected,
  installDateTimeEdited,
  numberEdited,
  propertyContactSelected,
  shipDateEdited,
  strikeDateTimeEdited,
  superiorContact1Selected,
  superiorContact2Selected,
} from "./jobSlice";
import { createSlice } from "@reduxjs/toolkit";

export interface JobDetailsState {
  isDirty: boolean;
}

const initialState: JobDetailsState = {
  isDirty: false,
};

const makeDirty = (state: JobDetailsState) => {
  state.isDirty = true;
};

export const jobDetailsSlice = createSlice({
  name: "jobDetails",
  initialState,
  reducers: {
    jobUpdated: (state) => {
      state.isDirty = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(numberEdited, makeDirty);
    builder.addCase(clientContactSelected, makeDirty);
    builder.addCase(propertyContactSelected, makeDirty);
    builder.addCase(installContactSelected, makeDirty);
    builder.addCase(superiorContact1Selected, makeDirty);
    builder.addCase(superiorContact2Selected, makeDirty);
    builder.addCase(artworkReceivedDateEdited, makeDirty);
    builder.addCase(artworkApprovedDateEdited, makeDirty);
    builder.addCase(shipDateEdited, makeDirty);
    builder.addCase(inHandDateEdited, makeDirty);
    builder.addCase(installDateTimeEdited, makeDirty);
    builder.addCase(strikeDateTimeEdited, makeDirty);
  },
});

export const { jobUpdated } = jobDetailsSlice.actions;

export default jobDetailsSlice.reducer;
