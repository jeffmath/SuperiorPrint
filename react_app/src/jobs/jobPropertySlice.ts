import { Property } from "@/domain/Property";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { propertiesSlice } from "../entities/createEntitiesSlice";

export const jobPropertySlice = createSlice({
  name: "jobProperty",
  initialState: <Property | null>null,
  reducers: {
    jobPropertySupplied: (_state, action: PayloadAction<Property | null>) =>
      action.payload,
  },
  extraReducers: (builder) => {
    builder.addCase(propertiesSlice.actions.entitySaved, (state, action) => {
      const { entity } = action.payload;
      return state?.isSameEntity(entity) ? <Property>entity : state;
    });
  },
});

export const { jobPropertySupplied } = jobPropertySlice.actions;

export default jobPropertySlice.reducer;
