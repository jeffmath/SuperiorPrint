import * as React from "react";
import { JobCreator } from "./JobCreator";
import { JobSelector } from "./JobSelector";
import { useAppSelector } from "../store/reduxHooks";
import { JobDetails } from "./JobDetails";
import { LineItems } from "./LineItems";
import { LineItemDetails } from "./LineItemDetails";

export function JobsPage() {
  const job = useAppSelector((state) => state.job);
  const lineItem = useAppSelector((state) => state.lineItem);
  return (
    <div>
      <div className="flex flex-row flex-wrap space-x-16 items-center">
        <JobSelector />
        <JobCreator />
      </div>
      {job && (
        <div>
          <JobDetails />
          <LineItems />
          {lineItem && <LineItemDetails />}
        </div>
      )}
    </div>
  );
}
