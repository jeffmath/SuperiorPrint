import { Job } from "@/domain/Job";
import * as superagent from "superagent";
import { auth } from "../logins/Auth";
import { onError } from "../util/errorDialog";
import { DateDeserializer } from "../util/DateDeserializer";
import { LineItem } from "@/domain/LineItem";
import { Property } from "@/domain/Property";
import { Contact } from "@/domain/Contact";
import { Client } from "@/domain/Client";
import { ContactDto } from "@/data-transfer/ContactDto";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { JobDto, JobStub } from "@/data-transfer/JobDto";
import { ClientDto } from "@/data-transfer/ClientDto";
import { PropertyDto } from "@/data-transfer/PropertyDto";

export interface FetchJobRawResult {
  job: JobDto;
  client: ClientDto;
  property: PropertyDto;
  contacts: ContactDto[];
}

export interface FetchJobResult {
  job: Job;
  client: Client;
  property: Property;
  contacts: Contact[];
}

class JobService {
  async fetchJob(
    id: Id,
    clients: NamedEntity[],
    properties: NamedEntity[],
  ): Promise<FetchJobResult> {
    try {
      let res = await superagent.get(`/api/job/${id}`).set(auth.header);
      const rawResult = res.body as FetchJobRawResult;
      const result = {
        job: Job.fromDto(rawResult.job),
        client: Client.fromDto(rawResult.client),
        property: Property.fromDto(rawResult.property),
        contacts: rawResult.contacts.map((dto: ContactDto) =>
          Contact.fromContactDto(dto, clients, properties),
        ),
      };
      DateDeserializer.deserializeDates(result.job);
      return result;
    } catch (err) {
      onError(err, `Could not fetch job`);
      throw err;
    }
  }

  async fetchJobs(clientId: Id, propertyId: Id): Promise<JobStub[]> {
    try {
      const res = await superagent
        .get(`/api/jobs/${clientId}/${propertyId}`)
        .set(auth.header);
      return res.body as JobStub[];
    } catch (err) {
      onError(err, `Could not fetch jobs`);
      throw err;
    }
  }

  async createJob(clientId: Id, propertyId: Id): Promise<FetchJobResult> {
    try {
      const res = await superagent
        .post(`/api/job/${clientId}/${propertyId}`)
        .set(auth.header);
      const id = res.text as string;
      return jobService.fetchJob(id, [], []);
    } catch (err) {
      onError(err, `Could not create job`);
      throw err;
    }
  }

  async updateJob(job: Job): Promise<void> {
    // update the job's details on the server
    try {
      await superagent.put("/api/job").set(auth.header).send(job.renderDto());
    } catch (err) {
      onError(err, `Could not update job details`);
      throw err;
    }
  }

  async deleteJob(job: Job): Promise<void> {
    try {
      await superagent.delete(`/api/job/${job.renderKeyString()}`).set(auth.header);
    } catch (err) {
      onError(err, `Could not delete job`);
      throw err;
    }
  }

  async updateLineItem(job: Job, lineItem: LineItem): Promise<void> {
    try {
      await superagent
        .put(`/api/lineItem/${job.renderKeyString()}`)
        .set(auth.header)
        .send(lineItem);
    } catch (err) {
      onError(err, `Could not save line-item details`);
      throw err;
    }
  }

  async addLineItem(job: Job): Promise<LineItem> {
    try {
      const res = await superagent
        .post(`/api/lineItem/${job.renderKeyString()}`)
        .set(auth.header);
      return LineItem.fromDto(res.body);
    } catch (err) {
      onError(err, `Could not add line-item`);
      throw err;
    }
  }

  async deleteLineItem(jobId: Id, lineItemId: Id): Promise<void> {
    try {
      await superagent.delete(`/api/lineItem/${jobId}/${lineItemId}`).set(auth.header);
    } catch (err) {
      onError(err, `Could not delete line-item`);
      throw err;
    }
  }
}

export const jobService = new JobService();
