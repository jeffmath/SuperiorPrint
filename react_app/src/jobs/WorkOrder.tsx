import * as React from "react";
import Logo from "../../public/images/logo.jpg";
import { useAppSelector } from "../store/reduxHooks";
import { selectDistinctLineItemClientSignImages } from "./jobClientSignImagesSlice";
import { selectAllPotentialJobRelatedContacts } from "../store/multiSliceSelectors";

export function WorkOrder() {
  const job = useAppSelector((state) => state.job!);
  const client = useAppSelector((state) => state.jobClient!);
  const property = useAppSelector((state) => state.jobProperty!);
  const contacts = useAppSelector(selectAllPotentialJobRelatedContacts);
  const lineItem = useAppSelector((state) => state.lineItem!);
  const location = lineItem.findLocationInProperty(property);
  const clientSignImages = useAppSelector(selectDistinctLineItemClientSignImages);
  const materials = useAppSelector((state) => state.materials);
  const finishings = useAppSelector((state) => state.finishings);

  return (
    <div className="work-order relative">
      <div className="absolute">
        <img src={Logo} alt="logo" />
      </div>
      <div className="w-full min-h-20 flex flex-row items-center justify-center">
        <div className="text-4xl font-bold italic">WORK ORDER</div>
      </div>
      <div className="flex flex-row mt-5">
        <div className="flex-1">
          <table className="work-order-table">
            <tbody>
              <tr style={{ height: "40px" }}>
                <td className="font-bold" style={{ minWidth: "100px" }}>
                  JOB NAME
                </td>
                <td>{client.renderNameString() + " - " + job.renderNumberString()}</td>
              </tr>
              <tr>
                <td>Code</td>
                <td>{lineItem.renderCodeString()}</td>
              </tr>
              <tr>
                <td>Name</td>
                <td>{lineItem.renderNameString()}</td>
              </tr>
              <tr>
                <td>File link(s)</td>
                <td>
                  {clientSignImages?.map((image, i) => (
                    <div key={i}>{image.renderNameString()}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Material</td>
                <td>{lineItem.renderMaterialString(materials)}</td>
              </tr>
              <tr>
                <td>Finishing</td>
                <td>{lineItem.renderFinishingString(finishings)}</td>
              </tr>
              <tr>
                <td>Width</td>
                <td>{lineItem.renderWidthString()}</td>
              </tr>
              <tr>
                <td>Height</td>
                <td>{lineItem.renderHeightString()}</td>
              </tr>
              <tr>
                <td>Quantity</td>
                <td>{lineItem.renderQuantityString()}</td>
              </tr>
              <tr>
                <td>Artwork received</td>
                <td>
                  {lineItem.renderArtworkReceivedDateString() ||
                    job.renderArtworkReceivedDateString()}
                </td>
              </tr>
              <tr>
                <td>Artwork approved</td>
                <td>
                  {lineItem.renderArtworkApprovedDateString() ||
                    job.renderArtworkApprovedDateString()}
                </td>
              </tr>
              <tr>
                <td>Notes</td>
                <td>
                  {lineItem.renderInstallNotesLines().map((l, i) => (
                    <div key={i}>{l}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Ship date</td>
                <td>{lineItem.renderShipDateString() || job.renderShipDateString()}</td>
              </tr>
              <tr>
                <td>In-hand date</td>
                <td>
                  {lineItem.renderInHandDateString() || job.renderInHandDateString()}
                </td>
              </tr>
              <tr>
                <td>Ship to</td>
                <td>{job.renderInstallContactNameString(contacts)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex-1 flex flex-col flex-wrap items-center">
          {location?.hasImage() && (
            <img
              src={"/api/locationImage/" + location.renderImageKeyString()}
              className="contained-image block"
              alt="Location image"
              style={{ maxHeight: "220px", marginBottom: "5px" }}
            />
          )}
          {clientSignImages?.map((image, i) => (
            <div key={i} style={{ display: image.renderCssDisplayValue() }}>
              <img
                src={"/api/signImage/" + image.renderKeyString()}
                className="contained-image"
                alt="Sign image"
                style={{
                  marginRight: "5px",
                  marginBottom: "5px",
                  maxHeight: "220px",
                }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
