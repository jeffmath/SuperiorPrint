import {
  lineItemSelected,
  artworkApprovedDateEdited,
  artworkReceivedDateEdited,
  clientSignImageSelected,
  codeEdited,
  finishingSelected,
  heightEdited,
  inHandDateEdited,
  installDateTimeEdited,
  installNotesEdited,
  locationSelected,
  materialSelected,
  nameEdited,
  priceEachEdited,
  printerSelected,
  quantityEdited,
  shipDateEdited,
  strikeDateTimeEdited,
  surfaceSelected,
  widthEdited,
} from "./lineItemSlice";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { lineItemDeleted, lineItemUpdated } from "./jobSlice";
import { LineItem } from "@/domain/LineItem";

export interface LineItemDetailsState {
  isDirty: boolean;
  clientSignImageToView?: ClientSignImageInfo;
  isImageModalOpen: boolean;
  isImageUploadModalOpen: boolean;
  unsavedLineItem: LineItem | null;
}

const initialState: LineItemDetailsState = {
  isDirty: false,
  isImageModalOpen: false,
  isImageUploadModalOpen: false,
  unsavedLineItem: null,
};

const makeDirty = (state: { isDirty: boolean }) => {
  state.isDirty = true;
};

export const lineItemDetailsSlice = createSlice({
  name: "lineItemDetails",
  initialState,
  reducers: {
    clientSignImageSelectedForViewing: (
      state,
      action: PayloadAction<ClientSignImageInfo>,
    ) => {
      state.clientSignImageToView = action.payload;
    },
    imageModalOpened: (state) => {
      state.isImageModalOpen = true;
    },
    imageModalClosed: (state) => {
      state.isImageModalOpen = false;
    },
    imageUploadModalOpened: (state) => {
      state.isImageUploadModalOpen = true;
    },
    imageUploadModalClosed: (state) => {
      state.isImageUploadModalOpen = false;
    },
    unsavedLineItemSaved: (state) => {
      state.unsavedLineItem = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(nameEdited, makeDirty);
    builder.addCase(codeEdited, makeDirty);
    builder.addCase(locationSelected, makeDirty);
    builder.addCase(widthEdited, makeDirty);
    builder.addCase(heightEdited, makeDirty);
    builder.addCase(surfaceSelected, makeDirty);
    builder.addCase(materialSelected, makeDirty);
    builder.addCase(finishingSelected, makeDirty);
    builder.addCase(quantityEdited, makeDirty);
    builder.addCase(priceEachEdited, makeDirty);
    builder.addCase(printerSelected, makeDirty);
    builder.addCase(clientSignImageSelected, makeDirty);
    builder.addCase(artworkReceivedDateEdited, makeDirty);
    builder.addCase(artworkApprovedDateEdited, makeDirty);
    builder.addCase(shipDateEdited, makeDirty);
    builder.addCase(inHandDateEdited, makeDirty);
    builder.addCase(installDateTimeEdited, makeDirty);
    builder.addCase(strikeDateTimeEdited, makeDirty);
    builder.addCase(installNotesEdited, makeDirty);
    builder.addMatcher(makesClean, (state) => {
      state.isDirty = false;
    });
  },
});

function makesClean(action: { type: string }): boolean {
  return (<string[]>[
    lineItemUpdated.type,
    lineItemDeleted.type,
    lineItemSelected.type,
  ]).includes(action.type);
}

export const {
  clientSignImageSelectedForViewing,
  imageModalOpened,
  imageModalClosed,
  imageUploadModalOpened,
  imageUploadModalClosed,
  unsavedLineItemSaved,
} = lineItemDetailsSlice.actions;

export default lineItemDetailsSlice.reducer;
