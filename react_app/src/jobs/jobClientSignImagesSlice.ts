import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { State } from "../store/State";

export const jobClientSignImagesSlice = createSlice({
  name: "jobClientSignImages",
  initialState: <ClientSignImageInfo[]>[],
  reducers: {
    imagesFetched: (_state, action: PayloadAction<ClientSignImageInfo[]>) =>
      action.payload,
  },
});

export const { imagesFetched } = jobClientSignImagesSlice.actions;

export default jobClientSignImagesSlice.reducer;

const selectJobClientSignImages = (state: State) => state.jobClientSignImages;

const selectLineItem = (state: State) => state.lineItem;

export const selectDistinctLineItemClientSignImages = createSelector(
  [selectJobClientSignImages, selectLineItem],
  (images, lineItem) => lineItem?.getDistinctClientSignImages(images),
);
