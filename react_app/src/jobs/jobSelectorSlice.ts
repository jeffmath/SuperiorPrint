import { NamedEntity } from "@/domain/NamedEntity";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";
import { propertiesSlice } from "../entities/createEntitiesSlice";
import { PropertyType } from "../entity/EntityType";
import { jobDeleted } from "./jobSlice";
import { JobStub } from "@/data-transfer/JobDto";

export interface JobSelectorState {
  selectedClientId: Id | null;
  properties: NamedEntity[];
  selectedPropertyId: Id | null;
  jobs: JobStub[];
  selectedJobId: Id | null;
}

const initialState: JobSelectorState = {
  properties: [],
  jobs: [],
  selectedClientId: null,
  selectedPropertyId: null,
  selectedJobId: null,
};

export const jobSelectorSlice = createSlice({
  name: "jobSelector",
  initialState,
  reducers: {
    clientSelected: (state, action: PayloadAction<Id>) => {
      state.selectedClientId = action.payload;
      state.properties = [];
      state.jobs = [];
      state.selectedJobId = null;
    },
    propertiesFetched: (state, action: PayloadAction<NamedEntity[]>) => {
      state.properties = action.payload;
    },
    propertySelected: (state, action: PayloadAction<Id | null>) => {
      state.selectedPropertyId = action.payload;
      state.jobs = [];
      state.selectedJobId = null;
    },
    jobsFetched: (state, action: PayloadAction<JobStub[]>) => {
      state.jobs = action.payload;
    },
    jobSelected: (state, action: PayloadAction<Id | null>) => {
      state.selectedJobId = action.payload;
    },
  },
  extraReducers: (builder) => {
    // reconcile a name-change of an entry in the properties dropdown
    builder.addCase(propertiesSlice.actions.entitySaved, (state, action) => {
      const { payload } = action;
      if (payload.entityType === PropertyType)
        state.properties = state.properties.map((p) =>
          p.isSameEntity(payload.entity) ? payload.entity : p,
        );
    });
    // reconcile a deletion of an entry in the properties dropdown
    builder.addCase(propertiesSlice.actions.entityDeleted, (state, action) => {
      const { payload } = action;
      if (payload.entityType === PropertyType)
        state.properties = state.properties.filter(
          (p) => !p.hasSameKey(action.payload.key),
        );
    });
    // when a job has been deleted, remove it from this slice's jobs list, if it's present
    builder.addCase(jobDeleted, (state, action) => {
      state.jobs = state.jobs.filter((job) => job.id !== action.payload);
    });
  },
});

export const {
  clientSelected,
  propertiesFetched,
  propertySelected,
  jobsFetched,
  jobSelected,
} = jobSelectorSlice.actions;

export default jobSelectorSlice.reducer;
