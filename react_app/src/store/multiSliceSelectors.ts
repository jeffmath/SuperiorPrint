import { State } from "./State";
import { createSelector } from "@reduxjs/toolkit";

const selectJobContacts = (state: State) => state.jobContacts;
const selectInstallers = (state: State) => state.installers;
const selectSuperiorContacts = (state: State) => state.superiorContacts;

export const selectAllPotentialJobRelatedContacts = createSelector(
  [selectJobContacts, selectInstallers, selectSuperiorContacts],
  (jobContacts, installers, superiorContacts) => [
    ...jobContacts,
    ...installers,
    ...superiorContacts,
  ],
);
