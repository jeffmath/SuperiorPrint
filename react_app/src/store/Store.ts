import { Action, combineReducers, Dispatch, Reducer } from "redux";
import {
  clientsFilterSlice,
  contactsFilterSlice,
  propertiesFilterSlice,
} from "../entities/createEntitiesFilterSlice";
import reduceReducers from "reduce-reducers";
import { configureStore } from "@reduxjs/toolkit";
import { State } from "./State";
import LoginReducer from "../logins/loginSlice";
import JobSelectorReducer from "../jobs/jobSelectorSlice";
import {
  clientsSlice,
  propertiesSlice,
  finishingsSlice,
  materialsSlice,
  printersSlice,
  surfacesSlice,
  usersSlice,
} from "../entities/createEntitiesSlice";
import {
  clientsPageSlice,
  contactsPageSlice,
  propertiesPageSlice,
} from "../entities/createEntitiesPageSlice";
import JobCreatorReducer from "../jobs/jobCreatorSlice";
import JobReducer from "../jobs/jobSlice";
import JobDetailsReducer from "../jobs/jobDetailsSlice";
import PropertyReducer from "../properties/propertySlice";
import JobPropertyReducer from "../jobs/jobPropertySlice";
import JobClientReducer from "../jobs/jobClientSlice";
import JobClientSignImagesReducer from "../jobs/jobClientSignImagesSlice";
import JobContactsReducer from "../jobs/jobContactsSlice";
import LineItemReducer from "../jobs/lineItemSlice";
import LineItemsReducer from "../jobs/lineItemsSlice";
import LineItemDetailsReducer from "../jobs/lineItemDetailsSlice";
import { clientSlice } from "../entity/createEntitySlice";
import SuperiorContactsReducer from "../contacts/superiorContactsSlice";
import InstallersReducer from "../contacts/installersSlice";
import ContactsSearchBarReducer from "../contacts/contactsSearchBarSlice";
import ContactReducer from "../contacts/contactSlice";
import ClientSignImagesTableReducer from "../clients/clientSignImagesTableSlice";
import wholeStateReducer from "./WholeStateReducer";
import ChangePasswordReducer from "../logins/changePasswordSlice";
import LocationsTableReducer from "../properties/locationsTableSlice";
import SmallSetEntitiesTableReducer from "../smallSetEntities/smallSetEntitiesTableSlice";

/**
 * Note that mutable domain objects are stored throughout this store, due to our push for
 * better encapsulation of their data. (Meaning, it is desired that these objects control
 * the updates to their own state.) Immer should detect and throw an error when it senses
 * that any of those domain objects have been mutated between dispatches.  This is as it
 * should be, for the domain objects' methods which cause mutation of their state should
 * only ever be called within a reducer.  Otherwise, Redux won't know that those objects
 * have changed, for the purpose of sending notifications to state listeners.
 */

type SliceReducers = {
  [K in keyof State]: Reducer<any, any, any>;
};

const sliceReducers: SliceReducers = {
  loginState: LoginReducer,
  jobSelectorState: JobSelectorReducer,
  jobCreatorState: JobCreatorReducer,
  job: JobReducer,
  jobDetailsState: JobDetailsReducer,
  jobProperty: JobPropertyReducer,
  jobClient: JobClientReducer,
  jobClientSignImages: JobClientSignImagesReducer,
  jobContacts: JobContactsReducer,
  lineItem: LineItemReducer,
  lineItemsState: LineItemsReducer,
  lineItemDetailsState: LineItemDetailsReducer,
  allPropertyStubs: propertiesSlice.reducer,
  propertiesPage: propertiesPageSlice.reducer,
  property: PropertyReducer,
  propertiesFilter: propertiesFilterSlice.reducer,
  locationsTableState: LocationsTableReducer,
  allClientStubs: clientsSlice.reducer,
  clientsPage: clientsPageSlice.reducer,
  client: clientSlice.reducer,
  clientsFilter: clientsFilterSlice.reducer,
  clientSignImagesTableState: ClientSignImagesTableReducer,
  contactsPage: contactsPageSlice.reducer,
  contact: ContactReducer,
  contactsFilter: contactsFilterSlice.reducer,
  contactsSearchBarState: ContactsSearchBarReducer,
  superiorContacts: SuperiorContactsReducer,
  installers: InstallersReducer,
  surfaces: surfacesSlice.reducer,
  surfacesTableState: SmallSetEntitiesTableReducer,
  materials: materialsSlice.reducer,
  materialsTableState: SmallSetEntitiesTableReducer,
  finishings: finishingsSlice.reducer,
  finishingsTableState: SmallSetEntitiesTableReducer,
  printers: printersSlice.reducer,
  printersTableState: SmallSetEntitiesTableReducer,
  users: usersSlice.reducer,
  usersTableState: SmallSetEntitiesTableReducer,
  changePasswordState: ChangePasswordReducer,
};

const combinedSliceReducers: Reducer<State> = combineReducers(sliceReducers);

const reducer = reduceReducers(
  wholeStateReducer,
  combinedSliceReducers,
) as Reducer<State>;

export const store = configureStore<State, Action>({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      // without this option, redux will complain about all the instances of our domain
      // entity classes that are now kept in this store, since those instances aren't
      // simple POJOs and therefore ane not serializable
      serializableCheck: false,
    }),
});

// for some reason, using the commented out part (which was prescribed) leads to the
// result not being able to catch type errors on the actions dispatched
export type AppDispatch = Dispatch; //typeof store.dispatch;
