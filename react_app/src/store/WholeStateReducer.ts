import { State } from "./State";
import { contactsSlice } from "../entities/createEntitiesSlice";
import { createSlice } from "@reduxjs/toolkit";
import { clientSignImagesTableSlice } from "../clients/clientSignImagesTableSlice";
import { lineItemSelected } from "../jobs/lineItemSlice";
import { Client } from "@/domain/Client";

/**
 * This reducer contains all the action handlers which rely on more than one slice
 * of the state atom.
 */
export const wholeStateSlice = createSlice({
  name: "wholeState",
  initialState: <State>{},
  reducers: {},
  extraReducers: (builder) => {
    // when the image stubs have been fetched for a client's sign-image-table, it may have
    // be due to an update to those images, so if the currently selected job has the same
    // client, store those image stubs as being those for the job's client
    builder.addCase(clientSignImagesTableSlice.actions.imagesFetched, (state, action) => {
      if (state.job?.hasClient(state.client as Client)) {
        state.jobClientSignImages = action.payload;
      }
    });
    // when an image has been deleted from a client's sign-image-table,
    // if the currently selected job has the same client, remove the corresponding
    // image-stub from the list of those for the job's client
    builder.addCase(clientSignImagesTableSlice.actions.imageDeleted, (state, action) => {
      if (state.job?.hasClient(state.client as Client)) {
        state.jobClientSignImages = state.jobClientSignImages.filter(
          (i) => !i.hasSameKey(action.payload),
        );
      }
    });
    // if the client or property selected in the contacts-page's (clients or
    // properties)-dropdown has been deleted, clear the page's contacts-list, so it will
    // be refreshed
    builder.addCase(contactsSlice.actions.entityDeleted, (state, action) => {
      const entityId = action.payload.key;
      const { contactsSearchBarState: barState } = state;
      if (barState.clientId === entityId || barState.propertyId === entityId) {
        state.contactsPage = null;
      }
    });
    // if a line-item has been selected, and the previously selected line-item is dirty,
    // store the latter where a listener will see it and use it to make an update,
    // if the user desires
    builder.addCase(lineItemSelected, (state, action) => {
      const { payload } = action;
      if (
        state.lineItem &&
        payload &&
        !state.lineItem.isSameEntity(payload) &&
        state.lineItemDetailsState.isDirty
      ) {
        state.lineItemDetailsState.unsavedLineItem = state.lineItem;
      }
    });
  },
});

export default wholeStateSlice.reducer;
