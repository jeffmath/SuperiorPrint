import type { AppDispatch } from "./Store";
import { useDispatch, useSelector } from "react-redux";
import type { State } from "./State";

// the Redux Toolkit docs advise that these be created for use throughout the app,
// instead of the untyped hooks which they wrap
export const useAppDispatch = useDispatch.withTypes<AppDispatch>();
export const useAppSelector = useSelector.withTypes<State>();
