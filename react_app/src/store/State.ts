import { Property } from "@/domain/Property";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { LineItem } from "@/domain/LineItem";
import { Job } from "@/domain/Job";
import { Contact } from "@/domain/Contact";
import { EntitiesPage } from "../entities/EntitiesPage";
import { Client } from "@/domain/Client";
import { NamedEntity } from "@/domain/NamedEntity";
import { LoginState } from "../logins/loginSlice";
import { JobSelectorState } from "../jobs/jobSelectorSlice";
import { JobCreatorState } from "../jobs/jobCreatorSlice";
import { LineItemsState } from "../jobs/lineItemsSlice";
import { LineItemDetailsState } from "../jobs/lineItemDetailsSlice";
import { EntitiesFilterState } from "../entities/createEntitiesFilterSlice";
import { LocationsTableState } from "../properties/locationsTableSlice";
import { ClientSignImagesTableState } from "../clients/clientSignImagesTableSlice";
import { ContactsSearchBarState } from "../contacts/contactsSearchBarSlice";
import { SmallSetEntitiesTableState } from "../smallSetEntities/smallSetEntitiesTableSlice";
import { ChangePasswordState } from "../logins/changePasswordSlice";
import { JobDetailsState } from "../jobs/jobDetailsSlice";

/**
 * Note that the values of most fields and selectors in this app are stored by this state
 * atom, regardless of whether the value or selection is referenced by more than one
 * component app-wide.  This is so that, when the user navigates to the different tabs
 * selected through the main nav-bar, their values and selections on each tab are
 * retained for the next time the user visits that tab.
 */
export interface State {
  /**
   * The user's logged-in status, plus the UI state of the login form.
   * The logged-in status is observed by the app component to decide whether to show
   * the login form or the rest of the app.
   */
  loginState: LoginState;

  /**
   * The UI state of the component which selects an existing job for viewing/editing.
   * Not referenced elsewhere.
   */
  jobSelectorState: JobSelectorState;

  /**
   * The UI state of the component which creates a new job for a given client and
   * property.  Not referenced elsewhere.
   */
  jobCreatorState: JobCreatorState;

  /**
   * The current job being viewed/edited.  Is observed by the work-order and deck
   * components.
   */
  job: Job | null;

  /**
   * The UI state of the details section for the current job being viewed/edited.
   * Not referenced elsewhere.
   */
  jobDetailsState: JobDetailsState;

  /**
   * The property associated with the current job being viewed/edited.
   * The job, line-item, work-order, and deck page need to pull from this
   * fully loaded object for their own views.
   */
  jobProperty: Property | null;

  /**
   * The client associated with the current job being viewed/edited.
   * The job, line-item, work-order, and deck page need to pull from this
   * fully loaded object for their own views.
   */
  jobClient: Client | null;

  /**
   * The client sign images which have been selected for the current job line-item
   * being edited.  Is observed by the work-order and deck components.
   */
  jobClientSignImages: ClientSignImageInfo[];

  /**
   * All the contacts associated with the client or property of the current
   * job being viewed/edited, along with all installers.  These represent all the
   * contacts which may be assigned to the job.
   */
  jobContacts: Contact[];

  /**
   * The current job line-item being viewed/edited.   Is observed by the
   * work-order and deck components.
   */
  lineItem: LineItem | null;

  /**
   * The UI state of the line-items table for the current job being edited.
   * Not referenced elsewhere.
   */
  lineItemsState: LineItemsState;

  /**
   * The UI state of the line-item details section for the current job being edited.
   * Not referenced elsewhere.
   */
  lineItemDetailsState: LineItemDetailsState;

  /**
   * Stubs of all properties in the database.  Are referenced by the contacts
   * and contact pages in the app.
   */
  allPropertyStubs: NamedEntity[];

  /**
   * The property-stubs displayed on the current page of the properties table
   * on the properties page.  Not referenced elsewhere, but we would like to avoid
   * reloading these when the user is bouncing between tabs.
   */
  propertiesPage: EntitiesPage | null;

  /**
   * The current property being viewed/edited.  Is observed by the jobs page
   * such that, if any of the properties it references are this property,
   * relevant changes to this property (and its locations) should reflect on
   * the jobs page. The same is true for the properties page.
   */
  property: Property | null;

  /**
   * The current filter settings for the properties displayed on the properties page.
   * Not referenced elsewhere.
   */
  propertiesFilter: EntitiesFilterState;

  /**
   * The locations of the current property being viewed/edited.   Is observed
   * by the jobs page such that, if the current job being edited is for this
   * property, relevant changes to this collection should reflect in the
   * job's location fields.
   */
  locationsTableState: LocationsTableState;

  /**
   * Stubs of all clients in the database.  Are referenced by the contacts
   * and contact pages in the app.
   */
  allClientStubs: NamedEntity[];

  /**
   * The client-stubs displayed on the current page of the clients table
   * on the clients page.  Not referenced elsewhere, but we would like to avoid
   * reloading these when the user is bouncing between tabs.
   */
  clientsPage: EntitiesPage | null;

  /**
   * The current client being viewed/edited.  Is observed by the jobs page such that,
   * if any of the clients it references are this client, relevant changes to this
   * client should reflect on the jobs page.  The same is true for the clients page.
   */
  client: Client | null;

  /**
   * The current filter settings for the clients displayed on the clients page.
   * Not referenced elsewhere.
   */
  clientsFilter: EntitiesFilterState;

  /**
   * The sign images collection for the current client being viewed/edited.
   * Is observed by the jobs page such that, if the current job being edited
   * is for this client, relevant changes to this collection
   * should reflect in the job's sign-image fields.
   */
  clientSignImagesTableState: ClientSignImagesTableState;

  /**
   * The contact-stubs displayed on the current page of the contacts table
   * on the contacts page.  Not referenced elsewhere, but we would like to avoid
   * reloading these when the user is bouncing between tabs.
   */
  contactsPage: EntitiesPage | null;

  /**
   * The current contact being viewed/edited.  Is observed by the jobs page in that,
   * if any of the contacts it references are this contact, relevant changes to this
   * contact should reflect on the jobs page.  The same is true for the contacts page.
   */
  contact: Contact | null;

  /**
   * The current search settings for contacts.  Not referenced elsewhere.
   */
  contactsFilter: EntitiesFilterState;
  contactsSearchBarState: ContactsSearchBarState;

  /**
   * All the contacts in the database who work for Superior Print.  These
   * may be associated with any job.
   */
  superiorContacts: Contact[];

  /**
   * All the contacts in the database who are installers. These
   * may be associated with any job.
   */
  installers: Contact[];

  /**
   * The entire membership of each class of small-set entity stored on the server.
   * Are referenced by the job currently being edited on the jobs page.
   */
  surfaces: NamedEntity[];
  materials: NamedEntity[];
  finishings: NamedEntity[];
  printers: NamedEntity[];
  users: NamedEntity[];

  /**
   * The UI state of the table components which list all the small-set entities of a
   * particular type.  Not referenced elsewhere.
   */
  surfacesTableState: SmallSetEntitiesTableState;
  materialsTableState: SmallSetEntitiesTableState;
  finishingsTableState: SmallSetEntitiesTableState;
  printersTableState: SmallSetEntitiesTableState;
  usersTableState: SmallSetEntitiesTableState;

  /**
   * The UI state of the change password form.  Not referenced elsewhere.
   */
  changePasswordState: ChangePasswordState;
}
