import { NamedEntity } from "@/domain/NamedEntity";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ClientType, ContactType, EntityType, PropertyType } from "./EntityType";

export interface SelectPayload {
  entityType: EntityType;
  entity: NamedEntity;
}

export interface UpdateNamePayload {
  entity: NamedEntity;
  name: string;
}

function createEntitySlice(entityType: EntityType) {
  return createSlice({
    name: entityType.name + "-entity",
    initialState: <NamedEntity | null>null,
    reducers: {
      entityFetched: (state, action: PayloadAction<SelectPayload>) =>
        action.payload.entityType === entityType ? action.payload.entity : state,
      nameEdited: (state, action: PayloadAction<UpdateNamePayload>) => {
        const { payload } = action;
        if (state?.isSameEntity(payload.entity)) state.onNameEdited(payload.name);
      },
    },
  });
}

export const clientSlice = createEntitySlice(ClientType);
export const propertySlice = createEntitySlice(PropertyType);
export const contactSlice = createEntitySlice(ContactType);

export function getEntitySlice(entityType: EntityType) {
  switch (entityType) {
    case ClientType:
      return clientSlice;
    case PropertyType:
      return propertySlice;
    case ContactType:
      return contactSlice;
    default:
      throw "Invalid entity-type specified.";
  }
}
