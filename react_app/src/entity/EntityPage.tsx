import * as React from "react";
import { ClientType, EntityType, PropertyType } from "./EntityType";
import { useParams } from "react-router-dom";
import { ClientSignImagesTable } from "../clients/ClientSignImagesTable";
import { EntityForm } from "./EntityForm";
import { LocationsTable } from "../properties/LocationsTable";

export type EntityPageProps = {
  entityType: EntityType;
};

export function EntityPage({ entityType }: EntityPageProps) {
  const entityId = useParams().id;
  if (!entityId) return null;
  return (
    <div>
      <EntityForm entityType={entityType} entityId={entityId} />
      {entityType === PropertyType && <LocationsTable />}
      {entityType === ClientType && <ClientSignImagesTable clientId={entityId} />}
    </div>
  );
}
