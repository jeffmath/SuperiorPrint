export interface EntityType {
  name: string;
  pluralName: string;
}

/**
 * Paged entity types
 */
export const ClientType: EntityType = { name: "client", pluralName: "clients" };
export const ContactType: EntityType = { name: "contact", pluralName: "contacts" };
export const PropertyType: EntityType = { name: "property", pluralName: "properties" };

/**
 * Small-set entity types
 */
export const FinishingType: EntityType = { name: "finishing", pluralName: "finishings" };
export const MaterialType: EntityType = { name: "material", pluralName: "materials" };
export const PrinterType: EntityType = { name: "printer", pluralName: "printers" };
export const SurfaceType: EntityType = { name: "surface", pluralName: "surfaces" };
export const UserType: EntityType = { name: "user", pluralName: "users" };
