import * as React from "react";
import { ClientType, ContactType, EntityType, PropertyType } from "./EntityType";
import { ContactSubform } from "../contacts/ContactSubform";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { entitiesService, hydratorForPagedEntity } from "../entities/EntitiesService";
import { getEntitySlice } from "./createEntitySlice";
import { Id } from "@/domain/Id";
import { useNavigate } from "react-router-dom";
import { yesNoDialog } from "../util/yesNoDialog";
import { useEffect } from "react";
import { BackButton } from "../widgets/BackButton";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

export interface EntityFormProps {
  entityType: EntityType;
  entityId: Id;
}

export function EntityForm({ entityType, entityId }: EntityFormProps) {
  const dispatch = useAppDispatch();
  const clients = useAppSelector((state) => state.allClientStubs);
  const properties = useAppSelector((state) => state.allPropertyStubs);
  const entity = useAppSelector((state) => {
    switch (entityType) {
      case ClientType:
        return state.client;
      case PropertyType:
        return state.property;
      case ContactType:
        return state.contact;
    }
  });
  const navigate = useNavigate();

  useEffect(() => {
    if (!entity && clients.length && properties.length) {
      const hydrate = hydratorForPagedEntity(entityType, clients, properties);
      entitiesService.fetchEntity(entityType, entityId, hydrate).then((entity) =>
        dispatch(
          getEntitySlice(entityType).actions.entityFetched({
            entity,
            entityType,
          }),
        ),
      );
    }
  }, [entity, clients, properties]);

  function handleNameChange(name: string) {
    dispatch(
      getEntitySlice(entityType).actions.nameEdited({
        entity: entity!,
        name,
      }),
    );
  }

  function handleSaveClick() {
    entitiesService.updateEntity(entity!, entityType, true).then(() => {
      navigate(`/${entityType.pluralName}`);
    });
  }

  function handleDeleteClick() {
    yesNoDialog(
      `Are you sure you want to delete this ${entityType.name}?`,
      (yes) =>
        yes &&
        entitiesService.deleteEntity(entity!, entityType, true).then(() => {
          navigate(`/${entityType.pluralName}`);
        }),
    );
  }

  return (
    <>
      <h4 className="form-header">
        Edit {entityType.name}
        <BackButton />
      </h4>
      <div className="mt-2">
        <Label htmlFor="entityName">Name</Label>
        <Input
          id="entityName"
          className="w-64"
          value={entity?.renderNameString() || ""}
          onChange={(e) => handleNameChange(e.currentTarget.value)}
        />
      </div>
      {entityType === ContactType && <ContactSubform />}
      <div className="mt-4">
        <Button onClick={handleSaveClick} disabled={!entity?.renderNameString()}>
          Save
        </Button>
        <Button
          variant="destructive"
          size="sm"
          className="ml-7"
          onClick={handleDeleteClick}
        >
          Delete
        </Button>
      </div>
    </>
  );
}
