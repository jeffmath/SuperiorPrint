import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Id } from "@/domain/Id";

export interface SmallSetEntitiesTableState {
  isEditingMap: { [id: string]: boolean };
}

const initialState: SmallSetEntitiesTableState = {
  isEditingMap: {},
};

export const smallSetEntitiesTableSlice = createSlice({
  name: "smallSetEntitiesTable",
  initialState,
  reducers: {
    entitySelectedForEdit: (state, action: PayloadAction<Id>) => {
      state.isEditingMap[action.payload] = true;
    },
    entitySaved: (state, action: PayloadAction<Id>) => {
      state.isEditingMap[action.payload] = false;
    },
  },
});

export const { entitySelectedForEdit, entitySaved } = smallSetEntitiesTableSlice.actions;

export default smallSetEntitiesTableSlice.reducer;
