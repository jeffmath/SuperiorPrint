import * as React from "react";
import {
  EntityType,
  FinishingType,
  MaterialType,
  PrinterType,
  SurfaceType,
  UserType,
} from "../entity/EntityType";
import { NamedEntity } from "@/domain/NamedEntity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faPencil, faRemove } from "@fortawesome/free-solid-svg-icons";
import { useAppDispatch, useAppSelector } from "../store/reduxHooks";
import { State } from "../store/State";
import { entitiesService } from "../entities/EntitiesService";
import { getEntitiesSlice } from "../entities/createEntitiesSlice";
import { yesNoDialog } from "../util/yesNoDialog";
import { entitySaved, entitySelectedForEdit } from "./smallSetEntitiesTableSlice";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

export interface SmallSetEntitiesTableProps {
  entityType: EntityType;
  onEntityAdd?: () => void;
}

export function SmallSetEntitiesTable({
  entityType,
  onEntityAdd,
}: SmallSetEntitiesTableProps) {
  const dispatch = useAppDispatch();
  const { isEditingMap } = useAppSelector((state) => getTableState(entityType, state));
  const entities = useAppSelector((state) => getEntities(entityType, state));
  const { actions } = getEntitiesSlice(entityType);

  function handleNameEdit(entity: NamedEntity, newValue: string) {
    dispatch(actions.entityNameEdited({ entityType, entity, name: newValue }));
  }

  function handleSaveClick(entity: NamedEntity) {
    entitiesService.updateEntity(entity, entityType, false).then(() => {
      dispatch(entitySaved(entity.renderKeyString()));
    });
  }

  function handleAddClick() {
    entitiesService.addEntity(entityType).then((entity) => {
      dispatch(actions.entityAdded({ entityType, entity }));
      onEntityAdd?.();
    });
  }

  function handleEditClick(entity: NamedEntity) {
    dispatch(entitySelectedForEdit(entity.renderKeyString()));
  }

  function handleDeleteClick(entity: NamedEntity) {
    yesNoDialog(
      `Are you sure you want to delete this ${entityType.name}?`,
      (yes) => yes && entitiesService.deleteEntity(entity, entityType, false),
    );
  }

  return (
    <div className="text-left">
      <h4 className="form-header">All {entityType.pluralName}</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add {entityType.name}
      </Button>
      <table className="table text-left mt-4">
        <thead>
          <tr>
            <th>Name</th>
            <th className="text-center">Delete</th>
          </tr>
        </thead>
        <tbody>
          {entities.map((entity, i) => (
            <tr key={i}>
              <td className="min-w-80">
                {!isEditingMap[entity.renderKeyString()] ? (
                  <>
                    {entity.renderNameString() || "(No name)"}
                    <Button
                      variant="secondary"
                      size="xs"
                      className="ml-4"
                      onClick={() => handleEditClick(entity)}
                    >
                      <FontAwesomeIcon icon={faPencil} />
                    </Button>
                  </>
                ) : (
                  <div className="flex items-center">
                    <Input
                      value={entity.renderNameString()}
                      required
                      className="form-control inline-block flex-1"
                      onChange={(e) => handleNameEdit(entity, e.currentTarget.value)}
                      onKeyUp={(e) => {
                        if (e.key === "Enter") {
                          e.stopPropagation();
                          handleSaveClick(entity);
                        }
                      }}
                    />
                    <Button
                      size="xs"
                      className="ml-1"
                      onClick={() => handleSaveClick(entity)}
                    >
                      <FontAwesomeIcon icon={faFloppyDisk} size="lg" />
                    </Button>
                  </div>
                )}
              </td>
              <td className="text-center">
                <Button
                  variant="destructive"
                  size="xs"
                  onClick={() => handleDeleteClick(entity)}
                >
                  <FontAwesomeIcon icon={faRemove} />
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

function getEntities(entityType: EntityType, state: State) {
  switch (entityType) {
    case SurfaceType:
      return state.surfaces;
    case MaterialType:
      return state.materials;
    case FinishingType:
      return state.finishings;
    case PrinterType:
      return state.printers;
    case UserType:
      return state.users;
  }
  throw "Invalid (or no) entity-type given";
}

function getTableState(entityType: EntityType, state: State) {
  switch (entityType) {
    case SurfaceType:
      return state.surfacesTableState;
    case MaterialType:
      return state.materialsTableState;
    case FinishingType:
      return state.finishingsTableState;
    case PrinterType:
      return state.printersTableState;
    case UserType:
      return state.usersTableState;
  }
  throw "Invalid (or no) entity-type given";
}
