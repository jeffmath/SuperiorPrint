import { EntityDto } from "./EntityDto";
import { Id } from "../domain/Id";
import { LineItemDto } from "./LineItemDto";

export interface JobDto extends EntityDto {
  number: string;
  clientId: Id;
  propertyId: Id;
  clientContactId?: Id;
  propertyContactId?: Id;
  installContactId?: Id;
  superiorPrintContact1Id?: Id;
  superiorPrintContact2Id?: Id;
  artworkReceivedDate?: Date;
  artworkApprovedDate?: Date;
  shipDate?: Date;
  inHandDate?: Date;
  installDateTime?: Date;
  strikeDateTime?: Date;
  lineItems: LineItemDto[];
}

export type JobStub = EntityDto & Pick<JobDto, "number" | "clientId" | "propertyId">;
