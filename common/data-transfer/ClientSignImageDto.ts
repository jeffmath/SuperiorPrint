import { NamedEntityDto } from "./NamedEntityDto";
import { Id } from "../domain/Id";
import { ImageDto } from "./ImageDto";
import { EntityDto } from "./EntityDto";

export interface ClientSignImageInfoDto extends NamedEntityDto {
  isTallerThanWide: boolean;
}

export interface ClientSignImageDto extends EntityDto, ImageDto {
  clientId: Id;
}
