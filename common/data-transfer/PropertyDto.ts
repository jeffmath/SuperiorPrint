import { NamedEntityDto } from "./NamedEntityDto";
import { LocationDto } from "./LocationDto";

export class PropertyDto extends NamedEntityDto {
  locations!: LocationDto[];
}
