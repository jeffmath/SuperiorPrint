export interface ImageDto {
  name: string;
  imageData: Buffer;
}
