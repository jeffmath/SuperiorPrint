import { EntityDto } from "./EntityDto";

export class NamedEntityDto extends EntityDto {
  name!: string;
}
