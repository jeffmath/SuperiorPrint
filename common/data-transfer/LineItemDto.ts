import { Id } from "../domain/Id";
import { NamedEntityDto } from "./NamedEntityDto";

export interface LineItemDto extends NamedEntityDto {
  code?: string;
  locationId?: Id;
  width?: number;
  height?: number;
  quantity?: number;
  surfaceId?: Id;
  materialId?: Id;
  finishingId?: Id;
  priceEach?: number;
  printerId?: Id;
  artworkReceivedDate?: Date;
  artworkApprovedDate?: Date;
  shipDate?: Date;
  inHandDate?: Date;
  installDateTime?: Date;
  strikeDateTime?: Date;
  installNotes?: string;
  clientSignImageIds: Id[];
}
