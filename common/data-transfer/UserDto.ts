import { NamedEntityDto } from "./NamedEntityDto";

export interface UserDto extends NamedEntityDto {
  salt: string;
  hash: string;
}
