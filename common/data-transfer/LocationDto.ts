import { NamedEntityDto } from "./NamedEntityDto";

export class LocationDto extends NamedEntityDto {
  imageId?: string;
}
