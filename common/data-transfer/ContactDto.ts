import { Id } from "../domain/Id";
import { NamedEntityDto } from "./NamedEntityDto";

export interface ContactDto extends NamedEntityDto {
  phoneNumber?: string;
  clientId?: Id;
  propertyId?: Id;
  isInstaller: boolean;
  isForSuperiorPrint: boolean;
}
