export const currency = (amount: number) => require("numeral")(amount).format("$0,0.00");
