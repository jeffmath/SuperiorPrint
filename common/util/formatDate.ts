import moment from "moment";

export function formatDate(date: Date | undefined): string {
  return date ? moment(date).format("M/D/YYYY").toString() : "";
}

export function formatTime(date: Date | undefined): string {
  return date ? moment(date).format("h:mm a").toString() : "";
}
