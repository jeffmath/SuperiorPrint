export function getStringLines(s: string): string[] {
  return s.split(/\n/g);
}
