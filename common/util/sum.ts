/**
 * Iterates through the given items, applying the given add function to each, to add the
 * item's amount to the running total. The final total is returned.
 */
export function sum<T>(
  items: T[] | undefined,
  add: (item: T, current: number) => number,
): number {
  let result = 0;
  items?.forEach((item) => {
    result = add(item, result);
  });
  return result;
}
