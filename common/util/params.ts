import { Id } from "../domain/Id";

export interface OptionParams {
  value: string;
  label: string;
}

export interface RouteLinkParams {
  toId: Id;
  text: string;
}
