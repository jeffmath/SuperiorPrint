import { NamedEntityDto } from "../data-transfer/NamedEntityDto";
import { OptionParams, RouteLinkParams } from "../util/params";
import { Entity } from "./Entity";
import { Id } from "./Id";

export class NamedEntity extends Entity {
  private name: string;

  constructor(id: Id, name: string) {
    super(id);
    this.name = name;
  }

  static fromDto(dto: NamedEntityDto): NamedEntity {
    return new NamedEntity(dto.id, dto.name);
  }

  renderNameString(): string {
    return this.name.trim();
  }

  renderOptionParams(): OptionParams {
    return {
      value: this.renderKeyString(),
      label: this.name,
    };
  }

  renderRouteLinkParams(): RouteLinkParams {
    return {
      toId: this.renderKeyString(),
      text: this.name,
    };
  }

  renderDto(): NamedEntityDto {
    return {
      ...super.renderDto(),
      name: this.name,
    };
  }

  onNameEdited(newValue: string) {
    this.name = newValue;
  }

  static renderNameStringForId(entities: NamedEntity[], id?: Id): string {
    const entity = id ? entities.find((i) => i.hasSameKey(id)) : undefined;
    return entity?.renderNameString() || "";
  }
}
