import { NamedEntity } from "./NamedEntity";
import { ClientSignImageInfoDto } from "../data-transfer/ClientSignImageDto";

export class ClientSignImageInfo extends NamedEntity {
  isTallerThanWide = false;

  static fromDto(dto: ClientSignImageInfoDto): ClientSignImageInfo {
    const result = new ClientSignImageInfo(dto.id, dto.name);
    result.isTallerThanWide = dto.isTallerThanWide;
    return result;
  }

  renderCssDisplayValue() {
    return this.isTallerThanWide ? "inline-block" : "";
  }
}
