import { Id } from "./Id";
import { NamedEntity } from "./NamedEntity";
import { LocationDto } from "../data-transfer/LocationDto";

export class Location extends NamedEntity {
  private imageId?: Id;

  static fromDto(dto: LocationDto): Location {
    const result = new Location(dto.id, dto.name);
    result.imageId = dto.imageId;
    return result;
  }

  renderDto(): LocationDto {
    return {
      ...super.renderDto(),
      imageId: this.imageId,
    };
  }

  hasImage(): boolean {
    return !!this.imageId;
  }

  renderImageKeyString(): string | undefined {
    return this.imageId;
  }

  onImageIdAssigned(id: Id) {
    this.imageId = id;
  }

  removeImage() {
    this.imageId = undefined;
  }
}
