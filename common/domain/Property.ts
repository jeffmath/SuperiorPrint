import { Location } from "./Location";
import { NamedEntity } from "./NamedEntity";
import { PropertyDto } from "../data-transfer/PropertyDto";
import { OptionParams } from "../util/params";
import { Id } from "./Id";

export class Property extends NamedEntity {
  private locations: Location[] = [];

  static fromDto(dto: PropertyDto): Property {
    const result = new Property(dto.id, dto.name);
    result.locations = dto.locations.map((locationDto) => Location.fromDto(locationDto));
    return result;
  }

  renderDto(): PropertyDto {
    return {
      ...super.renderDto(),
      locations: this.locations.map((location) => location.renderDto()),
    };
  }

  hasLocations(): boolean {
    return !!this.locations.length;
  }

  onLocationNameEdited(location: Location, newName: string) {
    this.locations.find((l) => l.isSameEntity(location))?.onNameEdited(newName);
  }

  onLocationImageSelected(location: Location, imageId: Id) {
    this.locations.find((l) => l.isSameEntity(location))?.onImageIdAssigned(imageId);
  }

  removeLocation(location: Location) {
    const { locations } = this;
    const index = locations.findIndex((l) => l.isSameEntity(location));
    if (index >= 0) locations.splice(index, 1);
    else throw "Location not found in property";
  }

  addLocation(location: Location) {
    this.locations.push(location);
  }

  removeLocationImage(location: Location) {
    this.locations.find((l) => l.isSameEntity(location))?.removeImage();
  }

  mapLocations(callback: (location: Location, index: number) => any) {
    return this.locations.map(callback);
  }

  findLocation(matcher: (location: Location) => boolean): Location | undefined {
    return this.locations.find(matcher);
  }

  renderLocationsOptionParams(): OptionParams[] {
    return this.locations.map((location) => location.renderOptionParams());
  }
}
