export interface JobDates {
  artworkReceivedDate?: Date;
  artworkApprovedDate?: Date;
  shipDate?: Date;
  inHandDate?: Date;
  installDateTime?: Date;
  strikeDateTime?: Date;
}
