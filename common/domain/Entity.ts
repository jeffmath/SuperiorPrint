import { Id } from "./Id";
import { EntityDto } from "../data-transfer/EntityDto";
import { immerable } from "immer";

export class Entity {
  [immerable] = true;

  private readonly id: Id;

  constructor(id: Id) {
    this.id = id;
  }

  renderDto(): EntityDto {
    return { id: this.id };
  }

  isSameEntity(other: Entity): boolean {
    return other.id === this.id;
  }

  hasSameKey(id: string) {
    return id === this.id;
  }

  renderKeyString(): string {
    return this.id;
  }
}
