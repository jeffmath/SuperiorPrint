import { NamedEntity } from "./NamedEntity";
import { ContactDto } from "../data-transfer/ContactDto";
import { Id } from "./Id";

export class Contact extends NamedEntity {
  private phoneNumber?: string;
  private client?: NamedEntity;
  private property?: NamedEntity;
  isInstaller: boolean = false;
  isForSuperiorPrint: boolean = false;

  static fromContactDto(
    dto: ContactDto,
    clients: NamedEntity[],
    properties: NamedEntity[],
  ): Contact {
    const result = new Contact(dto.id, dto.name);
    result.phoneNumber = dto.phoneNumber;
    result.client =
      (dto.clientId && clients.find((c) => c.hasSameKey(dto.clientId!))) || undefined;
    result.property =
      (dto.propertyId && properties.find((p) => p.hasSameKey(dto.propertyId!))) ||
      undefined;
    result.isInstaller = dto.isInstaller;
    result.isForSuperiorPrint = dto.isForSuperiorPrint;
    return result;
  }

  renderDto(): ContactDto {
    return {
      ...super.renderDto(),
      phoneNumber: this.phoneNumber,
      isInstaller: this.isInstaller,
      isForSuperiorPrint: this.isForSuperiorPrint,
      clientId: this.client?.renderKeyString(),
      propertyId: this.property?.renderKeyString(),
    };
  }

  renderType(): string {
    if (this.client) return "Client";
    if (this.property) return "Property";
    if (this.isInstaller) return "Installer";
    if (this.isForSuperiorPrint) return "Superior P&E";
    return "";
  }

  renderPhoneNumberString(): string {
    return this.phoneNumber || "";
  }

  isForClient(client: NamedEntity): boolean {
    return this.client?.isSameEntity(client) || false;
  }

  isForProperty(property: NamedEntity): boolean {
    return this.property?.isSameEntity(property) || false;
  }

  renderClientKeyString(): string {
    return this.client?.renderKeyString() || "";
  }

  renderPropertyKeyString(): string {
    return this.property?.renderKeyString() || "";
  }

  onPhoneNumberEdited(newValue: string) {
    this.phoneNumber = newValue;
  }

  onClientSelected(newValue?: NamedEntity) {
    this.client = newValue;
  }

  onPropertySelected(newValue?: NamedEntity) {
    this.property = newValue;
  }

  static renderPhoneNumberStringForId(contacts: Contact[], id?: Id): string {
    const contact = id ? contacts.find((c) => c.hasSameKey(id)) : undefined;
    return contact?.renderPhoneNumberString() || "";
  }
}
