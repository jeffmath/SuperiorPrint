import { LineItem } from "./LineItem";
import { JobDates } from "./JobDates";
import { JobDto } from "../data-transfer/JobDto";
import { Id } from "./Id";
import { Entity } from "./Entity";
import { Client } from "./Client";
import { formatDate, formatTime } from "../util/formatDate";
import { NamedEntity } from "./NamedEntity";
import { Contact } from "./Contact";
import { sum } from "../util/sum";
import { currency } from "../util/currency";

export class Job extends Entity {
  private number!: string;
  private clientId!: Id;
  private propertyId!: Id;
  private clientContactId?: Id;
  private propertyContactId?: Id;
  private installContactId?: Id;
  private superiorPrintContact1Id?: Id;
  private superiorPrintContact2Id?: Id;
  private artworkReceivedDate?: Date;
  private artworkApprovedDate?: Date;
  private shipDate?: Date;
  private inHandDate?: Date;
  private installDateTime?: Date;
  private strikeDateTime?: Date;
  private lineItems: LineItem[] = [];

  static fromDto(dto: JobDto): Job {
    const result = new Job(dto.id);
    result.number = dto.number;
    result.clientId = dto.clientId;
    result.propertyId = dto.propertyId;
    result.clientContactId = dto.clientContactId;
    result.propertyContactId = dto.propertyContactId;
    result.installContactId = dto.installContactId;
    result.superiorPrintContact1Id = dto.superiorPrintContact1Id;
    result.superiorPrintContact2Id = dto.superiorPrintContact2Id;
    result.artworkReceivedDate = dto.artworkReceivedDate;
    result.artworkApprovedDate = dto.artworkApprovedDate;
    result.shipDate = dto.shipDate;
    result.inHandDate = dto.inHandDate;
    result.installDateTime = dto.installDateTime;
    result.strikeDateTime = dto.strikeDateTime;
    result.lineItems = dto.lineItems.map((dto) => LineItem.fromDto(dto));
    return result;
  }

  hasClient(client: Client | null): boolean {
    return client?.hasSameKey(this.clientId) || false;
  }

  mapLineItems<U>(mapper: (lineItem: LineItem, index: number) => U): U[] {
    return this.lineItems.map(mapper);
  }

  renderDto(): JobDto {
    return {
      ...super.renderDto(),
      number: this.number,
      clientId: this.clientId,
      propertyId: this.propertyId,
      clientContactId: this.clientContactId,
      propertyContactId: this.propertyContactId,
      installContactId: this.installContactId,
      superiorPrintContact1Id: this.superiorPrintContact1Id,
      superiorPrintContact2Id: this.superiorPrintContact2Id,
      artworkReceivedDate: this.artworkReceivedDate,
      artworkApprovedDate: this.artworkApprovedDate,
      shipDate: this.shipDate,
      inHandDate: this.inHandDate,
      installDateTime: this.installDateTime,
      strikeDateTime: this.strikeDateTime,
      lineItems: this.lineItems.map((lineItem) => lineItem.renderDto()),
    };
  }

  produceLineItemWithKey(key: Id): LineItem | undefined {
    return this.lineItems.find((l) => l.hasSameKey(key));
  }

  renderNumberString(): string {
    return this.number.toString();
  }

  renderClientKeyString(): string {
    return this.clientId;
  }

  renderPropertyKeyString(): string {
    return this.propertyId;
  }

  renderTotalPriceString(): string {
    const total = sum(this?.lineItems, (l, current) => l.addTotalPriceToSum(current));
    return currency(total);
  }

  renderClientContactKeyString(): string | undefined {
    return this.clientContactId;
  }

  renderClientContactNameString(contacts: Contact[]): string {
    return NamedEntity.renderNameStringForId(contacts, this.clientContactId);
  }

  renderClientContactPhoneNumberString(contacts: Contact[]): string {
    return Contact.renderPhoneNumberStringForId(contacts, this.clientContactId);
  }

  renderPropertyContactKeyString(): string | undefined {
    return this.propertyContactId;
  }

  renderPropertyContactNameString(contacts: Contact[]): string {
    return NamedEntity.renderNameStringForId(contacts, this.propertyContactId);
  }

  renderPropertyContactPhoneNumberString(contacts: Contact[]): string {
    return Contact.renderPhoneNumberStringForId(contacts, this.propertyContactId);
  }

  renderInstallContactKeyString(): string | undefined {
    return this.installContactId;
  }

  renderInstallContactNameString(contacts: Contact[]): string {
    return NamedEntity.renderNameStringForId(contacts, this.installContactId);
  }

  renderInstallContactPhoneNumberString(contacts: Contact[]): string {
    return Contact.renderPhoneNumberStringForId(contacts, this.installContactId);
  }

  renderSuperiorPrintContact1KeyString(): string | undefined {
    return this.superiorPrintContact1Id;
  }

  renderSuperiorPrintContact1NameString(contacts: Contact[]): string {
    return NamedEntity.renderNameStringForId(contacts, this.superiorPrintContact1Id);
  }

  renderSuperiorPrintContact1PhoneNumberString(contacts: Contact[]): string {
    return Contact.renderPhoneNumberStringForId(contacts, this.superiorPrintContact1Id);
  }

  renderSuperiorPrintContact2KeyString(): string | undefined {
    return this.superiorPrintContact2Id;
  }

  renderSuperiorPrintContact2NameString(contacts: Contact[]): string {
    return NamedEntity.renderNameStringForId(contacts, this.superiorPrintContact2Id);
  }

  renderSuperiorPrintContact2PhoneNumberString(contacts: Contact[]): string {
    return Contact.renderPhoneNumberStringForId(contacts, this.superiorPrintContact2Id);
  }

  renderJobDates(): JobDates {
    return {
      artworkReceivedDate: this.artworkReceivedDate,
      artworkApprovedDate: this.artworkApprovedDate,
      shipDate: this.shipDate,
      inHandDate: this.inHandDate,
      installDateTime: this.installDateTime,
      strikeDateTime: this.strikeDateTime,
    };
  }

  renderArtworkReceivedDateString(): string {
    return formatDate(this.artworkReceivedDate);
  }

  renderArtworkApprovedDateString(): string {
    return formatDate(this.artworkApprovedDate);
  }

  renderShipDateString(): string {
    return formatDate(this.shipDate);
  }

  renderInHandDateString(): string {
    return formatDate(this.inHandDate);
  }

  renderInstallDateString(): string {
    return formatDate(this.installDateTime);
  }

  renderInstallTimeString(): string {
    return formatTime(this.installDateTime);
  }

  renderStrikeDateString(): string {
    return formatDate(this.strikeDateTime);
  }

  renderStrikeTimeString(): string {
    return formatTime(this.strikeDateTime);
  }

  onNumberEdited(newValue: string) {
    this.number = newValue;
  }

  onClientContactSelected(newValue?: Id) {
    this.clientContactId = newValue;
  }

  onPropertyContactSelected(newValue?: Id) {
    this.propertyContactId = newValue;
  }

  onInstallContactSelected(newValue?: Id) {
    this.installContactId = newValue;
  }

  onSuperiorContact1Selected(newValue?: Id) {
    this.superiorPrintContact1Id = newValue;
  }

  onSuperiorContact2Selected(newValue?: Id) {
    this.superiorPrintContact2Id = newValue;
  }

  onArtworkReceivedDateEdited(newValue?: Date) {
    this.artworkReceivedDate = newValue;
  }

  onArtworkApprovedDateEdited(newValue?: Date) {
    this.artworkApprovedDate = newValue;
  }

  onShipDateEdited(newValue?: Date) {
    this.shipDate = newValue;
  }

  onInHandDateEdited(newValue?: Date) {
    this.inHandDate = newValue;
  }

  onInstallDateTimeEdited(newValue?: Date) {
    this.installDateTime = newValue;
  }

  onStrikeDateTimeEdited(newValue?: Date) {
    this.strikeDateTime = newValue;
  }

  onLineItemAdded(lineItem: LineItem) {
    this.lineItems.push(lineItem);
  }

  onLineItemUpdated(lineItem: LineItem) {
    this.lineItems = this.lineItems.map((l) => (l.isSameEntity(lineItem) ? lineItem : l));
  }

  onLineItemDeleted(id: Id) {
    this.lineItems = this.lineItems.filter((l) => !l.hasSameKey(id));
  }
}
