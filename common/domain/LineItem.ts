import { NamedEntity } from "./NamedEntity";
import { Id } from "./Id";
import { Property } from "./Property";
import { ClientSignImageInfo } from "./ClientSignImageInfo";
import { uniq } from "lodash";
import { currency } from "../util/currency";
import { formatDate, formatTime } from "../util/formatDate";
import { getStringLines } from "../util/getStringLines";
import { LineItemDto } from "../data-transfer/LineItemDto";
import { JobDates } from "./JobDates";
import { immerable } from "immer";

export class LineItem extends NamedEntity {
  private code?: string;
  private locationId?: Id;
  private width?: number;
  private height?: number;
  private quantity?: number;
  private surfaceId?: Id;
  private materialId?: Id;
  private finishingId?: Id;
  private priceEach?: number;
  private printerId?: Id;
  private artworkReceivedDate?: Date;
  private artworkApprovedDate?: Date;
  private shipDate?: Date;
  private inHandDate?: Date;
  private installDateTime?: Date;
  private strikeDateTime?: Date;
  private installNotes?: string;
  private clientSignImageIds: Id[] = [];

  static fromDto(dto: LineItemDto): LineItem {
    const result = new LineItem(dto.id, dto.name);
    result.code = dto.code;
    result.locationId = dto.locationId;
    result.width = dto.width;
    result.height = dto.height;
    result.quantity = dto.quantity;
    result.surfaceId = dto.surfaceId;
    result.materialId = dto.materialId;
    result.finishingId = dto.finishingId;
    result.priceEach = dto.priceEach;
    result.printerId = dto.printerId;
    result.artworkReceivedDate = dto.artworkReceivedDate;
    result.artworkApprovedDate = dto.artworkApprovedDate;
    result.shipDate = dto.shipDate;
    result.inHandDate = dto.inHandDate;
    result.installDateTime = dto.installDateTime;
    result.strikeDateTime = dto.strikeDateTime;
    result.installNotes = dto.installNotes;
    result.clientSignImageIds = dto.clientSignImageIds;
    return result;
  }

  renderDto(): LineItemDto {
    const result = {
      ...super.renderDto(),
      ...this,
    };
    delete result[immerable];
    return result;
  }

  renderCodeString(): string {
    return this.code?.trim() || "";
  }

  renderLocationKeyString(): string {
    return this.locationId || "";
  }

  renderWidth(): number | undefined {
    return this.width;
  }

  renderWidthString(): string {
    return this.width?.toString() || "";
  }

  renderHeight(): number | undefined {
    return this.height;
  }

  renderHeightString(): string {
    return this.height?.toString() || "";
  }

  renderPriceEach(): number | undefined {
    return this.priceEach;
  }

  renderPriceEachString(): string {
    return this.priceEach?.toString() || "";
  }

  renderTotalPriceString(): string {
    const { priceEach, quantity } = this;
    return quantity && priceEach ? currency(quantity * priceEach) : "0";
  }

  renderQuantity(): number | undefined {
    return this.quantity;
  }

  renderQuantityString(): string {
    return this.quantity?.toString() || "";
  }

  renderMaterialString(materials: NamedEntity[]): string {
    return NamedEntity.renderNameStringForId(materials, this.materialId);
  }

  renderMaterialKeyString(): string {
    return this.materialId || "";
  }

  renderSurfaceString(surfaces: NamedEntity[]): string {
    return NamedEntity.renderNameStringForId(surfaces, this.surfaceId);
  }

  renderSurfaceKeyString(): string {
    return this.surfaceId || "";
  }

  renderFinishingString(finishings: NamedEntity[]): string {
    return NamedEntity.renderNameStringForId(finishings, this.finishingId);
  }

  renderFinishingKeyString(): string {
    return this.finishingId || "";
  }

  renderPrinterString(printers: NamedEntity[]): string {
    return NamedEntity.renderNameStringForId(printers, this.printerId);
  }

  renderPrinterKeyString(): string {
    return this.printerId || "";
  }

  renderJobDates(): JobDates {
    return {
      artworkReceivedDate: this.artworkReceivedDate,
      artworkApprovedDate: this.artworkApprovedDate,
      shipDate: this.shipDate,
      inHandDate: this.inHandDate,
      installDateTime: this.installDateTime,
      strikeDateTime: this.strikeDateTime,
    };
  }

  renderArtworkReceivedDateString(): string {
    return formatDate(this.artworkReceivedDate);
  }

  renderArtworkApprovedDateString(): string {
    return formatDate(this.artworkApprovedDate);
  }

  renderShipDateString(): string {
    return formatDate(this.shipDate);
  }

  renderInHandDateString(): string {
    return formatDate(this.inHandDate);
  }

  renderInstallDateString(): string {
    return formatDate(this.installDateTime);
  }

  renderInstallTimeString(): string {
    return formatTime(this.installDateTime);
  }

  renderStrikeDateString(): string {
    return formatDate(this.strikeDateTime);
  }

  renderStrikeTimeString(): string {
    return formatTime(this.strikeDateTime);
  }

  renderInstallNotesString(): string {
    return this.installNotes || "";
  }

  renderInstallNotesLines(): string[] {
    return getStringLines(this.installNotes || "");
  }

  renderClientSignImageKeyStrings(): string[] {
    return Array.from(
      { ...this.clientSignImageIds, length: this.quantity },
      (value) => value ?? "",
    );
  }

  onCodeEdited(newValue: string) {
    this.code = newValue;
  }

  onLocationSelected(newValue?: Id) {
    this.locationId = newValue;
  }

  onWidthEdited(newValue: number | undefined) {
    this.width = newValue;
  }

  onHeightEdited(newValue: number | undefined) {
    this.height = newValue;
  }

  onSurfaceSelected(newValue: Id) {
    this.surfaceId = newValue;
  }

  onMaterialSelected(newValue: Id) {
    this.materialId = newValue;
  }

  onFinishingSelected(newValue: Id) {
    this.finishingId = newValue;
  }

  onQuantityEdited(newValue?: number) {
    this.quantity = newValue;
  }

  onPriceEachEdited(newValue?: number) {
    this.priceEach = newValue;
  }

  onPrinterSelected(newValue: Id) {
    this.printerId = newValue;
  }

  onArtworkReceivedDateEdited(newValue?: Date) {
    this.artworkReceivedDate = newValue;
  }

  onArtworkApprovedDateEdited(newValue?: Date) {
    this.artworkApprovedDate = newValue;
  }

  onShipDateEdited(newValue?: Date) {
    this.shipDate = newValue;
  }

  onInHandDateEdited(newValue?: Date) {
    this.inHandDate = newValue;
  }

  onInstallDateTimeEdited(newValue?: Date) {
    this.installDateTime = newValue;
  }

  onStrikeDateEdited(newValue?: Date) {
    this.strikeDateTime = newValue;
  }

  onInstallNotesEdited(newValue: string) {
    this.installNotes = newValue;
  }

  onClientSignImageSelected(newValue: Id, index: number) {
    if (!newValue) {
      this!.clientSignImageIds.splice(index, 1);
    } else {
      this!.clientSignImageIds[index] = newValue;
    }
  }

  onClientSignImagesSelected(values: Id[]) {
    this.clientSignImageIds = values;
  }

  addTotalPriceToSum(sum: number): number {
    const { priceEach, quantity } = this;
    return sum + (quantity && priceEach ? quantity * priceEach : 0);
  }

  findLocationInProperty(property: Property) {
    return this.locationId
      ? property.findLocation((location) => location.hasSameKey(this.locationId!))
      : undefined;
  }

  getDistinctClientSignImages(fromImages: ClientSignImageInfo[]): ClientSignImageInfo[] {
    // create a list of the client-sign-image IDs for this
    // line-item where all the empty image slots are removed
    const emptiesRemoved = this.clientSignImageIds.filter((slot) => slot != null);

    // remove all duplicate client-sign-image IDs from the above list
    const duplicatesRemoved = uniq(emptiesRemoved);

    // map each client-sign-image ID in the above list to the
    // corresponding element in the given list of client-sign-images
    return duplicatesRemoved
      .map((signImageId) =>
        fromImages.find((image) => image.renderKeyString() === signImageId),
      )
      .filter((image): image is ClientSignImageInfo => !!image);
  }
}
