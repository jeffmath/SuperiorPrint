July 11th, 2024

#### On the relative likelihood of bugs between the two versions of the application

Both versions of the application have a potential for bugs caused by the use of stale
entity data.  That data consists of sets of entities which are cached on the client
because they are not expected to change often, so we wish to avoid fetching and
re-fetching them from every part of the application in which they are employed.  Because
most of the time, the cached version will still be correct.

With the older version of the application, all the cached data is stored together in the
same global, Redux-managed state atom, along with lots of other data.  In this form, it is
not obvious which portions of the data in the atom represent a cache.  For those that do,
there is no directly obvious means to invalidate them and refetch their updated data from
the server.  As such, when the user makes a change to a cached data item, rather than
invalidating the cache, the client code often modifies the cached data manually in
addition to sending the update to the server.  That these modifications must occur through
Redux actions only adds indirection and therefore, complexity.  It all leads to
significantly more effort being required to reconcile the cached data. Thus, it was
unappealing to consider the many potential bug scenarios in which the user changes data
which is cached for use in other parts of the app.  Therefore, many of these scenarios
were left unaddressed.

In contrast, with the newer, Next-based version of the application, no global client state
atom is employed.  While many of the same sets of entities are still cached on the client
side, those caches are provided to the React components through individual React hooks
which contain a method to refetch their contents.  With this arrangement, it is much more
obvious to the observer which data sets are being cached, as well as how to update those
caches.  When it takes a mere method call to refresh cached data, it becomes much easier
to address a vast majority the scenarios where the user is editing cached data which must
then be reconciled.  As a result, the newer version of the application has much better
coverage against bugs caused by stale cached entity data, when changes to the cached data
have been made by the current user.

For both versions, changes to cached entity data made by other users are not reported
through push notifications to the client app.  Therefore, both versions are susceptible to
bugs caused by the use of cached entity data which has been made stale due to the edits
performed by other users.
