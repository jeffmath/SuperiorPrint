import type { NextAuthConfig } from "next-auth";

export const authConfig = {
  pages: {
    signIn: "/login",
  },
  callbacks: {
    authorized({ auth }) {
      return !!auth?.user;
    },
    jwt({ token, user }) {
      // if available, store the user-ID in the token, so it is available during the
      // session() callback, below
      if (user) {
        token.id = user.id;
      }
      return token;
    },
    session({ session, token }) {
      // if available, transfer the user-ID from the token to the session, so that
      // it is available within the now-extended session object
      if (token?.id) {
        session.user.id = token.id as string;
      }
      return session;
    },
    // The code for this method comes from
    // https://github.com/nextauthjs/next-auth/discussions/3748#discussioncomment-7373546
    redirect({ url, baseUrl }: { url: string; baseUrl: string }) {
      const isRelativeUrl = url.startsWith("/");
      if (isRelativeUrl) {
        return `${baseUrl}${url}`;
      }

      const isSameOriginUrl = new URL(url).origin === baseUrl;
      const alreadyRedirected = url.includes("callbackUrl=");
      if (isSameOriginUrl && alreadyRedirected) {
        return decodeURIComponent(url.split("callbackUrl=")[1]);
      }

      if (isSameOriginUrl) {
        return url;
      }

      return baseUrl;
    },
  },
  providers: [],
  trustHost: true,
} satisfies NextAuthConfig;
