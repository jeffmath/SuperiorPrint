"use client";

import * as React from "react";
import { useMemo, useRef, useState } from "react";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { NamedEntity } from "@/domain/NamedEntity";
import {
  ClientType,
  ContactType,
  getEntityTypeWithName,
  PropertyType,
} from "@/app/lib/entityType";
import { BackButton } from "@/app/ui/general/backButton";
import { updateNamedEntity } from "@/app/lib/data/namedEntities/updateNamedEntity";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { deleteNamedEntity } from "@/app/lib/data/namedEntities/deleteNamedEntity";
import { useRouter } from "next/navigation";
import { errorModal } from "@/app/ui/modals/errorModal";
import { ContactSubform, ContactSubformMethods } from "@/app/ui/entity/contactSubform";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Contact } from "@/domain/Contact";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { useInstallContacts } from "@/app/lib/hooks/useInstallContacts";
import { useSuperiorContacts } from "@/app/lib/hooks/useSuperiorContacts";

export interface EntityFormProps {
  entityTypeName: string;
  entityDto: NamedEntityDto;
}

export function EntityForm({ entityTypeName, entityDto }: EntityFormProps) {
  const entityType = getEntityTypeWithName(entityTypeName);
  const isForClient = entityType === ClientType;
  const isForProperty = entityType === PropertyType;
  const isForContact = entityType === ContactType;
  const { allClients, refetch: refetchClients } = useAllClientStubs();
  const { allProperties, refetch: refetchProperties } = useAllPropertyStubs();
  const { refetch: refetchInstallContacts } = useInstallContacts();
  const { refetch: refetchSuperiorContacts } = useSuperiorContacts();
  const entity = useMemo(
    () =>
      isForContact
        ? Contact.fromContactDto(entityDto as ContactDto, allClients, allProperties)
        : NamedEntity.fromDto(entityDto),
    [entityDto, isForContact, allClients, allProperties],
  );
  const [name, setName] = useState(entity.renderNameString());
  const { push } = useRouter();
  const contactSubformRef = useRef<ContactSubformMethods>();

  function handleNameChange(value: string) {
    setName(value);
  }

  async function handleSaveClick() {
    if (isForContact) {
      const canSave = await contactSubformRef.current?.onSave();
      if (!canSave) return;
    }
    entity.onNameEdited(name);
    await updateNamedEntity(entity.renderDto(), entityType.name);
    push(`/entities/${entityType.pluralName}`);
    if (isForClient) await refetchClients();
    if (isForProperty) await refetchProperties();
    if (isForContact) {
      await refetchInstallContacts();
      await refetchSuperiorContacts();
    }
  }

  async function handleDeleteClick() {
    const confirmed = await yesNoModal(
      `Are you sure you want to delete this ${entityType.name}?`,
    );
    if (confirmed) {
      const result = await deleteNamedEntity(entityType.name, entity.renderKeyString());
      if (result.success) push(`/entities/${entityType.pluralName}`);
      else await errorModal("Delete failed", result.failureReason || "");
    }
  }

  return (
    <>
      <h4 className="form-header">
        Edit {entityType.name}
        <BackButton />
      </h4>
      <div className="mt-2">
        <Label htmlFor="entityName">Name</Label>
        <Input
          id="entityName"
          className="w-64"
          value={name}
          onChange={(e) => handleNameChange(e.currentTarget.value)}
        />
      </div>
      {entityType === ContactType && (
        <ContactSubform
          ref={contactSubformRef}
          contact={entity as Contact}
          allClients={allClients}
          allProperties={allProperties}
        />
      )}
      <div className="mt-4">
        <Button onClick={handleSaveClick} disabled={!name}>
          Save
        </Button>
        <Button
          variant="destructive"
          size="sm"
          className="ml-7"
          onClick={handleDeleteClick}
        >
          Delete
        </Button>
      </div>
    </>
  );
}
