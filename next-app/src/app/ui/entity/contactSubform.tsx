import * as React from "react";
import { forwardRef, useImperativeHandle, useState } from "react";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { Contact } from "@/domain/Contact";
import { fetchJobStubsReferencingEntity } from "@/app/lib/data/jobs/fetchJobStubsReferencingEntity";
import { errorModal } from "@/app/ui/modals/errorModal";
import { createJobsList } from "@/app/lib/data/jobs/createJobsList";

export interface ContactSubformProps {
  contact: Contact;
  allClients: NamedEntity[];
  allProperties: NamedEntity[];
}

export interface ContactSubformMethods {
  onSave(): Promise<boolean>;
}

export const ContactSubform = forwardRef(function ContactSubform(
  { contact, allClients, allProperties }: ContactSubformProps,
  ref,
) {
  const [phoneNumber, setPhoneNumber] = useState(contact.renderPhoneNumberString());
  const [isInstaller, setIsInstaller] = useState(contact.isInstaller);
  const [isForSuperiorPrint, setIsForSuperiorPrint] = useState(
    contact.isForSuperiorPrint,
  );
  const [clientId, setClientId] = useState<Id | undefined>(
    contact.renderClientKeyString(),
  );
  const [propertyId, setPropertyId] = useState<Id | undefined>(
    contact.renderPropertyKeyString(),
  );

  async function checkWhetherCanSave(): Promise<boolean> {
    if (
      (!isInstaller && contact.isInstaller) ||
      (!isForSuperiorPrint && contact.isForSuperiorPrint) ||
      (!clientId && contact.renderClientKeyString()) ||
      (!propertyId && contact.renderPropertyKeyString())
    ) {
      const jobStubs = await fetchJobStubsReferencingEntity(contact.renderKeyString());
      if (jobStubs.length) {
        await errorModal(
          "Cannot update",
          `Removals of this contact's role status or client/property membership cannot be stored while this contact is being referenced by the following jobs: ${createJobsList(
            jobStubs,
          )}`,
        );
        return false;
      }
    }
    return true;
  }

  async function onSave(): Promise<boolean> {
    if (!(await checkWhetherCanSave())) return false;

    contact.onPhoneNumberEdited(phoneNumber);
    contact.isInstaller = isInstaller;
    contact.isForSuperiorPrint = isForSuperiorPrint;
    contact.onClientSelected(
      clientId ? allClients.find((c) => c.hasSameKey(clientId)) : undefined,
    );
    contact.onPropertySelected(
      propertyId ? allProperties.find((p) => p.hasSameKey(propertyId)) : undefined,
    );
    return true;
  }

  useImperativeHandle(ref, (): ContactSubformMethods => ({ onSave }));

  function handlePhoneNumberChange(value: string) {
    setPhoneNumber(value);
  }

  function handleIsInstallerChange(value: boolean) {
    setIsInstaller(value);
  }

  function handleIsForSuperiorPrint(value: boolean) {
    setIsForSuperiorPrint(value);
  }

  function handleClientChange(value?: Id) {
    setClientId(value);
  }

  function handlePropertyChange(value?: Id) {
    setPropertyId(value);
  }

  return (
    <div className="flex flex-col space-y-4 mt-4">
      <div>
        <Label htmlFor="phoneNumber">Phone number</Label>
        <Input
          id="phoneNumber"
          className="w-36"
          value={phoneNumber}
          onChange={(e) => handlePhoneNumberChange(e.currentTarget.value)}
        />
      </div>
      <div className="flex items-center space-x-1">
        <Checkbox
          id="installerCheckbox"
          checked={isInstaller}
          onCheckedChange={handleIsInstallerChange}
        />
        <Label htmlFor="installerCheckbox">Is installer</Label>
      </div>
      <div className="flex items-center space-x-1">
        <Checkbox
          id="superiorCheckbox"
          checked={isForSuperiorPrint}
          onCheckedChange={handleIsForSuperiorPrint}
        />
        <Label htmlFor="superiorCheckbox">Works for Superior Print</Label>
      </div>
      <div>
        <Label htmlFor="clientSelect">Is with client</Label>
        <div className="flex flex-row items-center space-x-2">
          <Select value={clientId} onValueChange={handleClientChange}>
            <SelectTrigger id="clientSelect" className="w-72">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {allClients.map((client, i) => {
                const params = client.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
          <Button
            variant="secondary"
            size="xs"
            className="ml-2"
            onClick={() => handleClientChange(undefined)}
          >
            <FontAwesomeIcon icon={faRemove} />
          </Button>
        </div>
      </div>
      <div>
        <Label htmlFor="propertySelect">Is with property</Label>
        <div className="flex flex-row items-center space-x-2">
          <Select value={propertyId} onValueChange={handlePropertyChange}>
            <SelectTrigger id="propertySelect" className="w-72">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {allProperties.map((property, i) => {
                const params = property.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
          <Button
            variant="secondary"
            size="xs"
            onClick={() => handlePropertyChange(undefined)}
          >
            <FontAwesomeIcon icon={faRemove} />
          </Button>
        </div>
      </div>
    </div>
  );
});
