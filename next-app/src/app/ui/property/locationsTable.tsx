"use client";

import * as React from "react";
import { Location } from "@/domain/Location";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPencil,
  faRemove,
  faFloppyDisk,
  faCancel,
} from "@fortawesome/free-solid-svg-icons";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { useMemo, useState } from "react";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { Property } from "@/domain/Property";
import { ImageModal } from "@/app/ui/modals/imageModal";
import { ImageUploadModal } from "@/app/ui/modals/imageUploadModal";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { useRouter } from "next/navigation";
import { updatePropertyLocationName } from "@/app/lib/data/properties/updatePropertyLocationName";
import { NamedEntity } from "@/domain/NamedEntity";
import { createPropertyLocation } from "@/app/lib/data/properties/createPropertyLocation";
import { deletePropertyLocationImage } from "@/app/lib/data/properties/deletePropertyLocationImage";
import { createPropertyLocationImage } from "@/app/lib/data/properties/createPropertyLocationImage";
import { deletePropertyLocation } from "@/app/lib/data/properties/deletePropertyLocation";

export interface LocationsTableProps {
  propertyDto: PropertyDto;
}

export function LocationsTable({ propertyDto }: LocationsTableProps) {
  const [isEditingMap, setIsEditingMap] = useState<{ [id: string]: boolean }>({});
  const [nameMap, setNameMap] = useState<{ [id: string]: string | undefined }>({});
  const [locationForImageView, setLocationForImageView] = useState<
    Location | undefined
  >();
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isImageUploadModalOpen, setIsImageUploadModalOpen] = useState(false);
  const [locationForImageUpload, setLocationForImageUpload] = useState<
    Location | undefined
  >();
  const { refresh } = useRouter();

  const property = useMemo(() => Property.fromDto(propertyDto), [propertyDto]);
  const urlForImageView =
    locationForImageView &&
    `/api/locationImage/${locationForImageView.renderImageKeyString()}`;

  function handleNameChange(location: Location, name: string) {
    setNameMap({ ...nameMap, [location.renderKeyString()]: name });
  }

  async function handleSaveClick(location: Location) {
    const locationId = location.renderKeyString();
    const key = location.renderKeyString();
    const newName = nameMap[key]!;
    await updatePropertyLocationName(property.renderKeyString(), locationId, newName);
    location.onNameEdited(newName);
    setIsEditingMap({ ...isEditingMap, [key]: false });
  }

  async function handleAddClick() {
    const locationDto = await createPropertyLocation(property.renderKeyString());
    const location = Location.fromDto(locationDto);
    refresh();
    handleEditClick(location);
  }

  function handleEditClick(location: Location) {
    const key = location.renderKeyString();
    setIsEditingMap({ ...isEditingMap, [key]: true });
    setNameMap({
      ...nameMap,
      [key]: location.renderNameString(),
    });
  }

  function handleViewImageClick(location: Location) {
    setLocationForImageView(location);
    setIsImageModalOpen(true);
  }

  function handleImageModalClosed() {
    setIsImageModalOpen(false);
  }

  async function handleDeleteImageClick(location: Location) {
    const confirmed = await yesNoModal(
      "Are you sure you want to delete this location's image?",
    );
    if (confirmed) {
      await deletePropertyLocationImage(
        property.renderKeyString(),
        location.renderKeyString(),
      );
      refresh();
    }
  }

  function handleUploadImageClick(location: Location) {
    setLocationForImageUpload(location);
    setIsImageUploadModalOpen(true);
  }

  async function handleUploadFileSelected(formData: FormData) {
    const imageId = await createPropertyLocationImage(
      property.renderKeyString(),
      locationForImageUpload!.renderKeyString(),
      formData,
    );
    return [imageId];
  }

  function handleImagesUploaded() {
    refresh();
  }

  function handleImageUploadModalClose() {
    setIsImageUploadModalOpen(false);
  }

  function handleCancelEditClick(entity: NamedEntity) {
    const key = entity.renderKeyString();
    setIsEditingMap({ ...isEditingMap, [key]: false });
    setNameMap({ ...nameMap, [key]: undefined });
  }

  async function handleDeleteClick(location: Location) {
    const confirmed = await yesNoModal("Are you sure you want to delete this location?");
    if (confirmed) {
      await deletePropertyLocation(
        property.renderKeyString(),
        location.renderKeyString(),
      );
      refresh();
    }
  }

  return (
    <div className="text-left mt-8">
      <h4 className="form-header inline-block">Locations</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add location
      </Button>
      {property.hasLocations() && (
        <table className="table text-left mt-4">
          <thead>
            <tr>
              <th>Name</th>
              <th className="text-center">Image</th>
              <th className="text-center">Delete</th>
            </tr>
          </thead>
          <tbody>
            {property?.mapLocations((location, i) => {
              const id = location.renderKeyString();
              const isEditing = isEditingMap[id];
              const name = isEditing ? nameMap[id] : location.renderNameString();
              const imageId = location.renderImageKeyString();
              return (
                <tr key={i}>
                  <td className="min-w-80">
                    {!isEditing ? (
                      <>
                        <span>{name || "(No name)"}</span>
                        <Button
                          variant="secondary"
                          size="xs"
                          className="ml-4"
                          onClick={() => handleEditClick(location)}
                        >
                          <FontAwesomeIcon icon={faPencil} />
                        </Button>
                      </>
                    ) : (
                      <div className="flex items-center">
                        <Input
                          value={name}
                          required
                          autoFocus={true}
                          className="form-control inline-block flex-1"
                          onChange={(e) =>
                            handleNameChange(location, e.currentTarget.value)
                          }
                          onKeyUp={async (e) => {
                            if (
                              e.key === "Enter" &&
                              name?.length &&
                              name !== location.renderNameString()
                            ) {
                              e.stopPropagation();
                              await handleSaveClick(location);
                            }
                          }}
                        />
                        <Button
                          size="xs"
                          className="ml-1"
                          disabled={!name?.length || name === location.renderNameString()}
                          onClick={() => handleSaveClick(location)}
                        >
                          <FontAwesomeIcon icon={faFloppyDisk} size="sm" />
                        </Button>
                        <Button
                          size="xs"
                          variant="secondary"
                          className="ml-1"
                          onClick={() => handleCancelEditClick(location)}
                        >
                          <FontAwesomeIcon icon={faCancel} size="lg" />
                        </Button>
                      </div>
                    )}
                  </td>
                  <td className="h-full">
                    <div className="min-w-48 flex flex-row space-x-2 items-center justify-center">
                      {imageId && (
                        <Button size="xs" onClick={() => handleViewImageClick(location)}>
                          View
                        </Button>
                      )}
                      {imageId && (
                        <Button
                          variant="destructive"
                          size="xs"
                          onClick={() => handleDeleteImageClick(location)}
                        >
                          <FontAwesomeIcon icon={faRemove} />
                        </Button>
                      )}
                      <Button
                        variant="secondary"
                        size="xs"
                        onClick={() => handleUploadImageClick(location)}
                      >
                        Upload
                      </Button>
                    </div>
                  </td>
                  <td className="text-center">
                    <Button
                      variant="destructive"
                      size="xs"
                      onClick={() => handleDeleteClick(location)}
                    >
                      <FontAwesomeIcon icon={faRemove} />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      <ImageModal
        isOpen={isImageModalOpen}
        imageName={locationForImageView?.renderNameString()}
        imageUrl={urlForImageView}
        onClose={handleImageModalClosed}
      />
      <ImageUploadModal
        isOpen={isImageUploadModalOpen}
        isForMultiple={false}
        onFilesSelected={handleUploadFileSelected}
        onUploaded={handleImagesUploaded}
        onRequestClose={handleImageUploadModalClose}
      />
    </div>
  );
}
