import * as React from "react";
import { modalStyle } from "./modalStyle";
import Modal from "react-modal";

export interface ImageModalProps {
  isOpen: boolean;
  imageName?: string;
  imageUrl?: string;
  onClose: () => void;
}

export function ImageModal({ isOpen, imageName, imageUrl, onClose }: ImageModalProps) {
  return (
    <Modal
      isOpen={isOpen}
      contentLabel="Modal"
      style={modalStyle}
      onRequestClose={onClose}
      ariaHideApp={false}
    >
      <div className="modal-header">
        <h4 className="modal-title inline-block">{imageName}</h4>
      </div>
      <div className="modal-body text-center">
        {imageUrl && (
          <img
            src={imageUrl}
            style={{ maxWidth: "600px", maxHeight: "600px" }}
            alt="image"
          />
        )}
      </div>
    </Modal>
  );
}
