import Swal from "sweetalert2";

export async function yesNoModal(text: string, title: string = ""): Promise<boolean> {
  const config = {
    title: title,
    text: text,
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No",
  };
  return Swal.fire(config).then((result) => result.isConfirmed);
}
