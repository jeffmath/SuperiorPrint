import * as React from "react";
import { useState } from "react";
import { modalStyle } from "./modalStyle";
import { Id } from "@/domain/Id";
import ReactModal from "react-modal";

// an old version of this package is used here because the newer ones no longer provide
// image-preview thumbnails
const Dropzone = require("react-dropzone");

export interface ImageUploadModalProps {
  isForMultiple: boolean;
  isOpen: boolean;
  onFilesSelected: (formData: FormData) => Promise<Id[]>;
  onRequestClose: () => void;
  onUploaded: (imageIds: Id[]) => void;
}

export function ImageUploadModal({
  isForMultiple,
  isOpen,
  onFilesSelected,
  onRequestClose,
  onUploaded,
}: ImageUploadModalProps) {
  const [files, setFiles] = useState([] as File[]);
  const [isUploaded, setIsUploaded] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>(undefined);
  const [wasOpen, setWasOpen] = useState(false);

  // reset this modal's state each time it is opened
  if (isOpen !== wasOpen) {
    setWasOpen(isOpen);
    if (isOpen) {
      setFiles([]);
      setIsUploaded(false);
      setErrorMessage(undefined);
      return null;
    }
  }

  async function uploadImages(files: File[]) {
    // if no files were accepted, don't do anything
    if (!files || !files.length) return;

    setFiles(files);
    const formData = new FormData();
    files.forEach((file) => formData.append("files", file, file.name));
    const ids = await onFilesSelected(formData);

    setIsUploaded(true);
    onUploaded(ids);
  }

  return (
    <ReactModal
      isOpen={isOpen}
      contentLabel="Modal"
      style={modalStyle}
      ariaHideApp={false}
      onRequestClose={onRequestClose}
      shouldCloseOnOverlayClick={true}
    >
      <div className="modal-header">
        <h4 className="modal-title inline-block">
          Upload image{isForMultiple ? "s" : ""}
        </h4>
      </div>
      <form name="uploadForm" className="modal-body text-left">
        {!files.length && (
          <Dropzone onDrop={uploadImages} multiple={isForMultiple} accept="image/*">
            <div>
              Drag {filesNoun(isForMultiple)} here, or click to select{" "}
              {filesNoun(isForMultiple)} to upload.
            </div>
          </Dropzone>
        )}
        {files.length > 0 && (
          <div>
            File{isForMultiple ? "s" : ""}:
            <ul>
              {files.map((f, i) => (
                <li key={i} style={{ fontSize: "small" }}>
                  {f.name}
                  <br />
                  <img src={(f as any).preview} className="size-12" alt="image" />
                </li>
              ))}
            </ul>
            <span className="error">{errorMessage}</span>
            {isUploaded && "Success!"}
          </div>
        )}
      </form>
    </ReactModal>
  );
}

function filesNoun(isForMultiple: boolean) {
  return isForMultiple ? "files" : "a file";
}
