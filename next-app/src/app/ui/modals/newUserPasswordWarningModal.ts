import Swal from "sweetalert2";

export async function newUserPasswordWarningModal() {
  const message = `The new user has a default password of "spae16".  This represents a security vulnerability. Please have the new user change their password as soon as is possible.`;
  const config = {
    title: "Warning",
    html: message,
    showCancelButton: false,
    confirmButtonText: "Ok",
  };
  await Swal.fire(config);
}
