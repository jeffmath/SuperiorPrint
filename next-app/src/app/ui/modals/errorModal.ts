import Swal from "sweetalert2";

export function onError(error: any, text: string) {
  const message =
    error.response?.body ||
    error.response?.text?.substring(0, error.response.text.indexOf("<br>")) ||
    error.message;
  console.log(text + (message ? ": " + message : ""));
  errorModal(text, message);
}

export async function errorModal(text: string, message: string) {
  const config = {
    title: "Error",
    html: text + (message ? ":<br/> " + message : ""),
    showCancelButton: false,
    confirmButtonText: "Ok",
  };
  await Swal.fire(config);
}
