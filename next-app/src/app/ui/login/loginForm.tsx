"use client";

import * as React from "react";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { useState } from "react";
import { authenticate } from "@/app/lib/login/authenticate";
import { useFormState } from "react-dom";

export function LoginForm() {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const isLoginInfoInvalid = !userName.length || !password.length;

  const [errorMessage, formAction, isPending] = useFormState(authenticate, undefined);

  return (
    <form action={formAction}>
      <div className="mt-2">
        <Label htmlFor="userName">User name</Label>
        <Input
          id="userName"
          type="text"
          name="userName"
          className="w-36"
          value={userName}
          onChange={(e) => setUserName(e.currentTarget.value)}
        />
      </div>
      <div className="mt-2">
        <Label htmlFor="password">Password</Label>
        <Input
          id="password"
          type="password"
          name="password"
          className="w-36"
          value={password}
          onChange={(e) => setPassword(e.currentTarget.value)}
        />
      </div>
      <Button
        className="mt-4"
        disabled={isLoginInfoInvalid || isPending}
        aria-disabled={isLoginInfoInvalid || isPending}
      >
        Log in
      </Button>
      {errorMessage && <p className="mt-1.5 error">{errorMessage}</p>}
    </form>
  );
}
