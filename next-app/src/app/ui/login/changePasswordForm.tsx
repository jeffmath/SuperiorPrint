"use client";

import * as React from "react";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { useState } from "react";
import { changePassword } from "@/app/lib/data/users/changePassword";

export function ChangePasswordForm() {
  const [password, setPassword] = useState("");
  const [reenteredPassword, setReenteredPassword] = useState("");
  const [isPasswordChanged, setIsPasswordChanged] = useState(false);

  if (isPasswordChanged && password !== reenteredPassword) setIsPasswordChanged(false);

  async function handleChangePasswordClick() {
    await changePassword(password);
    setIsPasswordChanged(true);
  }

  return (
    <div className="text-left">
      <h4 className="form-header">Change password</h4>
      <p>Your password entries must match, and be at least five characters long.</p>
      <div className="mt-2">
        <Label htmlFor="newPassword">Password</Label>
        <Input
          id="newPassword"
          name="newPassword"
          type="password"
          className="w-56"
          value={password}
          onChange={(e) => setPassword(e.currentTarget.value)}
        />
      </div>
      <div className="mt-2">
        <Label htmlFor="reenteredPassword">Re-enter password</Label>
        <Input
          id="reenteredPassword"
          type="password"
          className="w-56"
          value={reenteredPassword}
          onChange={(e) => setReenteredPassword(e.currentTarget.value)}
        />
      </div>
      {!isPasswordChanged && (
        <Button
          className="mt-4"
          onClick={handleChangePasswordClick}
          disabled={password.length < 5 || password !== reenteredPassword}
        >
          Change password
        </Button>
      )}
      {isPasswordChanged && <p className="mt-1.5">Password successfully changed.</p>}
    </div>
  );
}
