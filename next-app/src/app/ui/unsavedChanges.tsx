"use client";

import { createContext, Dispatch, SetStateAction, useState } from "react";

export type UnsavedSection = "jobDetails" | "lineItemDetails";

export const UnsavedChangesContext = createContext<
  [
    unsavedSections: UnsavedSection[],
    setUnsavedSections: Dispatch<SetStateAction<UnsavedSection[]>>,
  ]
>([[], () => []]);

export function UnsavedChangesProvider({ children }: { children: React.ReactNode }) {
  const [unsavedSections, setUnsavedSections] = useState<UnsavedSection[]>([]);
  return (
    <UnsavedChangesContext.Provider value={[unsavedSections, setUnsavedSections]}>
      {children}
    </UnsavedChangesContext.Provider>
  );
}
