import React from "react";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";

export function BackButton() {
  const { back } = useRouter();
  return (
    <Button variant="secondary" size="xs" className="ml-4" onClick={back}>
      <FontAwesomeIcon icon={faArrowLeft} />
    </Button>
  );
}
