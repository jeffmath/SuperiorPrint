import * as React from "react";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { ReactNode } from "react";
import { OptionParams } from "@/util/params";
import { Control, Controller, FieldValues, Path } from "react-hook-form";

export interface SelectFieldProps<V extends FieldValues> {
  control: Control<V>;
  fieldName: keyof V;
  label: string;
  className?: string;
  id: Id;
  options: OptionParams[];
  suppliedButton?: ReactNode;
}

export function SelectField<V extends FieldValues>({
  control,
  fieldName,
  label,
  className,
  id,
  options,
  suppliedButton,
}: SelectFieldProps<V>) {
  return (
    <div className="relative">
      <div className="inline-block">
        <Label htmlFor={id}>{label}</Label>
        {control && (
          <Controller
            name={fieldName as Path<V>}
            control={control}
            render={({ field }) => {
              const { ref, ...fieldExRef } = field;
              return (
                <Select
                  {...fieldExRef}
                  value={field.value as string}
                  onValueChange={field.onChange}
                >
                  <SelectTrigger id={id} className={className || "min-w-24"}>
                    <SelectValue />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value="">&nbsp;</SelectItem>
                    {(options || []).map((option, i) => (
                      <SelectItem key={i} value={option.value}>
                        {option.label}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
              );
            }}
          />
        )}
      </div>
      {suppliedButton || <></>}
    </div>
  );
}
