import * as React from "react";
import "react-datetime/css/react-datetime.css";

import Datetime from "react-datetime";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { useState } from "react";
import { Control, Controller, FieldValues, Path } from "react-hook-form";

export interface DateTimeFieldProps<V extends FieldValues> {
  labelText: string;
  shouldShowTime?: boolean;
  control: Control<V>;
  fieldName: keyof V;
}

export function DateTimeField<V extends FieldValues>({
  shouldShowTime,
  labelText,
  control,
  fieldName,
}: DateTimeFieldProps<V>) {
  const [isDatePickerOpen, setIsDatePickerOpen] = useState(false);

  return (
    <div>
      <Label htmlFor="dateInput">{labelText}</Label>
      {control && (
        <Controller
          name={fieldName as Path<V>}
          control={control}
          render={({ field }) => {
            const { ref, ...fieldExRef } = field;
            return (
              <Datetime
                {...fieldExRef}
                value={field.value || ""}
                timeFormat={shouldShowTime || false}
                renderInput={(props, openCalendar, closeCalendar) => {
                  return (
                    <Input
                      {...props}
                      className={shouldShowTime ? "w-40" : "w-28"}
                      onClick={() => {
                        if (!isDatePickerOpen) openCalendar();
                        else closeCalendar();
                        setIsDatePickerOpen(!isDatePickerOpen);
                      }}
                      onBlur={() => setIsDatePickerOpen(false)}
                    ></Input>
                  );
                }}
                onChange={(value) => {
                  const translatedValue =
                    typeof value === "string"
                      ? value
                        ? Date.parse(value)
                        : null
                      : value?.toDate();
                  field.onChange(translatedValue);
                }}
              />
            );
          }}
        />
      )}
    </div>
  );
}
