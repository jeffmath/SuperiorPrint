import * as React from "react";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { FieldError, RegisterOptions } from "react-hook-form";

export interface InputFieldProps {
  label: string;
  id: Id;
  className: string;
  rules?: RegisterOptions;

  /**
   * A true value here instructs the field to display an asterisk after its label.
   */
  isRequired?: boolean;

  error?: FieldError;
}

export const InputField = React.forwardRef<HTMLInputElement, InputFieldProps>(
  (
    { label, id, className, rules, isRequired, error, ...props }: InputFieldProps,
    ref,
  ) => {
    return (
      <div>
        <Label htmlFor={id}>
          {label}
          {isRequired && "*"}
        </Label>
        <Input
          id={id}
          className={className}
          ref={ref}
          aria-invalid={error ? "true" : "false"}
          {...props}
        />
        {error && (
          <div className="text-sm error">
            {error?.type === "required" && <div>{label} is required</div>}
            {error?.type === "maxLength" && <div>Too long</div>}
            {error?.type === "pattern" && <div>Invalid input</div>}
            {error?.type === "min" && (
              <div>{typeof rules?.min === "number" && `Must be >= ${rules?.min}`}</div>
            )}
            {error?.type === "max" && (
              <div>{typeof rules?.max === "number" && `Must be <= ${rules?.max}`}</div>
            )}
          </div>
        )}
      </div>
    );
  },
);

export const intRegex = /^\d+$/;
export const floatOrIntRegex = /^\d+(\.\d{0,4})?$/;
export const moneyRegex = /^\d+(\.\d{0,2})?$/;
