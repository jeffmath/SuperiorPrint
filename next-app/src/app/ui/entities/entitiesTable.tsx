"use client";

import { Contact } from "@/domain/Contact";
import { cn } from "@/lib/utils";
import { NamedEntity } from "@/domain/NamedEntity";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { useMemo } from "react";
import { ContactType, getEntityTypeWithName } from "@/app/lib/entityType";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Button } from "@/components/ui/button";
import { createNamedEntity } from "@/app/lib/data/namedEntities/createNamedEntity";
import { Id } from "@/domain/Id";
import { useRouter } from "next/navigation";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";

export interface EntitiesTableProps {
  entityDtos: NamedEntityDto[];
  entityTypeName: string;
}

export function EntitiesTable({ entityDtos, entityTypeName }: EntitiesTableProps) {
  const entityType = getEntityTypeWithName(entityTypeName);
  const isForContacts = entityType === ContactType;
  const { push } = useRouter();
  const { allClients } = useAllClientStubs();
  const { allProperties } = useAllPropertyStubs();
  const entities = useMemo(
    () =>
      entityDtos.map((dto) => {
        switch (entityType) {
          case ContactType:
            return Contact.fromContactDto(dto as ContactDto, allClients, allProperties);
          default:
            return NamedEntity.fromDto(dto);
        }
      }),
    [entityDtos, entityType, allClients, allProperties],
  );

  function handleSelect(entityId: Id) {
    push(`/entity/${entityType.name}/${entityId}`);
  }

  async function handleAddClick() {
    await createNamedEntity(entityType.name, true);
  }

  return (
    <div className="flex flex-col items-center w-full mt-5">
      <table
        className={cn(
          "table table-hover text-left",
          isForContacts ? "w-[50rem]" : "w-[30rem]",
        )}
      >
        <thead>
          {!isForContacts && (
            <tr>
              <th>Name</th>
            </tr>
          )}
          {isForContacts && (
            <tr>
              <th>Name</th>
              <th className="text-center">Phone number</th>
              <th className="text-center">Type</th>
              <th className="text-center">Is contact for</th>
            </tr>
          )}
        </thead>
        <tbody>
          {entities?.map((entity, i) => (
            <tr key={i} onClick={() => handleSelect(entity.renderKeyString())}>
              <td>{entity.renderNameString() || "(No name)"}</td>
              {isForContacts && (
                <td className="text-center">
                  {(entity as Contact).renderPhoneNumberString()}
                </td>
              )}
              {isForContacts && (
                <td className="text-center">{(entity as Contact).renderType()}</td>
              )}
              {isForContacts && (
                <td className="text-center">
                  {getWhoIsContactFor(entity as Contact, allClients, allProperties)}
                </td>
              )}
            </tr>
          ))}
        </tbody>
      </table>
      <div className="mt-5">
        <Button variant="secondary" onClick={handleAddClick}>
          Add {entityType.name}
        </Button>
      </div>{" "}
    </div>
  );
}

function getWhoIsContactFor(
  contact: Contact,
  clients: NamedEntity[],
  properties: NamedEntity[],
): string | undefined {
  const client = clients.find((c) => contact.isForClient(c));
  if (client) return client.renderNameString();

  const property = properties.find((p) => contact.isForProperty(p));
  if (property) return property.renderNameString();

  return undefined;
}
