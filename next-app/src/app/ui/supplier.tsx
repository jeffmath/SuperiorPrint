"use client";

import * as React from "react";
import { ClientStub } from "@/data-transfer/ClientStub";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { PropertyStub } from "@/data-transfer/PropertyStub";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { useInstallContacts } from "@/app/lib/hooks/useInstallContacts";
import { ContactDto } from "@/data-transfer/ContactDto";
import { useSuperiorContacts } from "@/app/lib/hooks/useSuperiorContacts";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { useAllSmallSetEntities } from "@/app/lib/hooks/useAllSmallSetEntities";
import {
  FinishingType,
  MaterialType,
  PrinterType,
  SurfaceType,
} from "@/app/lib/entityType";

export interface SupplierProps {
  allClientStubs: ClientStub[];
  allPropertyStubs: PropertyStub[];
  installContactDtos: ContactDto[];
  superiorContactDtos: ContactDto[];
  allSurfaceDtos: NamedEntityDto[];
  allMaterialDtos: NamedEntityDto[];
  allFinishingDtos: NamedEntityDto[];
  allPrinterDtos: NamedEntityDto[];
}

export function Supplier({
  allClientStubs,
  allPropertyStubs,
  installContactDtos,
  superiorContactDtos,
  allSurfaceDtos,
  allMaterialDtos,
  allFinishingDtos,
  allPrinterDtos,
}: SupplierProps) {
  const clientStubs = useAllClientStubs();
  if (!clientStubs.allClients.length) clientStubs.supply(allClientStubs);

  const propertyStubs = useAllPropertyStubs();
  if (!propertyStubs.allProperties.length) propertyStubs.supply(allPropertyStubs);

  const installContacts = useInstallContacts();
  if (!installContacts.installContacts.length) installContacts.supply(installContactDtos);

  const superiorContacts = useSuperiorContacts();
  if (!superiorContacts.superiorContacts.length)
    superiorContacts.supply(superiorContactDtos);

  const surfaces = useAllSmallSetEntities(SurfaceType);
  if (!surfaces.entities.length) surfaces.supply(allSurfaceDtos);

  const materials = useAllSmallSetEntities(MaterialType);
  if (!materials.entities.length) materials.supply(allMaterialDtos);

  const finishings = useAllSmallSetEntities(FinishingType);
  if (!finishings.entities.length) finishings.supply(allFinishingDtos);

  const printers = useAllSmallSetEntities(PrinterType);
  if (!printers.entities.length) printers.supply(allPrinterDtos);

  return <></>;
}
