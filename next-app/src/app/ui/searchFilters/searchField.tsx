"use client";

import * as React from "react";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove, faSearch } from "@fortawesome/free-solid-svg-icons";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";
import { setEntitiesFiltersSearchTerm } from "@/app/lib/cache/findEntitiesFilters";
import { getEntityTypeWithName } from "@/app/lib/entityType";

export interface SearchFieldProps {
  domain: string;
  entityTypeName: string;
  searchTerm?: string;
}

export function SearchField({ domain, entityTypeName, searchTerm }: SearchFieldProps) {
  const [searchText, setSearchText] = useState(searchTerm || "");
  const entityType = getEntityTypeWithName(entityTypeName);

  async function handleSearch(term: string) {
    await setEntitiesFiltersSearchTerm("1", entityType, term);
  }

  return (
    <div className="inline-flex items-center space-x-1">
      <Label htmlFor="searchText">Search in {domain}</Label>
      <Input
        type="text"
        id="searchText"
        value={searchText}
        className="inline-block w-36"
        onChange={(e) => {
          setSearchText(e.currentTarget.value);
        }}
        onKeyUp={async (e) => {
          if (e.key === "Enter") await handleSearch(searchText);
        }}
      />
      <Button variant="secondary" size="xs" onClick={() => handleSearch(searchText)}>
        <FontAwesomeIcon icon={faSearch} />
      </Button>
      <Button
        variant="secondary"
        size="xs"
        onClick={async () => {
          setSearchText("");
          await handleSearch("");
        }}
      >
        <FontAwesomeIcon icon={faRemove} />
      </Button>
    </div>
  );
}
