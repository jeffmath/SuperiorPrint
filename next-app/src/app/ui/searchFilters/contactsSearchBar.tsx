"use client";

import * as React from "react";
import { Checkbox } from "@/components/ui/checkbox";
import { Label } from "@/components/ui/label";
import { cn } from "@/lib/utils";
import { NamedEntitySelect } from "@/app/ui/searchFilters/namedEntitySelect";
import { ClientType, PropertyType } from "@/app/lib/entityType";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { Id } from "@/domain/Id";
import {
  setContactsFiltersClientId,
  setContactsFiltersInstallersOnly,
  setContactsFiltersPropertyId,
} from "@/app/lib/cache/findContactsFilters";

export interface ContactsSearchBarProps {
  className?: string;
  installersOnly: boolean;
  clientId?: Id;
  propertyId?: Id;
}

export function ContactsSearchBar({
  className,
  installersOnly,
  clientId,
  propertyId,
}: ContactsSearchBarProps) {
  const { allClients } = useAllClientStubs();
  const { allProperties } = useAllPropertyStubs();

  async function handleInstallersOnlyChange(value: boolean) {
    await setContactsFiltersInstallersOnly("1", value);
  }

  async function handleClientSelection(id?: Id) {
    await setContactsFiltersClientId("1", id);
  }

  async function handlePropertySelection(id?: Id) {
    await setContactsFiltersPropertyId("1", id);
  }

  return (
    <div
      className={cn(className, "flex flex-wrap items-center justify-center space-x-8")}
    >
      <div className="inline-flex items-center space-x-1">
        <Checkbox
          id="installers-checkbox"
          checked={installersOnly}
          onCheckedChange={handleInstallersOnlyChange}
        />
        <Label htmlFor="installers-checkbox">Installers</Label>
      </div>
      <NamedEntitySelect
        entities={allClients}
        entityType={ClientType}
        selectedEntityId={clientId}
        onEntitySelected={handleClientSelection}
      />
      <NamedEntitySelect
        entities={allProperties}
        entityType={PropertyType}
        selectedEntityId={propertyId}
        onEntitySelected={handlePropertySelection}
      />
    </div>
  );
}
