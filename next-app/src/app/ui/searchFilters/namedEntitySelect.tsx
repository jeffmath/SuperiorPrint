import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Button } from "@/components/ui/button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import * as React from "react";
import { NamedEntity } from "@/domain/NamedEntity";
import { Id } from "@/domain/Id";
import { EntityType } from "@/app/lib/entityType";
import { capitalize } from "lodash";

export interface NamedEntitySelectProps {
  entities: NamedEntity[];
  entityType: EntityType;
  selectedEntityId?: Id;
  onEntitySelected: (id?: Id) => void;
}

export function NamedEntitySelect({
  entities,
  entityType,
  selectedEntityId,
  onEntitySelected,
}: NamedEntitySelectProps) {
  return (
    <div className="inline-flex items-center space-x-1">
      <Label htmlFor="clientSelect">{capitalize(entityType.name)}</Label>
      <Select value={selectedEntityId} onValueChange={onEntitySelected}>
        <SelectTrigger id="clientSelect" className="w-72">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="">&nbsp;</SelectItem>
          {entities.map((entity, i) => {
            const params = entity.renderOptionParams();
            return (
              <SelectItem key={i} value={params.value}>
                {params.label}
              </SelectItem>
            );
          })}
        </SelectContent>
      </Select>
      <Button
        variant="secondary"
        size="xs"
        className="ml-1"
        onClick={() => onEntitySelected()}
      >
        <FontAwesomeIcon icon={faRemove} />
      </Button>
    </div>
  );
}
