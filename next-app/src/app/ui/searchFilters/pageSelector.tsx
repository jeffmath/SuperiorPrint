"use client";

import React, { useState } from "react";
import {
  Pagination,
  PaginationContent,
  PaginationFirst,
  PaginationItem,
  PaginationLast,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { setEntitiesFiltersPage } from "@/app/lib/cache/findEntitiesFilters";
import { getEntityTypeWithName } from "@/app/lib/entityType";

export interface PageSelectorProps {
  className?: string;
  totalItems: number;
  entityTypeName: string;
  page: number;
}

export function PageSelector({
  className,
  totalItems,
  entityTypeName,
  page,
}: PageSelectorProps) {
  const [selectedPage, setSelectedPage] = useState(page || 1);
  const itemsPerPage = 10;
  const lastPage = Math.ceil(totalItems / itemsPerPage);
  const entityType = getEntityTypeWithName(entityTypeName);

  function getPageRange(): number[] {
    const result = [];
    const minPage = Math.max(1, selectedPage - 2);
    const maxPage = Math.min(selectedPage + 2, lastPage);
    for (let i = minPage; i <= maxPage; i++) {
      result.push(i);
    }
    return result;
  }

  async function handlePageSelection(page: number) {
    setSelectedPage(page);
    await setEntitiesFiltersPage("1", entityType, page);
  }

  return (
    <Pagination className={className} currentPage={selectedPage} lastPage={lastPage}>
      <PaginationContent>
        <PaginationItem>
          <PaginationFirst href="#" onClick={() => handlePageSelection(1)} />
        </PaginationItem>
        <PaginationItem>
          <PaginationPrevious
            href="#"
            onClick={() => selectedPage > 1 && handlePageSelection(selectedPage - 1)}
          />
        </PaginationItem>
        {getPageRange().map((page, i) => (
          <PaginationItem key={i}>
            <PaginationLink
              href="#"
              isActive={page === selectedPage}
              onClick={() => handlePageSelection(page)}
            >
              {page}
            </PaginationLink>
          </PaginationItem>
        ))}
        <PaginationItem>
          <PaginationNext
            href="#"
            onClick={() => handlePageSelection(selectedPage + 1)}
          />
        </PaginationItem>
        <PaginationItem>
          <PaginationLast
            href="#"
            onClick={() => selectedPage < lastPage - 1 && handlePageSelection(lastPage)}
          />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  );
}
