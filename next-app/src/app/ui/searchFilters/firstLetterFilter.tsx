"use client";

import * as React from "react";
import {
  Pagination,
  PaginationContent,
  PaginationItem,
  PaginationLink,
} from "@/components/ui/pagination";
import { useState } from "react";
import { setEntitiesFiltersFirstLetter } from "@/app/lib/cache/findEntitiesFilters";
import { getEntityTypeWithName } from "@/app/lib/entityType";

export interface FirstLetterFilterProps {
  className?: string;
  entityTypeName: string;
  firstLetter?: string;
}

export function FirstLetterFilter({
  className,
  entityTypeName,
  firstLetter,
}: FirstLetterFilterProps) {
  const [selectedLetter, setSelectedLetter] = useState(firstLetter || "*");
  const entityType = getEntityTypeWithName(entityTypeName);

  async function handleLetterSelection(letter: string) {
    setSelectedLetter(letter);
    await setEntitiesFiltersFirstLetter("1", entityType, letter);
  }

  return (
    <Pagination className={className}>
      <PaginationContent className="flex-wrap justify-center">
        {letters.map((letter, i) => (
          <PaginationItem key={i} onClick={() => handleLetterSelection(letter)}>
            <PaginationLink href="#" isActive={letter === selectedLetter}>
              {letter === "*" ? "All" : letter === "0" ? "0-9" : letter}
            </PaginationLink>
          </PaginationItem>
        ))}
      </PaginationContent>
    </Pagination>
  );
}

// create the array of letters (and otherwise) by which filtering may occur
const letters = [] as string[];
letters[0] = "*";
letters[1] = "0";
const aCode = "A".charCodeAt(0);
for (let i = 0; i < 26; i++) letters[i + 2] = String.fromCharCode(aCode + i);
