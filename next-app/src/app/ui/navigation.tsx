"use client";

import * as React from "react";
import Logo from "@/images/logo.jpg";
import {
  NavigationMenu,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  navigationMenuTriggerStyle,
} from "@/shadcn/components/ui/navigation-menu";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/shadcn/components/ui/dropdown-menu";
import Link from "next/link";
import Image from "next/image";
import { usePathname } from "next/navigation";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";
import { logOut } from "@/app/lib/login/logOut";

export function Navigation() {
  async function handleLogOutClick() {
    await logOut();
  }

  return (
    <NavigationMenu className="2xl:container justify-start">
      <div className="hidden sm:flex sm:flex-row">
        <Image src={Logo} className="w-36 h-8" alt="Superior Print and Exhibit" />
        <span className="ml-4 text-lg">Management</span>
      </div>
      <NavigationMenuList className="ml-4">
        <NavLink path="/jobs" text="Jobs" />
        <NavLink path="/entities/properties" text="Properties" />
        <NavLink path="/entities/clients" text="Clients" />
        <NavLink path="/entities/contacts" text="Contacts" />
        <NavigationMenuItem>
          <DropdownMenu>
            <DropdownMenuTrigger className={navigationMenuTriggerStyle()}>
              Other
            </DropdownMenuTrigger>
            <DropdownMenuContent>
              <SmallSetEntitiesLink path="surfaces" text="Surfaces" />
              <SmallSetEntitiesLink path="materials" text="Materials" />
              <SmallSetEntitiesLink path="finishings" text="Finishings" />
              <SmallSetEntitiesLink path="printers" text="Printers" />
              <SmallSetEntitiesLink path="users" text="Users" />
              <OtherLink path="/changePassword" text="Change password" />
              <DropdownMenuItem onClick={handleLogOutClick}>Log out</DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </NavigationMenuItem>
      </NavigationMenuList>
    </NavigationMenu>
  );
}

function NavLink({ path, text }: { path: string; text: string }) {
  const pathname = usePathname();
  const isActive = pathname.endsWith(path);
  const { handleClickToLeavePage } = useUnsavedChanges(path);

  return (
    <NavigationMenuItem>
      <NavigationMenuLink
        asChild={true}
        active={isActive}
        className={navigationMenuTriggerStyle()}
      >
        <Link
          href={path}
          style={{
            pointerEvents: isActive ? "none" : "auto",
          }}
          onClick={handleClickToLeavePage}
        >
          {text}
        </Link>
      </NavigationMenuLink>
    </NavigationMenuItem>
  );
}

function SmallSetEntitiesLink({ path, text }: { path: string; text: string }) {
  const fullPath = `/smallSetEntities/${path}`;
  const { handleClickToLeavePage } = useUnsavedChanges(fullPath);
  return (
    <Link href={fullPath} onClick={handleClickToLeavePage}>
      <DropdownMenuItem>{text}</DropdownMenuItem>
    </Link>
  );
}

function OtherLink({ path, text }: { path: string; text: string }) {
  const { handleClickToLeavePage } = useUnsavedChanges(path);
  return (
    <Link href={path} onClick={handleClickToLeavePage}>
      <DropdownMenuItem>{text}</DropdownMenuItem>
    </Link>
  );
}
