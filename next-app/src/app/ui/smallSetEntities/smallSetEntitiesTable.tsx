"use client";

import * as React from "react";
import { getEntityTypeWithName, UserType } from "@/app/lib/entityType";
import { NamedEntity } from "@/domain/NamedEntity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCancel,
  faFloppyDisk,
  faPencil,
  faRemove,
} from "@fortawesome/free-solid-svg-icons";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { useState } from "react";
import { updateNamedEntity } from "@/app/lib/data/namedEntities/updateNamedEntity";
import { createNamedEntity } from "@/app/lib/data/namedEntities/createNamedEntity";
import { deleteNamedEntity } from "@/app/lib/data/namedEntities/deleteNamedEntity";
import { errorModal } from "@/app/ui/modals/errorModal";
import { newUserPasswordWarningModal } from "@/app/ui/modals/newUserPasswordWarningModal";
import { useAllSmallSetEntities } from "@/app/lib/hooks/useAllSmallSetEntities";

export interface SmallSetEntitiesTableProps {
  entityTypeName: string;
  entityDtos: NamedEntityDto[];
}

export function SmallSetEntitiesTable({
  entityTypeName,
  entityDtos,
}: SmallSetEntitiesTableProps) {
  const entityType = getEntityTypeWithName(entityTypeName);
  const [isEditingMap, setIsEditingMap] = useState<{ [id: string]: boolean }>({});
  const [oldNameMap, setOldNameMap] = useState<{ [id: string]: string | undefined }>({});
  const [entities, setEntities] = useState<NamedEntity[]>(
    entityDtos.map((dto) => NamedEntity.fromDto(dto)),
  );
  const allSmallSetEntities = useAllSmallSetEntities(entityType);
  function handleNameEdit(entity: NamedEntity, newValue: string) {
    entity.onNameEdited(newValue);
    setEntities([...entities]);
  }

  async function handleSaveClick(entity: NamedEntity) {
    await updateNamedEntity(entity.renderDto(), entityType.name);
    const key = entity.renderKeyString();
    setIsEditingMap({ ...isEditingMap, [key]: false });
    setOldNameMap({ ...oldNameMap, [key]: undefined });
    await allSmallSetEntities.refetch();
  }

  async function handleAddClick() {
    const entityDto = await createNamedEntity(entityType.name);
    const entity = NamedEntity.fromDto(entityDto);
    setEntities([entity, ...entities]);
    if (entityType === UserType) await newUserPasswordWarningModal();
    await allSmallSetEntities.refetch();
  }

  function handleEditClick(entity: NamedEntity) {
    const key = entity.renderKeyString();
    setIsEditingMap({ ...isEditingMap, [key]: true });
    setOldNameMap({
      ...oldNameMap,
      [key]: entity.renderNameString(),
    });
  }

  function handleCancelEditClick(entity: NamedEntity) {
    const key = entity.renderKeyString();
    setIsEditingMap({ ...isEditingMap, [key]: false });
    entity.onNameEdited(oldNameMap[key]!);
    setOldNameMap({ ...oldNameMap, [key]: undefined });
  }

  async function handleDeleteClick(entity: NamedEntity) {
    const confirmed = await yesNoModal(
      `Are you sure you want to delete this ${entityType.name}?`,
    );
    if (confirmed) {
      const result = await deleteNamedEntity(entityType.name, entity.renderKeyString());
      if (result.success) {
        setEntities(entities.filter((e) => !e.isSameEntity(entity)));
        await allSmallSetEntities.refetch();
      } else await errorModal("Delete failed", result.failureReason || "");
    }
  }

  return (
    <div className="text-left">
      <h4 className="form-header">All {entityType.pluralName}</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add {entityType.name}
      </Button>
      <table className="table text-left mt-4">
        <thead>
          <tr>
            <th>Name</th>
            <th className="text-center">Delete</th>
          </tr>
        </thead>
        <tbody>
          {entities.map((entity, i) => {
            const isEditing = isEditingMap[entity.renderKeyString()];
            return (
              <tr key={i}>
                <td className="min-w-80">
                  {!isEditing ? (
                    <>
                      {entity.renderNameString() || "(No name)"}
                      <Button
                        variant="secondary"
                        size="xs"
                        className="ml-4"
                        onClick={() => handleEditClick(entity)}
                      >
                        <FontAwesomeIcon icon={faPencil} />
                      </Button>
                    </>
                  ) : (
                    <div className="flex items-center">
                      <Input
                        value={entity.renderNameString()}
                        required
                        autoFocus
                        className="form-control inline-block flex-1"
                        onChange={(e) => handleNameEdit(entity, e.currentTarget.value)}
                        onKeyUp={async (e) => {
                          if (e.key === "Enter") {
                            e.stopPropagation();
                            await handleSaveClick(entity);
                          }
                        }}
                      />
                      <Button
                        size="xs"
                        className="ml-1"
                        onClick={() => handleSaveClick(entity)}
                      >
                        <FontAwesomeIcon icon={faFloppyDisk} size="lg" />
                      </Button>
                      <Button
                        size="xs"
                        variant="secondary"
                        className="ml-1"
                        onClick={() => handleCancelEditClick(entity)}
                      >
                        <FontAwesomeIcon icon={faCancel} size="lg" />
                      </Button>
                    </div>
                  )}
                </td>
                <td className="text-center">
                  <Button
                    variant="destructive"
                    size="xs"
                    onClick={() => handleDeleteClick(entity)}
                  >
                    <FontAwesomeIcon icon={faRemove} />
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
