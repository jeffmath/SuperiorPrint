"use client";

import * as React from "react";
import { useMemo, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRemove } from "@fortawesome/free-solid-svg-icons";
import { Button } from "@/components/ui/button";
import { Id } from "@/domain/Id";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { ClientSignImageInfoDto } from "@/data-transfer/ClientSignImageDto";
import { ImageModal } from "@/app/ui/modals/imageModal";
import { ImageUploadModal } from "@/app/ui/modals/imageUploadModal";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { deleteClientSignImage } from "@/app/lib/data/clientSignImages/deleteClientSignImage";
import { useRouter } from "next/navigation";
import { createClientSignImages } from "@/app/lib/data/clientSignImages/createClientSignImages";

export interface ClientSignImagesTableProps {
  clientId: Id;
  imageDtos: ClientSignImageInfoDto[];
}

export function ClientSignImagesTable({
  clientId,
  imageDtos,
}: ClientSignImagesTableProps) {
  const [imageForView, setImageForView] = useState<ClientSignImageInfo | undefined>();
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isImageUploadModalOpen, setIsImageUploadModalOpen] = useState(false);
  const images = useMemo(
    () => imageDtos.map((dto) => ClientSignImageInfo.fromDto(dto)),
    [imageDtos],
  );
  const urlForImageView =
    imageForView && `/api/signImage/${imageForView.renderKeyString()}`;
  const { refresh } = useRouter();

  function handleViewClick(image: ClientSignImageInfo) {
    setImageForView(image);
    setIsImageModalOpen(true);
  }

  function handleAddClick() {
    setIsImageUploadModalOpen(true);
  }

  function handleImageModalClose() {
    setIsImageModalOpen(false);
  }

  async function handleUploadFilesSelected(formData: FormData) {
    return await createClientSignImages(clientId, formData);
  }

  function handleImagesUploaded() {
    refresh();
  }

  function handleImageUploadModalClose() {
    setIsImageUploadModalOpen(false);
  }

  async function handleDeleteClick(image: ClientSignImageInfo) {
    const confirmed = await yesNoModal(
      "Are you sure you want to delete this sign image?",
    );
    if (confirmed) {
      await deleteClientSignImage(image.renderKeyString());
      refresh();
    }
  }

  return (
    <div className="text-left mt-8">
      <h4 className="form-header">Sign images</h4>
      <Button variant="secondary" className="ml-7" onClick={handleAddClick}>
        Add sign image(s)
      </Button>
      {!!images.length && (
        <table className="table text-left mt-4">
          <thead>
            <tr>
              <th>Name</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            {images.map((image, i) => (
              <tr key={i}>
                <td className="min-w-80">{image.renderNameString()}</td>
                <td className="min-w-36 flex flex-row space-x-2 items-center justify-center">
                  <Button size="xs" onClick={() => handleViewClick(image)}>
                    View
                  </Button>
                  <Button
                    variant="destructive"
                    size="xs"
                    onClick={() => handleDeleteClick(image)}
                  >
                    <FontAwesomeIcon icon={faRemove} />
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
      <ImageModal
        isOpen={isImageModalOpen}
        imageName={imageForView?.renderNameString()}
        imageUrl={urlForImageView}
        onClose={handleImageModalClose}
      />
      <ImageUploadModal
        isOpen={isImageUploadModalOpen}
        isForMultiple={true}
        onFilesSelected={handleUploadFilesSelected}
        onRequestClose={handleImageUploadModalClose}
        onUploaded={handleImagesUploaded}
      />
    </div>
  );
}
