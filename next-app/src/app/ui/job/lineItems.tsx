"use client";

import * as React from "react";
import { Button } from "@/components/ui/button";
import { Job } from "@/domain/Job";
import { useMemo, useState } from "react";
import { LineItem } from "@/domain/LineItem";
import { Location } from "@/domain/Location";
import { Property } from "@/domain/Property";
import { ImageModal } from "@/app/ui/modals/imageModal";
import { ImageUploadModal } from "@/app/ui/modals/imageUploadModal";
import { createPropertyLocationImage } from "@/app/lib/data/properties/createPropertyLocationImage";
import { Id } from "@/domain/Id";
import { createLineItem } from "@/app/lib/data/jobs/createLineItem";
import { JobDto } from "@/data-transfer/JobDto";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { useRouter } from "next/navigation";
import { setSelectedLineItemId } from "@/app/lib/cache/lineItemSelection";

export interface LineItemsProps {
  jobDto: JobDto;
  selectedLineItemId?: Id;
  jobPropertyDto: PropertyDto;
}

export function LineItems({
  jobDto,
  selectedLineItemId,
  jobPropertyDto,
}: LineItemsProps) {
  const job = useMemo(() => Job.fromDto(jobDto), [jobDto]);
  const jobProperty = useMemo(() => Property.fromDto(jobPropertyDto), [jobPropertyDto]);
  const [selectedLineItem, setSelectedLineItem] = useState<LineItem | undefined>(
    selectedLineItemId ? job.produceLineItemWithKey(selectedLineItemId) : undefined,
  );
  const [imageLocation, setImageLocation] = useState<Location | undefined>();
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isImageUploadModalOpen, setIsImageUploadModalOpen] = useState(false);
  const { refresh } = useRouter();

  async function handleLineItemSelect(lineItem: LineItem) {
    setSelectedLineItem(lineItem);
    await setSelectedLineItemId("1", lineItem.renderKeyString());
  }

  function handleViewLocationImageClick(lineItem: LineItem, property: Property) {
    setImageLocation(lineItem.findLocationInProperty(property));
    setIsImageModalOpen(true);
  }

  function handleImageModalClose() {
    setIsImageModalOpen(false);
  }

  function handleUploadLocationImageClick(lineItem: LineItem, property: Property) {
    setImageLocation(lineItem.findLocationInProperty(property));
    setIsImageUploadModalOpen(true);
  }

  async function handleUploadFileSelected(formData: FormData): Promise<Id[]> {
    const imageId = await createPropertyLocationImage(
      jobProperty.renderKeyString(),
      imageLocation!.renderKeyString(),
      formData,
    );
    return [imageId];
  }

  function handleImageUploadModalClose() {
    setIsImageUploadModalOpen(false);
  }

  async function handleAddLineItemClick() {
    const lineItemDto = await createLineItem(job.renderKeyString());
    const lineItem = LineItem.fromDto(lineItemDto);
    job.onLineItemAdded(lineItem);
    refresh();
  }

  if (!job || !jobProperty) return null;
  return (
    <div className="mt-8">
      <h4 className="form-header">Job line items</h4>
      <table className="table table-hover text-left w-full mt-2">
        <thead>
          <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Location</th>
            <th className="text-right">Price</th>
            <th className="text-center">Image</th>
          </tr>
        </thead>
        <tbody>
          {job.mapLineItems((lineItem, i) => {
            const location = lineItem.findLocationInProperty(jobProperty);
            return (
              <tr
                key={i}
                className={
                  selectedLineItem && lineItem.isSameEntity(selectedLineItem)
                    ? "active"
                    : ""
                }
                onClick={() => handleLineItemSelect(lineItem)}
              >
                <td>{lineItem.renderNameString()}</td>
                <td>{lineItem.renderCodeString()}</td>
                <td>{location && location.renderNameString()}</td>
                <td className="text-right">{lineItem.renderTotalPriceString()}</td>
                <td className="text-center">
                  {location && location.renderImageKeyString() && (
                    <Button
                      size="xs"
                      onClick={(e) => {
                        e.stopPropagation();
                        handleViewLocationImageClick(lineItem, jobProperty);
                      }}
                    >
                      View
                    </Button>
                  )}
                  {location && !location.renderImageKeyString() && (
                    <Button
                      variant="secondary"
                      size="xs"
                      onClick={(e) => {
                        e.stopPropagation();
                        handleUploadLocationImageClick(lineItem, jobProperty);
                      }}
                    >
                      Upload
                    </Button>
                  )}
                </td>
              </tr>
            );
          })}
        </tbody>
        <tfoot>
          <tr>
            <td>
              <Button variant="secondary" onClick={handleAddLineItemClick}>
                Add line item
              </Button>
            </td>
            <td />
            <td className="text-right">Total Price:</td>
            <td className="text-right">{job.renderTotalPriceString()}</td>
            <td />
          </tr>
        </tfoot>
      </table>
      {imageLocation && (
        <div>
          <ImageModal
            isOpen={isImageModalOpen}
            imageName={imageLocation.renderNameString()}
            imageUrl={`/api/locationImage/${imageLocation.renderImageKeyString()}`}
            onClose={handleImageModalClose}
          />
          <ImageUploadModal
            isOpen={isImageUploadModalOpen}
            isForMultiple={false}
            onFilesSelected={handleUploadFileSelected}
            onUploaded={(imageIds) => imageLocation?.onImageIdAssigned(imageIds[0])}
            onRequestClose={handleImageUploadModalClose}
          />
        </div>
      )}
    </div>
  );
}
