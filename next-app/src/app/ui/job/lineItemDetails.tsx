"use client";

import * as React from "react";
import { useEffect, useMemo, useState } from "react";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { LineItemDto } from "@/data-transfer/LineItemDto";
import { LineItem } from "@/domain/LineItem";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { setSelectedLineItemId } from "@/app/lib/cache/lineItemSelection";
import { deleteLineItem } from "@/app/lib/data/jobs/deleteLineItem";
import { Id } from "@/domain/Id";
import {
  floatOrIntRegex,
  InputField,
  intRegex,
  moneyRegex,
} from "@/app/ui/general/inputField";
import { updateLineItem } from "@/app/lib/data/jobs/updateLineItem";
import { SelectField } from "@/app/ui/general/selectField";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { Property } from "@/domain/Property";
import { useAllSmallSetEntities } from "@/app/lib/hooks/useAllSmallSetEntities";
import {
  FinishingType,
  MaterialType,
  PrinterType,
  SurfaceType,
} from "@/app/lib/entityType";
import { currency } from "@/util/currency";
import { ClientSignImageInfoDto } from "@/data-transfer/ClientSignImageDto";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { ImageModal } from "@/app/ui/modals/imageModal";
import { ImageUploadModal } from "@/app/ui/modals/imageUploadModal";
import { createClientSignImages } from "@/app/lib/data/clientSignImages/createClientSignImages";
import { useRouter } from "next/navigation";
import { fill, uniq } from "lodash";
import { JobDatesPanel } from "@/app/ui/job/jobDatesPanel";
import { useLeaveOrRefreshAppGuard } from "@/app/lib/hooks/useLeaveOrRefreshAppGuard";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";
import { WorkOrder } from "@/app/ui/job/workOrder";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { NamedEntity } from "@/domain/NamedEntity";
import { Job } from "@/domain/Job";
import { JobDto } from "@/data-transfer/JobDto";
import { formatDate, formatTime } from "@/util/formatDate";
import { getStringLines } from "@/util/getStringLines";
import { useInstallContacts } from "@/app/lib/hooks/useInstallContacts";
import { Deck } from "@/app/ui/job/deck";
import { useSuperiorContacts } from "@/app/lib/hooks/useSuperiorContacts";
import { ContactDto } from "@/data-transfer/ContactDto";
import { useContacts } from "@/app/lib/hooks/useContacts";
import { useForm } from "react-hook-form";

export interface LineItemDetailsProps {
  jobDto: JobDto;
  lineItemDto: LineItemDto;
  jobClientDto: NamedEntityDto;
  jobPropertyDto: PropertyDto;
  clientContactDtos: ContactDto[];
  propertyContactDtos: ContactDto[];
  jobClientSignImageInfoDtos: ClientSignImageInfoDto[];
}

interface DetailsFormData {
  name: string;
  code: string;
  locationId: Id;
  width?: number;
  height?: number;
  surfaceId: Id;
  materialId: Id;
  finishingId: Id;
  quantity?: number;
  priceEach?: number;
  printerId: Id;
  clientSignImageIds: Id[];
  artworkReceivedDate?: Date;
  artworkApprovedDate?: Date;
  shipDate?: Date;
  inHandDate?: Date;
  installDateTime?: Date;
  strikeDateTime?: Date;
  installNotes: string;
}

export function LineItemDetails({
  jobDto,
  lineItemDto,
  jobClientDto,
  jobPropertyDto,
  clientContactDtos,
  propertyContactDtos,
  jobClientSignImageInfoDtos,
}: LineItemDetailsProps) {
  const lineItem = useMemo(() => LineItem.fromDto(lineItemDto), [lineItemDto]);
  const job = useMemo(() => Job.fromDto(jobDto), [jobDto]);
  const jobId = jobDto.id;
  const jobClient = useMemo(() => NamedEntity.fromDto(jobClientDto), [jobClientDto]);
  const jobProperty = useMemo(() => Property.fromDto(jobPropertyDto), [jobPropertyDto]);
  const location = lineItem.findLocationInProperty(jobProperty);
  const jobClientSignImageInfos = useMemo(
    () => jobClientSignImageInfoDtos.map((dto) => ClientSignImageInfo.fromDto(dto)),
    [jobClientSignImageInfoDtos],
  );

  const name = lineItem.renderNameString();
  const code = lineItem.renderCodeString();
  const locationId = lineItem.renderLocationKeyString();
  const width = lineItem.renderWidth();
  const height = lineItem.renderHeight();
  const surfaceId = lineItem.renderSurfaceKeyString();
  const materialId = lineItem.renderMaterialKeyString();
  const finishingId = lineItem.renderFinishingKeyString();
  const quantity = lineItem.renderQuantity();
  const priceEach = lineItem.renderPriceEach();
  const printerId = lineItem.renderPrinterKeyString();
  const clientSignImageIds = lineItem.renderClientSignImageKeyStrings();
  // I tired of adding renderX() methods to LineItem, so here I just pull the date
  // values from the DTO, instead
  const {
    artworkReceivedDate,
    artworkApprovedDate,
    shipDate,
    inHandDate,
    installDateTime,
    strikeDateTime,
  } = lineItemDto;
  const installNotes = lineItem.renderInstallNotesString();
  const {
    register,
    handleSubmit,
    control,
    formState: { isDirty, isValid, errors },
    getValues,
    reset,
    watch,
  } = useForm<DetailsFormData>({
    mode: "onChange",
    defaultValues: {
      name,
      code,
      locationId,
      width,
      height,
      surfaceId,
      materialId,
      finishingId,
      quantity,
      priceEach,
      printerId,
      clientSignImageIds,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
      installNotes,
    },
  });

  const [clientSignImageToView, setClientSignImageToView] = useState<
    ClientSignImageInfo | undefined
  >();
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isImageUploadModalOpen, setIsImageUploadModalOpen] = useState(false);
  useLeaveOrRefreshAppGuard(isDirty);
  const [previousLineItem, setPreviousLineItem] = useState<LineItem>(lineItem);
  const [unsavedLineItem, setUnsavedLineItem] = useState<LineItem | undefined>();
  const { refresh } = useRouter();
  const [isShowingWorkOrder, setIsShowingWorkOrder] = useState(false);
  const [isShowingDeck, setIsShowingDeck] = useState(false);

  const [, setReRenderCount] = useState(0);
  function reRender() {
    setReRenderCount((i) => i + 1);
  }

  const [formQuantity, formPriceEach] = watch(["quantity", "priceEach"]);
  const totalPrice =
    formQuantity && formPriceEach ? currency(formQuantity * formPriceEach) : "0";

  const surfaces = useAllSmallSetEntities(SurfaceType).entities;
  const materials = useAllSmallSetEntities(MaterialType).entities;
  const finishings = useAllSmallSetEntities(FinishingType).entities;
  const printers = useAllSmallSetEntities(PrinterType).entities;

  const clientContacts = useContacts(clientContactDtos);
  const propertyContacts = useContacts(propertyContactDtos);
  const { installContacts } = useInstallContacts();
  const { superiorContacts } = useSuperiorContacts();

  const [resetTo, setResetTo] = useState<DetailsFormData | undefined>();

  const { setSectionAsUnsaved, setSectionAsSaved } = useUnsavedChanges();
  useEffect(() => {
    if (isDirty) setSectionAsUnsaved("lineItemDetails");
  }, [isDirty, setSectionAsUnsaved]);

  // if a different line-item was supplied for this render
  const formValues = getValues();
  if (!lineItem.isSameEntity(previousLineItem)) {
    if (isDirty) {
      previousLineItem.onNameEdited(formValues.name);
      previousLineItem.onCodeEdited(formValues.code);
      previousLineItem.onLocationSelected(formValues.locationId);
      previousLineItem.onWidthEdited(formValues.width);
      previousLineItem.onHeightEdited(formValues.height);
      previousLineItem.onSurfaceSelected(formValues.surfaceId);
      previousLineItem.onMaterialSelected(formValues.materialId);
      previousLineItem.onFinishingSelected(formValues.finishingId);
      previousLineItem.onQuantityEdited(formValues.quantity);
      previousLineItem.onPriceEachEdited(formValues.priceEach);
      previousLineItem.onPrinterSelected(formValues.printerId);
      previousLineItem.onClientSignImagesSelected(formValues.clientSignImageIds);
      previousLineItem.onArtworkReceivedDateEdited(formValues.artworkReceivedDate);
      previousLineItem.onArtworkApprovedDateEdited(formValues.artworkApprovedDate);
      previousLineItem.onShipDateEdited(formValues.shipDate);
      previousLineItem.onInHandDateEdited(formValues.inHandDate);
      previousLineItem.onInstallDateTimeEdited(formValues.installDateTime);
      previousLineItem.onStrikeDateEdited(formValues.strikeDateTime);
      previousLineItem.onInstallNotesEdited(formValues.installNotes);
      setUnsavedLineItem(previousLineItem);
    }

    setResetTo({
      name,
      code,
      locationId,
      width,
      height,
      surfaceId,
      materialId,
      finishingId,
      quantity,
      priceEach,
      printerId,
      clientSignImageIds,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
      installNotes,
    });

    setPreviousLineItem(lineItem);
  }

  // this is an effect only because React Hook Form prescribes that calls to its
  // reset() method be made within an effect (or event handler)
  useEffect(() => {
    if (resetTo) {
      reset(resetTo);
      setResetTo(undefined);
    }
  }, [resetTo]);

  useEffect(() => {
    if (unsavedLineItem) {
      (async () => {
        const confirmed = await yesNoModal("Save changes to job line item?");
        if (confirmed) {
          await updateLineItem(jobId, unsavedLineItem.renderDto());
        }
      })();
    }
  }, [unsavedLineItem, jobId]);

  async function handleSaveClick(data: DetailsFormData) {
    const {
      name,
      code,
      locationId,
      width,
      height,
      surfaceId,
      materialId,
      finishingId,
      quantity,
      priceEach,
      printerId,
      clientSignImageIds,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
      installNotes,
    } = data;
    lineItem.onNameEdited(name);
    lineItem.onCodeEdited(code);
    lineItem.onLocationSelected(locationId);
    lineItem.onWidthEdited(width);
    lineItem.onHeightEdited(height);
    lineItem.onSurfaceSelected(surfaceId);
    lineItem.onMaterialSelected(materialId);
    lineItem.onFinishingSelected(finishingId);
    lineItem.onQuantityEdited(quantity);
    lineItem.onPriceEachEdited(priceEach);
    lineItem.onPrinterSelected(printerId);
    lineItem.onClientSignImagesSelected(clientSignImageIds);
    lineItem.onArtworkReceivedDateEdited(artworkReceivedDate);
    lineItem.onArtworkApprovedDateEdited(artworkApprovedDate);
    lineItem.onShipDateEdited(shipDate);
    lineItem.onInHandDateEdited(inHandDate);
    lineItem.onInstallDateTimeEdited(installDateTime);
    lineItem.onStrikeDateEdited(strikeDateTime);
    lineItem.onInstallNotesEdited(installNotes);
    await updateLineItem(jobId, lineItem.renderDto());
    reset(data);
    setSectionAsSaved("lineItemDetails");
  }

  async function handleDeleteClick() {
    const confirmed = await yesNoModal("Are you sure you want to delete this line item?");
    if (confirmed) {
      await deleteLineItem(jobId, lineItem.renderKeyString());
      await setSelectedLineItemId("1", undefined);
    }
  }

  function onQuantityChange(value: string) {
    // adjust the length of the clientSignImageIds array to match the new quantity value
    const length = parseInt(value);
    const { clientSignImageIds: imageIds } = formValues;
    if (
      !isNaN(length) &&
      length >= 0 &&
      length <= quantityRules.max &&
      length !== imageIds.length
    ) {
      const oldLength = imageIds.length;
      imageIds.length = length;
      fill(imageIds, "", oldLength);
    }
  }

  function handleViewSignImageClick(id: Id) {
    setClientSignImageToView(
      jobClientSignImageInfos.find((info) => info.hasSameKey(id))!,
    );
    setIsImageModalOpen(true);
  }

  function handleImageModalClosed() {
    setIsImageModalOpen(false);
  }

  function handleImageUploadModalOpened() {
    setIsImageUploadModalOpen(true);
  }

  function handleImageUploadModalClosed() {
    setIsImageUploadModalOpen(false);
  }

  async function handleUploadFilesSelected(formData: FormData) {
    return await createClientSignImages(jobClient.renderKeyString(), formData);
  }

  async function handleClientSignImagesUpload() {
    refresh();
  }

  function handleViewWorkOrderClick() {
    setIsShowingWorkOrder(true);
  }

  function handleViewDeckClick() {
    setIsShowingDeck(true);
  }

  function handleBackToLineItemClick() {
    setIsShowingWorkOrder(false);
    setIsShowingDeck(false);
  }

  const widthOrHeightRules = { pattern: floatOrIntRegex, min: 0, max: 99999 };
  const quantityRules = { pattern: intRegex, min: 0, max: 32 };
  const priceEachRules = { pattern: moneyRegex, min: 0, max: 99999 };
  return (
    <form onSubmit={handleSubmit(handleSaveClick)}>
      <div className="mt-8 flex flex-col space-y-4">
        <div className="flex flex-row space-x-4 items-center">
          <h4 className="form-header">{(name || ``) + (code ? ` (${code})` : "")}</h4>
          {/*{isDirty && "dirty"} {!isValid && "invalid"}*/}
          <Button disabled={!isDirty || !isValid}>Save</Button>
          {!isShowingWorkOrder && !isShowingDeck && (
            <>
              <Button
                variant="secondary"
                type="button"
                onClick={handleViewWorkOrderClick}
              >
                View work order
              </Button>
              <Button variant="secondary" type="button" onClick={handleViewDeckClick}>
                View deck
              </Button>
              <Button variant="destructive" type="button" onClick={handleDeleteClick}>
                Delete
              </Button>
            </>
          )}
          {(isShowingWorkOrder || isShowingDeck) && (
            <Button variant="secondary" type="button" onClick={handleBackToLineItemClick}>
              Back to line item
            </Button>
          )}
        </div>
        {isShowingWorkOrder ? (
          <WorkOrder
            clientName={jobClient.renderNameString()}
            jobNumber={job.renderNumberString()}
            lineItemCode={code}
            lineItemName={name}
            jobClientSignImageInfos={getDistinctClientSignImages(
              jobClientSignImageInfos,
              formValues.clientSignImageIds,
            )}
            material={materials.find((m) => m.hasSameKey(materialId))?.renderNameString()}
            finishing={finishings
              .find((f) => f.hasSameKey(finishingId))
              ?.renderNameString()}
            width={width}
            height={height}
            quantity={quantity}
            jobArtworkReceivedDate={job.renderArtworkReceivedDateString()}
            jobArtworkApprovedDate={job.renderArtworkApprovedDateString()}
            jobShipDate={job.renderShipDateString()}
            jobInHandDate={job.renderInHandDateString()}
            lineItemArtworkReceivedDate={formatDate(artworkReceivedDate)}
            lineItemArtworkApprovedDate={formatDate(artworkApprovedDate)}
            lineItemShipDate={formatDate(shipDate)}
            lineItemInHandDate={formatDate(inHandDate)}
            installNoteLines={getStringLines(installNotes)}
            installContactName={job.renderInstallContactNameString(installContacts)}
            locationImageId={location?.renderImageKeyString()}
          />
        ) : isShowingDeck ? (
          <Deck
            clientName={jobClient.renderNameString()}
            lineItemCode={code}
            lineItemName={name}
            propertyName={jobProperty.renderNameString()}
            jobClientSignImageInfos={getDistinctClientSignImages(
              jobClientSignImageInfos,
              formValues.clientSignImageIds,
            )}
            locationImageId={location?.renderImageKeyString()}
            width={width}
            height={height}
            quantity={quantity}
            jobArtworkReceivedDate={job.renderArtworkReceivedDateString()}
            jobArtworkApprovedDate={job.renderArtworkApprovedDateString()}
            jobInHandDate={job.renderInHandDateString()}
            jobInstallDate={job.renderInstallDateString()}
            jobInstallTime={job.renderInstallTimeString()}
            jobStrikeDate={job.renderStrikeDateString()}
            jobStrikeTime={job.renderStrikeTimeString()}
            lineItemArtworkReceivedDate={formatDate(artworkReceivedDate)}
            lineItemArtworkApprovedDate={formatDate(artworkApprovedDate)}
            lineItemInHandDate={formatDate(inHandDate)}
            lineItemInstallDate={formatDate(installDateTime)}
            lineItemInstallTime={formatTime(installDateTime)}
            lineItemStrikeDate={formatDate(strikeDateTime)}
            lineItemStrikeTime={formatTime(strikeDateTime)}
            clientContactName={job.renderClientContactNameString(clientContacts)}
            clientContactPhoneNumber={job.renderClientContactPhoneNumberString(
              clientContacts,
            )}
            propertyContactName={job.renderPropertyContactNameString(propertyContacts)}
            propertyContactPhoneNumber={job.renderPropertyContactPhoneNumberString(
              propertyContacts,
            )}
            installContactName={job.renderInstallContactNameString(installContacts)}
            installContactPhoneNumber={job.renderInstallContactPhoneNumberString(
              installContacts,
            )}
            superiorPrint1ContactName={job.renderSuperiorPrintContact1NameString(
              superiorContacts,
            )}
            superiorPrint1ContactPhoneNumber={job.renderSuperiorPrintContact1PhoneNumberString(
              superiorContacts,
            )}
            superiorPrint2ContactName={job.renderSuperiorPrintContact2NameString(
              superiorContacts,
            )}
            superiorPrint2ContactPhoneNumber={job.renderSuperiorPrintContact2PhoneNumberString(
              superiorContacts,
            )}
            locationName={location?.renderNameString()}
            material={materials.find((m) => m.hasSameKey(materialId))?.renderNameString()}
            surface={surfaces.find((s) => s.hasSameKey(surfaceId))?.renderNameString()}
            finishing={finishings
              .find((f) => f.hasSameKey(finishingId))
              ?.renderNameString()}
            printer={printers.find((p) => p.hasSameKey(printerId))?.renderNameString()}
            installNoteLines={getStringLines(installNotes)}
          />
        ) : (
          <>
            <div className="flex flex-row space-x-4 items-start">
              <InputField
                {...register("name", { required: true })}
                label="Name"
                id="nameField"
                className="w-72"
                isRequired={true}
                error={errors.name}
              />
              <InputField
                {...register("code", { maxLength: 6 })}
                label="Code"
                id="codeField"
                className="w-24"
                error={errors.code}
              />
              <SelectField
                fieldName="locationId"
                control={control}
                label="Location"
                className="min-w-24"
                id="locationSelect"
                options={jobProperty.renderLocationsOptionParams()}
              />
            </div>
            <div className="flex flex-row space-x-4 items-start">
              <InputField
                {...register("width", widthOrHeightRules)}
                label="Width"
                id="widthField"
                className="w-20"
                rules={widthOrHeightRules}
                error={errors.width}
              />
              <InputField
                {...register("height", widthOrHeightRules)}
                label="Height"
                id="heightField"
                className="w-20"
                rules={widthOrHeightRules}
                error={errors.height}
              />
              <SelectField
                fieldName="surfaceId"
                control={control}
                label="Surface"
                id="surfaceSelect"
                options={surfaces.map((surface) => surface.renderOptionParams())}
              />
              <SelectField
                fieldName="materialId"
                control={control}
                label="Material"
                id="materialSelect"
                options={materials.map((material) => material.renderOptionParams())}
              />
              <SelectField
                fieldName="finishingId"
                control={control}
                label="Finishing"
                id="finishingSelect"
                options={finishings.map((finishing) => finishing.renderOptionParams())}
              />
            </div>
            <div className="flex flex-row space-x-4 items-start-inline">
              <InputField
                {...register("quantity", {
                  ...quantityRules,
                  onChange: (event) => onQuantityChange(event.target.value),
                })}
                label="Quantity"
                id="quantityField"
                className="w-20"
                rules={quantityRules}
                error={errors.quantity}
              />
              <InputField
                {...register("priceEach", {
                  ...priceEachRules,
                  onChange: reRender,
                })}
                label="Price each"
                id="priceEach"
                className="w-24 text-right"
                rules={priceEachRules}
                error={errors.priceEach}
              />
              <div className="text-right grid">
                <label
                  className="font-semibold text-sm relative top-[3px]"
                  htmlFor="totalPrice"
                >
                  Total price
                </label>
                <span className="relative top-0.5" id="totalPrice">
                  {totalPrice}
                </span>
              </div>
              <SelectField
                fieldName="printerId"
                control={control}
                label="Printer"
                id="printerSelect"
                options={printers.map((printer) => printer.renderOptionParams())}
              />
            </div>
            <div className="flex flex-row flex-wrap space-x-4 items-start">
              {formValues.clientSignImageIds.map((key, i) => (
                <SelectField
                  fieldName={`clientSignImageIds.${i}` as "clientSignImageIds"}
                  control={control}
                  key={`sign${i}`}
                  label={`Sign ${i + 1}`}
                  id={`sign${i}`}
                  className="min-w-24"
                  options={jobClientSignImageInfos.map((info) =>
                    info.renderOptionParams(),
                  )}
                  suppliedButton={
                    key && (
                      <Button
                        variant="secondary"
                        type="button"
                        size="xs"
                        className="ml-1 top-2"
                        onClick={() => handleViewSignImageClick(key)}
                      >
                        View
                      </Button>
                    )
                  }
                />
              ))}
              {clientSignImageToView && (
                <ImageModal
                  isOpen={isImageModalOpen}
                  imageName={clientSignImageToView.renderNameString()}
                  imageUrl={`/api/signImage/${clientSignImageToView.renderKeyString()}`}
                  onClose={handleImageModalClosed}
                />
              )}
            </div>
            <div>
              <Button
                variant="secondary"
                type="button"
                size="xs"
                onClick={handleImageUploadModalOpened}
              >
                Upload sign images
              </Button>
              <ImageUploadModal
                isOpen={isImageUploadModalOpen}
                isForMultiple={true}
                onFilesSelected={handleUploadFilesSelected}
                onRequestClose={handleImageUploadModalClosed}
                onUploaded={handleClientSignImagesUpload}
              />
            </div>
            <div>
              <h5 className="nb-1 font-bold">Override dates/times</h5>
              <JobDatesPanel control={control} />
            </div>
            <div>
              <Label htmlFor="installNotes">Installation notes</Label>
              <Textarea
                {...register("installNotes", {
                  maxLength: 2000,
                })}
                id="installNotes"
                className="form-control !w-full"
                rows={4}
              ></Textarea>
            </div>
          </>
        )}
      </div>
    </form>
  );
}

function getDistinctClientSignImages(
  fromImages: ClientSignImageInfo[],
  clientSignSlotImageIds: Id[],
): ClientSignImageInfo[] {
  // create a list of the client-sign-image IDs for this
  // line-item where all the empty image slots are removed
  const emptiesRemoved = clientSignSlotImageIds.filter((slot) => slot != null);

  // remove all duplicate client-sign-image IDs from the above list
  const duplicatesRemoved = uniq(emptiesRemoved);

  // map each client-sign-image ID in the above list to the
  // corresponding element in the given list of client-sign-images
  return duplicatesRemoved
    .map((signImageId) =>
      fromImages.find((image) => image.renderKeyString() === signImageId),
    )
    .filter((image): image is ClientSignImageInfo => !!image);
}
