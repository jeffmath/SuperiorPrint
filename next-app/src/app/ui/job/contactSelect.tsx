import * as React from "react";
import { Contact } from "@/domain/Contact";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Control, Controller, FieldValues, Path } from "react-hook-form";

export interface ContactSelectProps<V extends FieldValues> {
  control: Control<V>;
  fieldName: keyof V;
  labelText: string;
  contacts: Contact[];
}

export function ContactSelect<V extends FieldValues>({
  control,
  fieldName,
  labelText,
  contacts,
}: ContactSelectProps<V>) {
  return (
    <div>
      <Label htmlFor="contactSelect">{labelText}</Label>
      {control && (
        <Controller
          name={fieldName as Path<V>}
          control={control}
          render={({ field }) => {
            const { ref, ...fieldExRef } = field;
            return (
              <>
                <Select
                  {...fieldExRef}
                  value={field.value || ""}
                  onValueChange={(value) => field.onChange(value || null)}
                >
                  <SelectTrigger id="contactSelect" className="min-w-33">
                    <SelectValue />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value="">&nbsp;</SelectItem>
                    {contacts.map((contact, i) => {
                      const params = contact.renderOptionParams();
                      return (
                        <SelectItem key={i} value={params.value}>
                          {params.label}
                        </SelectItem>
                      );
                    })}
                  </SelectContent>
                </Select>
                <div>
                  <FontAwesomeIcon icon={faPhone} className="size-4" />
                  &nbsp;
                  {field.value &&
                    contacts
                      .find((c) => c.hasSameKey(field.value))
                      ?.renderPhoneNumberString()}
                </div>
              </>
            );
          }}
        />
      )}
    </div>
  );
}
