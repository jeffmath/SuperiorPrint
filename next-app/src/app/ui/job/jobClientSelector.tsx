"use client";

import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import * as React from "react";
import { Id } from "@/domain/Id";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { setJobSelectionClientId } from "@/app/lib/cache/selectJobSelections";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";

export interface JobClientSelectorProps {
  jobClientId?: Id;
}

export function JobClientSelector({
  jobClientId: selectedClientId,
}: JobClientSelectorProps) {
  const { allClients } = useAllClientStubs();
  const { checkToConfirmDiscard } = useUnsavedChanges();

  async function handleClientSelection(id: Id) {
    const mayProceed = await checkToConfirmDiscard();
    if (mayProceed) await setJobSelectionClientId("1", id);
  }

  return (
    <div>
      <Label htmlFor="clientSelect">Client</Label>
      <Select value={selectedClientId || ""} onValueChange={handleClientSelection}>
        <SelectTrigger id="clientSelect" className="min-w-36">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          {allClients.map((client, i) => {
            const params = client.renderOptionParams();
            return (
              <SelectItem key={i} value={params.value}>
                {params.label}
              </SelectItem>
            );
          })}
        </SelectContent>
      </Select>
    </div>
  );
}
