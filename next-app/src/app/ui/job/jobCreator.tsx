"use client";

import * as React from "react";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Button } from "@/components/ui/button";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { useState } from "react";
import { createJob } from "@/app/lib/data/jobs/createJob";
import { Id } from "@/domain/Id";
import { setAllJobSelections } from "@/app/lib/cache/selectJobSelections";
import { setSelectedLineItemId } from "@/app/lib/cache/lineItemSelection";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";

export interface JobCreatorProps {
  jobClientId: Id;
}

export function JobCreator({ jobClientId }: JobCreatorProps) {
  const properties = useAllPropertyStubs().allProperties;
  const [selectedPropertyId, setSelectedPropertyId] = useState<Id | undefined>();
  const { checkToConfirmDiscard } = useUnsavedChanges();

  function handlePropertySelection(value: Id) {
    setSelectedPropertyId(value);
  }

  async function handleCreateJobClick() {
    const mayProceed = await checkToConfirmDiscard();
    if (mayProceed) {
      const jobId = await createJob(jobClientId, selectedPropertyId!);
      await setAllJobSelections("1", jobClientId, selectedPropertyId!, jobId);
      setSelectedPropertyId(undefined);
      await setSelectedLineItemId("1", undefined);
    }
  }

  return (
    <div>
      <h4 className="form-header">Or, create a job</h4>
      <div className="mt-2 flex flex-row space-x-2 items-end">
        <div>
          <Label htmlFor="propertySelect">For property</Label>
          <Select
            value={selectedPropertyId || ""}
            onValueChange={handlePropertySelection}
          >
            <SelectTrigger id="propertySelect" className="min-w-36">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {properties.map((job, i) => {
                const params = job.renderOptionParams();
                return (
                  <SelectItem key={i} value={params.value}>
                    {params.label}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
        </div>
        <Button
          variant="secondary"
          className="ml-4"
          disabled={!selectedPropertyId}
          onClick={handleCreateJobClick}
        >
          Create
        </Button>
      </div>
    </div>
  );
}
