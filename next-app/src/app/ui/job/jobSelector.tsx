"use client";

import * as React from "react";
import { Id } from "@/domain/Id";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { JobStub } from "@/data-transfer/JobDto";
import { setJobSelectionJobId } from "@/app/lib/cache/selectJobSelections";
import { setSelectedLineItemId } from "@/app/lib/cache/lineItemSelection";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";

export interface JobSelectorProps {
  jobStubs: JobStub[];
  jobId?: Id;
}

export function JobSelector({ jobStubs, jobId: selectedJobId }: JobSelectorProps) {
  const { checkToConfirmDiscard } = useUnsavedChanges();
  async function handleJobSelection(id: Id) {
    const mayProceed = await checkToConfirmDiscard();
    if (mayProceed) {
      await setJobSelectionJobId("1", id);
      await setSelectedLineItemId("1", undefined);
    }
  }

  if (!jobStubs.length) return null;
  return (
    <div>
      <Label htmlFor="jobSelect">Job</Label>
      <Select value={selectedJobId || ""} onValueChange={handleJobSelection}>
        <SelectTrigger id="jobSelect" className="min-w-24">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          {jobStubs.map((job, i) => {
            return (
              <SelectItem key={i} value={job.id}>
                {job.number}
              </SelectItem>
            );
          })}
        </SelectContent>
      </Select>
    </div>
  );
}
