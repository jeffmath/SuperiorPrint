import * as React from "react";
import Image from "next/image";
import Logo from "@/images/logo.jpg";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { Id } from "@/domain/Id";

export interface DeckProps {
  clientName: string;
  lineItemCode?: string;
  lineItemName: string;
  propertyName: string;
  jobClientSignImageInfos: ClientSignImageInfo[];
  locationImageId?: Id;
  width?: number;
  height?: number;
  quantity?: number;
  jobArtworkReceivedDate?: string;
  jobArtworkApprovedDate?: string;
  jobInHandDate?: string;
  jobInstallDate?: string;
  jobInstallTime?: string;
  jobStrikeDate?: string;
  jobStrikeTime?: string;
  lineItemArtworkReceivedDate?: string;
  lineItemArtworkApprovedDate?: string;
  lineItemInHandDate?: string;
  lineItemInstallDate?: string;
  lineItemInstallTime?: string;
  lineItemStrikeDate?: string;
  lineItemStrikeTime?: string;
  clientContactName?: string;
  clientContactPhoneNumber?: string;
  propertyContactName?: string;
  propertyContactPhoneNumber?: string;
  installContactName?: string;
  installContactPhoneNumber?: string;
  superiorPrint1ContactName?: string;
  superiorPrint1ContactPhoneNumber?: string;
  superiorPrint2ContactName?: string;
  superiorPrint2ContactPhoneNumber?: string;
  locationName?: string;
  material?: string;
  surface?: string;
  finishing?: string;
  printer?: string;
  installNoteLines: string[];
}

export function Deck({
  clientName,
  lineItemCode,
  lineItemName,
  propertyName,
  jobClientSignImageInfos,
  locationImageId,
  width,
  height,
  quantity,
  jobArtworkReceivedDate,
  jobArtworkApprovedDate,
  jobInHandDate,
  jobInstallDate,
  jobInstallTime,
  jobStrikeDate,
  jobStrikeTime,
  lineItemArtworkReceivedDate,
  lineItemArtworkApprovedDate,
  lineItemInHandDate,
  lineItemInstallDate,
  lineItemInstallTime,
  lineItemStrikeDate,
  lineItemStrikeTime,
  clientContactName,
  clientContactPhoneNumber,
  propertyContactName,
  propertyContactPhoneNumber,
  installContactName,
  installContactPhoneNumber,
  superiorPrint1ContactName,
  superiorPrint1ContactPhoneNumber,
  superiorPrint2ContactName,
  superiorPrint2ContactPhoneNumber,
  locationName,
  material,
  surface,
  finishing,
  printer,
  installNoteLines,
}: DeckProps) {
  return (
    <div className="deck">
      <div className="flex flex-row justify-between">
        <div className="text-left">
          <Image
            src={Logo}
            className="ml-3.5 text-left max-h-12"
            alt="Superior Print and Exhibit"
          />
        </div>
        <div className="text-right font-bold">{`${clientName} - ${propertyName}`}</div>
      </div>
      <div className="deck-line-item-header text-left">
        {lineItemCode}
        <span className="ml-20">{lineItemName}</span>
      </div>
      <div className="w-full flex flex-row flex-nowrap justify-between pb-1 border-b border-solid border-black">
        <div className="w-full flex flex-row flex-nowrap">
          {jobClientSignImageInfos?.map((image, i) => (
            <div key={i} style={{ display: image.renderCssDisplayValue() }}>
              <img
                src={"/api/signImage/" + image.renderKeyString()}
                alt="Sign image"
                className="contained-image max-h-96"
              />
            </div>
          ))}
        </div>
        {locationImageId && (
          <div>
            <img
              src={`/api/locationImage/${locationImageId}`}
              className="contained-image max-h-96"
              alt="Location image"
            />
          </div>
        )}
      </div>
      <div className="w-full flex flex-row">
        <div className="flex-1 basis-3/5">
          <table className="deck-table w-full">
            <tbody>
              <tr>
                <td>File link(s):</td>
                <td colSpan={3}>
                  {jobClientSignImageInfos?.map((image, i) => (
                    <div key={i}>{image.renderNameString()}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Size:</td>
                <td>
                  {width}
                  <span className="ml-4">x </span>
                  {height}
                </td>
              </tr>
              <tr>
                <td>Quantity:</td>
                <td>{quantity}</td>
              </tr>
              <tr>
                <td>Artwork received:</td>
                <td>{lineItemArtworkReceivedDate || jobArtworkReceivedDate}</td>
              </tr>
              <tr>
                <td>Artwork approved:</td>
                <td>{lineItemArtworkApprovedDate || jobArtworkApprovedDate}</td>
              </tr>
              <tr>
                <td>In-hand date:</td>
                <td>{lineItemInHandDate || jobInHandDate}</td>
              </tr>
              <tr>
                <td>Install date:</td>
                <td>{lineItemInstallDate || jobInstallDate}</td>
                <td className="non-bold">Strike date:</td>
                <td>{lineItemStrikeDate || jobStrikeDate}</td>
              </tr>
              <tr>
                <td>Install time:</td>
                <td>{lineItemInstallTime || jobInstallTime}</td>
                <td className="non-bold">Strike time:</td>
                <td>{lineItemStrikeTime || jobStrikeTime}</td>
              </tr>
              <tr>
                <td>Client contact:</td>
                <td>{clientContactName}</td>
                <td>{clientContactPhoneNumber}</td>
              </tr>
              <tr>
                <td>Property contact:</td>
                <td>{propertyContactName}</td>
                <td>{propertyContactPhoneNumber}</td>
              </tr>
              <tr>
                <td>Install contact:</td>
                <td>{installContactName}</td>
                <td>{installContactPhoneNumber}</td>
              </tr>
              <tr>
                <td>SPE contact 1:</td>
                <td>{superiorPrint1ContactName}</td>
                <td>{superiorPrint1ContactPhoneNumber}</td>
              </tr>
              <tr>
                <td>SPE contact 2:</td>
                <td>{superiorPrint2ContactName}</td>
                <td>{superiorPrint2ContactPhoneNumber}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex-1 basis-2/5">
          <table className="deck-table">
            <tbody>
              <tr>
                <td>Location:</td>
                <td>{locationName}</td>
              </tr>
              <tr>
                <td>Material:</td>
                <td>{material}</td>
              </tr>
              <tr>
                <td>Surface:</td>
                <td>{surface}</td>
              </tr>
              <tr>
                <td>Finishing:</td>
                <td>{finishing}</td>
              </tr>
              <tr>
                <td>Printer:</td>
                <td>{printer}</td>
              </tr>
              <tr>
                <td className="install-notes" colSpan={2}>
                  <div style={{ minHeight: "110px" }}>
                    INSTALLATION NOTES:
                    <br />
                    {installNoteLines.map((l, i) => (
                      <div key={i}>{l}</div>
                    ))}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
