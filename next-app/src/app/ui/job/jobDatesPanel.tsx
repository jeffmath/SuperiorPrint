import * as React from "react";
import { DateTimeField } from "@/app/ui/general/dateTimeField";
import { Control, FieldValues } from "react-hook-form";

export interface JobDatesPanelProps<V extends FieldValues> {
  control: Control<V>;
}

export function JobDatesPanel<V extends FieldValues>({ control }: JobDatesPanelProps<V>) {
  return (
    <div className="border border-solid border-black p-0 pb-2.5 px-2.5 inline-flex flex-row flex-wrap space-x-4 items-center">
      <DateTimeField
        control={control}
        fieldName="artworkReceivedDate"
        labelText="Artwork received"
      />
      <DateTimeField
        control={control}
        fieldName="artworkApprovedDate"
        labelText="Artwork approved"
      />
      <DateTimeField control={control} fieldName="shipDate" labelText="Ship date" />
      <DateTimeField control={control} fieldName="inHandDate" labelText="In-hand date" />
      <DateTimeField
        control={control}
        fieldName="installDateTime"
        labelText="Install date & time"
        shouldShowTime={true}
      />
      <DateTimeField
        control={control}
        fieldName="strikeDateTime"
        labelText="Strike date & time"
        shouldShowTime={true}
      />
    </div>
  );
}
