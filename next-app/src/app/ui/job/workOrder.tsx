import * as React from "react";
import Image from "next/image";
import Logo from "@/images/logo.jpg";
import { ClientSignImageInfo } from "@/domain/ClientSignImageInfo";
import { Id } from "@/domain/Id";

export interface WorkOrderProps {
  clientName: string;
  jobNumber: string;
  lineItemCode?: string;
  lineItemName: string;
  jobClientSignImageInfos: ClientSignImageInfo[];
  material?: string;
  finishing?: string;
  width?: number;
  height?: number;
  quantity?: number;
  jobArtworkReceivedDate?: string;
  jobArtworkApprovedDate?: string;
  jobShipDate?: string;
  jobInHandDate?: string;
  lineItemArtworkReceivedDate?: string;
  lineItemArtworkApprovedDate?: string;
  lineItemShipDate?: string;
  lineItemInHandDate?: string;
  installNoteLines: string[];
  installContactName?: string;
  locationImageId?: Id;
}

export function WorkOrder({
  clientName,
  jobNumber,
  lineItemCode,
  lineItemName,
  jobClientSignImageInfos,
  material,
  finishing,
  width,
  height,
  quantity,
  jobArtworkReceivedDate,
  jobArtworkApprovedDate,
  jobShipDate,
  jobInHandDate,
  lineItemArtworkReceivedDate,
  lineItemArtworkApprovedDate,
  lineItemShipDate,
  lineItemInHandDate,
  installNoteLines,
  installContactName,
  locationImageId,
}: WorkOrderProps) {
  return (
    <div className="work-order relative">
      <div className="absolute">
        <Image src={Logo} alt="Superior Print and Exhibit" />
      </div>
      <div className="w-full min-h-20 flex flex-row items-center justify-center">
        <div className="text-4xl font-bold italic">WORK ORDER</div>
      </div>
      <div className="flex flex-row mt-5">
        <div className="flex-1">
          <table className="work-order-table">
            <tbody>
              <tr style={{ height: "40px" }}>
                <td className="font-bold" style={{ minWidth: "100px" }}>
                  JOB NAME
                </td>
                <td>{`${clientName} - ${jobNumber}`}</td>
              </tr>
              <tr>
                <td>Code</td>
                <td>{lineItemCode}</td>
              </tr>
              <tr>
                <td>Name</td>
                <td>{lineItemName}</td>
              </tr>
              <tr>
                <td>File link(s)</td>
                <td>
                  {jobClientSignImageInfos?.map((image, i) => (
                    <div key={i}>{image.renderNameString()}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Material</td>
                <td>{material}</td>
              </tr>
              <tr>
                <td>Finishing</td>
                <td>{finishing}</td>
              </tr>
              <tr>
                <td>Width</td>
                <td>{width}</td>
              </tr>
              <tr>
                <td>Height</td>
                <td>{height}</td>
              </tr>
              <tr>
                <td>Quantity</td>
                <td>{quantity}</td>
              </tr>
              <tr>
                <td>Artwork received</td>
                <td>{lineItemArtworkReceivedDate || jobArtworkReceivedDate}</td>
              </tr>
              <tr>
                <td>Artwork approved</td>
                <td>{lineItemArtworkApprovedDate || jobArtworkApprovedDate}</td>
              </tr>
              <tr>
                <td>Notes</td>
                <td>
                  {installNoteLines.map((l, i) => (
                    <div key={i}>{l}</div>
                  ))}
                </td>
              </tr>
              <tr>
                <td>Ship date</td>
                <td>{lineItemShipDate || jobShipDate}</td>
              </tr>
              <tr>
                <td>In-hand date</td>
                <td>{lineItemInHandDate || jobInHandDate}</td>
              </tr>
              <tr>
                <td>Ship to</td>
                <td>{installContactName}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex-1 flex flex-col flex-wrap items-center">
          {locationImageId && (
            <img
              src={`/api/locationImage/${locationImageId}`}
              className="contained-image block"
              alt="Location image"
              style={{ maxHeight: "220px", marginBottom: "5px" }}
            />
          )}
          {jobClientSignImageInfos?.map((image, i) => (
            <div key={i} style={{ display: image.renderCssDisplayValue() }}>
              <img
                src={`/api/signImage/${image.renderKeyString()}`}
                className="contained-image"
                alt="Sign image"
                style={{
                  marginRight: "5px",
                  marginBottom: "5px",
                  maxHeight: "220px",
                }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
