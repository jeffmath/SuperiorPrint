"use client";

import * as React from "react";
import { Id } from "@/domain/Id";
import { Button } from "@/components/ui/button";
import { JobDto } from "@/data-transfer/JobDto";
import { useEffect, useMemo, useState } from "react";
import { Job } from "@/domain/Job";
import { ContactSelect } from "@/app/ui/job/contactSelect";
import { ContactDto } from "@/data-transfer/ContactDto";
import { updateJob } from "@/app/lib/data/jobs/updateJob";
import { useInstallContacts } from "@/app/lib/hooks/useInstallContacts";
import { useSuperiorContacts } from "@/app/lib/hooks/useSuperiorContacts";
import Link from "next/link";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { NamedEntity } from "@/domain/NamedEntity";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { deleteJob } from "@/app/lib/data/jobs/deleteJob";
import { setJobSelectionJobId } from "@/app/lib/cache/selectJobSelections";
import { JobDatesPanel } from "@/app/ui/job/jobDatesPanel";
import { convertUndefinedFieldsToNull } from "@/app/lib/util/convertUndefinedFieldsToNull";
import { useLeaveOrRefreshAppGuard } from "@/app/lib/hooks/useLeaveOrRefreshAppGuard";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";
import { useContacts } from "@/app/lib/hooks/useContacts";
import { useForm } from "react-hook-form";
import { InputField } from "@/app/ui/general/inputField";

export interface JobDetailsProps {
  jobDto: JobDto;
  jobClientDto: NamedEntityDto;
  jobPropertyDto: NamedEntityDto;
  clientContactDtos: ContactDto[];
  propertyContactDtos: ContactDto[];
}

interface DetailsFormData {
  number: string;
  clientContactId?: Id;
  propertyContactId?: Id;
  installContactId?: Id;
  superiorPrintContact1Id?: Id;
  superiorPrintContact2Id?: Id;
  artworkReceivedDate?: Date;
  artworkApprovedDate?: Date;
  shipDate?: Date;
  inHandDate?: Date;
  installDateTime?: Date;
  strikeDateTime?: Date;
}

export function JobDetails({
  jobDto,
  jobClientDto,
  jobPropertyDto,
  clientContactDtos,
  propertyContactDtos,
}: JobDetailsProps) {
  const [previousJobDto, setPreviousJobDto] = useState(jobDto);
  const job = useMemo(() => Job.fromDto(jobDto), [jobDto]);
  const jobClient = useMemo(() => NamedEntity.fromDto(jobClientDto), [jobClientDto]);
  const jobProperty = useMemo(
    () => NamedEntity.fromDto(jobPropertyDto),
    [jobPropertyDto],
  );

  const number = job.renderNumberString();
  const clientContactId = job.renderClientContactKeyString();
  const propertyContactId = job.renderPropertyContactKeyString();
  const installContactId = job.renderInstallContactKeyString();
  const superiorPrintContact1Id = job.renderSuperiorPrintContact1KeyString();
  const superiorPrintContact2Id = job.renderSuperiorPrintContact2KeyString();
  // I tired of adding renderX() methods to Job, so here I just pull the date
  // values from the DTO, instead
  const {
    artworkReceivedDate,
    artworkApprovedDate,
    shipDate,
    inHandDate,
    installDateTime,
    strikeDateTime,
  } = jobDto;
  const {
    register,
    handleSubmit,
    control,
    formState: { isDirty, isValid, errors },
    reset,
  } = useForm<DetailsFormData>({
    mode: "onChange",
    defaultValues: {
      number,
      clientContactId,
      propertyContactId,
      installContactId,
      superiorPrintContact1Id,
      superiorPrintContact2Id,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
    },
  });

  useLeaveOrRefreshAppGuard(isDirty);

  const clientContacts = useContacts(clientContactDtos);
  const propertyContacts = useContacts(propertyContactDtos);
  const { installContacts } = useInstallContacts();
  const { superiorContacts } = useSuperiorContacts();

  const [resetTo, setResetTo] = useState<DetailsFormData | undefined>();

  const { setSectionAsUnsaved, setSectionAsSaved } = useUnsavedChanges();
  useEffect(() => {
    if (isDirty) setSectionAsUnsaved("jobDetails");
  }, [isDirty, setSectionAsUnsaved]);

  // if a different job was supplied for this render
  if (jobDto !== previousJobDto) {
    // get this component's form reset to the state of the current job
    setResetTo({
      number,
      clientContactId: (clientContactId || null) as any,
      propertyContactId,
      installContactId,
      superiorPrintContact1Id,
      superiorPrintContact2Id,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
    });
    setPreviousJobDto(jobDto);
  }

  // this is an effect only because React Hook Form prescribes that calls to its
  // reset() method be made within an effect (or event handler)
  useEffect(() => {
    if (resetTo) {
      reset(resetTo);
      setResetTo(undefined);
    }
  }, [resetTo]);

  async function handleSaveClick(data: DetailsFormData) {
    const {
      number,
      clientContactId,
      propertyContactId,
      installContactId,
      superiorPrintContact1Id,
      superiorPrintContact2Id,
      artworkReceivedDate,
      artworkApprovedDate,
      shipDate,
      inHandDate,
      installDateTime,
      strikeDateTime,
    } = data;
    job.onNumberEdited(number);
    job.onClientContactSelected(clientContactId);
    job.onPropertyContactSelected(propertyContactId);
    job.onInstallContactSelected(installContactId);
    job.onSuperiorContact1Selected(superiorPrintContact1Id);
    job.onSuperiorContact2Selected(superiorPrintContact2Id);
    job.onArtworkReceivedDateEdited(artworkReceivedDate);
    job.onArtworkApprovedDateEdited(artworkApprovedDate);
    job.onShipDateEdited(shipDate);
    job.onInHandDateEdited(inHandDate);
    job.onInstallDateTimeEdited(installDateTime);
    job.onStrikeDateTimeEdited(strikeDateTime);
    const dto = convertUndefinedFieldsToNull(job.renderDto());
    await updateJob(dto);
    reset(data);
    setSectionAsSaved("jobDetails");
  }

  async function handleDeleteClick() {
    const confirmed = await yesNoModal(
      `Are you sure you want to delete this job, and all its line items?`,
    );
    if (confirmed) {
      await deleteJob(job.renderKeyString());
      await setJobSelectionJobId("1", undefined);
    }
  }

  return (
    <form onSubmit={handleSubmit(handleSaveClick)}>
      <div className="mt-8 flex flex-col space-y-4">
        <div className="flex flex-row space-x-4 items-center">
          <h4 className="form-header">Job details</h4>
          <Button disabled={!isDirty || !isValid}>Save</Button>
          <Button variant="destructive" onClick={handleDeleteClick}>
            Delete
          </Button>
        </div>
        <div className="text-lg">
          <span className="font-bold">Client:</span>&nbsp;
          <Link
            href={`/entity/client/${jobClient.renderKeyString()}`}
            className="hover:underline"
          >
            {jobClient.renderNameString()}
          </Link>
          <span className="font-bold ml-7">Property:</span>&nbsp;
          <Link
            href={`/entity/property/${jobProperty.renderKeyString()}`}
            className="hover:underline"
          >
            {jobProperty.renderNameString()}
          </Link>
        </div>
        <div>
          <div className="flex flex-row flex-wrap space-x-6 items-baseline">
            <InputField
              {...register("number", { required: true })}
              label="Number"
              id="jobNumberField"
              className="min-w-24"
              isRequired={true}
              error={errors.number}
            />
            <ContactSelect
              fieldName="clientContactId"
              control={control}
              labelText="Client contact"
              contacts={clientContacts}
            />
            <ContactSelect
              fieldName="propertyContactId"
              control={control}
              labelText="Property contact"
              contacts={propertyContacts}
            />
            <ContactSelect
              fieldName="installContactId"
              control={control}
              labelText="Install contact"
              contacts={installContacts}
            />
            <ContactSelect
              fieldName="superiorPrintContact1Id"
              control={control}
              labelText="Superior P&E contact 1"
              contacts={superiorContacts}
            />
            <ContactSelect
              fieldName="superiorPrintContact2Id"
              control={control}
              labelText="Superior P&E contact 2"
              contacts={superiorContacts}
            />
          </div>
        </div>
        <div>
          <h5 className="font-bold">Default dates/times</h5>
          <JobDatesPanel control={control} />
        </div>
      </div>
    </form>
  );
}
