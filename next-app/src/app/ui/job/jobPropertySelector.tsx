"use client";

import * as React from "react";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { PropertyStub } from "@/data-transfer/PropertyStub";
import { Id } from "@/domain/Id";
import { setJobSelectionPropertyId } from "@/app/lib/cache/selectJobSelections";
import { useUnsavedChanges } from "@/app/lib/hooks/useUnsavedChanges";

export interface JobPropertySelectorProps {
  propertyStubs: PropertyStub[];
  jobPropertyId?: Id;
}

export function JobPropertySelector({
  propertyStubs,
  jobPropertyId: selectedPropertyId,
}: JobPropertySelectorProps) {
  const { checkToConfirmDiscard } = useUnsavedChanges();
  async function handlePropertySelection(id: Id) {
    const mayProceed = await checkToConfirmDiscard();
    if (mayProceed) await setJobSelectionPropertyId("1", id);
  }

  if (!propertyStubs.length) return null;
  return (
    <div>
      <Label htmlFor="propertySelect">Property</Label>
      <Select value={selectedPropertyId || ""} onValueChange={handlePropertySelection}>
        <SelectTrigger id="propertySelect" className="min-w-36">
          <SelectValue />
        </SelectTrigger>
        <SelectContent>
          {propertyStubs.map((property, i) => (
            <SelectItem key={i} value={property.id}>
              {property.name}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
    </div>
  );
}
