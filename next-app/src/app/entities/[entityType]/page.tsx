import { ContactType, getEntityTypeWithName } from "@/app/lib/entityType";
import * as React from "react";
import { EntitiesTable } from "@/app/ui/entities/entitiesTable";
import { fetchEntitiesPage } from "@/app/lib/data/pagedEntities/fetchEntitiesPage";
import { SearchField } from "@/app/ui/searchFilters/searchField";
import { FirstLetterFilter } from "@/app/ui/searchFilters/firstLetterFilter";
import { PageSelector } from "@/app/ui/searchFilters/pageSelector";
import { ContactsSearchBar } from "@/app/ui/searchFilters/contactsSearchBar";
import { getFindEntitiesFilters } from "@/app/lib/cache/findEntitiesFilters";
import { getFindContactsFilters } from "@/app/lib/cache/findContactsFilters";

export default async function EntitiesPage({
  params,
}: {
  params: { entityType: string };
}) {
  const entityType = getEntityTypeWithName(params.entityType);
  const isForContacts = entityType === ContactType;
  const { searchTerm, firstLetter, page } = await getFindEntitiesFilters("1", entityType);

  const contactsFilters = isForContacts ? await getFindContactsFilters("1") : undefined;
  const contactsFilter = isForContacts
    ? (filter: any, convertId: (id: string) => any) => {
        const { installersOnly, clientId, propertyId } = contactsFilters!;
        if (installersOnly) filter.isInstaller = true;
        if (clientId) filter.clientId = convertId(clientId);
        if (propertyId) filter.propertyId = convertId(propertyId);
      }
    : undefined;

  const { entityDtos, total: totalEntities } = await fetchEntitiesPage(
    entityType,
    page ? page - 1 : 0,
    10,
    firstLetter || "*",
    searchTerm || "",
    contactsFilter,
  );

  return (
    <main className="text-center">
      <SearchField
        domain="names"
        entityTypeName={params.entityType}
        searchTerm={searchTerm}
      />
      {isForContacts && (
        <ContactsSearchBar
          className="mt-3.5"
          installersOnly={contactsFilters!.installersOnly}
          clientId={contactsFilters!.clientId}
          propertyId={contactsFilters!.propertyId}
        />
      )}
      <FirstLetterFilter
        className="mt-3"
        entityTypeName={params.entityType}
        firstLetter={firstLetter}
      />
      <PageSelector
        className="mt-2.5"
        totalItems={totalEntities}
        entityTypeName={params.entityType}
        page={page}
      />
      <EntitiesTable entityDtos={entityDtos} entityTypeName={params.entityType} />
    </main>
  );
}
