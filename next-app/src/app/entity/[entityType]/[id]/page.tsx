import { Id } from "@/domain/Id";
import {
  ClientType,
  ContactType,
  getEntityTypeWithName,
  PropertyType,
} from "@/app/lib/entityType";
import { EntityForm } from "@/app/ui/entity/entityForm";
import { fetchNamedEntity } from "@/app/lib/data/namedEntities/fetchNamedEntity";
import { ClientSignImagesTable } from "@/app/ui/client/clientSignImagesTable";
import { fetchClientSignImageInfos } from "@/app/lib/data/clientSignImages/fetchClientSignImageInfos";
import { LocationsTable } from "@/app/ui/property/locationsTable";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { fetchProperty } from "@/app/lib/data/properties/fetchProperty";
import { fetchContact } from "@/app/lib/data/contacts/fetchContact";

export default async function EntityPage({
  params,
}: {
  params: { entityType: string; id: Id };
}) {
  const { entityType: entityTypeName, id } = params;
  const entityType = getEntityTypeWithName(entityTypeName);
  const isForClient = entityType === ClientType;
  const isForProperty = entityType === PropertyType;
  const isForContact = entityType === ContactType;
  const entityDto = isForProperty
    ? await fetchProperty(id)
    : isForContact
      ? await fetchContact(id)
      : await fetchNamedEntity(entityType, id);
  const clientSignImageDtos = isForClient ? await fetchClientSignImageInfos(id) : [];

  if (!entityDto) return null;
  return (
    <main>
      <EntityForm entityTypeName={entityTypeName} entityDto={entityDto} />
      {entityType === PropertyType && (
        <LocationsTable propertyDto={entityDto as PropertyDto} />
      )}
      {entityType === ClientType && (
        <ClientSignImagesTable clientId={id} imageDtos={clientSignImageDtos} />
      )}
    </main>
  );
}
