/**
 * This is needed because either Reach Server Components or Next.js prunes all
 * fields set to undefined of objects sent as parameters to a server action.
 */
export function convertUndefinedFieldsToNull(obj: any): any {
  Object.keys(obj).forEach((key) => {
    if (obj[key] === undefined) obj[key] = null;
  });
  return obj;
}
