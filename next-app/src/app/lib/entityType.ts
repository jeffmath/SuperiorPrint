export interface EntityType {
  name: string;
  pluralName: string;
}

/**
 * Paged entity types
 */
export const ClientType: EntityType = { name: "client", pluralName: "clients" };
export const ContactType: EntityType = { name: "contact", pluralName: "contacts" };
export const PropertyType: EntityType = { name: "property", pluralName: "properties" };

/**
 * Small-set entity types
 */
export const FinishingType: EntityType = { name: "finishing", pluralName: "finishings" };
export const MaterialType: EntityType = { name: "material", pluralName: "materials" };
export const PrinterType: EntityType = { name: "printer", pluralName: "printers" };
export const SurfaceType: EntityType = { name: "surface", pluralName: "surfaces" };
export const UserType: EntityType = { name: "user", pluralName: "users" };

export function getEntityTypeWithName(name: string): EntityType {
  switch (name) {
    case "client":
    case "clients":
      return ClientType;
    case "contact":
    case "contacts":
      return ContactType;
    case "property":
    case "properties":
      return PropertyType;
    case "finishing":
    case "finishings":
      return FinishingType;
    case "material":
    case "materials":
      return MaterialType;
    case "printer":
    case "printers":
      return PrinterType;
    case "surface":
    case "surfaces":
      return SurfaceType;
    case "user":
    case "users":
      return UserType;
    default:
      throw new Error(`Incorrect entity-type name specified: ${name}.`);
  }
}

export function isPagedEntityType(entityType: EntityType) {
  switch (entityType) {
    case ClientType:
    case PropertyType:
    case ContactType:
      return true;
    default:
      return false;
  }
}
