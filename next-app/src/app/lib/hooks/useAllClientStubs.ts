import { NamedEntity } from "@/domain/NamedEntity";
import { fetchAllClientStubs } from "@/app/lib/data/clients/fetchAllClientStubs";
import { ClientStub } from "@/data-transfer/ClientStub";

export function useAllClientStubs() {
  function toNamedEntities(stubs: ClientStub[]): NamedEntity[] {
    return stubs.map((stub) => NamedEntity.fromDto(stub));
  }
  return {
    allClients,
    supply: (stubs: ClientStub[]) => {
      allClients = toNamedEntities(stubs);
    },
    refetch: async () => {
      const stubs = await fetchAllClientStubs();
      allClients = toNamedEntities(stubs);
    },
  };
}

let allClients: NamedEntity[] = [];
