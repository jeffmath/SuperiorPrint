import { Contact } from "@/domain/Contact";
import { ContactDto } from "@/data-transfer/ContactDto";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { fetchSuperiorContacts } from "@/app/lib/data/contacts/fetchSuperiorContacts";

export function useSuperiorContacts() {
  const { allClients } = useAllClientStubs();
  const { allProperties } = useAllPropertyStubs();
  function toContacts(contactDtos: ContactDto[]): Contact[] {
    return contactDtos.map((dto) =>
      Contact.fromContactDto(dto, allClients, allProperties),
    );
  }
  return {
    superiorContacts,
    supply: (contactDtos: ContactDto[]) => {
      superiorContacts = toContacts(contactDtos);
    },
    refetch: async () => {
      const contactDtos = await fetchSuperiorContacts();
      superiorContacts = toContacts(contactDtos);
    },
  };
}

let superiorContacts: Contact[] = [];
