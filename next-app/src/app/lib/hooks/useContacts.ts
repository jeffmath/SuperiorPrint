import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useMemo } from "react";
import { Contact } from "@/domain/Contact";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { ContactDto } from "@/data-transfer/ContactDto";

export function useContacts(contactDtos: ContactDto[]) {
  const { allClients } = useAllClientStubs();
  const { allProperties } = useAllPropertyStubs();
  return useMemo(
    () =>
      contactDtos.map((dto) => Contact.fromContactDto(dto, allClients, allProperties)),
    [contactDtos, allClients, allProperties],
  );
}
