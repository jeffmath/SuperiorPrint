import { useContext } from "react";
import { UnsavedChangesContext, UnsavedSection } from "@/app/ui/unsavedChanges";
import * as React from "react";
import { yesNoModal } from "@/app/ui/modals/yesNoModal";
import { useRouter } from "next/navigation";

export function useUnsavedChanges(path?: string) {
  const [unsavedSections, setUnsavedSections] = useContext(UnsavedChangesContext);
  const { push } = useRouter();
  const areUnsavedChanges = !!unsavedSections.length;

  async function handleClickToLeavePage(e: React.MouseEvent) {
    if (path && areUnsavedChanges) {
      e.preventDefault();

      const confirmed = await yesNoModal(
        "Are you sure you wish to leave this page, and have your unsaved changes discarded?",
      );
      if (confirmed) {
        setUnsavedSections([]);
        push(path);
      }
    }
  }

  async function checkToConfirmDiscard(): Promise<boolean> {
    if (areUnsavedChanges) {
      const confirmed = await yesNoModal(
        "Are you sure you wish to leave the current job, and have your unsaved changes discarded?",
      );
      if (confirmed) setUnsavedSections([]);
      return confirmed;
    }
    return true;
  }

  function setSectionAsUnsaved(section: UnsavedSection) {
    if (unsavedSections.indexOf(section) < 0) {
      setUnsavedSections([...unsavedSections, section]);
    }
  }

  function setSectionAsSaved(section: UnsavedSection) {
    setUnsavedSections(unsavedSections.filter((unsaved) => unsaved !== section));
  }

  return {
    areUnsavedChanges,
    setSectionAsUnsaved,
    setSectionAsSaved,
    handleClickToLeavePage,
    checkToConfirmDiscard,
  };
}
