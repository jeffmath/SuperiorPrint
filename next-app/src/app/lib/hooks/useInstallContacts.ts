import { Contact } from "@/domain/Contact";
import { ContactDto } from "@/data-transfer/ContactDto";
import { useAllClientStubs } from "@/app/lib/hooks/useAllClientStubs";
import { useAllPropertyStubs } from "@/app/lib/hooks/useAllPropertyStubs";
import { fetchInstallContacts } from "@/app/lib/data/contacts/fetchInstallContacts";

export function useInstallContacts() {
  const { allClients } = useAllClientStubs();
  const { allProperties } = useAllPropertyStubs();
  function toContacts(contactDtos: ContactDto[]): Contact[] {
    return contactDtos.map((dto) =>
      Contact.fromContactDto(dto, allClients, allProperties),
    );
  }
  return {
    installContacts,
    supply: (contactDtos: ContactDto[]) => {
      installContacts = toContacts(contactDtos);
    },
    refetch: async () => {
      const contactDtos = await fetchInstallContacts();
      installContacts = toContacts(contactDtos);
    },
  };
}

let installContacts: Contact[] = [];
