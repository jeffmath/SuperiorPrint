import { NamedEntity } from "@/domain/NamedEntity";
import { EntityType } from "@/app/lib/entityType";
import { fetchNamedEntities } from "@/app/lib/data/namedEntities/fetchNamedEntities";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";

export function useAllSmallSetEntities(entityType: EntityType) {
  return {
    entities: allSmallSetEntities[entityType.name] || [],
    supply: (entityDtos: NamedEntityDto[]) => {
      allSmallSetEntities[entityType.name] = entityDtos.map((dto) =>
        NamedEntity.fromDto(dto),
      );
    },
    refetch: async () => {
      const entityDtos = await fetchNamedEntities(entityType);
      allSmallSetEntities[entityType.name] = entityDtos.map((dto) =>
        NamedEntity.fromDto(dto),
      );
    },
  };
}

let allSmallSetEntities: { [entityTypeName: string]: NamedEntity[] } = {};
