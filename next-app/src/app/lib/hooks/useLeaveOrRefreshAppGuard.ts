import { useEffect } from "react";

export function useLeaveOrRefreshAppGuard(isDirty: boolean) {
  useEffect(() => {
    function beforeUnload(e: BeforeUnloadEvent) {
      if (isDirty) {
        if (!confirm()) e.preventDefault();
      }
    }

    window.addEventListener("beforeunload", beforeUnload);

    return () => {
      window.removeEventListener("beforeunload", beforeUnload);
    };
  }, [isDirty]);
}
