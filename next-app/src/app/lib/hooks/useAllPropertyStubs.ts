import { NamedEntity } from "@/domain/NamedEntity";
import { PropertyStub } from "@/data-transfer/PropertyStub";
import { fetchAllPropertyStubs } from "@/app/lib/data/properties/fetchAllPropertyStubs";

export function useAllPropertyStubs() {
  return {
    allProperties,
    supply: (stubs: PropertyStub[]) => {
      allProperties = stubs.map((stub) => NamedEntity.fromDto(stub));
    },
    refetch: async () => {
      const stubs = await fetchAllPropertyStubs();
      allProperties = stubs.map((stub) => NamedEntity.fromDto(stub));
    },
  };
}

let allProperties: NamedEntity[] = [];
