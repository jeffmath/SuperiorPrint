"use server";

import { Id } from "@/domain/Id";
import { revalidateTag, unstable_cache } from "next/cache";

export interface FindContactsFilters {
  installersOnly: boolean;
  clientId?: Id;
  propertyId?: Id;
}

const findContactsFilters: { [userId: Id]: FindContactsFilters } = {};

export async function getFindContactsFilters(userId: Id): Promise<FindContactsFilters> {
  return unstable_cache(
    async (key: string) => findContactsFilters[key] || {},
    undefined,
    { tags: [getRevalidationTag(userId)] },
  )(userId);
}

function getRevalidationTag(key: string) {
  return `findContactsFilters-${key}`;
}

export async function setContactsFiltersInstallersOnly(
  userId: Id,
  installersOnly: boolean,
) {
  const filters = await getFindContactsFilters(userId);
  filters.installersOnly = installersOnly;
  findContactsFilters[userId] = filters;
  revalidateTag(getRevalidationTag(userId));
}

export async function setContactsFiltersClientId(userId: Id, clientId?: Id) {
  const filters = await getFindContactsFilters(userId);
  filters.clientId = clientId;
  findContactsFilters[userId] = filters;
  revalidateTag(getRevalidationTag(userId));
}

export async function setContactsFiltersPropertyId(userId: Id, propertyId?: Id) {
  const filters = await getFindContactsFilters(userId);
  filters.propertyId = propertyId;
  findContactsFilters[userId] = filters;
  revalidateTag(getRevalidationTag(userId));
}
