"use server";

import { Id } from "@/domain/Id";
import { revalidateTag, unstable_cache } from "next/cache";
import { EntityType } from "@/app/lib/entityType";

export interface FindEntitiesFilters {
  searchTerm?: string;
  firstLetter: string;
  page: number;
}

const findEntitiesFilters: { [key: string]: FindEntitiesFilters } = {};

function makeKey(userId: Id, entityType: EntityType): string {
  return `${userId}-${entityType.pluralName}`;
}

export async function getFindEntitiesFilters(
  userId: Id,
  entityType: EntityType,
): Promise<FindEntitiesFilters> {
  const key = makeKey(userId, entityType);
  return unstable_cache(
    async (key: string) => findEntitiesFilters[key] || {},
    undefined,
    { tags: [getRevalidationTag(key)] },
  )(key);
}

function getRevalidationTag(key: string) {
  return `findEntitiesFilters-${key}`;
}

export async function setEntitiesFiltersSearchTerm(
  userId: Id,
  entityType: EntityType,
  searchTerm?: string,
) {
  const filters = await getFindEntitiesFilters(userId, entityType);
  filters.searchTerm = searchTerm;
  const key = makeKey(userId, entityType);
  findEntitiesFilters[key] = filters;
  revalidateTag(getRevalidationTag(key));
}

export async function setEntitiesFiltersFirstLetter(
  userId: Id,
  entityType: EntityType,
  firstLetter: string,
) {
  const filters = await getFindEntitiesFilters(userId, entityType);
  filters.firstLetter = firstLetter;
  const key = makeKey(userId, entityType);
  findEntitiesFilters[key] = filters;
  revalidateTag(getRevalidationTag(key));
}

export async function setEntitiesFiltersPage(
  userId: Id,
  entityType: EntityType,
  page: number,
) {
  const filters = await getFindEntitiesFilters(userId, entityType);
  filters.page = page;
  const key = makeKey(userId, entityType);
  findEntitiesFilters[key] = filters;
  revalidateTag(getRevalidationTag(key));
}
