"use server";

import { Id } from "@/domain/Id";
import { revalidateTag, unstable_cache } from "next/cache";

export interface LineItemSelection {
  selectedLineItemId?: Id;
}

const lineItemSelection: { [userId: Id]: LineItemSelection } = {};

export async function getLineItemSelection(userId: Id): Promise<LineItemSelection> {
  return unstable_cache(
    async (userId: Id) => lineItemSelection[userId] || {},
    undefined,
    { tags: [getRevalidationTag(userId)] },
  )(userId);
}

function getRevalidationTag(userId: Id) {
  return `lineItemSelection-${userId}`;
}

export async function setSelectedLineItemId(userId: Id, lineItemId?: Id) {
  const selection = await getLineItemSelection(userId);
  selection.selectedLineItemId = lineItemId;
  lineItemSelection[userId] = selection;
  revalidateTag(getRevalidationTag(userId));
}
