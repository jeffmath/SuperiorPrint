"use server";

import { Id } from "@/domain/Id";
import { revalidateTag, unstable_cache } from "next/cache";

export interface SelectJobSelections {
  clientId?: Id;
  propertyId?: Id;
  jobId?: Id;
}

const selectJobSelections: { [userId: Id]: SelectJobSelections } = {};

export async function getSelectJobSelections(userId: Id): Promise<SelectJobSelections> {
  return unstable_cache(
    async (userId: Id) => selectJobSelections[userId] || {},
    undefined,
    { tags: [getRevalidationTag(userId)] },
  )(userId);
}

function getRevalidationTag(userId: Id) {
  return `jobSelections-${userId}`;
}

export async function setAllJobSelections(
  userId: Id,
  clientId: Id,
  propertyId: Id,
  jobId: Id,
) {
  const selections = await getSelectJobSelections(userId);
  selections.clientId = clientId;
  selections.propertyId = propertyId;
  selections.jobId = jobId;
  selectJobSelections[userId] = selections;
  revalidateTag(getRevalidationTag(userId));
}

export async function setJobSelectionClientId(userId: Id, clientId: Id) {
  const selections = await getSelectJobSelections(userId);
  selections.clientId = clientId;
  selections.propertyId = undefined;
  selections.jobId = undefined;
  selectJobSelections[userId] = selections;
  revalidateTag(getRevalidationTag(userId));
}

export async function setJobSelectionPropertyId(userId: Id, propertyId: Id) {
  const selections = await getSelectJobSelections(userId);
  selections.propertyId = propertyId;
  selections.jobId = undefined;
  selectJobSelections[userId] = selections;
  revalidateTag(getRevalidationTag(userId));
}

export async function setJobSelectionJobId(userId: Id, jobId?: Id) {
  const selections = await getSelectJobSelections(userId);
  selections.jobId = jobId;
  selectJobSelections[userId] = selections;
  revalidateTag(getRevalidationTag(userId));
}
