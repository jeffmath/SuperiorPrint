"use server";

import { LineItemDto } from "@/data-transfer/LineItemDto";
import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import {
  MongoJobDto,
  MongoLineItemDto,
  toLineItemDto,
} from "@/app/lib/data/mongo/mongoJobDto";
import { getCollection } from "@/app/lib/data/getCollection";

export async function createLineItem(jobId: Id): Promise<LineItemDto> {
  const collection = await getCollection<MongoJobDto>("Jobs");
  const job = await collection.findOne({ _id: new ObjectId(jobId) });
  if (!job) throw new Error("Job not found to which a line item should be added.");
  const lineItem = {
    _id: new ObjectId(),
    name: "",
    clientSignImageIds: [],
  } as MongoLineItemDto;
  job.lineItems.push(lineItem);
  await collection.updateOne({ _id: job._id }, { $set: job });
  return toLineItemDto(lineItem);
}
