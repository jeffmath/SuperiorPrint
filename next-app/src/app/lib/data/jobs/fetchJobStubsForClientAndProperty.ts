"use server";

import { JobStub } from "@/data-transfer/JobDto";
import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import {
  jobStubProjector,
  MongoJobStub,
  toJobStubs,
} from "@/app/lib/data/mongo/mongoJobStubs";
import { getCollection } from "@/app/lib/data/getCollection";
import { unstable_cache } from "next/cache";
import { jobStubsForClientAndPropertyTag } from "@/app/lib/data/revalidationTags";

export const fetchJobStubsForClientAndProperty = unstable_cache(
  async (clientId: Id, propertyId: Id): Promise<JobStub[]> => {
    const filter = {
      clientId: new ObjectId(clientId),
      propertyId: new ObjectId(propertyId),
    };
    const collection = await getCollection("Jobs");
    const stubs = await collection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  },
  undefined,
  { tags: [jobStubsForClientAndPropertyTag] },
);
