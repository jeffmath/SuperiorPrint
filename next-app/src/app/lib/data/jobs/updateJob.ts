"use server";

import { JobDto } from "@/data-transfer/JobDto";
import { MongoJobDto, toMongoJobDto } from "@/app/lib/data/mongo/mongoJobDto";
import { getCollection } from "@/app/lib/data/getCollection";
import { revalidateTag } from "next/cache";
import { jobStubsForClientAndPropertyTag } from "@/app/lib/data/revalidationTags";

export async function updateJob(jobDto: JobDto): Promise<void> {
  const mongoJob = toMongoJobDto(jobDto);
  const collection = await getCollection<MongoJobDto>("Jobs");
  await collection.updateOne({ _id: mongoJob._id }, { $set: mongoJob });
}
