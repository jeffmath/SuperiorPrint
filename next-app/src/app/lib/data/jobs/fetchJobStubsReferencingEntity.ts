"use server";

import { JobStub } from "@/data-transfer/JobDto";
import { ObjectId } from "mongodb";
import {
  jobStubProjector,
  MongoJobStub,
  toJobStubs,
} from "@/app/lib/data/mongo/mongoJobStubs";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoJobDto } from "@/app/lib/data/mongo/mongoJobDto";

export async function fetchJobStubsReferencingEntity(
  entityId: string,
): Promise<JobStub[]> {
  const collection = await getCollection<MongoJobDto>("Jobs");
  const entityObjectId = new ObjectId(entityId);
  const filter = {
    $or: [
      { clientId: entityObjectId },
      { propertyId: entityObjectId },
      { clientContactId: entityObjectId },
      { propertyContactId: entityObjectId },
      { installContactId: entityObjectId },
      { superiorPrintContact1Id: entityObjectId },
      { superiorPrintContact2Id: entityObjectId },
      { "lineItems.surfaceId": entityObjectId },
      { "lineItems.materialId": entityObjectId },
      { "lineItems.finishingId": entityObjectId },
      { "lineItems.printerId": entityObjectId },
    ],
  };
  const stubs = await collection
    .find(filter)
    .project<MongoJobStub>(jobStubProjector)
    .sort({ number: 1 })
    .toArray();
  return toJobStubs(stubs);
}
