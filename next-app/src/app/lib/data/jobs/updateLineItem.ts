"use server";

import { LineItemDto } from "@/data-transfer/LineItemDto";
import { Id } from "@/domain/Id";
import { remove } from "lodash";
import { fetchJob } from "@/app/lib/data/jobs/fetchJob";
import { updateJob } from "@/app/lib/data/jobs/updateJob";

export async function updateLineItem(jobId: Id, lineItemDto: LineItemDto) {
  const jobDto = await fetchJob(jobId);

  // swap out the existing line item for the changed version we were given
  if (!remove(jobDto.lineItems, (item) => item.id === lineItemDto.id).length)
    throw new Error("Line-item to update not found.");
  jobDto.lineItems.push(lineItemDto);

  // save the above change
  await updateJob(jobDto);
}
