"use server";

import { JobDto } from "@/data-transfer/JobDto";
import { Id } from "@/domain/Id";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoJobDto, toJobDto } from "@/app/lib/data/mongo/mongoJobDto";
import { ObjectId } from "mongodb";
import { sortBy } from "lodash";

export async function fetchJob(id: Id): Promise<JobDto> {
  const collection = await getCollection<MongoJobDto>("Jobs");
  const job = await collection.findOne({ _id: new ObjectId(id) });
  if (!job) throw new Error("Job not found.");

  // sort the job's line-items by their name
  job.lineItems = sortBy(job.lineItems, (l) => l.name);
  return toJobDto(job);
}
