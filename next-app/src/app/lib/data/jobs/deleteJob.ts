"use server";

import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoJobDto } from "@/app/lib/data/mongo/mongoJobDto";
import { revalidateTag } from "next/cache";
import { jobStubsForClientAndPropertyTag } from "@/app/lib/data/revalidationTags";

export async function deleteJob(id: Id): Promise<void> {
  const collection = await getCollection<MongoJobDto>("Jobs");
  await collection.deleteOne({ _id: new ObjectId(id) });
  revalidateTag(jobStubsForClientAndPropertyTag);
}
