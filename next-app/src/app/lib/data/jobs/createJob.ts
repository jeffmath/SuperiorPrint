"use server";

import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { MongoJobDto } from "@/app/lib/data/mongo/mongoJobDto";
import { getCollection } from "@/app/lib/data/getCollection";
import { fetchSuperiorContacts } from "@/app/lib/data/contacts/fetchSuperiorContacts";
import { toId } from "@/app/lib/data/mongo/mongoId";
import { revalidatePath, revalidateTag } from "next/cache";
import {
  jobStubsForClientAndPropertyTag,
  propertyStubsForClientTag,
} from "@/app/lib/data/revalidationTags";

export async function createJob(clientId: Id, propertyId: Id): Promise<Id> {
  const contacts = await fetchSuperiorContacts();
  const find = (name: string) => contacts.find((c) => c.name.startsWith(name));
  const robert = find("Robert")!;
  const joni = find("Joni")!;
  const job = {
    _id: new ObjectId(),
    clientId: new ObjectId(clientId),
    propertyId: new ObjectId(propertyId),
    number: "",
    superiorPrintContact1Id: new ObjectId(robert.id),
    superiorPrintContact2Id: new ObjectId(joni.id),
    lineItems: [],
  } as MongoJobDto;
  const collection = await getCollection<MongoJobDto>("Jobs");
  await collection.insertOne(job);
  revalidateTag(propertyStubsForClientTag);
  revalidateTag(jobStubsForClientAndPropertyTag);
  revalidatePath("/jobs");
  return toId(job._id)!;
}
