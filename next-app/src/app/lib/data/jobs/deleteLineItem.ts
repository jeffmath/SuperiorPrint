"use server";

import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { remove } from "lodash";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoJobDto } from "@/app/lib/data/mongo/mongoJobDto";
import { revalidatePath } from "next/cache";

export async function deleteLineItem(jobId: Id, lineItemId: Id): Promise<void> {
  const collection = await getCollection<MongoJobDto>("Jobs");
  const job = await collection.findOne({ _id: new ObjectId(jobId) });
  if (!job) throw new Error("Job not found from which a line item should be deleted.");
  remove(job.lineItems, (item) => item._id.toHexString() === lineItemId);
  await collection.updateOne({ _id: job._id }, { $set: job });
  revalidatePath("/jobs");
}
