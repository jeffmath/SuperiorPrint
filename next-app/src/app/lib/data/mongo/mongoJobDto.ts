import { JobDto } from "@/data-transfer/JobDto";
import { ObjectId } from "mongodb";
import { LineItemDto } from "@/data-transfer/LineItemDto";
import { toId, toMongoId } from "./mongoId";

export type MongoJobDto = Omit<
  JobDto,
  | "id"
  | "clientId"
  | "propertyId"
  | "clientContactId"
  | "propertyContactId"
  | "installContactId"
  | "superiorPrintContact1Id"
  | "superiorPrintContact2Id"
  | "lineItems"
> & {
  _id: ObjectId;
  clientId: ObjectId;
  propertyId: ObjectId;
  clientContactId?: ObjectId;
  propertyContactId?: ObjectId;
  installContactId?: ObjectId;
  superiorPrintContact1Id?: ObjectId;
  superiorPrintContact2Id?: ObjectId;
  lineItems: MongoLineItemDto[];
};

export type MongoLineItemDto = Omit<
  LineItemDto,
  | "id"
  | "locationId"
  | "surfaceId"
  | "materialId"
  | "finishingId"
  | "printerId"
  | "clientSignImageIds"
> & {
  _id: ObjectId;
  locationId?: ObjectId;
  surfaceId?: ObjectId;
  materialId?: ObjectId;
  finishingId?: ObjectId;
  printerId?: ObjectId;
  clientSignImageIds: ObjectId[];
};

export function toJobDto(job: MongoJobDto): JobDto {
  const result: JobDto & Partial<Pick<MongoJobDto, "_id">> = {
    ...job,
    id: toId(job._id)!,
    clientId: toId(job.clientId)!,
    propertyId: toId(job.propertyId)!,
    clientContactId: toId(job.clientContactId),
    propertyContactId: toId(job.propertyContactId),
    installContactId: toId(job.installContactId),
    superiorPrintContact1Id: toId(job.superiorPrintContact1Id),
    superiorPrintContact2Id: toId(job.superiorPrintContact2Id),
    lineItems: job.lineItems.map((item) => toLineItemDto(item)),
  };
  delete result._id;
  return result;
}

export function toMongoJobDto(job: JobDto): MongoJobDto {
  const result: MongoJobDto & Partial<Pick<JobDto, "id">> = {
    ...job,
    _id: toMongoId(job.id)!,
    clientId: toMongoId(job.clientId)!,
    propertyId: toMongoId(job.propertyId)!,
    clientContactId: toMongoId(job.clientContactId),
    propertyContactId: toMongoId(job.propertyContactId),
    installContactId: toMongoId(job.installContactId),
    superiorPrintContact1Id: toMongoId(job.superiorPrintContact1Id),
    superiorPrintContact2Id: toMongoId(job.superiorPrintContact2Id),
    lineItems: job.lineItems.map((item) => toMongoLineItemDto(item)),
  };
  delete result.id;
  return result;
}

export function toLineItemDto(lineItem: MongoLineItemDto): LineItemDto {
  const result: LineItemDto & Partial<Pick<MongoLineItemDto, "_id">> = {
    ...lineItem,
    id: toId(lineItem._id)!,
    locationId: toId(lineItem.locationId),
    surfaceId: toId(lineItem.surfaceId),
    materialId: toId(lineItem.materialId),
    finishingId: toId(lineItem.finishingId),
    printerId: toId(lineItem.printerId),
    clientSignImageIds: lineItem.clientSignImageIds.map((mongoId) => toId(mongoId)!),
  };
  delete result._id;
  return result;
}

export function toMongoLineItemDto(lineItem: LineItemDto): MongoLineItemDto {
  const result: MongoLineItemDto & Partial<Pick<LineItemDto, "id">> = {
    ...lineItem,
    _id: toMongoId(lineItem.id)!,
    locationId: toMongoId(lineItem.locationId),
    surfaceId: toMongoId(lineItem.surfaceId),
    materialId: toMongoId(lineItem.materialId),
    finishingId: toMongoId(lineItem.finishingId),
    printerId: toMongoId(lineItem.printerId),
    clientSignImageIds: lineItem.clientSignImageIds.map((id) => toMongoId(id)!),
  };
  delete result.id;
  return result;
}
