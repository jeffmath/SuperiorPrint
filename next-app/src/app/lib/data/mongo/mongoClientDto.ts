import { ClientDto } from "@/data-transfer/ClientDto";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { ObjectId, Document } from "mongodb";
import {
  ClientSignImageDto,
  ClientSignImageInfoDto,
} from "@/data-transfer/ClientSignImageDto";
import { ClientStub } from "@/data-transfer/ClientStub";

export function toClientDto(client: MongoClientDto): ClientDto {
  const result = {
    ...client,
    _id: undefined,
    id: toId(client._id)!,
  };
  delete result._id;
  return result;
}

export type MongoClientDto = Omit<ClientDto, "id"> & { _id: ObjectId };

export type MongoClientStub = Document & Omit<ClientStub, "id"> & { _id: ObjectId };

export function toClientStub(stub: MongoClientStub): ClientStub {
  return {
    id: toId(stub._id)!,
    name: stub.name,
  };
}

export type MongoClientSignImageInfoDto = Omit<ClientSignImageInfoDto, "id"> & {
  _id: ObjectId;
};

export type MongoClientSignImageDto = Omit<ClientSignImageDto, "id" | "clientId"> & {
  _id: ObjectId;
  clientId: ObjectId;
};

function toMongoClientSignImageDto(image: ClientSignImageDto): MongoClientSignImageDto {
  const result = {
    ...image,
    id: undefined,
    _id: new ObjectId(),
    clientId: toMongoId(image.clientId)!,
  };
  delete result.id;
  return result;
}
