import { ImageDto } from "@/data-transfer/ImageDto";
import { Document } from "mongodb";

export type MongoImageDto = Document & ImageDto;
