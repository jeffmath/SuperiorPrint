import { ObjectId } from "mongodb";
import { Id } from "@/domain/Id";

export function toId(mongoId?: ObjectId): Id | undefined {
  return mongoId?.toHexString();
}

export function toMongoId(id?: Id): ObjectId | undefined {
  return id ? new ObjectId(id) : undefined;
}
