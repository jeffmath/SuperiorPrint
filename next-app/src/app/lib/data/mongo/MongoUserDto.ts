import { Document, ObjectId } from "mongodb";
import { UserDto } from "@/data-transfer/UserDto";

export type MongoUserDto = Document & Omit<UserDto, "id"> & { _id: ObjectId };
