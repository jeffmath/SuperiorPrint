import { ObjectId } from "mongodb";
import { JobStub } from "@/data-transfer/JobDto";
import { toId } from "@/app/lib/data/mongo/mongoId";

export interface MongoJobStub {
  _id: ObjectId;
  number: string;
  clientId: ObjectId;
  propertyId: ObjectId;
}

export function toJobStubs(stubs: MongoJobStub[]): JobStub[] {
  return stubs.map((stub) => {
    const result: JobStub & Partial<Pick<MongoJobStub, "_id">> = {
      ...stub,
      id: toId(stub._id)!,
      clientId: toId(stub.clientId)!,
      propertyId: toId(stub.propertyId)!,
    };
    delete result._id;
    return result;
  });
}

export const jobStubProjector = { number: 1, clientId: 1, propertyId: 1 };
