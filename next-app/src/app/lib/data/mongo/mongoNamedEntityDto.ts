import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { ObjectId, Document } from "mongodb";

export type MongoNamedEntityDto = Document &
  Omit<NamedEntityDto, "id"> & { _id: ObjectId };

export function toNamedEntityDto(entity: MongoNamedEntityDto): NamedEntityDto {
  const result = { ...entity, _id: undefined, id: entity._id.toHexString() };
  delete result._id;
  return result;
}
