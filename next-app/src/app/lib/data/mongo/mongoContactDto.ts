import { ContactDto } from "@/data-transfer/ContactDto";
import { ObjectId } from "mongodb";
import { toId } from "@/app/lib/data/mongo/mongoId";

export type MongoContactDto = Omit<ContactDto, "id" | "clientId" | "propertyId"> & {
  _id: ObjectId;
  clientId?: ObjectId;
  propertyId?: ObjectId;
};

export function toContactDto(contact: MongoContactDto): ContactDto {
  const result = {
    ...contact,
    _id: undefined,
    id: toId(contact._id)!,
    clientId: toId(contact.clientId),
    propertyId: toId(contact.propertyId),
  };
  delete result._id;
  return result;
}
