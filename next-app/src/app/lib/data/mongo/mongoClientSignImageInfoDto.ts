import {
  ClientSignImageDto,
  ClientSignImageInfoDto,
} from "@/data-transfer/ClientSignImageDto";
import {
  MongoClientSignImageDto,
  MongoClientSignImageInfoDto,
} from "@/app/lib/data/mongo/mongoClientDto";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { ObjectId } from "mongodb";

export function toClientSignImageInfoDto(
  image: MongoClientSignImageInfoDto,
): ClientSignImageInfoDto {
  const result = {
    ...image,
    _id: undefined,
    id: toId(image._id)!,
  };
  delete result._id;
  return result;
}

export function toClientSignImageDto(image: MongoClientSignImageDto): ClientSignImageDto {
  const result = {
    ...image,
    _id: undefined,
    id: toId(image._id)!,
    clientId: toId(image.clientId)!,
  };
  delete result._id;
  return result;
}

export function toMongoClientSignImageDto(
  image: ClientSignImageDto,
): MongoClientSignImageDto {
  const result = {
    ...image,
    id: undefined,
    _id: new ObjectId(),
    clientId: toMongoId(image.clientId)!,
  };
  delete result.id;
  return result;
}
