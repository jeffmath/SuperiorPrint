import { PropertyStub } from "@/data-transfer/PropertyStub";
import { ObjectId } from "mongodb";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { LocationDto } from "@/data-transfer/LocationDto";
import { toId } from "@/app/lib/data/mongo/mongoId";

export type MongoPropertyDto = Omit<PropertyDto, "id" | "locations"> & {
  _id: ObjectId;
  locations: MongoLocationDto[];
};

export type MongoLocationDto = Omit<LocationDto, "_id" | "imageId"> & {
  _id: ObjectId;
  imageId?: ObjectId;
};

export function toPropertyDto(property: MongoPropertyDto): PropertyDto {
  const result = {
    ...property,
    _id: undefined,
    id: toId(property._id)!,
    locations: property.locations.map(toLocationDto),
  };
  delete result._id;
  return result;
}

export function toLocationDto(location: MongoLocationDto): LocationDto {
  const result = {
    ...location,
    _id: undefined,
    id: toId(location._id)!,
    imageId: location.imageId ? toId(location.imageId) : undefined,
  };
  delete result._id;
  return result;
}

export type MongoPropertyStub = Omit<PropertyStub, "id"> & { _id: ObjectId };

export function toPropertyStub(stub: MongoPropertyStub): PropertyStub {
  return {
    id: toId(stub._id)!,
    name: stub.name,
  };
}
