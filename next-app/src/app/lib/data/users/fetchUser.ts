import { UserType } from "@/app/lib/entityType";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { toNamedEntityDto } from "@/app/lib/data/mongo/mongoNamedEntityDto";
import { UserDto } from "@/data-transfer/UserDto";

export async function fetchUser(name: string): Promise<UserDto | null> {
  try {
    const collection = await getCollectionForEntityType(UserType);
    const userDto = await collection.findOne(
      { name },
      { projection: { _id: 1, name: 1, salt: 1, hash: 1 } },
    );
    return userDto ? (toNamedEntityDto(userDto) as UserDto) : null;
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch user data.");
  }
}
