"use server";

import crypto from "crypto";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { UserType } from "@/app/lib/entityType";
import { auth } from "@/auth";
import { Collection } from "mongodb";
import { MongoNamedEntityDto } from "@/app/lib/data/mongo/mongoNamedEntityDto";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { MongoUserDto } from "@/app/lib/data/mongo/MongoUserDto";

export async function changePassword(password: string) {
  const session = await auth();
  const userId = session?.user?.id;
  if (!userId) throw new Error("User ID not provided by current session.");
  let userDto: MongoUserDto | null;
  let collection: Collection<MongoNamedEntityDto>;
  try {
    collection = await getCollectionForEntityType(UserType);
    userDto = await collection.findOne<MongoUserDto>(
      { id: userId },
      { projection: { salt: 1 } },
    );
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to find user.");
  }
  if (!userDto) throw new Error("User not found.");

  // change the user's password hash to the salted hash of the password given
  const _id = toMongoId(userId);
  const hash = crypto
    .pbkdf2Sync(password, userDto.salt, 1000, 64, "sha1")
    .toString("hex");
  try {
    await collection.updateOne({ _id }, { $set: { hash } });
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to update user password.");
  }
}
