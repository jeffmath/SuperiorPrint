"use server";

import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { ClientType } from "@/app/lib/entityType";
import { ClientStub } from "@/data-transfer/ClientStub";
import { MongoClientStub, toClientStub } from "@/app/lib/data/mongo/mongoClientDto";

export async function fetchAllClientStubs(): Promise<ClientStub[]> {
  const collection = await getCollectionForEntityType(ClientType);
  return await collection
    .find()
    .sort({ name: 1 })
    .project<MongoClientStub>({ _id: 1, name: 1 })
    .map(toClientStub)
    .toArray();
}
