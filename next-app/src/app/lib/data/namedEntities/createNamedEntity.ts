"use server";

import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { ContactType, getEntityTypeWithName, PropertyType } from "@/app/lib/entityType";
import { ObjectId } from "mongodb";
import { MongoNamedEntityDto, toNamedEntityDto } from "../mongo/mongoNamedEntityDto";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { revalidatePath, revalidateTag } from "next/cache";
import { redirect } from "next/navigation";
import { MongoPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";
import { MongoClientDto } from "@/app/lib/data/mongo/mongoClientDto";
import { MongoContactDto } from "@/app/lib/data/mongo/mongoContactDto";
import { propertyStubsForClientTag } from "@/app/lib/data/revalidationTags";

export async function createNamedEntity(
  entityTypeName: string,
  shouldRedirect = false,
): Promise<NamedEntityDto> {
  const entityType = getEntityTypeWithName(entityTypeName);
  let entity = {
    _id: new ObjectId(),
    name: "",
  } as MongoNamedEntityDto | MongoClientDto | MongoPropertyDto | MongoContactDto;
  if (entityType === PropertyType) {
    const propertyFields = {
      locations: [],
    };
    entity = { ...entity, ...propertyFields };
  } else if (entityType === ContactType) {
    const contactFields = {
      phoneNumber: "",
      clientId: undefined,
      propertyId: undefined,
      isInstaller: false,
      isForSuperiorPrint: false,
    };
    entity = { ...entity, ...contactFields };
  }
  const collection = await getCollectionForEntityType(entityType);
  const result = await collection.insertOne(entity);
  entity._id = result.insertedId;

  revalidatePath(`/entities/[entityType]`, "page");
  if (entityType === PropertyType) revalidateTag(propertyStubsForClientTag);

  if (shouldRedirect) {
    redirect(`/entity/${entityType.name}/${entity._id}`);
  }

  return toNamedEntityDto(entity)!;
}
