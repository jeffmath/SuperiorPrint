"use server";

import { Id } from "@/domain/Id";
import {
  getEntityTypeWithName,
  isPagedEntityType,
  PropertyType,
} from "@/app/lib/entityType";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { toMongoId } from "../mongo/mongoId";
import { fetchJobStubsReferencingEntity } from "@/app/lib/data/jobs/fetchJobStubsReferencingEntity";
import { createJobsList } from "@/app/lib/data/jobs/createJobsList";
import { revalidatePath, revalidateTag } from "next/cache";
import { propertyStubsForClientTag } from "@/app/lib/data/revalidationTags";

export interface DeleteNamedEntityResult {
  success: boolean;
  failureReason?: string;
}

export async function deleteNamedEntity(
  entityTypeName: string,
  id: Id,
): Promise<DeleteNamedEntityResult> {
  // if there are any jobs with line items which reference the given entity
  const entityType = getEntityTypeWithName(entityTypeName);
  const stubs = await fetchJobStubsReferencingEntity(id);
  if (stubs?.length > 0) {
    // disallow the deletion, providing a list of those jobs as the reason
    return {
      success: false,
      failureReason: `The ${entityTypeName} cannot be deleted, as it is referenced by${
        !isPagedEntityType(entityType) ? " at least one line item of" : ""
      } the following jobs: ${createJobsList(stubs)}`,
    };
  }

  try {
    const collection = await getCollectionForEntityType(entityType);
    await collection.deleteOne({ _id: toMongoId(id) });
    revalidatePath(`/entities/${entityType.pluralName}`);
    revalidatePath(`/smallSetEntities/${entityType.pluralName}`);
    if (entityType === PropertyType) revalidateTag(propertyStubsForClientTag);
    return { success: true };
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to delete a named-entity.");
  }
}
