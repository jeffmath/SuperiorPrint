"use server";

import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import {
  ContactType,
  getEntityTypeWithName,
  PropertyType,
  UserType,
} from "@/app/lib/entityType";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { toMongoId } from "../mongo/mongoId";
import { revalidatePath, revalidateTag } from "next/cache";
import { ContactDto } from "@/data-transfer/ContactDto";
import { propertyStubsForClientTag } from "@/app/lib/data/revalidationTags";

export async function updateNamedEntity(
  entity: NamedEntityDto,
  entityTypeName: string,
): Promise<void> {
  try {
    const entityType = getEntityTypeWithName(entityTypeName);
    const collection = await getCollectionForEntityType(entityType);

    const _id = toMongoId(entity.id);
    let update: any = { name: entity.name };
    switch (entityType) {
      case UserType:
        update = { ...entity, _id };
        break;
      case ContactType:
        const contact = entity as ContactDto;
        update = {
          ...contact,
          _id,
          clientId: toMongoId(contact.clientId),
          propertyId: toMongoId(contact.propertyId),
        };
        delete update.id;
        break;
    }
    await collection.updateOne({ _id }, { $set: update });
    revalidatePath(`/entities/${entityType.pluralName}`);
    revalidatePath(`/smallSetEntities/${entityType.pluralName}`);
    if (entityType === PropertyType) revalidateTag(propertyStubsForClientTag);
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error(`Failed to update a ${entityTypeName}'s data.`);
  }
}
