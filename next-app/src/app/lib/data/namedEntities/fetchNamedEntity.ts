import { EntityType } from "@/app/lib/entityType";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { toNamedEntityDto } from "@/app/lib/data/mongo/mongoNamedEntityDto";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";

export async function fetchNamedEntity(
  entityType: EntityType,
  entityId: Id,
): Promise<NamedEntityDto | null> {
  try {
    const collection = await getCollectionForEntityType(entityType);
    const entityDto = await collection.findOne(
      { _id: new ObjectId(entityId) },
      { projection: { _id: 1, name: 1 } },
    );
    return entityDto ? toNamedEntityDto(entityDto) : null;
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch entity data.");
  }
}
