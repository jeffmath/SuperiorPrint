"use server";

import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { EntityType } from "@/app/lib/entityType";
import { getCollectionForEntityType } from "../getCollection";
import { toNamedEntityDto } from "@/app/lib/data/mongo/mongoNamedEntityDto";

export async function fetchNamedEntities(
  entityType: EntityType,
): Promise<NamedEntityDto[]> {
  try {
    const collection = await getCollectionForEntityType(entityType);

    const entities = await collection.find().sort({ name: 1 }).toArray();

    return entities.map(toNamedEntityDto);
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch entities data.");
  }
}
