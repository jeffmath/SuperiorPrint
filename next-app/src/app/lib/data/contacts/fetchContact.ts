import { ContactType } from "@/app/lib/entityType";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { MongoContactDto, toContactDto } from "@/app/lib/data/mongo/mongoContactDto";
import { ContactDto } from "@/data-transfer/ContactDto";

export async function fetchContact(id: Id): Promise<ContactDto | null> {
  try {
    const collection = await getCollectionForEntityType(ContactType);
    const dto = await collection.findOne({ _id: new ObjectId(id) });
    return dto ? toContactDto(dto as MongoContactDto) : null;
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch contact data.");
  }
}
