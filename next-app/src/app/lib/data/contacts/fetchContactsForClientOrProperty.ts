"use server";

import { ContactDto } from "@/data-transfer/ContactDto";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { Id } from "@/domain/Id";
import { MongoContactDto, toContactDto } from "@/app/lib/data/mongo/mongoContactDto";
import { getCollection } from "@/app/lib/data/getCollection";

export async function fetchContactsForClientOrProperty(
  clientId: Id,
  propertyId: Id,
): Promise<ContactDto[]> {
  const query = {
    $or: [{ clientId: toMongoId(clientId) }, { propertyId: toMongoId(propertyId) }],
  };
  const collection = await getCollection<MongoContactDto>("Contacts");
  const contacts = await collection.find(query).sort({ name: 1 }).toArray();
  return contacts.map((contact) => toContactDto(contact));
}
