"use server";

import { ContactDto } from "@/data-transfer/ContactDto";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoContactDto, toContactDto } from "@/app/lib/data/mongo/mongoContactDto";

export async function fetchSuperiorContacts(): Promise<ContactDto[]> {
  const collection = await getCollection<MongoContactDto>("Contacts");
  const contacts = await collection.find({ isForSuperiorPrint: true }).toArray();
  return contacts.map((contact) => toContactDto(contact));
}
