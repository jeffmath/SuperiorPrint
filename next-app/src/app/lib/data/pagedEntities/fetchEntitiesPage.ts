import { ContactType, EntityType } from "@/app/lib/entityType";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { MongoContactDto, toContactDto } from "@/app/lib/data/mongo/mongoContactDto";
import {
  MongoNamedEntityDto,
  toNamedEntityDto,
} from "@/app/lib/data/mongo/mongoNamedEntityDto";

export interface FetchEntitiesPageResult {
  entityDtos: NamedEntityDto[];
  total: number;
}

export async function fetchEntitiesPage(
  entityType: EntityType,
  pageNumber: number,
  entitiesPerPage: number,
  firstLetter: string,
  nameSearchText: string,
  modifyFilter?: (filter: object, convertId: (id: Id) => any) => void,
): Promise<FetchEntitiesPageResult> {
  try {
    const collection = await getCollectionForEntityType(entityType);

    // determine whether a first letter was specified
    const filter: { name?: RegExp } = {};
    if (firstLetter === "0") firstLetter = "\\d";
    const hasFirstLetter = firstLetter && firstLetter !== "*";

    // determine whether a name search text was specified
    const hasNameText = Boolean(nameSearchText);

    // add the name portion of the query according to which of
    // first letter and/or name search text (or neither, or both) were specified
    let nameRegex = null;
    if (hasFirstLetter && !hasNameText) nameRegex = "^" + firstLetter;
    else if (!hasFirstLetter && hasNameText) nameRegex = nameSearchText;
    else if (hasFirstLetter && hasNameText)
      nameRegex =
        nameSearchText.charAt(0).toUpperCase() !== firstLetter.toUpperCase()
          ? `(^${firstLetter})(.*)${nameSearchText}`
          : `(^${nameSearchText})|((^${firstLetter})(.*)${nameSearchText})`;
    if (nameRegex) filter.name = new RegExp(nameRegex, "i");

    if (modifyFilter) modifyFilter(filter, (id: Id) => new ObjectId(id));

    // when we find the given page number of entities from the database which match the
    // search parameters determined above
    const projection = entityType !== ContactType ? { _id: 1, name: 1 } : {};
    const entities = await collection
      .find(filter)
      .project<MongoNamedEntityDto>(projection)
      .sort({ name: 1 })
      .skip(pageNumber * entitiesPerPage)
      .limit(entitiesPerPage)
      .toArray();
    const count = await collection.countDocuments(filter);
    // respond with both the page of entities, and the total count
    return {
      entityDtos: entities.map((entity) =>
        entityType === ContactType
          ? toContactDto(entity as MongoContactDto)
          : toNamedEntityDto(entity),
      ),
      total: count,
    } as FetchEntitiesPageResult;
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch entities data.");
  }
}
