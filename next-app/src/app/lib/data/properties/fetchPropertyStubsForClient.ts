"use server";

import { fetchJobStubsReferencingEntity } from "@/app/lib/data/jobs/fetchJobStubsReferencingEntity";
import { Id } from "@/domain/Id";
import { uniq } from "lodash";
import { PropertyStub } from "@/data-transfer/PropertyStub";
import { fetchPropertyStubsWithIds } from "@/app/lib/data/properties/fetchPropertyStubsWithIds";
import { unstable_cache } from "next/cache";
import { propertyStubsForClientTag } from "@/app/lib/data/revalidationTags";

export const fetchPropertyStubsForClient = unstable_cache(
  async (clientId: Id): Promise<PropertyStub[]> => {
    // find all the jobs for the given client ID
    const jobStubs = await fetchJobStubsReferencingEntity(clientId);

    // find the distinct set of properties which are associated with those jobs
    const propertyIds = jobStubs.map((stub) => stub.propertyId);
    const propertyIdSet = uniq(propertyIds);
    return await fetchPropertyStubsWithIds(propertyIdSet);
  },
  undefined,
  { tags: [propertyStubsForClientTag] },
);
