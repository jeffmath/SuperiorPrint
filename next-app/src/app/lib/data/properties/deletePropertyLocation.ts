"use server";

import { Id } from "@/domain/Id";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";
import { ImageDto } from "@/data-transfer/ImageDto";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";

export async function deletePropertyLocation(
  propertyId: Id,
  locationId: Id,
): Promise<boolean> {
  // when we've found the property of the given ID
  const propertyCollection = await getCollection<MongoPropertyDto>("Properties");
  const property = await propertyCollection.findOne({
    _id: toMongoId(propertyId),
  });
  if (!property) throw new Error("Could not find property with location to be deleted");

  // remove the location of the given ID from the property
  const locations = property.locations;
  const location = locations.find((l) => toId(l._id) === locationId);
  if (!location) throw new Error("Could not find location within property");
  locations.splice(locations.indexOf(location), 1);
  await propertyCollection.updateOne({ _id: property._id }, { $set: property });

  // if the location had an associated image, delete it, too
  if (location.imageId) {
    const locationImageCollection = await getCollection<ImageDto>("LocationImages");
    await locationImageCollection.deleteOne({ _id: location.imageId });
  }

  return true;
}
