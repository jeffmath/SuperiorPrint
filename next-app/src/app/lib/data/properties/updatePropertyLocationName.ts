"use server";

import { Id } from "@/domain/Id";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";

export async function updatePropertyLocationName(
  propertyId: Id,
  locationId: Id,
  name: string,
): Promise<boolean> {
  const collection = await getCollection<MongoPropertyDto>("Properties");
  const property = await collection.findOne({
    _id: toMongoId(propertyId),
  });

  // if the property wasn't found, throw an error
  if (!property) throw new Error("Property not found.");

  // if the location of the given ID is not found within the property, throw an error
  const location = property.locations.find((l) => toId(l._id) === locationId);
  if (!location) throw new Error("Location not found.");

  // update the location's name
  location.name = name;
  await collection.updateOne({ _id: property._id }, { $set: property });
  return true;
}
