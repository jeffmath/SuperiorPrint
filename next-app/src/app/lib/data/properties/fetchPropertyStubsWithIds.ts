import { Id } from "@/domain/Id";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { getCollection } from "@/app/lib/data/getCollection";
import {
  MongoPropertyDto,
  MongoPropertyStub,
  toPropertyStub,
} from "@/app/lib/data/mongo/mongoPropertyDto";
import { PropertyStub } from "@/data-transfer/PropertyStub";

export async function fetchPropertyStubsWithIds(ids: Id[]): Promise<PropertyStub[]> {
  const objectIds = ids.map((id) => toMongoId(id)!);
  const collection = await getCollection<MongoPropertyDto>("Properties");
  const stubs = await collection
    .find({ _id: { $in: objectIds } })
    .project<MongoPropertyStub>({ name: 1 })
    .sort({ name: 1 })
    .toArray();
  return stubs.map(toPropertyStub);
}
