"use server";

import { getCollectionForEntityType } from "@/app/lib/data/getCollection";
import { PropertyType } from "@/app/lib/entityType";
import { MongoPropertyStub, toPropertyStub } from "@/app/lib/data/mongo/mongoPropertyDto";
import { PropertyStub } from "@/data-transfer/PropertyStub";

export async function fetchAllPropertyStubs(): Promise<PropertyStub[]> {
  const collection = await getCollectionForEntityType(PropertyType);
  const stubs = (await collection
    .find()
    .sort({ name: 1 })
    .project({ _id: 1, name: 1 })
    .toArray()) as MongoPropertyStub[];
  return stubs.map(toPropertyStub);
}
