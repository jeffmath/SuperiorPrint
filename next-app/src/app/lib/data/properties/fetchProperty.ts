import { getCollection } from "@/app/lib/data/getCollection";
import { Id } from "@/domain/Id";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { MongoPropertyDto, toPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { toLower } from "lodash";

export async function fetchProperty(id: Id): Promise<PropertyDto | null> {
  try {
    const collection = await getCollection<MongoPropertyDto>("Properties");

    const property = await collection.findOne({ _id: toMongoId(id) });

    if (!property) return null;
    const dto = toPropertyDto(property);
    const sortedLocations = dto.locations.toSorted((a, b) =>
      toLower(a.name) <= toLower(b.name) ? -1 : 1,
    );
    return {
      ...dto,
      locations: sortedLocations,
    };
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to fetch property.");
  }
}
