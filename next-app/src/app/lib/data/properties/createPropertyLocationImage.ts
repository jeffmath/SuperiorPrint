"use server";

import { Id } from "@/domain/Id";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";
import { ImageDto } from "@/data-transfer/ImageDto";

export async function createPropertyLocationImage(
  propertyId: Id,
  locationId: Id,
  formData: FormData,
): Promise<Id> {
  // ensure that a file was given
  const files = formData.getAll("files") as File[];
  if (!files.length) throw new Error("No file was sent");
  const file = files[0];

  // when we have queried for the property of the given ID
  const propertyCollection = await getCollection<MongoPropertyDto>("Properties");
  const property = await propertyCollection.findOne({
    _id: toMongoId(propertyId),
  });

  // if the property wasn't found, throw an error
  if (!property) throw new Error("Property not found.");

  // if location of the given ID is not found within the property, throw an error
  const location = property.locations.find((l) => toId(l._id) === locationId);
  if (!location) throw new Error("Location not found.");

  // insert the image
  const locationImageCollection = await getCollection<ImageDto>("LocationImages");
  const imageData = await file.arrayBuffer();
  const image = {
    name: file.name,
    imageData: Buffer.from(imageData),
  };
  const document = await locationImageCollection.insertOne(image);

  // update the property in terms of the location having that image
  location.imageId = document.insertedId;
  await propertyCollection.updateOne({ _id: property._id }, { $set: property });
  return toId(document.insertedId)!;
}
