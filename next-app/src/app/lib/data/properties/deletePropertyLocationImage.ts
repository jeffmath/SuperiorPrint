"use server";

import { Id } from "@/domain/Id";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoPropertyDto } from "@/app/lib/data/mongo/mongoPropertyDto";
import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { ImageDto } from "@/data-transfer/ImageDto";

export async function deletePropertyLocationImage(
  propertyId: Id,
  locationId: Id,
): Promise<boolean> {
  const propertyCollection = await getCollection<MongoPropertyDto>("Properties");
  const property = await propertyCollection.findOne({
    _id: toMongoId(propertyId),
  });

  // if the property wasn't found, throw an error
  if (!property) throw new Error("Property not found.");

  // if location of the given ID is not found within the property, throw an error
  const location = property.locations.find((l) => toId(l._id) === locationId);
  if (!location) throw new Error("Location not found.");

  // if the location doesn't have an image to delete, throw an error
  const imageId = location.imageId;
  if (!imageId) throw new Error("Location has no image to delete.");

  // delete the image
  const locationImageCollection = await getCollection<ImageDto>("LocationImages");
  await locationImageCollection.deleteOne({ _id: imageId });

  // update the property in terms of the location having no image
  location.imageId = undefined;
  await propertyCollection.updateOne({ _id: property._id }, { $set: property });
  return true;
}
