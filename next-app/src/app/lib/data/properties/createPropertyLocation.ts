"use server";

import { Id } from "@/domain/Id";
import { ObjectId } from "mongodb";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import {
  MongoLocationDto,
  MongoPropertyDto,
  toLocationDto,
} from "@/app/lib/data/mongo/mongoPropertyDto";
import { getCollection } from "@/app/lib/data/getCollection";
import { LocationDto } from "@/data-transfer/LocationDto";

export async function createPropertyLocation(propertyId: Id): Promise<LocationDto> {
  const collection = await getCollection<MongoPropertyDto>("Properties");
  const property = await collection.findOne({
    _id: toMongoId(propertyId),
  });
  if (!property) throw new Error("Could not find property in which to create location");

  const locationDto = { _id: new ObjectId(), name: "" } as MongoLocationDto;
  property.locations.push(locationDto);
  await collection.updateOne({ _id: property._id }, { $set: property });
  return toLocationDto(locationDto);
}
