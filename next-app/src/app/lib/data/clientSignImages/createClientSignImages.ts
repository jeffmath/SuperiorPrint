"use server";

import { toId, toMongoId } from "@/app/lib/data/mongo/mongoId";
import { revalidatePath } from "next/cache";
import { Id } from "@/domain/Id";
import { unlink, writeFile } from "fs/promises";
import sizeOf from "image-size";
import { getCollection } from "@/app/lib/data/getCollection";

// note that using FormData lets us pass Blobs to this server action, whereas
// doing so directly is not allowed
export async function createClientSignImages(
  clientId: Id,
  formData: FormData,
): Promise<Id[]> {
  if (!clientId) throw new Error("No client ID provided");

  // ensure that files were given
  const files = formData.getAll("files") as File[];
  if (!files.length) throw new Error("No file(s) sent");

  async function storeImageFile(file: File): Promise<Id> {
    // determine whether the image is taller than wide, by storing it to a file and then
    // using the image-size package to get its dimensions from the file; delete the
    // file afterward
    let isTallerThanWide = false;
    const imageData = await file.arrayBuffer();
    const path = `./${file.name}`;
    await writeFile(path, new DataView(imageData));
    try {
      const size = sizeOf(path);
      const { width, height, orientation } = size;
      isTallerThanWide =
        (orientation || 0) < 5
          ? (height || 0) > (width || 0)
          : (width || 0) > (height || 0);
      await unlink(path);
    } catch (err) {
      console.log(err);
    }

    // store the image in the database
    const collection = await getCollection("ClientSignImages");
    const image = {
      name: file.name,
      clientId: toMongoId(clientId!)!,
      imageData: Buffer.from(imageData),
      isTallerThanWide,
    };
    try {
      const result = await collection.insertOne(image);
      return toId(result.insertedId)!;
    } catch (error) {
      throw new Error(`Could not store image file ${file.name}`, { cause: error });
    }
  }

  // wait for all the images to be stored
  const ids = await Promise.all(files.map(storeImageFile));

  revalidatePath(`/entity/client/${clientId}`);
  return ids;
}
