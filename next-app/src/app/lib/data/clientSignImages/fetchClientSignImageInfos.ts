import { Id } from "@/domain/Id";
import { ClientSignImageInfoDto } from "@/data-transfer/ClientSignImageDto";
import { ObjectId } from "mongodb";
import { MongoClientSignImageInfoDto } from "@/app/lib/data/mongo/mongoClientDto";
import { toClientSignImageInfoDto } from "@/app/lib/data/mongo/mongoClientSignImageInfoDto";
import { getCollection } from "@/app/lib/data/getCollection";

export async function fetchClientSignImageInfos(
  clientId: Id,
): Promise<ClientSignImageInfoDto[]> {
  const collection = await getCollection("ClientSignImages");
  const infos = await collection
    .find({ clientId: new ObjectId(clientId) })
    .project<MongoClientSignImageInfoDto>({ name: 1, isTallerThanWide: 1 })
    // to make the sort below case-insensitive
    .collation({ locale: "en" })
    .sort({ name: 1 })
    .toArray();
  return infos.map((info) => toClientSignImageInfoDto(info));
}
