"use server";

import { Id } from "@/domain/Id";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { revalidatePath } from "next/cache";
import { getCollection } from "@/app/lib/data/getCollection";

export async function deleteClientSignImage(id: Id): Promise<boolean> {
  try {
    const collection = await getCollection("ClientSignImages");
    await collection.deleteOne({ _id: toMongoId(id) });
    revalidatePath("entity/client");
    return true;
  } catch (error) {
    console.error("Database Error:", error);
    throw new Error("Failed to delete a client-sign-image.");
  }
}
