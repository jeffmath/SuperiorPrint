import { Collection, MongoClient, Document } from "mongodb";
import { EntityType } from "@/app/lib/entityType";
import { MongoNamedEntityDto } from "@/app/lib/data/mongo/mongoNamedEntityDto";
import Capitalize from "lodash/capitalize";

/**
 * Note that using "localhost" here instead of an IP address can sometimes lead to
 * "Server selection timed out after 30000 ms" errors when trying to connect
 * to the database.
 */
const host = process.env.MONGO_HOST || "127.0.0.1";
const databaseUrl = `mongodb://${host}:27017/SuperiorPrint`;
const databaseName = "SuperiorPrint";

/**
 * See https://www.mongodb.com/docs/v5.0/administration/connection-pool-overview/
 * for where my understanding of MongoDB connection pooling derives.
 */
let connectedClient: MongoClient | undefined;

const shouldLog = false;

export async function getCollection<T extends Document>(
  name: string,
): Promise<Collection<T>> {
  if (!connectedClient) {
    const client = new MongoClient(databaseUrl);
    connectedClient = await client.connect();
    if (shouldLog) console.log("*** Created MongoDB client");
  } else if (shouldLog) console.log("--- MongoDB client reused");

  const db = connectedClient.db(databaseName);
  return db.collection(name);
}

export async function getCollectionForEntityType(
  entityType: EntityType,
): Promise<Collection<MongoNamedEntityDto>> {
  return getCollection<MongoNamedEntityDto>(Capitalize(entityType.pluralName));
}
