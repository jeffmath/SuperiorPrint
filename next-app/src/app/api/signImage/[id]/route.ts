import { NextRequest, NextResponse } from "next/server";
import { ObjectId } from "mongodb";
import { toClientSignImageDto } from "@/app/lib/data/mongo/mongoClientSignImageInfoDto";
import { getCollection } from "@/app/lib/data/getCollection";
import { MongoClientSignImageDto } from "@/app/lib/data/mongo/mongoClientDto";

export async function GET(_req: NextRequest, { params }: { params: { id: string } }) {
  const { id } = params;
  if (!id) return NextResponse.json({ error: "No image ID provided" }, { status: 400 });

  try {
    const collection = await getCollection<MongoClientSignImageDto>("ClientSignImages");
    const mongoImage = await collection.findOne({ _id: new ObjectId(id) });
    if (!mongoImage)
      return NextResponse.json({ error: "Image not found" }, { status: 400 });
    const image = toClientSignImageDto(mongoImage);
    const imageData = Buffer.from(image.imageData.toString("base64"), "base64");
    return new NextResponse(imageData, {
      headers: {
        status: "200",
        "Content-Type": "image/jpg",
      },
    });
  } catch (error) {
    return NextResponse.json(
      { message: "Could not retrieve image", error },
      { status: 500 },
    );
  }
}
