import { NextRequest, NextResponse } from "next/server";
import { getCollection } from "@/app/lib/data/getCollection";
import { toMongoId } from "@/app/lib/data/mongo/mongoId";
import { MongoImageDto } from "@/app/lib/data/mongo/mongoImageDto";

export async function GET(_req: NextRequest, { params }: { params: { id: string } }) {
  const { id } = params;
  if (!id)
    return NextResponse.json({ error: "No location-image ID provided" }, { status: 400 });

  try {
    const collection = await getCollection<MongoImageDto>("LocationImages");
    const image = await collection.findOne({ _id: toMongoId(id) });
    if (!image) return NextResponse.json({ error: "Image not found" }, { status: 400 });
    const imageData = Buffer.from(image.imageData.toString("base64"), "base64");
    return new NextResponse(imageData, {
      headers: {
        status: "200",
        "Content-Type": "image/jpg",
      },
    });
  } catch (error) {
    return NextResponse.json(
      { message: "Could not retrieve image", error },
      { status: 500 },
    );
  }
}
