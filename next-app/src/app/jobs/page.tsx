import { fetchJob } from "@/app/lib/data/jobs/fetchJob";
import { JobDetails } from "@/app/ui/job/jobDetails";
import { fetchNamedEntity } from "@/app/lib/data/namedEntities/fetchNamedEntity";
import { ClientType } from "@/app/lib/entityType";
import { fetchContactsForClientOrProperty } from "@/app/lib/data/contacts/fetchContactsForClientOrProperty";
import { JobSelectorRsc } from "@/app/jobs/jobSelectorRsc";
import { JobClientSelector } from "@/app/ui/job/jobClientSelector";
import { JobPropertySelectorRsc } from "@/app/jobs/jobPropertySelectorRsc";
import { getSelectJobSelections } from "@/app/lib/cache/selectJobSelections";
import { JobCreator } from "@/app/ui/job/jobCreator";
import { LineItems } from "@/app/ui/job/lineItems";
import { fetchProperty } from "@/app/lib/data/properties/fetchProperty";
import { getLineItemSelection } from "@/app/lib/cache/lineItemSelection";
import { LineItemDetails } from "@/app/ui/job/lineItemDetails";
import { fetchClientSignImageInfos } from "@/app/lib/data/clientSignImages/fetchClientSignImageInfos";

export default async function JobsPage() {
  const {
    clientId: jobClientId,
    propertyId: jobPropertyId,
    jobId,
  } = await getSelectJobSelections("1");
  const { selectedLineItemId } = await getLineItemSelection("1");

  const jobDto = jobId ? await fetchJob(jobId) : undefined;
  const jobClientDto =
    jobClientId && jobDto ? await fetchNamedEntity(ClientType, jobClientId) : undefined;
  const jobClientSignImageInfoDtos =
    jobClientId && jobDto ? await fetchClientSignImageInfos(jobClientId) : undefined;
  const jobPropertyDto =
    jobPropertyId && jobDto ? await fetchProperty(jobPropertyId) : undefined;
  const jobContactDtos =
    jobClientId && jobPropertyId && jobDto
      ? await fetchContactsForClientOrProperty(jobClientId, jobPropertyId)
      : undefined;
  const clientContactDtos =
    jobClientId && jobContactDtos
      ? jobContactDtos.filter((c) => c.clientId === jobClientId)
      : [];
  const propertyContactDtos =
    jobPropertyId && jobContactDtos
      ? jobContactDtos.filter((c) => c.propertyId === jobPropertyId)
      : [];
  const lineItemDto = selectedLineItemId
    ? jobDto?.lineItems?.find((lineItem) => lineItem.id === selectedLineItemId)
    : undefined;
  return (
    <div>
      <div className="flex flex-row flex-wrap space-x-16 items-center">
        <div>
          <h4 className="form-header">Select a job</h4>
          <div className="mt-2 flex flex-row space-x-4 items-center">
            <JobClientSelector jobClientId={jobClientId} />
            {jobClientId && (
              <>
                <JobPropertySelectorRsc
                  jobClientId={jobClientId}
                  jobPropertyId={jobPropertyId}
                />
                {jobPropertyId && (
                  <JobSelectorRsc
                    jobClientId={jobClientId}
                    jobPropertyId={jobPropertyId}
                    jobId={jobId}
                  />
                )}
              </>
            )}
          </div>
        </div>
        {jobClientId && <JobCreator jobClientId={jobClientId} />}
      </div>
      {jobDto && jobClientDto && jobPropertyDto && (
        <div>
          <JobDetails
            jobDto={jobDto}
            jobClientDto={jobClientDto}
            jobPropertyDto={jobPropertyDto}
            clientContactDtos={clientContactDtos}
            propertyContactDtos={propertyContactDtos}
          />
          <LineItems
            jobDto={jobDto}
            selectedLineItemId={selectedLineItemId}
            jobPropertyDto={jobPropertyDto}
          />
          {jobClientId && lineItemDto && jobClientSignImageInfoDtos && (
            <LineItemDetails
              jobDto={jobDto}
              lineItemDto={lineItemDto}
              jobClientDto={jobClientDto}
              jobPropertyDto={jobPropertyDto}
              clientContactDtos={clientContactDtos}
              propertyContactDtos={propertyContactDtos}
              jobClientSignImageInfoDtos={jobClientSignImageInfoDtos}
            />
          )}
        </div>
      )}
    </div>
  );
}
