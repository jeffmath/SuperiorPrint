import { Id } from "@/domain/Id";
import { JobSelector } from "@/app/ui/job/jobSelector";
import { fetchJobStubsForClientAndProperty } from "@/app/lib/data/jobs/fetchJobStubsForClientAndProperty";
import * as React from "react";

export interface JobSelectorRscProps {
  jobClientId: Id;
  jobPropertyId: Id;
  jobId?: Id;
}

export async function JobSelectorRsc({
  jobClientId,
  jobPropertyId,
  jobId,
}: JobSelectorRscProps) {
  const jobStubs = await fetchJobStubsForClientAndProperty(jobClientId, jobPropertyId);

  return <JobSelector jobStubs={jobStubs} jobId={jobId} />;
}
