import * as React from "react";
import { Id } from "@/domain/Id";
import { fetchPropertyStubsForClient } from "@/app/lib/data/properties/fetchPropertyStubsForClient";
import { JobPropertySelector } from "@/app/ui/job/jobPropertySelector";

export interface JobPropertySelectorRscProps {
  jobClientId: Id;
  jobPropertyId?: Id;
}

export async function JobPropertySelectorRsc({
  jobClientId,
  jobPropertyId,
}: JobPropertySelectorRscProps) {
  const propertyStubs = await fetchPropertyStubsForClient(jobClientId);
  return (
    <JobPropertySelector propertyStubs={propertyStubs} jobPropertyId={jobPropertyId} />
  );
}
