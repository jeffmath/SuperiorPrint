import { ChangePasswordForm } from "@/app/ui/login/changePasswordForm";

export default async function ChangePasswordPage() {
  return <ChangePasswordForm />;
}
