import type { Metadata } from "next";
import "../tailwind/output.css";
import React from "react";
import "../../node_modules/sweetalert2/dist/sweetalert2.min.css";
import { Navigation } from "@/app/ui/navigation";
import { fetchAllClientStubs } from "@/app/lib/data/clients/fetchAllClientStubs";
import { Supplier } from "@/app/ui/supplier";
import { fetchAllPropertyStubs } from "@/app/lib/data/properties/fetchAllPropertyStubs";
import { fetchInstallContacts } from "@/app/lib/data/contacts/fetchInstallContacts";
import { fetchSuperiorContacts } from "@/app/lib/data/contacts/fetchSuperiorContacts";
import { fetchNamedEntities } from "@/app/lib/data/namedEntities/fetchNamedEntities";
import {
  FinishingType,
  MaterialType,
  PrinterType,
  SurfaceType,
} from "@/app/lib/entityType";
import { UnsavedChangesProvider } from "@/app/ui/unsavedChanges";
import { auth } from "@/auth";

export const metadata: Metadata = {
  title: "Superior Print and Exhibit",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const session = await auth();

  let content = <div className="2xl:container p-4">{children}</div>;
  if (session?.user) {
    const allClientStubs = await fetchAllClientStubs();
    const allPropertyStubs = await fetchAllPropertyStubs();
    const installContactDtos = await fetchInstallContacts();
    const superiorContactDtos = await fetchSuperiorContacts();
    const allSurfaceDtos = await fetchNamedEntities(SurfaceType);
    const allMaterialDtos = await fetchNamedEntities(MaterialType);
    const allFinishingDtos = await fetchNamedEntities(FinishingType);
    const allPrinterDtos = await fetchNamedEntities(PrinterType);
    content = (
      <>
        <Supplier
          allClientStubs={allClientStubs}
          allPropertyStubs={allPropertyStubs}
          installContactDtos={installContactDtos}
          superiorContactDtos={superiorContactDtos}
          allSurfaceDtos={allSurfaceDtos}
          allMaterialDtos={allMaterialDtos}
          allFinishingDtos={allFinishingDtos}
          allPrinterDtos={allPrinterDtos}
        />
        <UnsavedChangesProvider>
          <div className="bg-[#f8f8f8] p-1">
            <Navigation />
          </div>
          <div className="2xl:container p-4">{children}</div>
        </UnsavedChangesProvider>
      </>
    );
  }

  return (
    <html lang="en" className="text-base">
      <body className="bg-[#ffc594]" suppressHydrationWarning={true}>
        {content}
      </body>
    </html>
  );
}
