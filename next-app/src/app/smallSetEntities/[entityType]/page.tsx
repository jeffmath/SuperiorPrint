import * as React from "react";
import { getEntityTypeWithName } from "@/app/lib/entityType";
import { fetchNamedEntities } from "@/app/lib/data/namedEntities/fetchNamedEntities";
import { SmallSetEntitiesTable } from "@/app/ui/smallSetEntities/smallSetEntitiesTable";

export default async function SmallSetEntitiesPage({
  params,
}: {
  params: { entityType: string };
}) {
  const entityType = getEntityTypeWithName(params.entityType);
  const entityDtos = await fetchNamedEntities(entityType);
  return (
    <main>
      <SmallSetEntitiesTable entityTypeName={entityType.name} entityDtos={entityDtos} />
    </main>
  );
}
