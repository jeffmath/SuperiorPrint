import Image from "next/image";
import Logo from "@/images/logo.jpg";
import * as React from "react";
import { LoginForm } from "@/app/ui/login/loginForm";

export default async function LoginPage() {
  return (
    <div className="2xl:container text-left mt-4">
      <Image
        src={Logo}
        className="border border-black border-solid"
        alt="Superior Print and Exhibit"
      />
      <h4 className="form-header mt-4">Log in</h4>
      <LoginForm />
    </div>
  );
}
