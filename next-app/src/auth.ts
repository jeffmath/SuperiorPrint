import NextAuth from "next-auth";
import { authConfig } from "./auth.config";
import Credentials from "next-auth/providers/credentials";
import { z } from "zod";
import { fetchUser } from "@/app/lib/data/users/fetchUser";
import crypto from "crypto";

export const { auth, signIn, signOut } = NextAuth({
  ...authConfig,
  providers: [
    Credentials({
      async authorize(credentials) {
        const parsedCredentials = z
          .object({ userName: z.string().min(1), password: z.string().min(5) })
          .safeParse(credentials);

        if (parsedCredentials.success) {
          const { userName, password } = parsedCredentials.data;
          const user = await fetchUser(userName);
          if (!user) return null;
          const hash = crypto
            .pbkdf2Sync(password, user.salt, 1000, 64, "sha1")
            .toString("hex");
          if (hash === user.hash) return user;
        }

        console.log("Invalid credentials");
        return null;
      },
    }),
  ],
});
