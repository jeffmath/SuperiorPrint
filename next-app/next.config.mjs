/** @type {import('next').NextConfig} */
const nextConfig = {
  /* Prevents long-winded "Critical dependency" console warnings concerning the
  MongoDB driver */
  webpack: (config) => {
    config.module.unknownContextCritical = false
    return config
  },
  output: "standalone",
};

export default nextConfig;
