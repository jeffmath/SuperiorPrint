import { MongoClient } from "mongodb";
import { JobDto } from "@/data-transfer/JobDto";

async function convertLineItemDateStringsToDates() {
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const collection = db.collection<JobDto>("Jobs");
  const jobs = await collection.find().toArray();
  await Promise.all(
    jobs.map(async (job) => {
      let isModified = false;
      const dateFields = [
        "artworkReceivedDate",
        "artworkApprovedDate",
        "shipDate",
        "inHandDate",
        "installDateTime",
        "strikeDateTime",
      ];
      job.lineItems.forEach((item) => {
        dateFields.forEach((field) => {
          if (typeof item[field] === "string") {
            item[field] = new Date(item[field]);
            isModified = true;
          }
        });
      });
      if (isModified) {
        const result = await collection.updateOne({ _id: job._id }, { $set: job });
        console.log(`UPDATED ${job._id.toHexString()}: ${result.modifiedCount}`);
      }
    }),
  );
}

convertLineItemDateStringsToDates().then(() => process.exit(0));
