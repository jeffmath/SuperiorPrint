import { MongoClient } from "mongodb";

const oldDatabaseUrl = `mongodb://localhost:27017`;
const newDatabaseUrl = `mongodb://localhost:27018`;
const databaseName = "SuperiorPrint";

async function copyAllCollectionsToOtherMongoInstance() {
  const oldClient = new MongoClient(oldDatabaseUrl);
  const oldDb = oldClient.db(databaseName);
  const newClient = new MongoClient(newDatabaseUrl);
  const newDb = newClient.db(databaseName);
  const collections = await oldDb.collections();
  await Promise.all(
    collections.map(async (collection) => {
      const all = await collection.find().toArray();
      const { collectionName: name } = collection;
      const newCollection = await newDb.createCollection(name);
      const result = await newCollection.insertMany(all);
      console.log(`${name}: ${JSON.stringify(result)}`);
    }),
  );
}

void copyAllCollectionsToOtherMongoInstance().then(() => process.exit(0));
