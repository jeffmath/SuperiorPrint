import { MongoClient, ObjectId } from "mongodb";

interface ClientSignImageDocument {
  _id: ObjectId;
  clientId: ObjectId | string;
}

async function convertClientSignImageClientIdsToObjectIds() {
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const collection = db.collection<ClientSignImageDocument>("ClientSignImages");
  const images = await collection.find().toArray();
  await Promise.all(
    images.map(async (image) => {
      const { _id, clientId } = image;
      if (typeof clientId === "string") {
        console.log(`updating ${clientId}`);
        const result = await collection.updateOne(
          { _id },
          { $set: { clientId: new ObjectId(clientId) } },
        );
        console.log(`UPDATED ${clientId}: ${result.modifiedCount}`);
      } else console.log(`skipping ${clientId}`);
    }),
  );
}

convertClientSignImageClientIdsToObjectIds().then(() => process.exit(0));
