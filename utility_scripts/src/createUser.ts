import * as crypto from "crypto";
import { MongoClient, ObjectId } from "mongodb";

async function createUser(userName: string, password: string) {
  // create a new user in the database with a salted hash of the given password
  const salt = crypto.randomBytes(16).toString("hex");
  const user: UserDto = {
    _id: new ObjectId(),
    name: userName,
    salt: salt,
    hash: crypto.pbkdf2Sync(password, salt, 1000, 64, "sha1").toString("hex"),
  };
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const col = db.collection<UserDto>("Users");
  await col.insertOne(user);
}

createUser("test", "password").then(() => process.exit(0));

interface UserDto {
  _id: ObjectId;
  name: string;
  salt: string;
  hash: string;
}
