import { MongoClient } from "mongodb";
import { convertStringsToIds } from "./util/convertStringsToIds";
import { JobDto } from "@/data-transfer/JobDto";

async function convertJobContactIdsToObjectIds() {
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const collection = db.collection<JobDto>("Jobs");
  const jobs = await collection.find().toArray();
  await Promise.all(
    jobs.map(async (job) => {
      const { _id } = job;
      if (
        typeof job.clientContactId === "string" ||
        typeof job.propertyContactId === "string" ||
        typeof job.superiorPrintContact1Id === "string" ||
        typeof job.superiorPrintContact2Id === "string" ||
        typeof job.installContactId === "string"
      ) {
        console.log(`updating ${_id}`);
        convertStringsToIds(job);
        const result = await collection.updateOne({ _id }, { $set: job });
        console.log(`UPDATED ${_id}: ${result.modifiedCount}`);
      } else console.log(`skipping ${_id}`);
    }),
  );
}

convertJobContactIdsToObjectIds().then(() => process.exit(0));
