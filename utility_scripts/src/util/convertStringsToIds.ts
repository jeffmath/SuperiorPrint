import { ObjectId } from "mongodb";

/**
 * Converts all string-based ID fields named "_id" or ending in "Id" in the given
 * document to Object IDs.
 * Recursively processes arrays into the document.
 * We use this because we should never write Object IDs as strings into the database.
 * Yet, the IDs we get from the client will always be in string form.  So, this provides
 * an easy way to convert all those found within a document supplied by the client.
 */
export function convertStringsToIds(doc: object | string): object {
  if (typeof doc === "string") return new ObjectId(doc);

  const names = Object.getOwnPropertyNames(doc);
  names.forEach((name) => {
    const value = doc[name];
    if (value && Array.isArray(value)) {
      doc[name] = value.map((v) => (v ? convertStringsToIds(v) : null));
      return;
    }

    if (name !== "_id" && !name.endsWith("Id")) return;

    if (!value || typeof value != "string") return;

    doc[name] = new ObjectId(value);
  });
  return doc;
}
