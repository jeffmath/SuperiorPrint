import { MongoClient } from "mongodb";
import { ContactDto } from "@/data-transfer/ContactDto";
import { convertStringsToIds } from "./util/convertStringsToIds";

async function convertContactIdsToObjectIds() {
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const collection = db.collection<ContactDto>("Contacts");
  const contacts = await collection.find().toArray();
  await Promise.all(
    contacts.map(async (contact) => {
      const { _id, name, clientId, propertyId } = contact;
      if (
        typeof _id === "string" ||
        typeof clientId === "string" ||
        typeof propertyId === "string"
      ) {
        console.log(`updating ${name}`);
        convertStringsToIds(contact);
        const result = await collection.updateOne({ _id: _id }, { $set: contact });
        console.log(`UPDATED ${_id}: ${result.modifiedCount}`);
      } else console.log(`skipping ${name}`);
    }),
  );
}

convertContactIdsToObjectIds().then(() => process.exit(0));
