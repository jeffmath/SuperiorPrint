import { MongoClient, ObjectId } from "mongodb";

async function convertLineItemStringIdsToObjectIds() {
  const dbUrl = "mongodb://localhost/SuperiorPrint";
  const dbName = "SuperiorPrint";
  const client = new MongoClient(dbUrl);
  const db = client.db(dbName);
  const collection = db.collection<JobDto>("Jobs");
  const jobs = await collection.find().toArray();
  await Promise.all(
    jobs.map(async (job) => {
      let isModified = false;
      const lineItemFields = [
        "_id",
        "locationId",
        "surfaceId",
        "materialId",
        "printerId",
        "finishingId",
      ];
      job.lineItems.forEach((lineItem) => {
        lineItemFields.forEach((field) => {
          const fieldValue = lineItem[field];
          if (typeof fieldValue === "string") {
            lineItem[field] = new ObjectId(fieldValue);
            isModified = true;
          }
        });
        lineItem.clientSignImageIds =
          lineItem.clientSignImageIds?.map((imageId) => {
            if (typeof imageId === "string") {
              isModified = true;
              return new ObjectId(imageId);
            }
            return imageId;
          }) || [];
      });
      if (isModified) {
        const result = await collection.updateOne({ _id: job._id }, { $set: job });
        console.log(`UPDATED ${job._id.toHexString()}: ${result.modifiedCount}`);
      }
    }),
  );
}

convertLineItemStringIdsToObjectIds().then(() => process.exit(0));

interface JobDto {
  lineItems: LineItemDto[];
}

type Id = ObjectId | string;

interface LineItemDto {
  _id: Id;
  locationId?: Id;
  surfaceId?: Id;
  materialId?: Id;
  finishingId?: Id;
  printerId?: Id;
  clientSignImageIds: Id[];
}
