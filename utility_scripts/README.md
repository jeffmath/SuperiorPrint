##### To run any of these scripts

First, compile the script to JavaScript by running 'tsc'
from this file's folder (which employs the folder's tsconfig.json file). 
You can then create an IDEA Node configuration to run 
the resulting .js file in the ./dist/utility_scripts folder.
Debugging should work since a source-map file is generated.