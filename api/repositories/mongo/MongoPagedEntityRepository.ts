import {
  GetEntitiesPageResult,
  PagedEntityRepository,
  PagedEntitiesTypeName,
} from "../PagedEntityRepository";
import { Collection, Db, ObjectId } from "mongodb";
import { MongoClientDto } from "./MongoClientRepository";
import { MongoContactDto } from "./MongoContactRepository";
import { MongoPropertyDto } from "./MongoPropertyRepository";
import { Id } from "@/domain/Id";
import { MongoNamedEntityDto, toNamedEntities } from "./MongoNamedEntityDto";

export class MongoPagedEntityRepository implements PagedEntityRepository {
  private readonly clientCollection: Collection<MongoClientDto>;
  private readonly contactCollection: Collection<MongoContactDto>;
  private readonly propertyCollection: Collection<MongoPropertyDto>;

  constructor(db: Db) {
    this.clientCollection = db.collection("Clients");
    this.contactCollection = db.collection("Contacts");
    this.propertyCollection = db.collection("Properties");
  }

  async getEntitiesPage(
    entitiesTypeName: PagedEntitiesTypeName,
    pageNumber: number,
    entitiesPerPage: number,
    firstLetter: string,
    nameSearchText: string,
    modifyFilter: (filter: object, convertId: (id: Id) => any) => void,
  ): Promise<GetEntitiesPageResult> {
    // determine which database collection to pull from
    let collection:
      | Collection<MongoClientDto>
      | Collection<MongoContactDto>
      | Collection<MongoPropertyDto>;
    switch (entitiesTypeName) {
      case "clients":
        collection = this.clientCollection;
        break;
      case "contacts":
        collection = this.contactCollection;
        break;
      case "properties":
        collection = this.propertyCollection;
        break;
    }

    // determine whether a first letter was specified
    const filter: { name?: RegExp } = {};
    if (firstLetter === "0") firstLetter = "\\d";
    const hasFirstLetter = firstLetter && firstLetter !== "*";

    // determine whether a name search text was specified
    const hasNameText = Boolean(nameSearchText);

    // add the name portion of the query according to which of
    // first letter and/or name search text (or neither, or both) were specified
    let nameRegex = null;
    if (hasFirstLetter && !hasNameText) nameRegex = "^" + firstLetter;
    else if (!hasFirstLetter && hasNameText) nameRegex = nameSearchText;
    else if (hasFirstLetter && hasNameText)
      nameRegex =
        nameSearchText.charAt(0).toUpperCase() !== firstLetter.toUpperCase()
          ? "(^" + firstLetter + ")(.*)" + nameSearchText
          : "(^" +
            nameSearchText +
            ")|((^" +
            firstLetter +
            ")(.*)" +
            nameSearchText +
            ")";
    if (nameRegex) filter.name = new RegExp(nameRegex, "i");

    if (modifyFilter) modifyFilter(filter, (id: Id) => new ObjectId(id));

    // when we find the given page number of entities from the database which match the
    // search parameters determined above
    const projection = entitiesTypeName !== "contacts" ? { _id: 1, name: 1 } : {};
    const entities = await collection
      .find(filter)
      .project<MongoNamedEntityDto>(projection)
      .sort({ name: 1 })
      .skip(pageNumber * entitiesPerPage)
      .limit(entitiesPerPage)
      .toArray();
    const count = await collection.countDocuments(filter);
    // respond with both the page of entities, and the total count
    return { entities: toNamedEntities(entities), total: count } as GetEntitiesPageResult;
  }
}
