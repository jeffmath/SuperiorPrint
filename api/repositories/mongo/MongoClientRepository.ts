import { ClientRepository } from "../ClientRepository";
import { Collection, Db, ObjectId } from "mongodb";
import { ImageDto } from "@/data-transfer/ImageDto";
import { ClientDto } from "@/data-transfer/ClientDto";
import {
  ClientSignImageDto,
  ClientSignImageInfoDto,
} from "@/data-transfer/ClientSignImageDto";
import { Id } from "@/domain/Id";
import { toId, toMongoId } from "./idConversion";

export class MongoClientRepository implements ClientRepository {
  private clientCollection: Collection<MongoClientDto>;
  private clientSignImageCollection: Collection<MongoClientSignImageDto>;

  constructor(db: Db) {
    this.clientCollection = db.collection("Clients");
    this.clientSignImageCollection = db.collection("ClientSignImages");
  }

  async getAllClientStubs(): Promise<ClientDto[]> {
    const clients = await this.clientCollection.find().sort({ name: 1 }).toArray();
    return clients.map((client) => toClientDto(client));
  }

  async getClient(id: Id): Promise<ClientDto | null> {
    const client = await this.clientCollection.findOne({ _id: new ObjectId(id) });
    return client ? toClientDto(client) : null;
  }

  async addClientSignImage(image: ClientSignImageDto): Promise<void> {
    const mongoImage = toMongoClientSignImageDto(image);
    await this.clientSignImageCollection.insertOne(mongoImage);
  }

  async getClientSignImageInfos(clientId: Id): Promise<ClientSignImageInfoDto[]> {
    const infos = await this.clientSignImageCollection
      .find({ clientId: new ObjectId(clientId) })
      .project<MongoClientSignImageInfoDto>({ name: 1, isTallerThanWide: 1 })
      .sort({ name: 1 })
      .toArray();
    return infos.map((info) => toClientSignImageInfoDto(info));
  }

  async getClientSignImage(id: Id): Promise<ImageDto | null> {
    const image = await this.clientSignImageCollection.findOne({ _id: new ObjectId(id) });
    return image ? toClientSignImageDto(image) : null;
  }

  async createClient(): Promise<ClientDto> {
    const client = { name: "" } as MongoClientDto;
    client._id = (await this.clientCollection.insertOne(client)).insertedId;
    return toClientDto(client);
  }

  async updateClient(id: Id, name: string): Promise<void> {
    await this.clientCollection.updateOne({ _id: new ObjectId(id) }, { $set: { name } });
  }

  async deleteClient(id: Id): Promise<void> {
    await this.clientCollection.deleteOne({ _id: new ObjectId(id) });
  }

  async deleteClientSignImage(id: Id): Promise<void> {
    await this.clientSignImageCollection.deleteOne({ _id: new ObjectId(id) });
  }
}

export type MongoClientDto = Omit<ClientDto, "id"> & { _id: ObjectId };

export type MongoClientSignImageInfoDto = Omit<ClientSignImageInfoDto, "id"> & {
  _id: ObjectId;
};

export type MongoClientSignImageDto = Omit<ClientSignImageDto, "id" | "clientId"> & {
  _id: ObjectId;
  clientId: ObjectId;
};

function toMongoClientSignImageDto(image: ClientSignImageDto): MongoClientSignImageDto {
  const result = {
    ...image,
    id: undefined,
    _id: new ObjectId(),
    clientId: toMongoId(image.clientId)!,
  };
  delete result.id;
  return result;
}

function toClientDto(client: MongoClientDto): ClientDto {
  const result = {
    ...client,
    _id: undefined,
    id: toId(client._id)!,
  };
  delete result._id;
  return result;
}

function toClientSignImageInfoDto(
  image: MongoClientSignImageInfoDto,
): ClientSignImageInfoDto {
  const result = {
    ...image,
    _id: undefined,
    id: toId(image._id)!,
  };
  delete result._id;
  return result;
}

function toClientSignImageDto(image: MongoClientSignImageDto): ClientSignImageDto {
  const result = {
    ...image,
    _id: undefined,
    id: toId(image._id)!,
    clientId: toId(image.clientId)!,
  };
  delete result._id;
  return result;
}
