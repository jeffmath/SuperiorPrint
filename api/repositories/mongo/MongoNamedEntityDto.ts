import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import { ObjectId } from "mongodb";

export type MongoNamedEntityDto = Omit<NamedEntityDto, "id"> & { _id: ObjectId };

export function toNamedEntity(entity: MongoNamedEntityDto): NamedEntityDto {
  const result = { ...entity, _id: undefined, id: entity._id.toHexString() };
  delete result._id;
  return result;
}

export function toNamedEntities(entities: MongoNamedEntityDto[]): NamedEntityDto[] {
  return entities.map(toNamedEntity);
}
