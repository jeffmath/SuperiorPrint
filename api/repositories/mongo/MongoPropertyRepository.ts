import {
  AddLocationImageResult,
  DeleteLocationImageResult,
  PropertyRepository,
  UpdateLocationNameResult,
} from "../PropertyRepository";
import { PropertyStub } from "@/data-transfer/PropertyStub";
import { Collection, Db, ObjectId } from "mongodb";
import { ImageDto } from "@/data-transfer/ImageDto";
import { Id } from "@/domain/Id";
import { LocationDto } from "@/data-transfer/LocationDto";
import { PropertyDto } from "@/data-transfer/PropertyDto";
import { toId, toMongoId } from "./idConversion";

export class MongoPropertyRepository implements PropertyRepository {
  private propertyCollection: Collection<MongoPropertyDto>;
  private locationImageCollection: Collection<ImageDto>;

  constructor(db: Db) {
    this.propertyCollection = db.collection("Properties");
    this.locationImageCollection = db.collection("LocationImages");
  }

  async getAllPropertyStubs(): Promise<PropertyStub[]> {
    const stubs = await this.propertyCollection
      .find()
      .project<MongoPropertyStub>({ _id: 1, name: 1 })
      .sort({ name: 1 })
      .toArray();
    return toPropertyStubs(stubs);
  }

  async getProperty(id: Id): Promise<PropertyDto | null> {
    const property = await this.propertyCollection.findOne({ _id: toMongoId(id) });
    return property ? toProperty(property) : null;
  }

  async getPropertyStubsHavingIds(ids: string[]): Promise<PropertyStub[]> {
    const objectIds = ids.map((id) => toMongoId(id)!);
    const stubs = await this.propertyCollection
      .find({ _id: { $in: objectIds } })
      .project<MongoPropertyStub>({ name: 1 })
      .sort({ name: 1 })
      .toArray();
    return toPropertyStubs(stubs);
  }

  getLocationImage(id: Id): Promise<ImageDto | null> {
    return this.locationImageCollection.findOne({ _id: toMongoId(id) });
  }

  async addLocationImage(
    propertyId: Id,
    locationId: Id,
    image: ImageDto,
  ): Promise<AddLocationImageResult> {
    // when we have queried for the property of the given ID
    const result = {} as AddLocationImageResult;
    const property = await this.propertyCollection.findOne({
      _id: toMongoId(propertyId),
    });

    // if the property wasn't found, return that result
    if (!property) {
      result.propertyNotFound = true;
      return result;
    }

    // if location of the given ID is not found within the property,
    // return that result
    const location = property.locations.find((l) => toId(l._id) === locationId);
    if (!location) {
      result.locationNotFound = true;
      return result;
    }

    // insert the image
    const document = await this.locationImageCollection.insertOne(image);

    // update the property in terms of the location having that image
    const imageId = document.insertedId;
    location.imageId = imageId;
    await this.propertyCollection.updateOne({ _id: property._id }, { $set: property });
    result.imageId = imageId.toString();
    return result;
  }

  async deleteLocationImage(
    propertyId: Id,
    locationId: Id,
  ): Promise<DeleteLocationImageResult> {
    const result = {} as DeleteLocationImageResult;
    const property = await this.propertyCollection.findOne({
      _id: toMongoId(propertyId),
    });

    // if the property wasn't found, return that result
    if (!property) {
      result.propertyNotFound = true;
      return result;
    }

    // if location of the given ID is not found within the property,
    // return that result
    const location = property.locations.find((l) => toId(l._id) === locationId);
    if (!location) {
      result.locationNotFound = true;
      return result;
    }

    // if the location doesn't have an image to delete, return that result
    const imageId = location.imageId;
    if (!imageId) {
      result.locationHasNoImage = true;
      return result;
    }

    // delete the image
    await this.locationImageCollection.deleteOne({ _id: imageId });

    // update the property in terms of the location having no image
    location.imageId = undefined;
    await this.propertyCollection.updateOne({ _id: property._id }, { $set: property });
    return result;
  }

  async createProperty(): Promise<PropertyDto> {
    const property = { _id: new ObjectId(), name: "", locations: [] } as MongoPropertyDto;
    const result = await this.propertyCollection.insertOne(property);
    property._id = result.insertedId;
    return toProperty(property);
  }

  async updateProperty(stub: PropertyStub): Promise<void> {
    return void this.propertyCollection.updateOne(
      { _id: toMongoId(stub.id) },
      { $set: { name: stub.name } },
    );
  }

  async updateLocationName(
    propertyId: Id,
    locationId: Id,
    name: string,
  ): Promise<UpdateLocationNameResult> {
    const result = {} as UpdateLocationNameResult;
    const property = await this.propertyCollection.findOne({
      _id: toMongoId(propertyId),
    });

    // if the property wasn't found, return that result
    if (!property) {
      result.propertyNotFound = true;
      return result;
    }

    // if location of the given ID is not found within the property,
    // return that result
    const location = property.locations.find((l) => toId(l._id) === locationId);
    if (!location) {
      result.locationNotFound = true;
      return result;
    }

    // update the location's name
    location.name = name;
    await this.propertyCollection.updateOne({ _id: property._id }, { $set: property });
    return result;
  }

  async deleteProperty(id: Id): Promise<void> {
    return void this.propertyCollection.deleteOne({ _id: toMongoId(id) });
  }

  async createLocation(propertyId: Id): Promise<LocationDto> {
    const property = await this.propertyCollection.findOne({
      _id: toMongoId(propertyId),
    });
    if (!property) throw new Error("Could not find property in which to create location");
    const location = { _id: new ObjectId(), name: "" } as MongoLocationDto;
    property.locations.push(location);
    await this.propertyCollection.updateOne({ _id: property._id }, { $set: property });
    return toLocation(location);
  }

  async deleteLocation(propertyId: Id, locationId: Id): Promise<void> {
    // when we've found the property of the given ID
    const property = await this.propertyCollection.findOne({
      _id: toMongoId(propertyId),
    });
    if (!property) throw new Error("Could not find property with location to be deleted");

    // remove the location of the given ID from the property
    const locations = property.locations;
    const location = locations.find((l) => toId(l._id) === locationId);
    if (!location) throw new Error("Could not find location within property");
    locations.splice(locations.indexOf(location), 1);
    await this.propertyCollection.updateOne({ _id: property._id }, { $set: property });

    // if the location had an associated image, delete it, too
    if (location.imageId) {
      await this.locationImageCollection.deleteOne({ _id: location.imageId });
    }
  }
}

export type MongoPropertyStub = Omit<PropertyStub, "id"> & { _id: ObjectId };

export type MongoPropertyDto = Omit<PropertyDto, "id" | "locations"> & {
  _id: ObjectId;
  locations: MongoLocationDto[];
};

export type MongoLocationDto = Omit<LocationDto, "_id" | "imageId"> & {
  _id: ObjectId;
  imageId?: ObjectId;
};

function toPropertyStub(stub: MongoPropertyStub): PropertyStub {
  return {
    id: toId(stub._id)!,
    name: stub.name,
  };
}

function toPropertyStubs(stubs: MongoPropertyStub[]): PropertyStub[] {
  return stubs.map(toPropertyStub);
}

function toProperty(property: MongoPropertyDto): PropertyDto {
  const result = {
    ...property,
    _id: undefined,
    id: toId(property._id)!,
    locations: property.locations.map(toLocation),
  };
  delete result._id;
  return result;
}

function toLocation(location: MongoLocationDto): LocationDto {
  const result = {
    ...location,
    _id: undefined,
    id: toId(location._id)!,
    imageId: toId(location.imageId),
  };
  delete result._id;
  return result;
}
