import { ContactRepository } from "../ContactRepository";
import { Collection, Db, ObjectId } from "mongodb";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Id } from "@/domain/Id";
import { toId, toMongoId } from "./idConversion";

export class MongoContactRepository implements ContactRepository {
  private contactCollection: Collection<MongoContactDto>;

  constructor(private db: Db) {
    this.contactCollection = db.collection("Contacts");
  }

  async getContact(id: Id): Promise<ContactDto | null> {
    const contact = await this.contactCollection.findOne({ _id: toMongoId(id) });
    return contact ? toContact(contact) : null;
  }

  async getContactsFor(clientId: Id, propertyId: Id): Promise<ContactDto[]> {
    const query = {
      $or: [{ clientId: toMongoId(clientId) }, { propertyId: toMongoId(propertyId) }],
    };
    const contacts = await this.contactCollection.find(query).sort({ name: 1 }).toArray();
    return toContacts(contacts);
  }

  async getSuperiorContacts(): Promise<ContactDto[]> {
    const contacts = await this.contactCollection
      .find({ isForSuperiorPrint: true })
      .toArray();
    return toContacts(contacts);
  }

  async getInstallers(): Promise<ContactDto[]> {
    const contacts = await this.contactCollection.find({ isInstaller: true }).toArray();
    return toContacts(contacts);
  }

  async createContact(): Promise<ContactDto> {
    const contact: MongoContactDto = {
      _id: new ObjectId(),
      name: "",
      phoneNumber: "",
      clientId: undefined,
      propertyId: undefined,
      isInstaller: false,
      isForSuperiorPrint: false,
    };
    await this.contactCollection.insertOne(contact);
    return toContact(contact);
  }

  async updateContact(contact: ContactDto): Promise<void> {
    const mongoContact = {
      ...contact,
      _id: toMongoId(contact.id),
      clientId: toMongoId(contact.clientId),
      propertyId: toMongoId(contact.propertyId),
    };
    return void this.contactCollection.updateOne(
      { _id: mongoContact._id },
      { $set: mongoContact },
    );
  }

  async deleteContact(id: Id): Promise<void> {
    return void this.contactCollection.deleteOne({ _id: toMongoId(id) });
  }
}

export type MongoContactDto = Omit<ContactDto, "id" | "clientId" | "propertyId"> & {
  _id: ObjectId;
  clientId?: ObjectId;
  propertyId?: ObjectId;
};

function toContacts(contacts: MongoContactDto[]): ContactDto[] {
  return contacts.map(toContact);
}

function toContact(contact: MongoContactDto): ContactDto {
  return {
    ...contact,
    id: toId(contact._id)!,
    clientId: toId(contact.clientId),
    propertyId: toId(contact.propertyId),
  };
}
