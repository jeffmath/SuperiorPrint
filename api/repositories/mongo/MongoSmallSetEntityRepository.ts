import {
  SmallSetEntityRepository,
  SmallSetEntityType,
} from "../SmallSetEntityRepository";
import { Collection, Db, ObjectId } from "mongodb";
import { Id } from "@/domain/Id";
import { NamedEntityDto } from "@/data-transfer/NamedEntityDto";
import {
  MongoNamedEntityDto,
  toNamedEntities,
  toNamedEntity,
} from "./MongoNamedEntityDto";
import { toMongoId } from "./idConversion";

export class MongoSmallSetEntityRepository implements SmallSetEntityRepository {
  private readonly surfaceCollection: Collection<MongoNamedEntityDto>;
  private readonly materialCollection: Collection<MongoNamedEntityDto>;
  private readonly finishingCollection: Collection<MongoNamedEntityDto>;
  private readonly printerCollection: Collection<MongoNamedEntityDto>;
  private readonly userCollection: Collection<MongoNamedEntityDto>;

  constructor(db: Db) {
    this.surfaceCollection = db.collection("Surfaces");
    this.materialCollection = db.collection("Materials");
    this.finishingCollection = db.collection("Finishings");
    this.printerCollection = db.collection("Printers");
    this.userCollection = db.collection("Users");
  }

  async getAllEntities(type: SmallSetEntityType): Promise<NamedEntityDto[]> {
    const entities = await this.getTypeCollection(type)
      .find()
      .sort({ name: 1 })
      .toArray();
    return toNamedEntities(entities);
  }

  async getEntity(type: SmallSetEntityType, id: Id): Promise<NamedEntityDto | null> {
    const entity = await this.getTypeCollection(type).findOne({ _id: toMongoId(id) });
    return entity ? toNamedEntity(entity) : null;
  }

  async getEntityByName(
    type: SmallSetEntityType,
    name: string,
  ): Promise<NamedEntityDto | null> {
    const entity = await this.getTypeCollection(type).findOne({ name });
    return entity ? toNamedEntity(entity) : null;
  }

  async createEntity(
    type: SmallSetEntityType,
    data?: NamedEntityDto,
  ): Promise<NamedEntityDto> {
    const entity = data
      ? { ...data, _id: new ObjectId() }
      : ({ name: "" } as MongoNamedEntityDto);
    const result = await this.getTypeCollection(type).insertOne(entity);
    entity._id = result.insertedId;
    return toNamedEntity(entity)!;
  }

  async updateEntity(type: SmallSetEntityType, entity: NamedEntityDto): Promise<void> {
    const _id = toMongoId(entity.id);
    return void this.getTypeCollection(type).updateOne(
      { _id },
      {
        $set: type === "user" ? { ...entity, _id } : { name: entity.name },
      },
    );
  }

  async deleteEntity(type: SmallSetEntityType, id: Id): Promise<void> {
    return void this.getTypeCollection(type).deleteOne({ _id: toMongoId(id) });
  }

  private getTypeCollection(type: SmallSetEntityType): Collection<MongoNamedEntityDto> {
    let col;
    switch (type) {
      case "surface":
        col = this.surfaceCollection;
        break;
      case "material":
        col = this.materialCollection;
        break;
      case "finishing":
        col = this.finishingCollection;
        break;
      case "printer":
        col = this.printerCollection;
        break;
      case "user":
        col = this.userCollection;
        break;
      default:
        throw new Error("Unknown entity type: " + type);
    }
    return col;
  }
}
