import { JobRepository } from "../JobRepository";
import { Collection, Db, ObjectId } from "mongodb";
import { sortBy, remove } from "lodash";
import { LineItemDto } from "@/data-transfer/LineItemDto";
import { ContactDto } from "@/data-transfer/ContactDto";
import { Id } from "@/domain/Id";
import { JobDto, JobStub } from "@/data-transfer/JobDto";
import { toId, toMongoId } from "./idConversion";

export class MongoJobRepository implements JobRepository {
  private jobCollection: Collection<MongoJobDto>;

  constructor(db: Db) {
    this.jobCollection = db.collection("Jobs");
  }

  async getJobStubs(clientId: Id, propertyId: Id): Promise<JobStub[]> {
    const filter = {
      clientId: new ObjectId(clientId),
      propertyId: new ObjectId(propertyId),
    };
    const stubs = await this.jobCollection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJob(id: Id): Promise<JobDto | null> {
    const job = await this.jobCollection.findOne({ _id: new ObjectId(id) });
    if (!job) return null;
    // sort the job's line-items by their name
    job.lineItems = sortBy(job.lineItems, (l) => l.name);
    return toJobDto(job);
  }

  async getJobStubsForClient(id: Id): Promise<JobStub[]> {
    const stubs = await this.jobCollection
      .find({ clientId: new ObjectId(id) })
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJobStubsUsingClientSignImage(clientId: Id, imageId: Id): Promise<JobStub[]> {
    const filter = {
      clientId: new ObjectId(clientId),
      "lineItems.clientSignImageIds": new ObjectId(imageId),
    };
    const stubs = await this.jobCollection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJobStubsForProperty(id: Id): Promise<JobStub[]> {
    const stubs = await this.jobCollection
      .find({ propertyId: new ObjectId(id) })
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJobStubsReferencingLocation(
    propertyId: Id,
    locationId: Id,
  ): Promise<JobStub[]> {
    const filter = {
      propertyId: new ObjectId(propertyId),
      "lineItems.locationId": new ObjectId(locationId),
    };
    const stubs = await this.jobCollection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJobStubsReferencingContact(contactId_: string): Promise<JobStub[]> {
    const contactId = new ObjectId(contactId_);
    const filter = {
      $or: [
        { clientContactId: contactId },
        { propertyContactId: contactId },
        { installContactId: contactId },
        { superiorPrintContact1Id: contactId },
        { superiorPrintContact2Id: contactId },
      ],
    };
    const stubs = await this.jobCollection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async getJobStubsReferencingEntity(entityId: string): Promise<JobStub[]> {
    const entityObjectId = new ObjectId(entityId);
    const filter = {
      $or: [
        { "lineItems.surfaceId": entityObjectId },
        { "lineItems.materialId": entityObjectId },
        { "lineItems.finishingId": entityObjectId },
        { "lineItems.printerId": entityObjectId },
      ],
    };
    const stubs = await this.jobCollection
      .find(filter)
      .project<MongoJobStub>(jobStubProjector)
      .sort({ number: 1 })
      .toArray();
    return toJobStubs(stubs);
  }

  async createJob(
    clientId: Id,
    propertyId: Id,
    robert: ContactDto,
    joni: ContactDto,
  ): Promise<JobDto> {
    const job = {
      _id: new ObjectId(),
      clientId: new ObjectId(clientId),
      propertyId: new ObjectId(propertyId),
      number: "",
      superiorPrintContact1Id: new ObjectId(robert.id),
      superiorPrintContact2Id: new ObjectId(joni.id),
      lineItems: [],
    } as MongoJobDto;
    await this.jobCollection.insertOne(job);
    return toJobDto(job);
  }

  async updateJob(job: JobDto): Promise<void> {
    const mongoJob = toMongoJobDto(job);
    await this.jobCollection.updateOne({ _id: mongoJob._id }, { $set: mongoJob });
  }

  async deleteJob(id: Id): Promise<void> {
    await this.jobCollection.deleteOne({ _id: new ObjectId(id) });
  }

  async addLineItem(jobId: Id): Promise<LineItemDto> {
    const job = await this.jobCollection.findOne({ _id: new ObjectId(jobId) });
    if (!job) throw new Error("Job not found to which a line item should be added.");
    const lineItem = {
      _id: new ObjectId(),
      name: "",
      clientSignImageIds: [],
    } as MongoLineItemDto;
    job.lineItems.push(lineItem);
    await this.jobCollection.updateOne({ _id: job._id }, { $set: job });
    return toLineItemDto(lineItem);
  }

  async deleteLineItem(jobId: Id, lineItemId: Id): Promise<void> {
    const job = await this.jobCollection.findOne({ _id: new ObjectId(jobId) });
    if (!job) throw new Error("Job not found from which a line item should be deleted.");
    remove(job.lineItems, (item) => item._id.toHexString() === lineItemId);
    await this.jobCollection.updateOne({ _id: job._id }, { $set: job });
  }
}

export interface MongoJobStub {
  _id: ObjectId;
  number: string;
  clientId: ObjectId;
  propertyId: ObjectId;
}

export type MongoJobDto = Omit<
  JobDto,
  | "id"
  | "clientId"
  | "propertyId"
  | "clientContactId"
  | "propertyContactId"
  | "installContactId"
  | "superiorPrintContact1Id"
  | "superiorPrintContact2Id"
  | "lineItems"
> & {
  _id: ObjectId;
  clientId: ObjectId;
  propertyId: ObjectId;
  clientContactId?: ObjectId;
  propertyContactId?: ObjectId;
  installContactId?: ObjectId;
  superiorPrintContact1Id?: ObjectId;
  superiorPrintContact2Id?: ObjectId;
  lineItems: MongoLineItemDto[];
};

export type MongoLineItemDto = Omit<
  LineItemDto,
  | "id"
  | "locationId"
  | "surfaceId"
  | "materialId"
  | "finishingId"
  | "printerId"
  | "clientSignImageIds"
> & {
  _id: ObjectId;
  locationId?: ObjectId;
  surfaceId?: ObjectId;
  materialId?: ObjectId;
  finishingId?: ObjectId;
  printerId?: ObjectId;
  clientSignImageIds: ObjectId[];
};

function toJobStubs(stubs: MongoJobStub[]): JobStub[] {
  return stubs.map((stub) => {
    const result: JobStub & Partial<Pick<MongoJobStub, "_id">> = {
      ...stub,
      id: toId(stub._id)!,
      clientId: toId(stub.clientId)!,
      propertyId: toId(stub.propertyId)!,
    };
    delete result._id;
    return result;
  });
}

function toJobDto(job: MongoJobDto): JobDto {
  const result: JobDto & Partial<Pick<MongoJobDto, "_id">> = {
    ...job,
    id: toId(job._id)!,
    clientId: toId(job.clientId)!,
    propertyId: toId(job.propertyId)!,
    clientContactId: toId(job.clientContactId),
    propertyContactId: toId(job.propertyContactId),
    installContactId: toId(job.installContactId),
    superiorPrintContact1Id: toId(job.superiorPrintContact1Id),
    superiorPrintContact2Id: toId(job.superiorPrintContact2Id),
    lineItems: job.lineItems.map((item) => toLineItemDto(item)),
  };
  delete result._id;
  return result;
}

function toMongoJobDto(job: JobDto): MongoJobDto {
  const result: MongoJobDto & Partial<Pick<JobDto, "id">> = {
    ...job,
    _id: toMongoId(job.id)!,
    clientId: toMongoId(job.clientId)!,
    propertyId: toMongoId(job.propertyId)!,
    clientContactId: toMongoId(job.clientContactId),
    propertyContactId: toMongoId(job.propertyContactId),
    installContactId: toMongoId(job.installContactId),
    superiorPrintContact1Id: toMongoId(job.superiorPrintContact1Id),
    superiorPrintContact2Id: toMongoId(job.superiorPrintContact2Id),
    lineItems: job.lineItems.map((item) => toMongoLineItemDto(item)),
  };
  delete result.id;
  return result;
}

function toLineItemDto(lineItem: MongoLineItemDto): LineItemDto {
  const result: LineItemDto & Partial<Pick<MongoLineItemDto, "_id">> = {
    ...lineItem,
    id: toId(lineItem._id)!,
    locationId: toId(lineItem.locationId),
    surfaceId: toId(lineItem.surfaceId),
    materialId: toId(lineItem.materialId),
    finishingId: toId(lineItem.finishingId),
    printerId: toId(lineItem.printerId),
    clientSignImageIds: lineItem.clientSignImageIds.map((mongoId) => toId(mongoId)!),
  };
  delete result._id;
  return result;
}

function toMongoLineItemDto(lineItem: LineItemDto): MongoLineItemDto {
  const result: MongoLineItemDto & Partial<Pick<LineItemDto, "id">> = {
    ...lineItem,
    _id: toMongoId(lineItem.id)!,
    locationId: toMongoId(lineItem.locationId),
    surfaceId: toMongoId(lineItem.surfaceId),
    materialId: toMongoId(lineItem.materialId),
    finishingId: toMongoId(lineItem.finishingId),
    printerId: toMongoId(lineItem.printerId),
    clientSignImageIds: lineItem.clientSignImageIds.map((id) => toMongoId(id)!),
  };
  delete result.id;
  return result;
}

const jobStubProjector = { number: 1, clientId: 1, propertyId: 1 };
