import { PropertyRepository } from "./PropertyRepository";
import { Db } from "mongodb";
import { MongoPropertyRepository } from "./mongo/MongoPropertyRepository";
import { PagedEntityRepository } from "./PagedEntityRepository";
import { MongoPagedEntityRepository } from "./mongo/MongoPagedEntityRepository";
import { ClientRepository } from "./ClientRepository";
import { MongoClientRepository } from "./mongo/MongoClientRepository";
import { ContactRepository } from "./ContactRepository";
import { MongoContactRepository } from "./mongo/MongoContactRepository";
import { SmallSetEntityRepository } from "./SmallSetEntityRepository";
import { MongoSmallSetEntityRepository } from "./mongo/MongoSmallSetEntityRepository";
import { JobRepository } from "./JobRepository";
import { MongoJobRepository } from "./mongo/MongoJobRepository";

export let jobRepository: JobRepository;
export let propertyRepository: PropertyRepository;
export let pagedEntityRepository: PagedEntityRepository;
export let clientRepository: ClientRepository;
export let contactRepository: ContactRepository;
export let smallSetEntityRepository: SmallSetEntityRepository;

export function createRepositories(db: Db) {
  jobRepository = new MongoJobRepository(db);
  propertyRepository = new MongoPropertyRepository(db);
  pagedEntityRepository = new MongoPagedEntityRepository(db);
  clientRepository = new MongoClientRepository(db);
  contactRepository = new MongoContactRepository(db);
  smallSetEntityRepository = new MongoSmallSetEntityRepository(db);
}
