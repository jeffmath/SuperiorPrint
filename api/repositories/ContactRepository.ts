import { ContactDto } from "../../common/data-transfer/ContactDto";
import { Id } from "../../common/domain/Id";

export interface ContactRepository {
  getContact(id: Id): Promise<ContactDto | null>;

  /**
   * Returns the contacts in the database which are associated with either the given
   * client or property.
   */
  getContactsFor(clientId: Id, propertyId: Id): Promise<ContactDto[]>;

  getSuperiorContacts(): Promise<ContactDto[]>;

  getInstallers(): Promise<ContactDto[]>;

  createContact(): Promise<ContactDto>;

  updateContact(contact: ContactDto): Promise<void>;

  deleteContact(id: Id): Promise<void>;
}
