import { ImageDto } from "../../common/data-transfer/ImageDto";
import { ClientDto } from "../../common/data-transfer/ClientDto";
import {
  ClientSignImageDto,
  ClientSignImageInfoDto,
} from "../../common/data-transfer/ClientSignImageDto";
import { Id } from "../../common/domain/Id";

export interface ClientRepository {
  getAllClientStubs(): Promise<ClientDto[]>;

  getClient(id: Id): Promise<ClientDto | null>;

  addClientSignImage(image: Omit<ClientSignImageDto, "id">): Promise<void>;

  getClientSignImageInfos(clientId: Id): Promise<ClientSignImageInfoDto[]>;

  getClientSignImage(id: Id): Promise<ImageDto | null>;

  createClient(): Promise<ClientDto>;

  updateClient(id: Id, name: string): Promise<void>;

  deleteClient(id: Id): Promise<void>;

  deleteClientSignImage(id: Id): Promise<void>;
}
