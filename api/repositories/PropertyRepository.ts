import { PropertyStub } from "../../common/data-transfer/PropertyStub";
import { ImageDto } from "../../common/data-transfer/ImageDto";
import { Id } from "../../common/domain/Id";
import { LocationDto } from "../../common/data-transfer/LocationDto";
import { PropertyDto } from "../../common/data-transfer/PropertyDto";

export interface PropertyRepository {
  getAllPropertyStubs(): Promise<PropertyStub[]>;

  getProperty(id: Id): Promise<PropertyDto | null>;

  getPropertyStubsHavingIds(ids: Id[]): Promise<PropertyStub[]>;

  getLocationImage(id: Id): Promise<ImageDto | null>;

  addLocationImage(
    propertyId: Id,
    locationId: Id,
    image: ImageDto,
  ): Promise<AddLocationImageResult>;

  deleteLocationImage(propertyId: Id, locationId: Id): Promise<DeleteLocationImageResult>;

  createProperty(): Promise<PropertyDto>;

  updateProperty(property: PropertyDto): Promise<void>;

  updateLocationName(
    propertyId: Id,
    locationId: Id,
    name: string,
  ): Promise<UpdateLocationNameResult>;

  deleteProperty(id: Id): Promise<void>;

  createLocation(propertyId: Id): Promise<LocationDto>;

  deleteLocation(propertyId: Id, locationId: Id): Promise<void>;
}

export interface AddLocationImageResult {
  propertyNotFound?: boolean;
  locationNotFound?: boolean;
  imageId?: Id;
}

export interface DeleteLocationImageResult {
  propertyNotFound?: boolean;
  locationNotFound?: boolean;
  locationHasNoImage?: boolean;
}

export interface UpdateLocationNameResult {
  propertyNotFound?: boolean;
  locationNotFound?: boolean;
}
