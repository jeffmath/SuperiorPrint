import { Id } from "../../common/domain/Id";
import { NamedEntityDto } from "../../common/data-transfer/NamedEntityDto";

export type PagedEntitiesTypeName = "properties" | "clients" | "contacts";

export interface PagedEntityRepository {
  getEntitiesPage(
    entitiesTypeName: PagedEntitiesTypeName,
    pageNumber: number,
    entitiesPerPage: number,
    firstLetter: string,
    nameSearchText: string,
    modifyFilter?: (filter: object, convertId: (id: Id) => any) => void,
  ): Promise<GetEntitiesPageResult>;
}

export interface GetEntitiesPageResult {
  entities: NamedEntityDto[];
  total: number;
}
