import { LineItemDto } from "../../common/data-transfer/LineItemDto";
import { ContactDto } from "../../common/data-transfer/ContactDto";
import { Id } from "../../common/domain/Id";
import { JobDto, JobStub } from "../../common/data-transfer/JobDto";

export interface JobRepository {
  getJobStubs(clientId: Id, propertyId: Id): Promise<JobStub[]>;

  getJob(id: Id): Promise<JobDto | null>;

  getJobStubsForClient(id: Id): Promise<JobStub[]>;

  getJobStubsUsingClientSignImage(clientId: Id, imageId: Id): Promise<JobStub[]>;

  getJobStubsForProperty(id: Id): Promise<JobStub[]>;

  getJobStubsReferencingLocation(propertyId: Id, locationId: Id): Promise<JobStub[]>;

  getJobStubsReferencingContact(contactId: Id): Promise<JobStub[]>;

  getJobStubsReferencingEntity(entityId: Id): Promise<JobStub[]>;

  createJob(
    clientId: Id,
    propertyId: Id,
    robert: ContactDto,
    joni: ContactDto,
  ): Promise<JobDto>;

  updateJob(job: JobDto): Promise<void>;

  deleteJob(id: Id): Promise<void>;

  addLineItem(jobId: Id): Promise<LineItemDto>;

  deleteLineItem(jobId: Id, lineItemId: Id): Promise<void>;
}
