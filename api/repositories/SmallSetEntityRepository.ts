import { Id } from "../../common/domain/Id";
import { NamedEntityDto } from "../../common/data-transfer/NamedEntityDto";

export type SmallSetEntityType =
  | "surface"
  | "material"
  | "finishing"
  | "printer"
  | "user";

export interface SmallSetEntityRepository {
  getAllEntities(type: SmallSetEntityType): Promise<NamedEntityDto[]>;

  getEntity(type: SmallSetEntityType, id: Id): Promise<NamedEntityDto | null>;

  getEntityByName(type: SmallSetEntityType, name: string): Promise<NamedEntityDto | null>;

  createEntity(type: SmallSetEntityType, data?: NamedEntityDto): Promise<NamedEntityDto>;

  updateEntity(type: SmallSetEntityType, entity: NamedEntityDto): Promise<void>;

  deleteEntity(type: SmallSetEntityType, id: Id): Promise<void>;
}
