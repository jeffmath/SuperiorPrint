import { MongoClient } from "mongodb";
import { createRepositories } from "../repositories/repositories";

const dbUrl = "mongodb://localhost/SuperiorPrint";
const dbName = "SuperiorPrint";

export async function connect() {
  const client = new MongoClient(dbUrl);
  await client.connect();
  const db = client.db(dbName);
  createRepositories(db);
}
