import * as clients from "./controllers/clients";
import * as jwt from "express-jwt";
const multiparty = require("connect-multiparty");
import * as smallSetEntities from "./controllers/smallSetEntities";
import * as contacts from "./controllers/contacts";
import * as jobs from "./controllers/jobs";
import * as properties from "./controllers/properties";
import * as login from "./controllers/login";
import * as express from "express";
import { jwtSecret } from "./controllers/login";
import { Request, Response } from "express";

const auth = jwt({
  secret: jwtSecret,
  userProperty: "password",
  algorithms: ["HS256"],
});

const addAuth = (handler: (req: Request, res: Response) => void) => [auth, handler];

export const router = express.Router();

router.get("/login/:userName/:password", login.login);
router.put("/changePassword/:userId/:newPassword", addAuth(login.changePassword));

router.get("/client/:id", addAuth(clients.getClient));
router.get("/clients/", addAuth(clients.getAllClientStubs));
router.get("/clients/:pageNumber", addAuth(clients.getClientsPage));
router.post("/signImages/:clientId", multiparty(), addAuth(clients.addSignImages));
router.get("/signImages/:clientId", addAuth(clients.getClientSignImageInfos));
router.get("/signImage/:id", clients.getClientSignImage);
router.post("/client", addAuth(clients.createClient));
router.put("/client", addAuth(clients.updateClient));
router.delete("/client/:id", addAuth(clients.deleteClient));
router.delete("/signImage/:clientId/:signImageId", addAuth(clients.deleteSignImage));

router.get("/property/:id", addAuth(properties.getProperty));
router.get("/properties", addAuth(properties.getAllPropertyStubs));
router.get("/properties/client/:clientId", addAuth(properties.getPropertyStubsForClient));
router.get("/properties/:pageNumber", addAuth(properties.getPropertiesPage));
router.post("/property", addAuth(properties.createProperty));
router.put("/property", addAuth(properties.updateProperty));
router.put(
  "/location/:propertyId/:locationId/:name",
  addAuth(properties.updateLocationName),
);
router.delete("/property/:id", addAuth(properties.deleteProperty));
router.post("/location/:propertyId", addAuth(properties.createLocation));
router.delete("/location/:propertyId/:locationId", addAuth(properties.deleteLocation));
router.post(
  "/locationImage/:propertyId/:locationId",
  multiparty(),
  addAuth(properties.addLocationImage),
);
router.get("/locationImage/:imageId", properties.getLocationImage);
router.delete(
  "/locationImage/:propertyId/:locationId",
  addAuth(properties.deleteLocationImage),
);

router.get("/contact/:id", addAuth(contacts.getContact));
router.get("/contacts/superior", addAuth(contacts.getSuperiorContacts));
router.get("/contacts/installers", addAuth(contacts.getInstallers));
router.get("/contacts/:pageNumber", addAuth(contacts.getContactsPage));
router.post("/contact", addAuth(contacts.createContact));
router.put("/contact", addAuth(contacts.updateContact));
router.delete("/contact/:contactId", addAuth(contacts.deleteContact));

router.get("/jobs/:clientId/:propertyId", addAuth(jobs.getJobStubs));
router.get("/job/:jobId", addAuth(jobs.getJob));
router.put("/job", addAuth(jobs.updateJob));
router.post("/job/:clientId/:propertyId", addAuth(jobs.createJob));
router.delete("/job/:jobId", addAuth(jobs.deleteJob));
router.put("/lineItem/:jobId", addAuth(jobs.updateLineItem));
router.post("/lineItem/:jobId", addAuth(jobs.addLineItem));
router.delete("/lineItem/:jobId/:lineItemId", addAuth(jobs.deleteLineItem));

router.get("/surfaces/", addAuth(smallSetEntities.getSurfaces));
router.get("/materials/", addAuth(smallSetEntities.getMaterials));
router.get("/finishings/", addAuth(smallSetEntities.getFinishings));
router.get("/printers/", addAuth(smallSetEntities.getPrinters));
router.get("/users/", addAuth(smallSetEntities.getUsers));
router.post("/surface/", addAuth(smallSetEntities.createSurface));
router.post("/material/", addAuth(smallSetEntities.createMaterial));
router.post("/finishing/", addAuth(smallSetEntities.createFinishing));
router.post("/printer/", addAuth(smallSetEntities.createPrinter));
router.post("/user/", addAuth(smallSetEntities.createUser));
router.delete("/surface/:id", addAuth(smallSetEntities.deleteSurface));
router.delete("/material/:id", addAuth(smallSetEntities.deleteMaterial));
router.delete("/finishing/:id", addAuth(smallSetEntities.deleteFinishing));
router.delete("/printer/:id", addAuth(smallSetEntities.deletePrinter));
router.delete("/user/:id", addAuth(smallSetEntities.deleteUser));
router.put("/surface", addAuth(smallSetEntities.updateSurface));
router.put("/material", addAuth(smallSetEntities.updateMaterial));
router.put("/finishing", addAuth(smallSetEntities.updateFinishing));
router.put("/printer", addAuth(smallSetEntities.updatePrinter));
router.put("/user", addAuth(smallSetEntities.updateUser));
