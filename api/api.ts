import { router } from "./router";
import { connect } from "./database/connect";
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as path from "path";
import * as express from "express";
import { Request, Response } from "express";

connect();

const api = express();
api.use(logger("dev"));
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: false }));
api.use(cookieParser());

// add our API routes
api.use("/api", router);

// handle all other routes by returning the app's index page, so its client-side
// router can navigate to the route within the app
const appPath = "./react_app/public";
api.get("/*", (_req, res) => {
  res.sendFile(path.join(__dirname, appPath, "/index.html"));
});

api.use(handleNotFoundRequest);
api.use(handleUnauthorizedError);
api.use(handleOtherError);

startHttpsServer();
startHttpServer();

function startHttpsServer() {
  const https = require("https");
  const options = {
    // TODO - reinstate SSL certs once app is used for more than just demo purposes
    // ca: fs.readFileSync('./api/ssl/superiorprintandexhibit_us.ca-bundle'),
    // key: fs.readFileSync('./api/ssl/superiorprintandexhibit_us.key'),
    // cert: fs.readFileSync('./api/ssl/superiorprintandexhibit_us.crt')
  };
  const httpsPort = 443;
  https.createServer(options, api).listen(httpsPort, () => onListening(httpsPort));
}

function startHttpServer() {
  // have the server redirect to HTTPS (TODO: we are skipping this for now, until the
  // app is used for more than just demo purposes and the SSL certificates are renewed)
  const http = require("http");
  const httpPort = 80;
  // function redirectToHttp(req: Request, res: Response) {
  //     res.writeHead(301, {Location: "https://" + req.headers["host"] + req.url})
  //     res.end()
  // }
  // TODO replace api with redirectToHttp when app is no longer for demo
  http.createServer(api).listen(httpPort, () => onListening(httpPort));
}

function handleNotFoundRequest(_req: Request, res: Response) {
  res.sendFile(path.join(__dirname, appPath, "index.html"));
}

function handleUnauthorizedError(err: Error, _req: Request, res: Response) {
  if (err.name === "UnauthorizedError") {
    res.status(401);
    res.json({ message: err.name + ": " + err.message });
  }
}

function handleOtherError(err: Error, _req: Request, res: Response) {
  res.status(500);
  res.render("error", {
    message: err.message,
    error: err,
  });
}

function onListening(port: number) {
  console.log(`API listening on port ${port}`);
}
