import * as crypto from "crypto";
import { badRequest, notFound, ok, serverError } from "./util/responses";
import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { smallSetEntityRepository } from "../repositories/repositories";
import { UserDto } from "../../common/data-transfer/UserDto";

export async function login(req: Request, res: Response) {
  const { userName, password } = req.params;
  if (!userName || !password)
    return badRequest(res, "User name or password not supplied");

  try {
    const entity = await smallSetEntityRepository.getEntityByName("user", userName);
    if (!entity) return badRequest(res, "User not found.");

    // if the given password does not hash to the same result
    // as the actual password, respond that the login is invalid
    const user = entity as UserDto;
    const hash = crypto.pbkdf2Sync(password, user.salt, 1000, 64, "sha1").toString("hex");
    if (hash !== user.hash) return ok(res, "Incorrect password");

    // respond with the user found, along with a JWT for subsequent API calls
    // to supply in order to authenticate that user
    const result = {
      userId: user.id,
      token: generateJwt(password),
    };
    ok(res, result);
  } catch (err) {
    serverError(res, err);
  }
}

export async function changePassword(req: Request, res: Response) {
  const { userId, newPassword } = req.params;
  if (!userId || !newPassword) return badRequest(res, "User ID or password not supplied");

  try {
    const entity = await smallSetEntityRepository.getEntity("user", userId);
    if (!entity) return notFound(res, "User not found.");

    // change the user's password hash to the salted hash
    // of the password given
    const user = entity as UserDto;
    user.hash = crypto
      .pbkdf2Sync(newPassword, user.salt, 1000, 64, "sha1")
      .toString("hex");
    await smallSetEntityRepository.updateEntity("user", user);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

// TODO extract this secret to a config file if this app is ever used for more than
// just demo purposes
export const jwtSecret = "conservatism";

/**
 * Creates (and returns) a JSON Web Token containing the given e-mail address,
 * which expires after one day.
 */
function generateJwt(forEmailAddress: string) {
  // have the JWT expire after one day
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 1);

  // generate the JWT;
  const fields = {
    emailAddress: forEmailAddress,
    exp: expiry.getTime() / 1000,
  };
  return jwt.sign(fields, jwtSecret);
}
