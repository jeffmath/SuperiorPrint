import * as fs from "fs";
import { uniq } from "lodash";
import { Request, Response } from "express";
import { conflict, notFound, ok, serverError } from "./util/responses";
import { getFile } from "./util/requestHelpers";
import { createJobsList } from "./util/createJobsList";
import {
  pagedEntityRepository,
  jobRepository,
  propertyRepository,
} from "../repositories/repositories";
import { ImageDto } from "../../common/data-transfer/ImageDto";

export async function getPropertyStubsForClient(req: Request, res: Response) {
  // when we've found all the jobs for the given client ID
  const { clientId } = req.params;
  try {
    const jobStubs = await jobRepository.getJobStubsForClient(clientId);
    // find the distinct set of properties which are associated with those jobs
    const propertyIds = jobStubs.map((stub) => stub.propertyId);
    const propertyIdSet = uniq(propertyIds);
    const propertyStubs =
      await propertyRepository.getPropertyStubsHavingIds(propertyIdSet);
    ok(res, propertyStubs);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getAllPropertyStubs(_req: Request, res: Response) {
  try {
    const properties = await propertyRepository.getAllPropertyStubs();
    ok(res, properties);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getProperty(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const property = await propertyRepository.getProperty(id);
    ok(res, property);
  } catch (err) {
    serverError(res, err);
  }
}

export function addLocationImage(req: Request, res: Response) {
  const file = getFile(req);
  fs.readFile(file.path, async (err, data) => {
    if (err) return serverError(res, err);

    const image: ImageDto = {
      name: file.name,
      imageData: data,
    };
    const { propertyId, locationId } = req.params;
    try {
      const result = await propertyRepository.addLocationImage(
        propertyId,
        locationId,
        image,
      );
      if (result.propertyNotFound) return notFound(res, "property not found");
      else if (result.locationNotFound) return notFound(res, "location not found");
      ok(res, result.imageId);
    } catch (err) {
      serverError(res, err);
    }
  });
}

export async function getLocationImage(req: Request, res: Response) {
  const { imageId } = req.params;
  try {
    const image = await propertyRepository.getLocationImage(imageId);
    if (!image) return serverError(res, "Location image not found.");

    const imageData = Buffer.from(image.imageData.toString("base64"), "base64");
    res.writeHead(200, { "Content-Type": "image/jpg" });
    res.end(imageData, "binary");
  } catch (err) {
    serverError(res, err);
  }
}

export async function deleteLocationImage(req: Request, res: Response) {
  const { propertyId, locationId } = req.params;
  try {
    const result = await propertyRepository.deleteLocationImage(propertyId, locationId);
    if (result.propertyNotFound) return notFound(res, "property not found");
    else if (result.locationNotFound) return notFound(res, "location not found");
    else if (result.locationHasNoImage)
      return notFound(res, "location has no image to delete");
    ok(res, "deleted");
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Responds with the given page number of properties from the database which match
 * the given search parameters, and also with the total number of properties
 * which match those parameters.
 */
export async function getPropertiesPage(req: Request, res: Response) {
  const pageNumber = parseInt(req.params.pageNumber) - 1;
  const { firstLetter, nameSearchText } = req.query;
  try {
    const page = await pagedEntityRepository.getEntitiesPage(
      "properties",
      pageNumber,
      10,
      <string>firstLetter,
      <string>nameSearchText,
    );
    ok(res, page);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Creates a new property in the database.
 */
export async function createProperty(_req: Request, res: Response) {
  try {
    const property = await propertyRepository.createProperty();
    ok(res, property);
  } catch (err) {
    serverError(res, err);
  }
}

export async function updateProperty(req: Request, res: Response) {
  const propertyStub = req.body;
  try {
    await propertyRepository.updateProperty(propertyStub);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export async function updateLocationName(req: Request, res: Response) {
  const { propertyId, locationId, name } = req.params;
  try {
    const result = await propertyRepository.updateLocationName(
      propertyId,
      locationId,
      name,
    );
    if (result.propertyNotFound) return notFound(res, "property not found");
    else if (result.locationNotFound) return notFound(res, "location not found");
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Removes the property of the given ID from the database.
 */
export async function deleteProperty(req: Request, res: Response) {
  // if there are any jobs which are for the given property
  const { id } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsForProperty(id);
    if (stubs && stubs.length > 0)
      // disallow the delete, providing a list of those jobs as the reason
      return conflict(
        res,
        "The property cannot be deleted, as it is specified" +
          " as the property for the following jobs: " +
          createJobsList(stubs),
      );

    // otherwise, delete the property
    await propertyRepository.deleteProperty(id);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Creates a new location for the given property in the database.
 */
export async function createLocation(req: Request, res: Response) {
  const { propertyId } = req.params;
  try {
    const location = await propertyRepository.createLocation(propertyId);
    ok(res, location);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Removes the location of the given ID from its property in the database.
 */
export async function deleteLocation(req: Request, res: Response) {
  // if there are any jobs which have line items for the given location
  const { propertyId, locationId } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsReferencingLocation(
      propertyId,
      locationId,
    );
    if (stubs && stubs.length > 0)
      // disallow the delete, providing a list of those jobs as the reason
      return conflict(
        res,
        "The location cannot be deleted, as it is specified" +
          " as the location of one or more line items" +
          " for the following jobs: " +
          createJobsList(stubs),
      );

    await propertyRepository.deleteLocation(propertyId, locationId);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}
