import { remove } from "lodash";
import { Request, Response } from "express";
import { notFound, ok, serverError } from "./util/responses";
import { LineItemDto } from "../../common/data-transfer/LineItemDto";
import {
  clientRepository,
  contactRepository,
  jobRepository,
  propertyRepository,
} from "../repositories/repositories";
import { JobDto } from "../../common/data-transfer/JobDto";

export async function getJobStubs(req: Request, res: Response) {
  const { clientId, propertyId } = req.params;
  try {
    const stubs = await jobRepository.getJobStubs(clientId, propertyId);
    ok(res, stubs);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getJob(req: Request, res: Response) {
  const id = req.params.jobId;
  try {
    const job = await jobRepository.getJob(id);
    if (!job) return serverError(res, new Error("Job not found."));

    const client = await clientRepository.getClient(job.clientId);
    const property = await propertyRepository.getProperty(job.propertyId);
    const contacts = await contactRepository.getContactsFor(job.clientId, job.propertyId);
    ok(res, { job, client, property, contacts });
  } catch (err) {
    serverError(res, err);
  }
}

export async function updateJob(req: Request, res: Response) {
  // if we find the old version of the job we are going to update
  const changedJob = req.body as JobDto;
  try {
    const job = await jobRepository.getJob(changedJob.id);
    if (!job) return notFound(res, "Job not found");

    // copy the line items from the old version into the updated job
    // (since the latter shouldn't come with any, as we aren't updating them)
    changedJob.lineItems = job.lineItems;

    // update the job in the database
    await jobRepository.updateJob(changedJob);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export async function updateLineItem(req: Request, res: Response) {
  // if we can find the parent job of the line item to be updated
  const { jobId } = req.params;
  const lineItem = req.body as LineItemDto;
  try {
    const job = await jobRepository.getJob(jobId);
    if (!job) return notFound(res, "Job not found in which to update line item.");

    // swap out the existing line item for the changed version we were given
    if (!remove(job.lineItems, (item) => item.id === lineItem.id).length)
      return notFound(res, "Line-item not found");
    job.lineItems.push(lineItem);

    // save the above change
    await jobRepository.updateJob(job);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export async function addLineItem(req: Request, res: Response) {
  const { jobId } = req.params;
  try {
    const lineItem = await jobRepository.addLineItem(jobId);
    ok(res, lineItem);
  } catch (err) {
    serverError(res, err);
  }
}

export async function deleteLineItem(req: Request, res: Response) {
  const { jobId, lineItemId } = req.params;
  try {
    await jobRepository.deleteLineItem(jobId, lineItemId);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export async function createJob(req: Request, res: Response) {
  // when we've found the Robert and Joni Superior contacts in the database
  const { clientId, propertyId } = req.params;
  try {
    const contacts = await contactRepository.getSuperiorContacts();

    // create a job with those contacts set
    const find = (name: string) => contacts.find((c) => c.name.startsWith(name));
    const job = await jobRepository.createJob(
      clientId,
      propertyId,
      find("Robert")!,
      find("Joni")!,
    );
    ok(res, job.id);
  } catch (err) {
    serverError(res, err);
  }
}

export async function deleteJob(req: Request, res: Response) {
  const id = req.params.jobId;
  try {
    await jobRepository.deleteJob(id);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}
