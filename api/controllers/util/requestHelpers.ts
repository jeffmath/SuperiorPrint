import { Request } from "express";

export interface File {
  path: string;
  name: string;
}

export function getFile(req: Request): File {
  const { files } = req as any;
  const entry = files[Object.keys(files)[0]];
  return entry[Object.keys(entry)[0]];
}

export function getFiles(req: Request): File[] {
  const { files } = req as any;
  return Object.keys(files).map((k) => {
    const entry = files[k];
    return entry[Object.keys(entry)[0]];
  });
}
