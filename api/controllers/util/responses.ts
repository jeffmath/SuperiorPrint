import { Response } from "express";

export function ok(res: Response, data?: any) {
  res.status(200);
  if (typeof data === "string") res.send(data);
  else res.json(data || "");
}

export function badRequest(res: Response, message: string) {
  console.error(`400 (bad request): ${message}`);
  res.status(400);
  res.json(message);
}

export function serverError(res: Response, error: any) {
  console.error(error);
  res.status(500);
  res.json(error);
}

export function notFound(res: Response, message: string) {
  console.error(`404 (not found): ${message}`);
  res.status(409);
  res.json(message);
}

export function conflict(res: Response, message: string) {
  console.error(`409 (conflict): ${message}`);
  res.status(409);
  res.json(message);
}
