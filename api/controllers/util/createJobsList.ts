import { JobStub } from "@/data-transfer/JobDto";

export function createJobsList(jobs: JobStub[]): string {
  return jobs.map((job) => job.number).join(", ");
}
