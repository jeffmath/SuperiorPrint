import * as fs from "fs";
import { imageSize } from "image-size";
import { Request, Response } from "express";
import { badRequest, conflict, ok, serverError } from "./util/responses";
import { getFiles } from "./util/requestHelpers";
import { createJobsList } from "./util/createJobsList";
import {
  clientRepository,
  pagedEntityRepository,
  jobRepository,
} from "../repositories/repositories";

export async function getClient(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const client = await clientRepository.getClient(id);
    ok(res, client);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getAllClientStubs(_req: Request, res: Response) {
  try {
    const clients = await clientRepository.getAllClientStubs();
    ok(res, clients);
  } catch (err) {
    serverError(res, err);
  }
}

export function addSignImages(req: Request, res: Response) {
  const { clientId } = req.params;
  const files = getFiles(req);
  let filesLeft = files.length;
  if (!filesLeft) badRequest(res, "No file(s) sent");
  files.forEach((file) => {
    // image-size fails on CMYK (vs. RGB) JPEG images
    let isTallerThanWide = false;
    try {
      const size = imageSize(file.path);
      isTallerThanWide = (size.height || 0) > (size.width || 0);
    } catch (err) {
      console.log(err);
    }

    fs.readFile(file.path, async (err, data) => {
      if (err) return serverError(res, err);

      const image = {
        name: file.name,
        clientId,
        imageData: data,
        isTallerThanWide,
      };
      try {
        await clientRepository.addClientSignImage(image);
        if (--filesLeft === 0) ok(res);
      } catch (err) {
        serverError(res, err);
      }
    });
  });
}

/**
 * Responds with a sorted list of the names (and corresponding IDs) of all
 * client sign images in the database for the given client ID.
 */
export async function getClientSignImageInfos(req: Request, res: Response) {
  const { clientId } = req.params;
  try {
    const infos = await clientRepository.getClientSignImageInfos(clientId);
    ok(res, infos);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getClientSignImage(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const image = await clientRepository.getClientSignImage(id);
    if (!image) return serverError(res, "Client sign image not found.");

    const imageData = Buffer.from(image.imageData.toString("base64"), "base64");
    res.writeHead(200, { "Content-Type": "image/jpg" });
    res.end(imageData, "binary");
  } catch (err) {
    serverError(res, err);
  }
}

export async function getClientsPage(req: Request, res: Response) {
  const pageNumber = parseInt(req.params.pageNumber) - 1;
  const { firstLetter, nameSearchText } = req.query;
  try {
    const page = await pagedEntityRepository.getEntitiesPage(
      "clients",
      pageNumber,
      10,
      <string>firstLetter,
      <string>nameSearchText,
    );
    ok(res, page);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Creates a new client in the database.
 */
export async function createClient(_req: Request, res: Response) {
  try {
    const client = await clientRepository.createClient();
    ok(res, client);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Updates the client of the given ID in the database with the field values
 * from the given client object.
 */
export async function updateClient(req: Request, res: Response) {
  const { id, name } = req.body;
  try {
    await clientRepository.updateClient(id, name);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Removes the client of the given ID from the database.
 */
export async function deleteClient(req: Request, res: Response) {
  // if there are any jobs which are for the given client
  const { id } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsForClient(id);
    if (stubs && stubs.length > 0)
      // disallow the delete, providing a list of those jobs as the reason
      return conflict(
        res,
        "The client cannot be deleted, as it is specified" +
          " as the client for the following jobs: " +
          createJobsList(stubs),
      );

    // otherwise, delete the client
    await clientRepository.deleteClient(id);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Removes the sign-image of the given ID from its client in the database.
 */
export async function deleteSignImage(req: Request, res: Response) {
  // if there are any jobs which have line items which reference the given sign-image
  const { clientId, signImageId } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsUsingClientSignImage(
      clientId,
      signImageId,
    );
    if (stubs && stubs.length > 0)
      // disallow the delete, providing a list of those jobs as the reason
      return conflict(
        res,
        "The sign-image cannot be deleted, as it is being used" +
          " by one or more line items" +
          " for the following jobs: " +
          createJobsList(stubs),
      );

    // otherwise, delete the sign-image
    await clientRepository.deleteClientSignImage(signImageId);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}
