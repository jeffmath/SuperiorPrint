import * as crypto from "crypto";
import { Request, Response } from "express";
import { notFound, ok, serverError } from "./util/responses";
import { createJobsList } from "./util/createJobsList";
import { jobRepository, smallSetEntityRepository } from "../repositories/repositories";
import { SmallSetEntityType } from "../repositories/SmallSetEntityRepository";
import { UserDto } from "../../common/data-transfer/UserDto";

export async function getSmallSetEntities(type: SmallSetEntityType, res: Response) {
  try {
    const entities = await smallSetEntityRepository.getAllEntities(type);
    ok(res, entities);
  } catch (err) {
    serverError(res, err);
  }
}

export function getSurfaces(_req: Request, res: Response) {
  void getSmallSetEntities("surface", res);
}

export function getMaterials(_req: Request, res: Response) {
  void getSmallSetEntities("material", res);
}

export function getFinishings(_req: Request, res: Response) {
  void getSmallSetEntities("finishing", res);
}

export function getPrinters(_req: Request, res: Response) {
  void getSmallSetEntities("printer", res);
}

export function getUsers(_req: Request, res: Response) {
  void getSmallSetEntities("user", res);
}

export async function createSmallSetEntity(type: SmallSetEntityType, res: Response) {
  try {
    const entity = await smallSetEntityRepository.createEntity(type);
    ok(res, entity);
  } catch (err) {
    serverError(res, err);
  }
}

export function createSurface(_req: Request, res: Response) {
  void createSmallSetEntity("surface", res);
}

export function createMaterial(_req: Request, res: Response) {
  void createSmallSetEntity("material", res);
}

export function createFinishing(_req: Request, res: Response) {
  void createSmallSetEntity("finishing", res);
}

export function createPrinter(_req: Request, res: Response) {
  void createSmallSetEntity("printer", res);
}

export async function createUser(_req: Request, res: Response) {
  const salt = crypto.randomBytes(16).toString("hex");
  const password = "spae16";
  const data = {
    name: "",
    salt: salt,
    hash: crypto.pbkdf2Sync(password, salt, 1000, 64, "sha1").toString("hex"),
  } as UserDto;
  try {
    const user = await smallSetEntityRepository.createEntity("user", data);
    ok(res, user);
  } catch (err) {
    serverError(res, err);
  }
}

export async function deleteSmallSetEntity(
  req: Request,
  type: SmallSetEntityType,
  res: Response,
) {
  // if there are any jobs with line items which reference the given entity
  const { id } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsReferencingEntity(id);
    if (stubs && stubs.length > 0)
      // disallow the deletion, providing a list of those jobs as the reason
      return notFound(
        res,
        "It cannot be deleted, as it is referenced" +
          " by at least one line item of the following jobs: " +
          createJobsList(stubs),
      );

    // delete the entity
    await smallSetEntityRepository.deleteEntity(type, id);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export function deleteSurface(req: Request, res: Response) {
  void deleteSmallSetEntity(req, "surface", res);
}

export function deleteMaterial(req: Request, res: Response) {
  void deleteSmallSetEntity(req, "material", res);
}

export function deleteFinishing(req: Request, res: Response) {
  void deleteSmallSetEntity(req, "finishing", res);
}

export function deletePrinter(req: Request, res: Response) {
  void deleteSmallSetEntity(req, "printer", res);
}

export function deleteUser(req: Request, res: Response) {
  void deleteSmallSetEntity(req, "user", res);
}

export async function updateSmallSetEntity(
  req: Request,
  type: SmallSetEntityType,
  res: Response,
) {
  const entity = req.body;
  try {
    await smallSetEntityRepository.updateEntity(type, entity);
    ok(res);
  } catch (err) {
    serverError(res, err);
  }
}

export function updateSurface(req: Request, res: Response) {
  void updateSmallSetEntity(req, "surface", res);
}

export function updateMaterial(req: Request, res: Response) {
  void updateSmallSetEntity(req, "material", res);
}

export function updateFinishing(req: Request, res: Response) {
  void updateSmallSetEntity(req, "finishing", res);
}

export function updatePrinter(req: Request, res: Response) {
  void updateSmallSetEntity(req, "printer", res);
}

export function updateUser(req: Request, res: Response) {
  void updateSmallSetEntity(req, "user", res);
}
