import {
  contactRepository,
  pagedEntityRepository,
  jobRepository,
} from "../repositories/repositories";
import { Request, Response } from "express";
import { conflict, ok, serverError } from "./util/responses";
import { createJobsList } from "./util/createJobsList";

export async function getContact(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const contact = await contactRepository.getContact(id);
    ok(res, contact);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getContactsPage(req: Request, res: Response) {
  const pageNumber = parseInt(req.params.pageNumber) - 1;
  const { clientId, propertyId, firstLetter, nameSearchText } = req.query;
  const installersOnly = req.query.installersOnly === "true";
  try {
    const page = await pagedEntityRepository.getEntitiesPage(
      "contacts",
      pageNumber,
      10,
      <string>firstLetter,
      <string>nameSearchText,
      function (filter: any, convertId: (id: string) => any) {
        if (installersOnly) filter.isInstaller = true;
        if (clientId) filter.clientId = convertId(<string>clientId);
        if (propertyId) filter.propertyId = convertId(<string>propertyId);
      },
    );
    ok(res, page);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getSuperiorContacts(_req: Request, res: Response) {
  try {
    const contacts = await contactRepository.getSuperiorContacts();
    ok(res, contacts);
  } catch (err) {
    serverError(res, err);
  }
}

export async function getInstallers(_req: Request, res: Response) {
  try {
    const contacts = await contactRepository.getInstallers();
    ok(res, contacts);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Creates a new contact in the database.
 */
export async function createContact(_req: Request, res: Response) {
  try {
    const contact = await contactRepository.createContact();
    ok(res, contact);
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Updates the contact of the given ID in the database with the field values
 * from the given contact object.
 */
export async function updateContact(req: Request, res: Response) {
  const contact = req.body;
  try {
    await contactRepository.updateContact(contact);
    ok(res, "updated");
  } catch (err) {
    serverError(res, err);
  }
}

/**
 * Removes the contact of the given ID from the database.
 */
export async function deleteContact(req: Request, res: Response) {
  // if there are any jobs which reference the given contact
  const { contactId } = req.params;
  try {
    const stubs = await jobRepository.getJobStubsReferencingContact(contactId);
    if (stubs && stubs.length > 0)
      // disallow the delete, providing a list of those jobs as the reason
      return conflict(
        res,
        "The contact cannot be deleted, as it is referenced" +
          " as a contact for the following jobs: " +
          createJobsList(stubs),
      );

    // otherwise, delete the contact
    await contactRepository.deleteContact(contactId);
    ok(res, "deleted");
  } catch (err) {
    serverError(res, err);
  }
}
